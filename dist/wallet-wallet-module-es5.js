(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["wallet-wallet-module"],{

/***/ "./node_modules/ng2-tooltip-directive/fesm5/ng2-tooltip-directive.js":
/*!***************************************************************************!*\
  !*** ./node_modules/ng2-tooltip-directive/fesm5/ng2-tooltip-directive.js ***!
  \***************************************************************************/
/*! exports provided: TooltipComponent, TooltipDirective, TooltipModule, ɵa */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TooltipComponent", function() { return TooltipComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TooltipDirective", function() { return TooltipDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TooltipModule", function() { return TooltipModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return TooltipOptionsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");




/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TooltipComponent = /** @class */ (function () {
    function TooltipComponent(elementRef, renderer) {
        this.elementRef = elementRef;
        this.renderer = renderer;
        this._show = false;
        this.events = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    TooltipComponent.prototype.transitionEnd = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.show) {
            this.events.emit({
                type: 'shown'
            });
        }
    };
    Object.defineProperty(TooltipComponent.prototype, "show", {
        get: /**
         * @return {?}
         */
        function () {
            return this._show;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (value) {
                this.setPosition();
            }
            this._show = this.hostClassShow = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipComponent.prototype, "placement", {
        get: /**
         * @return {?}
         */
        function () {
            return this.data.options.placement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipComponent.prototype, "autoPlacement", {
        get: /**
         * @return {?}
         */
        function () {
            return this.data.options.autoPlacement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipComponent.prototype, "element", {
        get: /**
         * @return {?}
         */
        function () {
            return this.data.element;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipComponent.prototype, "elementPosition", {
        get: /**
         * @return {?}
         */
        function () {
            return this.data.elementPosition;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipComponent.prototype, "options", {
        get: /**
         * @return {?}
         */
        function () {
            return this.data.options;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipComponent.prototype, "value", {
        get: /**
         * @return {?}
         */
        function () {
            return this.data.value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipComponent.prototype, "tooltipOffset", {
        get: /**
         * @return {?}
         */
        function () {
            return Number(this.data.options.offset);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipComponent.prototype, "isThemeLight", {
        get: /**
         * @return {?}
         */
        function () {
            return this.options['theme'] === 'light';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    TooltipComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.setPlacementClass();
        this.setCustomClass();
        this.setStyles();
    };
    /**
     * @return {?}
     */
    TooltipComponent.prototype.setPosition = /**
     * @return {?}
     */
    function () {
        var e_1, _a;
        if (this.setHostStyle(this.placement) || !this.autoPlacement) {
            this.setPlacementClass(this.placement);
            return;
        }
        else {
            /** @type {?} */
            var placements = ['top', 'right', 'bottom', 'left'];
            try {
                for (var placements_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__values"])(placements), placements_1_1 = placements_1.next(); !placements_1_1.done; placements_1_1 = placements_1.next()) {
                    var placement = placements_1_1.value;
                    if (this.setHostStyle(placement)) {
                        this.setPlacementClass(placement);
                        return;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (placements_1_1 && !placements_1_1.done && (_a = placements_1.return)) _a.call(placements_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
    };
    /**
     * @param {?=} placement
     * @return {?}
     */
    TooltipComponent.prototype.setPlacementClass = /**
     * @param {?=} placement
     * @return {?}
     */
    function (placement) {
        this.renderer.addClass(this.elementRef.nativeElement, 'tooltip-' + placement);
    };
    /**
     * @param {?} placement
     * @return {?}
     */
    TooltipComponent.prototype.setHostStyle = /**
     * @param {?} placement
     * @return {?}
     */
    function (placement) {
        /** @type {?} */
        var isSvg = this.element instanceof SVGElement;
        /** @type {?} */
        var tooltip = this.elementRef.nativeElement;
        /** @type {?} */
        var isCustomPosition = !this.elementPosition.right;
        /** @type {?} */
        var elementHeight = isSvg ? this.element.getBoundingClientRect().height : this.element.offsetHeight;
        /** @type {?} */
        var elementWidth = isSvg ? this.element.getBoundingClientRect().width : this.element.offsetWidth;
        /** @type {?} */
        var tooltipHeight = tooltip.clientHeight;
        /** @type {?} */
        var tooltipWidth = tooltip.clientWidth;
        /** @type {?} */
        var scrollY = window.pageYOffset;
        if (isCustomPosition) {
            elementHeight = 0;
            elementWidth = 0;
        }
        /** @type {?} */
        var topStyle;
        /** @type {?} */
        var leftStyle;
        if (placement === 'top') {
            topStyle = (this.elementPosition.top + scrollY) - (tooltipHeight + this.tooltipOffset);
        }
        if (placement === 'bottom') {
            topStyle = (this.elementPosition.top + scrollY) + elementHeight + this.tooltipOffset;
        }
        if (placement === 'top' || placement === 'bottom') {
            leftStyle = (this.elementPosition.left + elementWidth / 2) - tooltipWidth / 2;
        }
        if (placement === 'left') {
            leftStyle = this.elementPosition.left - tooltipWidth - this.tooltipOffset;
        }
        if (placement === 'right') {
            leftStyle = this.elementPosition.left + elementWidth + this.tooltipOffset;
        }
        if (placement === 'left' || placement === 'right') {
            topStyle = (this.elementPosition.top + scrollY) + elementHeight / 2 - tooltip.clientHeight / 2;
        }
        /** @type {?} */
        var topEdge = topStyle;
        /** @type {?} */
        var bottomEdge = topStyle + tooltipHeight;
        /** @type {?} */
        var leftEdge = leftStyle;
        /** @type {?} */
        var rightEdge = leftStyle + tooltipWidth;
        if ((topEdge < 0 || bottomEdge > document.body.clientHeight || leftEdge < 0 || rightEdge > document.body.clientWidth) && this.autoPlacement) {
            return false;
        }
        this.hostStyleTop = topStyle + 'px';
        this.hostStyleLeft = leftStyle + 'px';
        return true;
    };
    /**
     * @return {?}
     */
    TooltipComponent.prototype.setZIndex = /**
     * @return {?}
     */
    function () {
        if (this.options['zIndex'] !== 0) {
            this.hostStyleZIndex = this.options['zIndex'];
        }
    };
    /**
     * @return {?}
     */
    TooltipComponent.prototype.setPointerEvents = /**
     * @return {?}
     */
    function () {
        if (this.options['pointerEvents']) {
            this.hostStylePointerEvents = this.options['pointerEvents'];
        }
    };
    /**
     * @return {?}
     */
    TooltipComponent.prototype.setCustomClass = /**
     * @return {?}
     */
    function () {
        if (this.options['tooltipClass']) {
            this.renderer.addClass(this.elementRef.nativeElement, this.options['tooltipClass']);
        }
    };
    /**
     * @return {?}
     */
    TooltipComponent.prototype.setAnimationDuration = /**
     * @return {?}
     */
    function () {
        if (Number(this.options['animationDuration']) != this.options['animationDurationDefault']) {
            this.hostStyleTransition = 'opacity ' + this.options['animationDuration'] + 'ms';
        }
    };
    /**
     * @return {?}
     */
    TooltipComponent.prototype.setStyles = /**
     * @return {?}
     */
    function () {
        this.setZIndex();
        this.setPointerEvents();
        this.setAnimationDuration();
        this.hostClassShadow = this.options['shadow'];
        this.hostClassLight = this.isThemeLight;
        this.hostStyleMaxWidth = this.options['maxWidth'] + "px";
        this.hostStyleWidth = this.options['width'] ? this.options['width'] + "px" : '';
    };
    TooltipComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"], args: [{
                    selector: 'tooltip',
                    template: "<div *ngIf=\"isThemeLight\" class=\"tooltip-arrow\"></div>\r\n\r\n<div *ngIf=\"options['contentType'] === 'template' else htmlOrStringTemplate\" \r\n    [ngClass]=\"{'tooltip-arrow': isThemeLight }\">\r\n\r\n\t<ng-container *ngTemplateOutlet=\"value\"></ng-container>\r\n</div>\r\n\r\n<ng-template #htmlOrStringTemplate>\r\n\t<div [innerHTML]=\"value\"></div>\r\n</ng-template>\r\n",
                    host: {
                        'class': 'tooltip'
                    },
                    styles: [":host{max-width:200px;background-color:#000;color:#fff;text-align:center;border-radius:6px;padding:5px 8px;position:absolute;pointer-events:none;z-index:1000;display:block;opacity:0;transition:opacity .3s;top:0;left:0}:host.tooltip-show{opacity:1}:host.tooltip-shadow{box-shadow:0 7px 15px -5px rgba(0,0,0,.4)}:host.tooltip-light.tooltip-shadow{box-shadow:0 5px 15px -5px rgba(0,0,0,.4)}:host.tooltip::after{content:\"\";position:absolute;border-style:solid}:host.tooltip-top::after{top:100%;left:50%;margin-left:-5px;border-width:5px;border-color:#000 transparent transparent}:host.tooltip-bottom::after{bottom:100%;left:50%;margin-left:-5px;border-width:5px;border-color:transparent transparent #000}:host.tooltip-left::after{top:50%;left:100%;margin-top:-5px;border-width:5px;border-color:transparent transparent transparent #000}:host.tooltip-right::after{top:50%;right:100%;margin-top:-5px;border-width:5px;border-color:transparent #000 transparent transparent}:host.tooltip-light::after{display:none}:host.tooltip-light{border:1px solid rgba(0,0,0,.06);background-color:#fff;color:#000}:host.tooltip-light .tooltip-arrow{position:absolute;width:10px;height:10px;-webkit-transform:rotate(135deg);transform:rotate(135deg);background-color:rgba(0,0,0,.07)}:host.tooltip-light .tooltip-arrow::after{background-color:#fff;content:'';display:block;position:absolute;width:10px;height:10px}:host.tooltip-top.tooltip-light{margin-top:-2px}:host.tooltip-top.tooltip-light .tooltip-arrow{top:100%;left:50%;margin-top:-4px;margin-left:-5px;background:linear-gradient(to bottom left,rgba(0,0,0,.07) 50%,transparent 50%)}:host.tooltip-top.tooltip-light .tooltip-arrow::after{top:1px;right:1px}:host.tooltip-bottom.tooltip-light .tooltip-arrow{bottom:100%;left:50%;margin-bottom:-4px;margin-left:-5px;background:linear-gradient(to top right,rgba(0,0,0,.1) 50%,transparent 50%)}:host.tooltip-bottom.tooltip-light .tooltip-arrow::after{top:-1px;right:-1px}:host.tooltip-left.tooltip-light .tooltip-arrow{top:50%;left:100%;margin-top:-5px;margin-left:-4px;background:linear-gradient(to bottom right,rgba(0,0,0,.07) 50%,transparent 50%)}:host.tooltip-left.tooltip-light .tooltip-arrow::after{top:1px;right:-1px}:host.tooltip-right.tooltip-light .tooltip-arrow{top:50%;right:100%;margin-top:-5px;margin-right:-4px;background:linear-gradient(to top left,rgba(0,0,0,.07) 50%,transparent 50%)}:host.tooltip-right.tooltip-light .tooltip-arrow::after{top:-1px;right:1px}"]
                }] }
    ];
    /** @nocollapse */
    TooltipComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] }
    ]; };
    TooltipComponent.propDecorators = {
        data: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
        hostStyleTop: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['style.top',] }],
        hostStyleLeft: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['style.left',] }],
        hostStyleZIndex: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['style.z-index',] }],
        hostStyleTransition: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['style.transition',] }],
        hostStyleWidth: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['style.width',] }],
        hostStyleMaxWidth: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['style.max-width',] }],
        hostStylePointerEvents: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['style.pointer-events',] }],
        hostClassShow: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['class.tooltip-show',] }],
        hostClassShadow: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['class.tooltip-shadow',] }],
        hostClassLight: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['class.tooltip-light',] }],
        transitionEnd: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['transitionend', ['$event'],] }],
        show: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
    };
    return TooltipComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * This is not a real service, but it looks like it from the outside.
 * It's just an InjectionToken used to import the config (initOptions) object, provided from the outside
 * @type {?}
 */
var TooltipOptionsService = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["InjectionToken"]('TooltipOptions');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var defaultOptions = {
    'placement': 'top',
    'autoPlacement': true,
    'contentType': 'string',
    'showDelay': 0,
    'hideDelay': 300,
    'hideDelayMobile': 0,
    'hideDelayTouchscreen': 0,
    'zIndex': 0,
    'animationDuration': 300,
    'animationDurationDefault': 300,
    'trigger': 'hover',
    'tooltipClass': '',
    'display': true,
    'displayMobile': true,
    'displayTouchscreen': true,
    'shadow': true,
    'theme': 'dark',
    'offset': 8,
    'maxWidth': '',
    'id': false,
    'hideDelayAfterClick': 2000
};
/** @type {?} */
var backwardCompatibilityOptions = {
    'delay': 'showDelay',
    'show-delay': 'showDelay',
    'hide-delay': 'hideDelay',
    'hide-delay-mobile': 'hideDelayTouchscreen',
    'hideDelayMobile': 'hideDelayTouchscreen',
    'z-index': 'zIndex',
    'animation-duration': 'animationDuration',
    'animation-duration-default': 'animationDurationDefault',
    'tooltip-class': 'tooltipClass',
    'display-mobile': 'displayTouchscreen',
    'displayMobile': 'displayTouchscreen',
    'max-width': 'maxWidth'
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TooltipDirective = /** @class */ (function () {
    function TooltipDirective(initOptions, elementRef, componentFactoryResolver, appRef, injector) {
        this.initOptions = initOptions;
        this.elementRef = elementRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.appRef = appRef;
        this.injector = injector;
        this._showDelay = 0;
        this._hideDelay = 300;
        this._options = {};
        this.events = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    Object.defineProperty(TooltipDirective.prototype, "options", {
        get: /**
         * @return {?}
         */
        function () {
            return this._options;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (value && defaultOptions) {
                this._options = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipDirective.prototype, "isTooltipDestroyed", {
        get: /**
         * @return {?}
         */
        function () {
            return this.componentRef && this.componentRef.hostView.destroyed;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipDirective.prototype, "destroyDelay", {
        get: /**
         * @return {?}
         */
        function () {
            if (this._destroyDelay) {
                return this._destroyDelay;
            }
            else {
                return Number(this.getHideDelay()) + Number(this.options['animationDuration']);
            }
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._destroyDelay = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipDirective.prototype, "tooltipPosition", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.options['position']) {
                return this.options['position'];
            }
            else {
                return this.elementPosition;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    TooltipDirective.prototype.onMouseEnter = /**
     * @return {?}
     */
    function () {
        if (this.isDisplayOnHover == false) {
            return;
        }
        this.show();
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.onMouseLeave = /**
     * @return {?}
     */
    function () {
        if (this.options['trigger'] === 'hover') {
            this.destroyTooltip();
        }
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.isDisplayOnClick == false) {
            return;
        }
        this.show();
        this.hideAfterClickTimeoutId = window.setTimeout((/**
         * @return {?}
         */
        function () {
            _this.destroyTooltip();
        }), this.options['hideDelayAfterClick']);
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    TooltipDirective.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        this.initOptions = this.renameProperties(this.initOptions);
        /** @type {?} */
        var changedOptions = this.getProperties(changes);
        changedOptions = this.renameProperties(changedOptions);
        this.applyOptionsDefault(defaultOptions, changedOptions);
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.destroyTooltip({
            fast: true
        });
        if (this.componentSubscribe) {
            this.componentSubscribe.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.getShowDelay = /**
     * @return {?}
     */
    function () {
        return this.options['showDelay'];
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.getHideDelay = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var hideDelay = this.options['hideDelay'];
        /** @type {?} */
        var hideDelayTouchscreen = this.options['hideDelayTouchscreen'];
        return this.isTouchScreen ? hideDelayTouchscreen : hideDelay;
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    TooltipDirective.prototype.getProperties = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        /** @type {?} */
        var properties = {};
        for (var prop in changes) {
            if (prop !== 'options' && prop !== 'tooltipValue') {
                properties[prop] = changes[prop].currentValue;
            }
            if (prop === 'options') {
                properties = changes[prop].currentValue;
            }
        }
        return properties;
    };
    /**
     * @param {?} options
     * @return {?}
     */
    TooltipDirective.prototype.renameProperties = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        for (var prop in options) {
            if (backwardCompatibilityOptions[prop]) {
                options[backwardCompatibilityOptions[prop]] = options[prop];
                delete options[prop];
            }
        }
        return options;
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.getElementPosition = /**
     * @return {?}
     */
    function () {
        this.elementPosition = this.elementRef.nativeElement.getBoundingClientRect();
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.createTooltip = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.clearTimeouts();
        this.getElementPosition();
        this.createTimeoutId = window.setTimeout((/**
         * @return {?}
         */
        function () {
            _this.appendComponentToBody(TooltipComponent);
        }), this.getShowDelay());
        this.showTimeoutId = window.setTimeout((/**
         * @return {?}
         */
        function () {
            _this.showTooltipElem();
        }), this.getShowDelay());
    };
    /**
     * @param {?=} options
     * @return {?}
     */
    TooltipDirective.prototype.destroyTooltip = /**
     * @param {?=} options
     * @return {?}
     */
    function (options) {
        var _this = this;
        if (options === void 0) { options = {
            fast: false
        }; }
        this.clearTimeouts();
        if (this.isTooltipDestroyed == false) {
            this.hideTimeoutId = window.setTimeout((/**
             * @return {?}
             */
            function () {
                _this.hideTooltip();
            }), options.fast ? 0 : this.getHideDelay());
            this.destroyTimeoutId = window.setTimeout((/**
             * @return {?}
             */
            function () {
                if (!_this.componentRef || _this.isTooltipDestroyed) {
                    return;
                }
                _this.appRef.detachView(_this.componentRef.hostView);
                _this.componentRef.destroy();
                _this.events.emit({
                    type: 'hidden',
                    position: _this.tooltipPosition
                });
            }), options.fast ? 0 : this.destroyDelay);
        }
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.showTooltipElem = /**
     * @return {?}
     */
    function () {
        this.clearTimeouts();
        ((/** @type {?} */ (this.componentRef.instance))).show = true;
        this.events.emit({
            type: 'show',
            position: this.tooltipPosition
        });
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.hideTooltip = /**
     * @return {?}
     */
    function () {
        if (!this.componentRef || this.isTooltipDestroyed) {
            return;
        }
        ((/** @type {?} */ (this.componentRef.instance))).show = false;
        this.events.emit({
            type: 'hide',
            position: this.tooltipPosition
        });
    };
    /**
     * @param {?} component
     * @param {?=} data
     * @return {?}
     */
    TooltipDirective.prototype.appendComponentToBody = /**
     * @param {?} component
     * @param {?=} data
     * @return {?}
     */
    function (component, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        this.componentRef = this.componentFactoryResolver
            .resolveComponentFactory(component)
            .create(this.injector);
        ((/** @type {?} */ (this.componentRef.instance))).data = {
            value: this.tooltipValue,
            element: this.elementRef.nativeElement,
            elementPosition: this.tooltipPosition,
            options: this.options
        };
        this.appRef.attachView(this.componentRef.hostView);
        /** @type {?} */
        var domElem = (/** @type {?} */ (((/** @type {?} */ (this.componentRef.hostView))).rootNodes[0]));
        document.body.appendChild(domElem);
        this.componentSubscribe = ((/** @type {?} */ (this.componentRef.instance))).events.subscribe((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.handleEvents(event);
        }));
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.clearTimeouts = /**
     * @return {?}
     */
    function () {
        if (this.createTimeoutId) {
            clearTimeout(this.createTimeoutId);
        }
        if (this.showTimeoutId) {
            clearTimeout(this.showTimeoutId);
        }
        if (this.hideTimeoutId) {
            clearTimeout(this.hideTimeoutId);
        }
        if (this.destroyTimeoutId) {
            clearTimeout(this.destroyTimeoutId);
        }
    };
    Object.defineProperty(TooltipDirective.prototype, "isDisplayOnHover", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.options['display'] == false) {
                return false;
            }
            if (this.options['displayTouchscreen'] == false && this.isTouchScreen) {
                return false;
            }
            if (this.options['trigger'] !== 'hover') {
                return false;
            }
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipDirective.prototype, "isDisplayOnClick", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.options['display'] == false) {
                return false;
            }
            if (this.options['displayTouchscreen'] == false && this.isTouchScreen) {
                return false;
            }
            if (this.options['trigger'] != 'click') {
                return false;
            }
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TooltipDirective.prototype, "isTouchScreen", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
            /** @type {?} */
            var mq = (/**
             * @param {?} query
             * @return {?}
             */
            function (query) {
                return window.matchMedia(query).matches;
            });
            if (('ontouchstart' in window)) {
                return true;
            }
            // include the 'heartz' as a way to have a non matching MQ to help terminate the join
            // https://git.io/vznFH
            /** @type {?} */
            var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
            return mq(query);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} defaultOptions
     * @param {?} options
     * @return {?}
     */
    TooltipDirective.prototype.applyOptionsDefault = /**
     * @param {?} defaultOptions
     * @param {?} options
     * @return {?}
     */
    function (defaultOptions$$1, options) {
        this.options = Object.assign({}, defaultOptions$$1, this.initOptions || {}, options);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    TooltipDirective.prototype.handleEvents = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event.type === 'shown') {
            this.events.emit({
                type: 'shown',
                position: this.tooltipPosition
            });
        }
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.show = /**
     * @return {?}
     */
    function () {
        if (!this.componentRef || this.isTooltipDestroyed) {
            this.createTooltip();
        }
        else if (!this.isTooltipDestroyed) {
            this.showTooltipElem();
        }
    };
    /**
     * @return {?}
     */
    TooltipDirective.prototype.hide = /**
     * @return {?}
     */
    function () {
        this.destroyTooltip();
    };
    TooltipDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"], args: [{
                    selector: '[tooltip]'
                },] }
    ];
    /** @nocollapse */
    TooltipDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [TooltipOptionsService,] }] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"] }
    ]; };
    TooltipDirective.propDecorators = {
        options: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['options',] }],
        tooltipValue: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['tooltip',] }],
        placement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['placement',] }],
        autoPlacement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['autoPlacement',] }],
        contentType: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['content-type',] }],
        hideDelayMobile: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['hide-delay-mobile',] }],
        hideDelayTouchscreen: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['hideDelayTouchscreen',] }],
        zIndex: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['z-index',] }],
        animationDuration: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['animation-duration',] }],
        trigger: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['trigger',] }],
        tooltipClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['tooltip-class',] }],
        display: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['display',] }],
        displayMobile: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['display-mobile',] }],
        displayTouchscreen: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['displayTouchscreen',] }],
        shadow: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['shadow',] }],
        theme: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['theme',] }],
        offset: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['offset',] }],
        width: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['width',] }],
        maxWidth: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['max-width',] }],
        id: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['id',] }],
        showDelay: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['show-delay',] }],
        hideDelay: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['hide-delay',] }],
        hideDelayAfterClick: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['hideDelayAfterClick',] }],
        pointerEvents: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['pointerEvents',] }],
        position: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['position',] }],
        events: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"] }],
        onMouseEnter: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['focusin',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['mouseenter',] }],
        onMouseLeave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['focusout',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['mouseleave',] }],
        onClick: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['click',] }]
    };
    return TooltipDirective;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TooltipModule = /** @class */ (function () {
    function TooltipModule() {
    }
    /**
     * @param {?} initOptions
     * @return {?}
     */
    TooltipModule.forRoot = /**
     * @param {?} initOptions
     * @return {?}
     */
    function (initOptions) {
        return {
            ngModule: TooltipModule,
            providers: [
                {
                    provide: TooltipOptionsService,
                    useValue: initOptions
                }
            ]
        };
    };
    TooltipModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"], args: [{
                    declarations: [
                        TooltipDirective,
                        TooltipComponent
                    ],
                    imports: [
                        _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
                    ],
                    exports: [
                        TooltipDirective
                    ],
                    entryComponents: [
                        TooltipComponent
                    ]
                },] }
    ];
    return TooltipModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */



//# sourceMappingURL=ng2-tooltip-directive.js.map

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/wallet/wallet-view/wallet-view.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/wallet/wallet-view/wallet-view.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wallet\">\r\n    <!-- Wallet Details -->\r\n    <div class=\"row\" *ngIf=\"hasAccount\">\r\n        <div class=\"col-md-4\">\r\n            <div class=\"row\">\r\n                <div class=\"col-12\">\r\n                    <div class=\"wallet__actions\">\r\n                        <div class=\"wallet_title\">\r\n                            <p>ProximaX Wallet (XPX)</p>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"keyPanel\">\r\n                        <div class=\"balanceUsd\">\r\n                            <p>USD {{xpxBalanceUSD}}</p>\r\n                        </div>\r\n\r\n                        <div class=\"balanceUsd\">\r\n                            <small>{{xpxBalance}} XPX</small>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"wallet_account\">\r\n                        <div class=\"wallet_key_button\" (click)=\"showKeys(accountKey)\">\r\n                            <i class=\"fa fa-id-card\"></i> Account Key\r\n                        </div>\r\n                        <div class=\"wallet_qr_button\" (click)=\"showQrCode(qrContent)\">\r\n                            <i class=\"fa fa-qrcode\"></i> QR code\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-8\">\r\n            <div class=\"staticTable\">\r\n                <table class=\"rwd-table\">\r\n                    <tr class=\"trHead\">\r\n                        <th>DATE</th>\r\n                        <th>SENDER</th>\r\n                        <th>MESSAGE</th>\r\n                        <th>AMOUNT</th>\r\n                    </tr>\r\n                    <tr *ngIf=\"noTransactions\">\r\n                        <td colspan=\"4\" class=\"transPlacement\">No transactions yet...</td>\r\n                    </tr>\r\n                    <ng-container *ngIf=\"hasTransactions\">\r\n                        <tr *ngFor=\"let transactionDetail of transferTransactions; let i = index\"\r\n                            (click)=\"showTransDetails(transactionDetailsContent, transactionDetail)\">\r\n                            <td>\r\n                                <div class=\"trans_date\">\r\n                                    <div class=\"trans_month\">{{ transactionDetail.month | uppercase }}</div>\r\n                                    <div class=\"trans_day\">{{ transactionDetail.day }}</div>\r\n                                </div>\r\n                            </td>\r\n                            <td>{{ (transactionDetail.sender | slice:0:12) + '......' + (transactionDetail.sender | slice:-2) }}\r\n                            </td>\r\n                            <td>{{ transactionDetail.message === '' ? 'empty' : (transactionDetail.message | slice:0:8) + '......' }}\r\n                            </td>\r\n                            <td [className]=\"transactionDetail.type === RAW_ADDRESS ? 'creditClass' : 'debitClass'\">\r\n                                {{ transactionDetail.type === RAW_ADDRESS ? ('+' + transactionDetail.amount) : ('-'+transactionDetail.amount) }}\r\n                                XPX\r\n                            </td>\r\n                        </tr>\r\n                    </ng-container>\r\n                </table>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <!-- Setup Wallet -->\r\n    <div class=\"row\" *ngIf=\"noAccount\">\r\n        <div class=\"col-md-12\">\r\n            <div class=\"accountCanvass\">\r\n                <div class=\"accountPanel\">\r\n                    <img src=\"../../../assets/images/wallet_icon.png\" alt=\"wallet\">\r\n                    <p>Do you have ProximaX Wallet?</p>\r\n                    <div class=\"accountButton\">\r\n                        <div class=\"create_button\" (click)=\"createWallet()\">\r\n                            <button class=\"login-btn button\" type=\"submit\"><i class=\"fa fa-plus\"></i> Create Wallet\r\n                            </button>\r\n                        </div>\r\n                        <div class=\"link_button\" (click)=\"linkWallet()\">\r\n                            <button class=\"login-btn button\" type=\"submit\"><i class=\"fa fa-link\"></i> Link Wallet\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n<!-- Account Key Modal -->\r\n<ng-template #accountKey let-modal>\r\n    <div class=\"modal-body\">\r\n        <div class=\"keyCanvass\">\r\n            <div class=\"keyPanel\">\r\n                <p class=\"modalTitle\">Account Key</p>\r\n                <div class=\"keyGroup\">\r\n                    <p class=\"modalSubTitle\">Public Key</p>\r\n                    <div class=\"input-group\">\r\n                        <input type=\"text\" class=\"form-control\" value=\"{{PUBLIC_KEY}}\" readonly>\r\n                        <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-outline-secondary btnCopy\" ngxClipboard [cbContent]=\"PUBLIC_KEY\"\r\n                                (click)=\"copied()\">\r\n                                <i class=\"fa fa-copy\"></i>\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"keyGroup\">\r\n                    <p class=\"modalSubTitle\">Private Key</p>\r\n                    <div class=\"input-group\">\r\n                        <input type=\"text\" class=\"form-control\" value=\"\" readonly>\r\n                        <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-outline-secondary btnCopy\" ngxClipboard [cbContent]=\"\">\r\n                                <i class=\"fa fa-eye-slash\"></i>\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"modal.close('Save click')\">Close</button>\r\n    </div>\r\n</ng-template>\r\n\r\n\r\n<!-- QR Code Modal -->\r\n<ng-template #qrContent let-modal>\r\n    <div class=\"modal-body\">\r\n        <div class=\"qrCanvass\">\r\n            <div class=\"qrPanel\">\r\n                <p class=\"modalTitle\">USD Wallet Address</p>\r\n                <div class=\"input-group\">\r\n                    <input type=\"text\" class=\"form-control\" value=\"{{ADDRESS}}\" readonly>\r\n                    <div class=\"input-group-append\">\r\n                        <button class=\"btn btn-outline-secondary btnCopy\" ngxClipboard [cbContent]=\"ADDRESS\"\r\n                            (click)=\"copied()\" tooltip=\"Copy Address\">\r\n                            <i class=\" fa fa-copy\"></i>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <ngx-qrcode [(qrc-value)]=\"ADDRESS\"></ngx-qrcode>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"modal.close('Save click')\">Close</button>\r\n    </div>\r\n</ng-template>\r\n\r\n\r\n<!-- Transaction Details Modal -->\r\n<ng-template #transactionDetailsContent let-modal>\r\n    <div class=\"modal-body\">\r\n        <div class=\"transCanvass\">\r\n            <div class=\"transPanel\">\r\n                <p class=\"modalTitle\">Transaction Information</p>\r\n                <div class=\"transGroup\">\r\n                    <div\r\n                        [className]=\"transactionDetail.type === RAW_ADDRESS ? 'trans_amount_creditClass' : 'trans_amount_debitClass'\">\r\n                        <i class=\"fa fa-exchange\" aria-hidden=\"true\"></i>\r\n                    </div>\r\n\r\n                    <div\r\n                        [className]=\"transactionDetail.type === RAW_ADDRESS ? 'trans_amount_creditClass' : 'trans_amount_debitClass'\">\r\n                        {{ transactionDetail.amount }} XPX\r\n                    </div>\r\n                    <div class=\"transList transList_panel\">\r\n                        <div class=\"transItem\">\r\n                            <div class=\"trans_title\">Date</div>\r\n                            <div class=\"trans_value\">\r\n                                {{ transactionDetail.month + ' ' + transactionDetail.day + ',' + transactionDetail.year + ' | ' + transactionDetail.time}}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"transItem\">\r\n                            <div class=\"trans_title\">Fee</div>\r\n                            <div class=\"trans_value\">{{ transactionDetail.fee }} XPX</div>\r\n                        </div>\r\n                        <div class=\"transItem\">\r\n                            <div class=\"trans_title\">To</div>\r\n                            <div class=\"trans_value_hash\">\r\n                                {{ transactionDetail.address }}\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"transItem\">\r\n                            <div class=\"trans_title\">From</div>\r\n                            <div class=\"trans_value_hash\">\r\n                                {{ ADDRESS }}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"transListHash transList_panelHash\">\r\n                        <div class=\"transHash\">\r\n                            <div class=\"trans_hash_code\">\r\n                                <p><b>Message</b></p>\r\n                                <div class=\"messagePanel\">\r\n                                    <p class=\"hash\">\r\n                                        {{ transactionDetail.message === '' ? 'empty' : (transactionDetail.message) }}\r\n                                    </p>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"transHash\">\r\n                            <div class=\"trans_hash_code\">\r\n                                <p><b>Signer</b></p>\r\n                                <p class=\"hash\">{{ transactionDetail.signer}}</p>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"transHash\">\r\n                            <div class=\"trans_hash_code\">\r\n                                <p><b>Signature</b></p>\r\n                                <p class=\"hash\">{{ transactionDetail.signature}}</p>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"modal.close('Save click')\">Close</button>\r\n    </div>\r\n</ng-template>"

/***/ }),

/***/ "./src/app/wallet/store/wallet.action.ts":
/*!***********************************************!*\
  !*** ./src/app/wallet/store/wallet.action.ts ***!
  \***********************************************/
/*! exports provided: GetWallet, EditWallet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetWallet", function() { return GetWallet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditWallet", function() { return EditWallet; });
var GetWallet = /** @class */ (function () {
    function GetWallet() {
    }
    GetWallet.type = '[Wallet Page] GetWallet';
    return GetWallet;
}());

var EditWallet = /** @class */ (function () {
    function EditWallet(payload) {
        this.payload = payload;
    }
    EditWallet.type = '[Edit Wallet Page] EditWallet';
    EditWallet.ctorParameters = function () { return [
        { type: FormData }
    ]; };
    return EditWallet;
}());



/***/ }),

/***/ "./src/app/wallet/store/wallet.state.ts":
/*!**********************************************!*\
  !*** ./src/app/wallet/store/wallet.state.ts ***!
  \**********************************************/
/*! exports provided: WalletState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletState", function() { return WalletState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _app_core_models_profile_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/core/models/profile.model */ "./src/app/core/models/profile.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _app_core_services_users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @app/core/services/users.service */ "./src/app/core/services/users.service.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm5/ngxs-store.js");
/* harmony import */ var _wallet_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./wallet.action */ "./src/app/wallet/store/wallet.action.ts");
/* harmony import */ var _app_auth_store_auth_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app/auth/store/auth.action */ "./src/app/auth/store/auth.action.ts");






// NGXS



var WalletState = /** @class */ (function () {
    function WalletState(authService, usersService, store) {
        this.authService = authService;
        this.usersService = usersService;
        this.store = store;
    }
    WalletState.getProfile = function (state) {
        return state.profile;
    };
    WalletState.prototype.getProfile = function (_a, _b) {
        var _this = this;
        var patchState = _a.patchState;
        return this.authService.getProfile().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (result) {
            _this.store.dispatch(new _app_auth_store_auth_action__WEBPACK_IMPORTED_MODULE_8__["ReplaceProfilePhoto"](result.image));
            console.log(result);
            patchState({
                profile: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    };
    WalletState.ctorParameters = function () { return [
        { type: _app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] },
        { type: _app_core_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UsersService"] },
        { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Store"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Action"])(_wallet_action__WEBPACK_IMPORTED_MODULE_7__["GetWallet"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _wallet_action__WEBPACK_IMPORTED_MODULE_7__["GetWallet"]]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], WalletState.prototype, "getProfile", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Selector"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_profile_model__WEBPACK_IMPORTED_MODULE_2__["ProfileStateModel"]]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], WalletState, "getProfile", null);
    WalletState = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_6__["State"])({
            name: 'wallet',
            defaults: {
                profile: null,
                profiles: [],
                session_token: null
            }
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _app_core_services_users_service__WEBPACK_IMPORTED_MODULE_5__["UsersService"], _ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Store"]])
    ], WalletState);
    return WalletState;
}());



/***/ }),

/***/ "./src/app/wallet/wallet-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/wallet/wallet-routing.module.ts ***!
  \*************************************************/
/*! exports provided: WalletRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletRoutingModule", function() { return WalletRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _wallet_view_wallet_view_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./wallet-view/wallet-view.component */ "./src/app/wallet/wallet-view/wallet-view.component.ts");




var routes = [
    {
        path: '',
        component: _wallet_view_wallet_view_component__WEBPACK_IMPORTED_MODULE_3__["WalletViewComponent"]
    }
];
var WalletRoutingModule = /** @class */ (function () {
    function WalletRoutingModule() {
    }
    WalletRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], WalletRoutingModule);
    return WalletRoutingModule;
}());



/***/ }),

/***/ "./src/app/wallet/wallet-view/wallet-view.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/wallet/wallet-view/wallet-view.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import 'https://fonts.googleapis.com/css?family=Montserrat:300,400,700';\n.staticTable {\n  border: 1px solid #b8b8b8;\n  border-radius: 0.4rem;\n}\n.staticTable .rwd-table {\n  min-width: 100%;\n  font-size: 1.2rem;\n  color: #929292;\n}\n.staticTable .rwd-table tr {\n  border-bottom: 1px solid #ddd;\n}\n.staticTable .rwd-table th {\n  display: none;\n}\n.staticTable .rwd-table td {\n  display: block;\n}\n.staticTable .rwd-table td:first-child {\n  padding-top: 0.5em;\n}\n.staticTable .rwd-table td:last-child {\n  padding-bottom: 0.5em;\n}\n.staticTable .rwd-table td:before {\n  content: attr(data-th) \": \";\n  font-weight: bold;\n  width: 6.5em;\n  display: inline-block;\n}\n@media (min-width: 480px) {\n  .staticTable .rwd-table td:before {\n    display: none;\n  }\n}\n.staticTable .rwd-table th, .staticTable .rwd-table td {\n  text-align: left;\n}\n@media (min-width: 480px) {\n  .staticTable .rwd-table th, .staticTable .rwd-table td {\n    display: table-cell;\n    padding: 0.25em 0.5em;\n  }\n  .staticTable .rwd-table th:first-child, .staticTable .rwd-table td:first-child {\n    padding-left: 0;\n  }\n  .staticTable .rwd-table th:last-child, .staticTable .rwd-table td:last-child {\n    padding-right: 0;\n  }\n}\nbody {\n  padding: 0 2em;\n  font-family: Montserrat, sans-serif;\n  -webkit-font-smoothing: antialiased;\n  text-rendering: optimizeLegibility;\n  color: #444;\n  background: #eee;\n}\nh1 {\n  font-weight: normal;\n  letter-spacing: -1px;\n  color: #34495E;\n}\n.rwd-table {\n  background: #ffffff;\n  color: #000;\n  border-radius: 0.4em;\n  overflow: hidden;\n}\n.rwd-table .trHead {\n  background-color: #fafafa;\n}\n.rwd-table .transPlacement {\n  text-align: center !important;\n}\n.rwd-table tr th {\n  color: #757575;\n}\n.rwd-table tr:hover {\n  background-color: #fafafa;\n  cursor: pointer;\n}\n.rwd-table th, .rwd-table td {\n  margin: 0.5em 1em;\n}\n@media (min-width: 480px) {\n  .rwd-table th, .rwd-table td {\n    padding: 1em !important;\n  }\n}\n.rwd-table th, .rwd-table td:before {\n  color: #000000;\n}\n.rwd-table .debitClass {\n  color: #d24700;\n}\n.rwd-table .creditClass {\n  color: #00a79b;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2FsbGV0L3dhbGxldC12aWV3L0M6XFxVc2Vyc1xcQVNVU1xcRGVza3RvcFxcUHJvamVjdFxcaW1hZ2VyYXgtc2lyaXVzLWZyb250ZW5kXFxzaXJpdXMtbGl2ZXdhbGwtLS1mcm9udGVuZC9zcmNcXGFwcFxcd2FsbGV0XFx3YWxsZXQtdmlld1xcd2FsbGV0LXZpZXcuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3dhbGxldC93YWxsZXQtdmlldy93YWxsZXQtdmlldy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFzRVEsd0VBQUE7QUFwRVI7RUFDRSx5QkFBQTtFQUNBLHFCQUFBO0FDQUY7QURFRTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUNBSjtBREVJO0VBRUUsNkJBQUE7QUNETjtBRElJO0VBQ0UsYUFBQTtBQ0ZOO0FES0k7RUFDRSxjQUFBO0FDSE47QURLTTtFQUNFLGtCQUFBO0FDSFI7QURLTTtFQUNFLHFCQUFBO0FDSFI7QURNTTtFQUNFLDJCQUFBO0VBQ0EsaUJBQUE7RUFHQSxZQUFBO0VBQ0EscUJBQUE7QUNOUjtBRFNRO0VBVEY7SUFVSSxhQUFBO0VDTlI7QUFDRjtBRFVJO0VBQ0UsZ0JBQUE7QUNSTjtBRFVNO0VBSEY7SUFJSSxtQkFBQTtJQUNBLHFCQUFBO0VDUE47RURTTTtJQUNFLGVBQUE7RUNQUjtFRFVNO0lBQ0UsZ0JBQUE7RUNSUjtBQUNGO0FEc0JBO0VBQ0UsY0FBQTtFQUNBLG1DQUFBO0VBQ0EsbUNBQUE7RUFDQSxrQ0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ25CRjtBRHNCQTtFQUNFLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxjQUFBO0FDbkJGO0FEc0JBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtBQ25CSjtBRHFCRTtFQUNFLHlCQUFBO0FDbkJKO0FEc0JFO0VBQ0UsNkJBQUE7QUNwQko7QUR3Qkk7RUFDSSxjQUFBO0FDdEJSO0FEMEJFO0VBQ0UseUJBQUE7RUFDQSxlQUFBO0FDeEJKO0FENkJFO0VBQ0UsaUJBQUE7QUMzQko7QUQ0Qkk7RUFGRjtJQUdJLHVCQUFBO0VDekJKO0FBQ0Y7QUQyQkU7RUFDRSxjQUFBO0FDekJKO0FENEJFO0VBQ0UsY0FBQTtBQzFCSjtBRDZCRTtFQUNFLGNBQUE7QUMzQkoiLCJmaWxlIjoic3JjL2FwcC93YWxsZXQvd2FsbGV0LXZpZXcvd2FsbGV0LXZpZXcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkYnJlYWtwb2ludC1hbHBoYTogNDgwcHg7IC8vIGFkanVzdCB0byB5b3VyIG5lZWRzXHJcblxyXG4uc3RhdGljVGFibGUge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNiOGI4Yjg7XHJcbiAgYm9yZGVyLXJhZGl1czogLjRyZW07XHJcblxyXG4gIC5yd2QtdGFibGUge1xyXG4gICAgbWluLXdpZHRoOiAxMDAlOyAvLyBhZGp1c3QgdG8geW91ciBuZWVkc1xyXG4gICAgZm9udC1zaXplOiAxLjJyZW07XHJcbiAgICBjb2xvcjogZGFya2VuKCNiOGI4YjgsIDE1JSk7XHJcbiAgICBcclxuICAgIHRyIHtcclxuICAgICAgLy8gYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkZGQ7XHJcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICB0aCB7XHJcbiAgICAgIGRpc3BsYXk6IG5vbmU7IC8vIGZvciBhY2Nlc3NpYmlsaXR5LCB1c2UgYSB2aXN1YWxseSBoaWRkZW4gbWV0aG9kIGhlcmUgaW5zdGVhZCEgVGhhbmtzLCByZWRkaXQhICAgXHJcbiAgICB9XHJcbiAgICBcclxuICAgIHRkIHtcclxuICAgICAgZGlzcGxheTogYmxvY2s7IFxyXG4gICAgICBcclxuICAgICAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IC41ZW07XHJcbiAgICAgIH1cclxuICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogLjVlbTtcclxuICAgICAgfVxyXG5cclxuICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6IGF0dHIoZGF0YS10aClcIjogXCI7IC8vIHdobyBrbmV3IHlvdSBjb3VsZCBkbyB0aGlzPyBUaGUgaW50ZXJuZXQsIHRoYXQncyB3aG8uXHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcblxyXG4gICAgICAgIC8vIG9wdGlvbmFsIHN0dWZmIHRvIG1ha2UgaXQgbG9vayBuaWNlclxyXG4gICAgICAgIHdpZHRoOiA2LjVlbTsgLy8gbWFnaWMgbnVtYmVyIDooIGFkanVzdCBhY2NvcmRpbmcgdG8geW91ciBvd24gY29udGVudFxyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAvLyBlbmQgb3B0aW9uc1xyXG4gICAgICAgIFxyXG4gICAgICAgIEBtZWRpYSAobWluLXdpZHRoOiAkYnJlYWtwb2ludC1hbHBoYSkge1xyXG4gICAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgdGgsIHRkIHtcclxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgXHJcbiAgICAgIEBtZWRpYSAobWluLXdpZHRoOiAkYnJlYWtwb2ludC1hbHBoYSkge1xyXG4gICAgICAgIGRpc3BsYXk6IHRhYmxlLWNlbGw7XHJcbiAgICAgICAgcGFkZGluZzogLjI1ZW0gLjVlbTtcclxuICAgICAgICBcclxuICAgICAgICAmOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgIHBhZGRpbmctbGVmdDogMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICB9XHJcbn1cclxuXHJcblxyXG4vLyBwcmVzZW50YXRpb25hbCBzdHlsaW5nXHJcblxyXG5AaW1wb3J0ICdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9TW9udHNlcnJhdDozMDAsNDAwLDcwMCc7XHJcblxyXG5ib2R5IHtcclxuICBwYWRkaW5nOiAwIDJlbTtcclxuICBmb250LWZhbWlseTogTW9udHNlcnJhdCwgc2Fucy1zZXJpZjtcclxuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcclxuICB0ZXh0LXJlbmRlcmluZzogb3B0aW1pemVMZWdpYmlsaXR5O1xyXG4gIGNvbG9yOiAjNDQ0O1xyXG4gIGJhY2tncm91bmQ6ICNlZWU7XHJcbn1cclxuXHJcbmgxIHtcclxuICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gIGxldHRlci1zcGFjaW5nOiAtMXB4O1xyXG4gIGNvbG9yOiAjMzQ0OTVFO1xyXG59XHJcblxyXG4ucndkLXRhYmxlIHtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAuNGVtO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgLnRySGVhZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmFmYWZhO1xyXG4gIH1cclxuXHJcbiAgLnRyYW5zUGxhY2VtZW50IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgdHIge1xyXG4gICAgdGgge1xyXG4gICAgICAgIGNvbG9yOiAjNzU3NTc1O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdHI6aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmYTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcblxyXG5cclxuXHJcbiAgdGgsIHRkIHtcclxuICAgIG1hcmdpbjogLjVlbSAxZW07XHJcbiAgICBAbWVkaWEgKG1pbi13aWR0aDogJGJyZWFrcG9pbnQtYWxwaGEpIHsgXHJcbiAgICAgIHBhZGRpbmc6IDFlbSAhaW1wb3J0YW50OyBcclxuICAgIH1cclxuICB9XHJcbiAgdGgsIHRkOmJlZm9yZSB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICB9XHJcblxyXG4gIC5kZWJpdENsYXNzIHtcclxuICAgIGNvbG9yOiAjZDI0NzAwO1xyXG4gIH1cclxuXHJcbiAgLmNyZWRpdENsYXNzIHtcclxuICAgIGNvbG9yOiAjMDBhNzliO1xyXG4gIH1cclxufSIsIkBpbXBvcnQgJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Nb250c2VycmF0OjMwMCw0MDAsNzAwJztcbi5zdGF0aWNUYWJsZSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNiOGI4Yjg7XG4gIGJvcmRlci1yYWRpdXM6IDAuNHJlbTtcbn1cbi5zdGF0aWNUYWJsZSAucndkLXRhYmxlIHtcbiAgbWluLXdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgY29sb3I6ICM5MjkyOTI7XG59XG4uc3RhdGljVGFibGUgLnJ3ZC10YWJsZSB0ciB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkO1xufVxuLnN0YXRpY1RhYmxlIC5yd2QtdGFibGUgdGgge1xuICBkaXNwbGF5OiBub25lO1xufVxuLnN0YXRpY1RhYmxlIC5yd2QtdGFibGUgdGQge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5zdGF0aWNUYWJsZSAucndkLXRhYmxlIHRkOmZpcnN0LWNoaWxkIHtcbiAgcGFkZGluZy10b3A6IDAuNWVtO1xufVxuLnN0YXRpY1RhYmxlIC5yd2QtdGFibGUgdGQ6bGFzdC1jaGlsZCB7XG4gIHBhZGRpbmctYm90dG9tOiAwLjVlbTtcbn1cbi5zdGF0aWNUYWJsZSAucndkLXRhYmxlIHRkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IGF0dHIoZGF0YS10aCkgXCI6IFwiO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgd2lkdGg6IDYuNWVtO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNDgwcHgpIHtcbiAgLnN0YXRpY1RhYmxlIC5yd2QtdGFibGUgdGQ6YmVmb3JlIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59XG4uc3RhdGljVGFibGUgLnJ3ZC10YWJsZSB0aCwgLnN0YXRpY1RhYmxlIC5yd2QtdGFibGUgdGQge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuQG1lZGlhIChtaW4td2lkdGg6IDQ4MHB4KSB7XG4gIC5zdGF0aWNUYWJsZSAucndkLXRhYmxlIHRoLCAuc3RhdGljVGFibGUgLnJ3ZC10YWJsZSB0ZCB7XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgICBwYWRkaW5nOiAwLjI1ZW0gMC41ZW07XG4gIH1cbiAgLnN0YXRpY1RhYmxlIC5yd2QtdGFibGUgdGg6Zmlyc3QtY2hpbGQsIC5zdGF0aWNUYWJsZSAucndkLXRhYmxlIHRkOmZpcnN0LWNoaWxkIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gIH1cbiAgLnN0YXRpY1RhYmxlIC5yd2QtdGFibGUgdGg6bGFzdC1jaGlsZCwgLnN0YXRpY1RhYmxlIC5yd2QtdGFibGUgdGQ6bGFzdC1jaGlsZCB7XG4gICAgcGFkZGluZy1yaWdodDogMDtcbiAgfVxufVxuXG5ib2R5IHtcbiAgcGFkZGluZzogMCAyZW07XG4gIGZvbnQtZmFtaWx5OiBNb250c2VycmF0LCBzYW5zLXNlcmlmO1xuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgdGV4dC1yZW5kZXJpbmc6IG9wdGltaXplTGVnaWJpbGl0eTtcbiAgY29sb3I6ICM0NDQ7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG59XG5cbmgxIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgbGV0dGVyLXNwYWNpbmc6IC0xcHg7XG4gIGNvbG9yOiAjMzQ0OTVFO1xufVxuXG4ucndkLXRhYmxlIHtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgY29sb3I6ICMwMDA7XG4gIGJvcmRlci1yYWRpdXM6IDAuNGVtO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnJ3ZC10YWJsZSAudHJIZWFkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmYTtcbn1cbi5yd2QtdGFibGUgLnRyYW5zUGxhY2VtZW50IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG59XG4ucndkLXRhYmxlIHRyIHRoIHtcbiAgY29sb3I6ICM3NTc1NzU7XG59XG4ucndkLXRhYmxlIHRyOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmYTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLnJ3ZC10YWJsZSB0aCwgLnJ3ZC10YWJsZSB0ZCB7XG4gIG1hcmdpbjogMC41ZW0gMWVtO1xufVxuQG1lZGlhIChtaW4td2lkdGg6IDQ4MHB4KSB7XG4gIC5yd2QtdGFibGUgdGgsIC5yd2QtdGFibGUgdGQge1xuICAgIHBhZGRpbmc6IDFlbSAhaW1wb3J0YW50O1xuICB9XG59XG4ucndkLXRhYmxlIHRoLCAucndkLXRhYmxlIHRkOmJlZm9yZSB7XG4gIGNvbG9yOiAjMDAwMDAwO1xufVxuLnJ3ZC10YWJsZSAuZGViaXRDbGFzcyB7XG4gIGNvbG9yOiAjZDI0NzAwO1xufVxuLnJ3ZC10YWJsZSAuY3JlZGl0Q2xhc3Mge1xuICBjb2xvcjogIzAwYTc5Yjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/wallet/wallet-view/wallet-view.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/wallet/wallet-view/wallet-view.component.ts ***!
  \*************************************************************/
/*! exports provided: WalletViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletViewComponent", function() { return WalletViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_services_proximax_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/services/proximax.service */ "./src/app/core/services/proximax.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "./node_modules/tsjs-xpx-chain-sdk/dist/index.js");
/* harmony import */ var tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm5/ngxs-store.js");
/* harmony import */ var _store_wallet_state__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store/wallet.state */ "./src/app/wallet/store/wallet.state.ts");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./../../profile/store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm5/ngxs-router-plugin.js");




// SweetAlert


// NGXS






var WalletViewComponent = /** @class */ (function () {
    function WalletViewComponent(proximax, modalService, store) {
        this.proximax = proximax;
        this.modalService = modalService;
        this.store = store;
        // NGXS
        this.userId = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_8__["AuthState"].getUserId);
        this.transactions = [];
        this.transferTransactions = [];
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.hasAccount = false;
        this.noAccount = false;
        this.hasTransactions = false;
        this.noTransactions = false;
        this.profileDetails = [];
        this.wallet_object = [];
    }
    WalletViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_10__["GetProfile"]()).subscribe(function (user) {
            console.log('-----Profile from Wallet------', user);
            var _addr = user.profile.profile.wallet_address;
            var _pub = user.profile.profile.wallet_public_key;
            var noAccount = _this.proximax.getAccountInfo(_addr);
            // check if account has wallet
            if (noAccount == true) {
                _this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_11__["Navigate"](['/home']));
                // console.log('no account');
                _this.proximax.generateNewAccountTest();
            }
            else {
                // console.log('has account');
                _this.hasAccount = true;
                _this.noAccount = false;
                _this.ADDRESS = _addr;
                _this.RAW_PUBLIC_KEY = _pub;
                _this.RAW_ADDRESS = _addr.replace(/-/g, '');
                _this.PUBLIC_KEY = (_pub).match(new RegExp('.{1,6}', 'g')).join('-');
                // Load Data
                _this.getBalance(_addr);
                _this.getTransactions(_this.RAW_PUBLIC_KEY);
            }
        });
    };
    WalletViewComponent.prototype.getBalance = function (_addr) {
        var _this = this;
        this.proximax.getBalance(_addr).subscribe(function (amount) {
            // console.log('Amount:', amount);
            _this.xpxBalance = parseFloat(amount.toString()).toFixed(6);
            _this.proximax.getPriceUSD().subscribe(function (price) {
                // console.log('Price:', price);
                var balance = _this.xpxBalance * price;
                _this.xpxBalanceUSD = parseFloat(balance.toString()).toFixed(2);
            });
        });
    };
    WalletViewComponent.prototype.getTransactions = function (public_key) {
        var _this = this;
        this.proximax.generateTransactionsList(public_key)
            .subscribe(function (transactionList) {
            // console.log('transactions....', transactionList.length);
            if (transactionList.length == 0) {
                _this.hasTransactions = false;
                _this.noTransactions = true;
                // console.log('No transactions yet....');
            }
            else {
                var _transactionList = transactionList
                    .filter(function (tx) { return tx.type == tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["TransactionType"].TRANSFER; }) // Filter transfer transactions only
                    .map(function (tx) {
                    var _tx = tx;
                    var _address = _tx.recipient.plain().toString().match(new RegExp('.{1,6}', 'g')).join('-');
                    var _date = new Date(_tx.deadline.value.toString());
                    var _fee = parseFloat((_tx.maxFee.compact() / Math.pow(10, 6)).toString()).toFixed(6);
                    var _amount = parseFloat((_tx.mosaics[0].amount.compact() / Math.pow(10, 6)).toString()).toFixed(6);
                    return {
                        address: _address,
                        type: _tx.recipient.plain(),
                        sender: _tx.signer.address.plain(),
                        signer: _tx.signer.publicKey.toString(),
                        signature: _tx.signature.toString(),
                        message: _tx.message.payload.toString(),
                        amount: _amount,
                        fee: _fee,
                        month: _this.months[_date.getMonth()],
                        day: (_date.getUTCDate() < 10 ? ('0' + _date.getUTCDate()) : ('' + _date.getUTCDate())),
                        year: _date.getUTCFullYear(),
                        time: _date.toLocaleTimeString()
                    };
                });
                _this.transferTransactions = _transactionList;
                _this.hasTransactions = true;
                _this.noTransactions = false;
                // console.log('transactions....', JSON.stringify(this.transferTransactions));
                console.log('Has transactions....');
            }
        });
    };
    WalletViewComponent.prototype.copied = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            position: 'bottom-end',
            title: 'Copied to clipboard',
            type: 'success',
            showConfirmButton: false,
            timer: 1000,
            backdrop: 'transparent'
        });
    };
    WalletViewComponent.prototype.showKeys = function (content) {
        var _this = this;
        this.modalService.open(content, { size: 'lg' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    WalletViewComponent.prototype.showQrCode = function (content) {
        var _this = this;
        this.modalService.open(content, { size: 'sm' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    WalletViewComponent.prototype.showTransDetails = function (content, transactionDetail) {
        var _this = this;
        // console.log("TCL: showTransDetails -> transactionDetail", transactionDetail)
        this.transactionDetail = transactionDetail;
        this.modalService.open(content, { size: 'lg' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    WalletViewComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    WalletViewComponent.ctorParameters = function () { return [
        { type: _core_services_proximax_service__WEBPACK_IMPORTED_MODULE_2__["ProximaxService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"] },
        { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Store"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Select"])(_store_wallet_state__WEBPACK_IMPORTED_MODULE_7__["WalletState"].getProfile),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_9__["Observable"])
    ], WalletViewComponent.prototype, "profile$", void 0);
    WalletViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wallet-view',
            template: __webpack_require__(/*! raw-loader!./wallet-view.component.html */ "./node_modules/raw-loader/index.js!./src/app/wallet/wallet-view/wallet-view.component.html"),
            styles: [__webpack_require__(/*! ./wallet-view.component.scss */ "./src/app/wallet/wallet-view/wallet-view.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_services_proximax_service__WEBPACK_IMPORTED_MODULE_2__["ProximaxService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"],
            _ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Store"]])
    ], WalletViewComponent);
    return WalletViewComponent;
}());



/***/ }),

/***/ "./src/app/wallet/wallet.module.ts":
/*!*****************************************!*\
  !*** ./src/app/wallet/wallet.module.ts ***!
  \*****************************************/
/*! exports provided: WalletModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletModule", function() { return WalletModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _wallet_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./wallet-routing.module */ "./src/app/wallet/wallet-routing.module.ts");
/* harmony import */ var ngx_qrcode2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-qrcode2 */ "./node_modules/ngx-qrcode2/index.js");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-clipboard */ "./node_modules/ngx-clipboard/fesm5/ngx-clipboard.js");
/* harmony import */ var ng2_tooltip_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-tooltip-directive */ "./node_modules/ng2-tooltip-directive/fesm5/ng2-tooltip-directive.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm5/ngxs-store.js");
/* harmony import */ var _store_wallet_state__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./store/wallet.state */ "./src/app/wallet/store/wallet.state.ts");
/* harmony import */ var _wallet_view_wallet_view_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./wallet-view/wallet-view.component */ "./src/app/wallet/wallet-view/wallet-view.component.ts");








// NGXS



var WalletModule = /** @class */ (function () {
    function WalletModule() {
    }
    WalletModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [_wallet_view_wallet_view_component__WEBPACK_IMPORTED_MODULE_10__["WalletViewComponent"]],
            imports: [
                _wallet_routing_module__WEBPACK_IMPORTED_MODULE_4__["WalletRoutingModule"],
                ngx_qrcode2__WEBPACK_IMPORTED_MODULE_5__["NgxQRCodeModule"],
                ngx_clipboard__WEBPACK_IMPORTED_MODULE_6__["ClipboardModule"],
                ng2_tooltip_directive__WEBPACK_IMPORTED_MODULE_7__["TooltipModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModalModule"],
                _ngxs_store__WEBPACK_IMPORTED_MODULE_8__["NgxsModule"].forFeature([_store_wallet_state__WEBPACK_IMPORTED_MODULE_9__["WalletState"]])
            ]
        })
    ], WalletModule);
    return WalletModule;
}());



/***/ })

}]);
//# sourceMappingURL=wallet-wallet-module-es5.js.map
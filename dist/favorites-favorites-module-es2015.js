(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["favorites-favorites-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/favorites/favorites.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/favorites/favorites.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"favorites\">\r\n  <div class=\"filter\">\r\n    <div class=\"link\">Today</div>\r\n    <div class=\"link\">This Week</div>\r\n    <div class=\"link\">This Month</div>\r\n    <div class=\"link\">All</div>\r\n  </div>\r\n</div> -->\r\n<ngb-tabset type=\"pills\" (tabChange)=\"onTabChange($event)\">\r\n  <ng-container *ngFor=\"let option of options\">\r\n    <ngb-tab id=\"{{ option.id }}\">\r\n      <ng-template ngbTabTitle>\r\n        <div class=\"link\">{{ option.name }}</div>\r\n      </ng-template>\r\n      <ng-template ngbTabContent>\r\n        <div class=\"layout\">\r\n          <div class=\"item\" *ngFor=\"let post of posts$ | async\">\r\n            <img\r\n              *ngIf=\"post.type !== 'video'\"\r\n              [src]=\"getUrl(post.image)\"\r\n              class=\"avatar\"\r\n              [routerLink]=\"['/post/', post.id]\"\r\n            />\r\n            <div class=\"video-overlay\" *ngIf=\"post.type === 'video'\" [routerLink]=\"['/post/', post.id]\">\r\n              <video>\r\n                <source [src]=\"getUrl(post.image)\" />\r\n              </video>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </ng-template>\r\n    </ngb-tab>\r\n  </ng-container>\r\n</ngb-tabset>\r\n"

/***/ }),

/***/ "./src/app/favorites/favorites-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/favorites/favorites-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: FavoritesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoritesRoutingModule", function() { return FavoritesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _favorites_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./favorites.component */ "./src/app/favorites/favorites.component.ts");




const routes = [
    {
        path: '',
        component: _favorites_component__WEBPACK_IMPORTED_MODULE_3__["FavoritesComponent"]
    }
];
let FavoritesRoutingModule = class FavoritesRoutingModule {
};
FavoritesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], FavoritesRoutingModule);



/***/ }),

/***/ "./src/app/favorites/favorites.component.scss":
/*!****************************************************!*\
  !*** ./src/app/favorites/favorites.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zhdm9yaXRlcy9mYXZvcml0ZXMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/favorites/favorites.component.ts":
/*!**************************************************!*\
  !*** ./src/app/favorites/favorites.component.ts ***!
  \**************************************************/
/*! exports provided: FavoritesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoritesComponent", function() { return FavoritesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @app/post/store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @app/post/store/post.state */ "./src/app/post/store/post.state.ts");




// NGXS



let FavoritesComponent = class FavoritesComponent {
    constructor(store) {
        this.store = store;
        this.options = [
            {
                id: '1',
                name: 'Today'
            },
            {
                id: '2',
                name: 'This Week'
            },
            {
                id: '3',
                name: 'This Month'
            },
            {
                id: '4',
                name: 'All'
            }
        ];
        this.getToday();
    }
    getToday() {
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__["GetAllFavoritesToday"]());
    }
    getWeek() {
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__["GetAllFavoritesWeek"]());
    }
    getMonth() {
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__["GetAllFavoritesMonth"]());
    }
    getAll() {
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__["GetAllFavorites"]());
    }
    getUrl(url) {
        return `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urlForImage}${url}`;
    }
    // openModal(id: number) {
    //   this.store.dispatch(new GetPost(id)).subscribe(() => {
    //     this.modalService.open(PostModalComponent);
    //   });
    // }
    onTabChange(event) {
        switch (event.nextId) {
            case '1':
                this.getToday();
                break;
            case '2':
                this.getWeek();
                break;
            case '3':
                this.getMonth();
                break;
            case '4':
                this.getAll();
                break;
            default:
                console.log('none');
        }
    }
};
FavoritesComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Select"])(_app_post_store_post_state__WEBPACK_IMPORTED_MODULE_6__["PostState"].filteredPosts),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
], FavoritesComponent.prototype, "posts$", void 0);
FavoritesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-favorites',
        template: __webpack_require__(/*! raw-loader!./favorites.component.html */ "./node_modules/raw-loader/index.js!./src/app/favorites/favorites.component.html"),
        styles: [__webpack_require__(/*! ./favorites.component.scss */ "./src/app/favorites/favorites.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"]])
], FavoritesComponent);



/***/ }),

/***/ "./src/app/favorites/favorites.module.ts":
/*!***********************************************!*\
  !*** ./src/app/favorites/favorites.module.ts ***!
  \***********************************************/
/*! exports provided: FavoritesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoritesModule", function() { return FavoritesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _favorites_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./favorites.component */ "./src/app/favorites/favorites.component.ts");
/* harmony import */ var _app_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _favorites_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./favorites-routing.module */ "./src/app/favorites/favorites-routing.module.ts");





let FavoritesModule = class FavoritesModule {
};
FavoritesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_favorites_component__WEBPACK_IMPORTED_MODULE_2__["FavoritesComponent"]],
        imports: [_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _favorites_routing_module__WEBPACK_IMPORTED_MODULE_4__["FavoritesRoutingModule"]]
    })
], FavoritesModule);



/***/ })

}]);
//# sourceMappingURL=favorites-favorites-module-es2015.js.map
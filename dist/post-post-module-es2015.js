(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["post-post-module"],{

/***/ "./node_modules/@fortawesome/free-brands-svg-icons/faFacebookSquare.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/@fortawesome/free-brands-svg-icons/faFacebookSquare.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fab';
var iconName = 'facebook-square';
var width = 448;
var height = 512;
var ligatures = [];
var unicode = 'f082';
var svgPathData = 'M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z';

exports.definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

exports.faFacebookSquare = exports.definition;
exports.prefix = prefix;
exports.iconName = iconName;
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;

/***/ }),

/***/ "./node_modules/@fortawesome/free-brands-svg-icons/faPinterest.js":
/*!************************************************************************!*\
  !*** ./node_modules/@fortawesome/free-brands-svg-icons/faPinterest.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fab';
var iconName = 'pinterest';
var width = 496;
var height = 512;
var ligatures = [];
var unicode = 'f0d2';
var svgPathData = 'M496 256c0 137-111 248-248 248-25.6 0-50.2-3.9-73.4-11.1 10.1-16.5 25.2-43.5 30.8-65 3-11.6 15.4-59 15.4-59 8.1 15.4 31.7 28.5 56.8 28.5 74.8 0 128.7-68.8 128.7-154.3 0-81.9-66.9-143.2-152.9-143.2-107 0-163.9 71.8-163.9 150.1 0 36.4 19.4 81.7 50.3 96.1 4.7 2.2 7.2 1.2 8.3-3.3.8-3.4 5-20.3 6.9-28.1.6-2.5.3-4.7-1.7-7.1-10.1-12.5-18.3-35.3-18.3-56.6 0-54.7 41.4-107.6 112-107.6 60.9 0 103.6 41.5 103.6 100.9 0 67.1-33.9 113.6-78 113.6-24.3 0-42.6-20.1-36.7-44.8 7-29.5 20.5-61.3 20.5-82.6 0-19-10.2-34.9-31.4-34.9-24.9 0-44.9 25.7-44.9 60.2 0 22 7.4 36.8 7.4 36.8s-24.5 103.8-29 123.2c-5 21.4-3 51.6-.9 71.2C65.4 450.9 0 361.1 0 256 0 119 111 8 248 8s248 111 248 248z';

exports.definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

exports.faPinterest = exports.definition;
exports.prefix = prefix;
exports.iconName = iconName;
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;

/***/ }),

/***/ "./node_modules/@fortawesome/free-brands-svg-icons/faTwitterSquare.js":
/*!****************************************************************************!*\
  !*** ./node_modules/@fortawesome/free-brands-svg-icons/faTwitterSquare.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fab';
var iconName = 'twitter-square';
var width = 448;
var height = 512;
var ligatures = [];
var unicode = 'f081';
var svgPathData = 'M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-48.9 158.8c.2 2.8.2 5.7.2 8.5 0 86.7-66 186.6-186.6 186.6-37.2 0-71.7-10.8-100.7-29.4 5.3.6 10.4.8 15.8.8 30.7 0 58.9-10.4 81.4-28-28.8-.6-53-19.5-61.3-45.5 10.1 1.5 19.2 1.5 29.6-1.2-30-6.1-52.5-32.5-52.5-64.4v-.8c8.7 4.9 18.9 7.9 29.6 8.3a65.447 65.447 0 0 1-29.2-54.6c0-12.2 3.2-23.4 8.9-33.1 32.3 39.8 80.8 65.8 135.2 68.6-9.3-44.5 24-80.6 64-80.6 18.9 0 35.9 7.9 47.9 20.7 14.8-2.8 29-8.3 41.6-15.8-4.9 15.2-15.2 28-28.8 36.1 13.2-1.4 26-5.1 37.8-10.2-8.9 13.1-20.1 24.7-32.9 34z';

exports.definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

exports.faTwitterSquare = exports.definition;
exports.prefix = prefix;
exports.iconName = iconName;
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/post/post-create/post-create.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/post/post-create/post-create.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"post-create\">\r\n  <button class=\"back\" (click)=\"back()\">\r\n    <fa-icon [icon]=\"faArrowAltCircleLeft\"></fa-icon>\r\n  </button>\r\n  <div class=\"title\">Create Post</div>\r\n  <form novalidate=\"\" [formGroup]=\"form\" (ngSubmit)=\"onAddPost(form)\">\r\n    <div class=\"grid\">\r\n      <div class=\"twelve fields\">\r\n        <textarea formControlName=\"caption\" class=\"caption\" rows=\"1\" placeholder=\"Caption\" #caption></textarea>\r\n        <select formControlName=\"board_id\" class=\"caption\" placeholder=\"Select a Board\">\r\n          <option></option>\r\n          <option *ngFor=\"let board of boards$ | async\" [value]=\"board.id\">\r\n            <p *ngIf=\"userId == board.owned_by.id\">{{ board.name }}</p>\r\n          </option>\r\n          <!-- <div *ngFor=\"let board of boards$ | async\">\r\n            <option [value]=\"board.id\">{{ board.name }}</option>\r\n          </div> -->\r\n        </select>\r\n\r\n        <div\r\n          class=\"alert danger\"\r\n          *ngIf=\"\r\n            (!form.get('caption').valid && form.get('caption').touched) ||\r\n            (form.get('caption').untouched && formSubmitAttempt)\r\n          \"\r\n        >\r\n          This field is required!\r\n        </div>\r\n\r\n        <!-- <div\r\n                class=\"alert danger\"\r\n                *ngIf=\"\r\n                  (!form.get('image').valid && form.get('image').touched) ||\r\n                  (form.get('image').untouched && formSubmitAttempt)\r\n                \"\r\n              >\r\n                This field is required!\r\n              </div> -->\r\n        <button type=\"submit\" class=\"create-post button\" [disabled]=\"!getSubmitButton()\">\r\n          Post it!\r\n        </button>\r\n\r\n        <button style=\"margin-top:200px;\" class=\"button\" [routerLink]=\"['/boards']\">\r\n          Create Board\r\n        </button>\r\n      </div>\r\n      <div class=\"twelve draggable-input-file\">\r\n        <input type=\"file\" (change)=\"onSelectFile($event)\" formControlName=\"image\" accept=\"image/*, video/*\" />\r\n        <div class=\"image-container\">\r\n          <ng-container *ngIf=\"url$ | async as url\">\r\n            <img *ngIf=\"selectedFile && (format$ | async) === 'image'\" [src]=\"url\" />\r\n            <video *ngIf=\"selectedFile && (format$ | async) === 'video'\" [src]=\"url\" controls></video>\r\n          </ng-container>\r\n          <button class=\"delete button\" type=\"button\" (click)=\"removeFile()\">\r\n            <fa-icon [icon]=\"faTrash\"></fa-icon>\r\n          </button>\r\n        </div>\r\n        <div class=\"content-box\" *ngIf=\"!selectedFile\">\r\n          <div class=\"content\">\r\n            <div class=\"text-and-icon\">\r\n              <fa-icon [icon]=\"faArrowAltCircleUp\"></fa-icon>\r\n              <!-- <i class=\"fa fa-arrow-circle-up\"></i> -->\r\n              <div class=\"text\">Drag and drop or click to upload</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/post/post-detail/post-detail.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/post/post-detail/post-detail.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-container *ngIf=\"post$ | async as post\">\r\n  <form [formGroup]=\"form\" (ngSubmit)=\"onAddComment(form)\" class=\"form\">\r\n    <div class=\"post-modal\">\r\n      <div class=\"avatar\" *ngIf=\"post.type !== 'video'\">\r\n        <img [src]=\"getUrl(post.image)\" ngbAutofocus />\r\n      </div>\r\n      <div class=\"video\" *ngIf=\"post.type === 'video'\">\r\n        <video controls ngbAutofocus>\r\n          <source [src]=\"getUrl(post.image)\" />\r\n        </video>\r\n      </div>\r\n      <div class=\"body\">\r\n        <div style=\"padding-top:10px;\" (click)=\"gotoProfile(post.post_by.id)\">\r\n          <img\r\n            [src]=\"post.post_by.image !== null ? decode(post.post_by.image) : 'assets/images/no-image.png'\"\r\n            alt=\"\"\r\n            class=\"comment-box-avatar\"\r\n          />\r\n          &nbsp;{{ post.post_by.fname }}\r\n        </div>\r\n        <div></div>\r\n        <div class=\"caption\">{{ post.caption }}</div>\r\n        <div class=\"options\">\r\n          <button type=\"button\" class=\"big like\" (click)=\"onLikePost(post)\">\r\n            {{ post.likes.length }}\r\n            <i\r\n              class=\"fa fa-heart option\"\r\n              [ngStyle]=\"{\r\n                'color': findIdEqualUserId(post.likes) ? '#f36621' : null\r\n              }\"\r\n            ></i>\r\n          </button>\r\n\r\n          <!-- <div *ngIf=\"userWallet != null\" class=\"tip-icon\" aria-label=\"My icon\" [inlineSVG]=\"'assets/svg/tip.svg'\"\r\n            (click)=\"showQrCode(qrContent)\">\r\n          </div> -->\r\n          <div\r\n            *ngIf=\"userWallet != null\"\r\n            class=\"tip-icon\"\r\n            aria-label=\"My icon\"\r\n            [inlineSVG]=\"'assets/svg/tip.svg'\"\r\n            (click)=\"openTipModal(userWallet)\"\r\n          ></div>\r\n          <!-- <div class=\"share-icon\" aria-label=\"My icon\" [inlineSVG]=\"'assets/svg/share.svg'\"></div> -->\r\n          <button type=\"button\" class=\"delete\" *ngIf=\"userId === post.user_id\" (click)=\"onDeletePost(post)\">\r\n            Delete\r\n          </button>\r\n          <button style=\"float:left\" type=\"button\" class=\"delete\">\r\n            Pin Post\r\n          </button>\r\n        </div>\r\n        <div class=\"comment-text\">Comments</div>\r\n        <div class=\"comments\">\r\n          <div class=\"single-comment\" *ngFor=\"let comment of post.comments; let i = index\">\r\n            <div>\r\n              <img\r\n                (click)=\"gotoProfile(comment.user.id)\"\r\n                [src]=\"getImage(comment.user.image)\"\r\n                alt=\"\"\r\n                class=\"comment-avatar\"\r\n              />\r\n            </div>\r\n            <div class=\"comment-content\">\r\n              <div class=\"comment-body\">\r\n                <div class=\"name\" *ngIf=\"comment.user.social_account === null\" (click)=\"gotoProfile(comment.user.id)\">\r\n                  {{ comment.user.fname }} {{ comment.user.lname }}\r\n                  <span class=\"date\">{{ comment.created_at | dateAgo }}</span>\r\n                </div>\r\n                <div class=\"name\" *ngIf=\"comment.user.social_account !== null\">\r\n                  {{ comment.user.fname }}\r\n                  <span class=\"date\">{{ comment.created_at | dateAgo }}</span>\r\n                </div>\r\n                {{ comment.comment }}\r\n              </div>\r\n              <div class=\"comment-options\">\r\n                <button type=\"button\" class=\"like\" (click)=\"onLikeComment(comment)\">\r\n                  {{ comment.likes.length }}\r\n                  <i\r\n                    class=\"fa fa-heart\"\r\n                    [ngStyle]=\"{\r\n                      'color': findIdEqualUserId(comment.likes) ? '#f36621' : null\r\n                    }\"\r\n                  ></i>\r\n                </button>\r\n                <!-- <i class=\"fa fa-comment comment\"></i> -->\r\n                <button\r\n                  type=\"button\"\r\n                  class=\"delete\"\r\n                  *ngIf=\"userId === comment.user_id\"\r\n                  (click)=\"onDeleteComment(comment)\"\r\n                >\r\n                  Delete\r\n                </button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"comment-box\">\r\n            <img\r\n              [src]=\"profilePhoto ? getImage(profilePhoto) : 'assets/images/no-image.png'\"\r\n              alt=\"\"\r\n              class=\"comment-box-avatar\"\r\n            />\r\n            <input type=\"text\" class=\"comment-box-input\" placeholder=\"Add a comment\" formControlName=\"comment\" />\r\n            <button type=\"submit\" class=\"comment-box-button\" [disabled]=\"form.invalid\">\r\n              Submit\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </form>\r\n\r\n  <!-- Address QR Code Modal -->\r\n  <ng-template #qrContent let-modal>\r\n    <div class=\"modal-body\">\r\n      <div class=\"qrCanvass\">\r\n        <div class=\"qrPanel\">\r\n          <p class=\"modalTitle\">Scan Wallet Address</p>\r\n          <div class=\"input-group\">\r\n            <input type=\"text\" class=\"form-control\" value=\"{{ userWallet }}\" readonly />\r\n          </div>\r\n          <ngx-qrcode [(qrc-value)]=\"userWallet\"></ngx-qrcode>\r\n          <p class=\"modalFooter\">Open your ProximaX Mobile Wallet to proceed</p>\r\n          <div class=\"modalImage\"><img src=\"../../../assets/images/wallet_icon.png\" alt=\"wallet\" /></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n      <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"modal.close('Save click')\">Close</button>\r\n    </div>\r\n  </ng-template>\r\n</ng-container>\r\n"

/***/ }),

/***/ "./src/app/post/post-create/post-create.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/post/post-create/post-create.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Bvc3QvcG9zdC1jcmVhdGUvcG9zdC1jcmVhdGUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/post/post-create/post-create.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/post/post-create/post-create.component.ts ***!
  \***********************************************************/
/*! exports provided: PostCreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostCreateComponent", function() { return PostCreateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _store_post_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");
/* harmony import */ var _store_post_state__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../store/post.state */ "./src/app/post/store/post.state.ts");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");





// NGXS





// Font Awesome

let PostCreateComponent = class PostCreateComponent {
    constructor(formBuilder, store) {
        this.formBuilder = formBuilder;
        this.store = store;
        this.userId = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_4__["AuthState"].getUserId);
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        // Font Awesome
        this.faArrowAltCircleUp = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faArrowAltCircleUp"];
        this.faArrowAltCircleLeft = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faArrowAltCircleLeft"];
        this.faTrash = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faTrash"];
        this.form = this.formBuilder.group({
            caption: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            board_id: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            image: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.form.valueChanges.subscribe(() => {
            this.caption.nativeElement.style.height = 'auto';
            this.caption.nativeElement.style.height = `${this.caption.nativeElement.scrollHeight}px`;
        });
        this.store.dispatch(new _store_post_action__WEBPACK_IMPORTED_MODULE_6__["GetAllBoards"]());
        this.userId;
    }
    onSelectFile(event) {
        if (this.selectedFile === null || this.selectedFile === undefined) {
            this.selectedFile = event.target.files[0];
            this.store.dispatch(new _store_post_action__WEBPACK_IMPORTED_MODULE_6__["PreviewImageVideo"](event, this.url));
        }
    }
    removeFile() {
        this.selectedFile = null;
        this.form.get('image').setValue(null);
    }
    setSubmitButton(flag) {
        this.submitButtonStatus = flag;
    }
    getSubmitButton() {
        return this.submitButtonStatus;
    }
    back() {
        this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/home']));
    }
    onAddPost(form) {
        console.log(form.value);
        console.log(this.selectedFile);
        const formData = new FormData();
        formData.append('caption', form.value.caption);
        formData.append('image', this.selectedFile);
        formData.append('board_id', form.value.board_id);
        console.log(formData);
        this.formSubmitAttempt = true;
        if (form.valid) {
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            this.store.dispatch(new _store_post_action__WEBPACK_IMPORTED_MODULE_6__["AddPost"](formData)).subscribe(() => {
                this.setSubmitButton(true);
                this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/home']));
            }, err => {
                console.log(err);
                this.setSubmitButton(true);
            });
        }
    }
};
PostCreateComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('caption', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], PostCreateComponent.prototype, "caption", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Select"])(_store_post_state__WEBPACK_IMPORTED_MODULE_8__["PostState"].getFileUrl),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
], PostCreateComponent.prototype, "url$", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Select"])(_store_post_state__WEBPACK_IMPORTED_MODULE_8__["PostState"].getFormat),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
], PostCreateComponent.prototype, "format$", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Select"])(_store_post_state__WEBPACK_IMPORTED_MODULE_8__["PostState"].getAllBoards),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
], PostCreateComponent.prototype, "boards$", void 0);
PostCreateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-post-create',
        template: __webpack_require__(/*! raw-loader!./post-create.component.html */ "./node_modules/raw-loader/index.js!./src/app/post/post-create/post-create.component.html"),
        styles: [__webpack_require__(/*! ./post-create.component.scss */ "./src/app/post/post-create/post-create.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
], PostCreateComponent);



/***/ }),

/***/ "./src/app/post/post-detail/post-detail.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/post/post-detail/post-detail.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form {\n  padding: 2.5rem 20vw 8rem 20vw;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcG9zdC9wb3N0LWRldGFpbC9DOlxcVXNlcnNcXEFTVVNcXERlc2t0b3BcXFByb2plY3RcXGltYWdlcmF4LXNpcml1cy1mcm9udGVuZFxcc2lyaXVzLWxpdmV3YWxsLS0tZnJvbnRlbmQvc3JjXFxhcHBcXHBvc3RcXHBvc3QtZGV0YWlsXFxwb3N0LWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcG9zdC9wb3N0LWRldGFpbC9wb3N0LWRldGFpbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDhCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9wb3N0L3Bvc3QtZGV0YWlsL3Bvc3QtZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcm0ge1xyXG4gIHBhZGRpbmc6IDIuNXJlbSAyMHZ3IDhyZW0gMjB2dztcclxufVxyXG4iLCIuZm9ybSB7XG4gIHBhZGRpbmc6IDIuNXJlbSAyMHZ3IDhyZW0gMjB2dztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/post/post-detail/post-detail.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/post/post-detail/post-detail.component.ts ***!
  \***********************************************************/
/*! exports provided: PostDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostDetailComponent", function() { return PostDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_shared_reusable_functions_sweet_alerts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @app/shared/reusable-functions/sweet-alerts */ "./src/app/shared/reusable-functions/sweet-alerts.ts");
/* harmony import */ var _app_shared_modals_tip_modal_tip_modal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @app/shared/modals/tip-modal/tip-modal.component */ "./src/app/shared/modals/tip-modal/tip-modal.component.ts");
/* harmony import */ var _app_shared_modals_qr_modal_qr_modal_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app/shared/modals/qr-modal/qr-modal.component */ "./src/app/shared/modals/qr-modal/qr-modal.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app/post/store/post.state */ "./src/app/post/store/post.state.ts");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var _app_shared_store_comment_action__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app/shared/store/comment.action */ "./src/app/shared/store/comment.action.ts");
/* harmony import */ var _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @app/post/store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _fortawesome_free_brands_svg_icons_faFacebookSquare__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons/faFacebookSquare */ "./node_modules/@fortawesome/free-brands-svg-icons/faFacebookSquare.js");
/* harmony import */ var _fortawesome_free_brands_svg_icons_faFacebookSquare__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_brands_svg_icons_faFacebookSquare__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _fortawesome_free_brands_svg_icons_faTwitterSquare__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons/faTwitterSquare */ "./node_modules/@fortawesome/free-brands-svg-icons/faTwitterSquare.js");
/* harmony import */ var _fortawesome_free_brands_svg_icons_faTwitterSquare__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_brands_svg_icons_faTwitterSquare__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _fortawesome_free_brands_svg_icons_faPinterest__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons/faPinterest */ "./node_modules/@fortawesome/free-brands-svg-icons/faPinterest.js");
/* harmony import */ var _fortawesome_free_brands_svg_icons_faPinterest__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_brands_svg_icons_faPinterest__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_20__);










// NGXS










// NG-Bootstrap Modal

// SweetAlert2

let PostDetailComponent = class PostDetailComponent {
    // userWallet = this.store.selectSnapshot(AuthState.getUserWalletAddress);
    constructor(fb, store, modalService, route, location) {
        this.fb = fb;
        this.store = store;
        this.modalService = modalService;
        this.route = route;
        this.location = location;
        this.fbIcon = _fortawesome_free_brands_svg_icons_faFacebookSquare__WEBPACK_IMPORTED_MODULE_15__["faFacebookSquare"];
        this.pinIcon = _fortawesome_free_brands_svg_icons_faPinterest__WEBPACK_IMPORTED_MODULE_17__["faPinterest"];
        this.tweetIcon = _fortawesome_free_brands_svg_icons_faTwitterSquare__WEBPACK_IMPORTED_MODULE_16__["faTwitterSquare"];
        this.ENDPOINT_URL = _env_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].urlForImage;
        this.userId = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_12__["AuthState"].getUserId);
        this.getPost();
        this.form = this.fb.group({
            post_id: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            comment: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.profilePhoto = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_12__["AuthState"].getProfilePhoto);
        // console.clear();
        // console.log('WALLET: ', this.userWallet);
    }
    gotoProfile(id) {
        this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_18__["Navigate"]([`profile/profile-view/${id}`]));
    }
    getPost() {
        this.route.params.subscribe(params => {
            this.id = params.id;
            this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_14__["GetPost"](params.id)).subscribe(val => {
                console.log(val);
                this.userWallet = val.post.post.post_by.wallet_address;
                console.clear();
                console.log('Owners Address: ', this.userWallet);
                this.form.get('post_id').setValue(params.id);
            });
        });
    }
    getUrl(url) {
        return `${_env_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].urlForImage}${url}`;
    }
    decode(image) {
        return `${this.ENDPOINT_URL}${decodeURI(image)}`;
    }
    onAddComment(form) {
        console.log(form.value);
        this.store.dispatch(new _app_shared_store_comment_action__WEBPACK_IMPORTED_MODULE_13__["AddComment"](form.value)).subscribe(() => {
            this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_14__["GetPost"](form.value.post_id)).subscribe(() => form.get('comment').reset());
        }, err => {
            console.log(err);
        });
    }
    onDeletePost(data) {
        Object(_app_shared_reusable_functions_sweet_alerts__WEBPACK_IMPORTED_MODULE_5__["confirmDelete"])(`${data.caption}`).then(result => {
            console.log(result);
            if (result.value) {
                this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_14__["DeletePost"](data)).subscribe(() => {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_20___default.a.fire('Success', 'Successfully Deleted', 'success').then(() => {
                        this.location.back();
                    });
                }, err => {
                    console.log(err);
                });
            }
        });
    }
    onDeleteComment(data) {
        console.log(data);
        Object(_app_shared_reusable_functions_sweet_alerts__WEBPACK_IMPORTED_MODULE_5__["confirmDelete"])(`${data.comment}`).then(result => {
            console.log(result);
            if (result.value) {
                this.store.dispatch(new _app_shared_store_comment_action__WEBPACK_IMPORTED_MODULE_13__["DeleteComment"](data)).subscribe(() => {
                    this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_14__["GetPost"](this.form.get('post_id').value)).subscribe(() => {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_20___default.a.fire('Success', 'Successfully Deleted', 'success');
                    });
                }, err => {
                    console.log(err);
                });
            }
        });
    }
    onLikeComment(data) {
        console.log(data);
        this.store.dispatch(new _app_shared_store_comment_action__WEBPACK_IMPORTED_MODULE_13__["LikeComment"](data)).subscribe(() => {
            this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_14__["GetPost"](this.form.get('post_id').value));
        }, err => {
            console.log(err);
        });
    }
    onLikePost(data) {
        console.log(data);
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_14__["LikePost"](data)).subscribe(() => {
            if (this.form.get('post_id').value !== null) {
                this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_14__["GetPost"](this.form.get('post_id').value));
            }
        }, err => {
            console.log(err);
        });
    }
    getImage(url) {
        return `${_env_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].urlForImage}${url}`;
    }
    findIdEqualUserId(likes) {
        const found = likes.some(el => el.user_id == this.userId);
        if (!found) {
            return false;
        }
        else {
            return true;
        }
    }
    openTipModal() {
        // this.modalService.open(TipModalComponent);
        const modal = this.modalService.open(_app_shared_modals_tip_modal_tip_modal_component__WEBPACK_IMPORTED_MODULE_6__["TipModalComponent"]);
        modal.componentInstance.userWallet = this.userWallet;
        modal.result.then(() => { });
    }
    openQrModal() {
        this.modalService.open(_app_shared_modals_qr_modal_qr_modal_component__WEBPACK_IMPORTED_MODULE_7__["QrModalComponent"]);
    }
    showQrCode(content) {
        this.modalService.open(content, { size: 'sm' }).result.then(result => {
            this.closeResult = `Closed with: ${result}`;
        }, reason => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    getDismissReason(reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return `with: ${reason}`;
        }
    }
};
PostDetailComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Store"] },
    { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__["NgbModal"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_9__["Location"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Select"])(_app_post_store_post_state__WEBPACK_IMPORTED_MODULE_11__["PostState"].getPost),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
], PostDetailComponent.prototype, "post$", void 0);
PostDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-post-detail',
        template: __webpack_require__(/*! raw-loader!./post-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/post/post-detail/post-detail.component.html"),
        styles: [__webpack_require__(/*! ./post-detail.component.scss */ "./src/app/post/post-detail/post-detail.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Store"],
        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_19__["NgbModal"],
        _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
        _angular_common__WEBPACK_IMPORTED_MODULE_9__["Location"]])
], PostDetailComponent);



/***/ }),

/***/ "./src/app/post/post-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/post/post-routing.module.ts ***!
  \*********************************************/
/*! exports provided: PostRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostRoutingModule", function() { return PostRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _post_create_post_create_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./post-create/post-create.component */ "./src/app/post/post-create/post-create.component.ts");
/* harmony import */ var _post_detail_post_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./post-detail/post-detail.component */ "./src/app/post/post-detail/post-detail.component.ts");





const routes = [
    {
        path: 'new',
        component: _post_create_post_create_component__WEBPACK_IMPORTED_MODULE_3__["PostCreateComponent"]
    },
    {
        path: ':id',
        component: _post_detail_post_detail_component__WEBPACK_IMPORTED_MODULE_4__["PostDetailComponent"]
    }
];
let PostRoutingModule = class PostRoutingModule {
};
PostRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PostRoutingModule);



/***/ }),

/***/ "./src/app/post/post.module.ts":
/*!*************************************!*\
  !*** ./src/app/post/post.module.ts ***!
  \*************************************/
/*! exports provided: PostModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostModule", function() { return PostModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _post_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./post-routing.module */ "./src/app/post/post-routing.module.ts");
/* harmony import */ var _post_create_post_create_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./post-create/post-create.component */ "./src/app/post/post-create/post-create.component.ts");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm2015/angular-fontawesome.js");
/* harmony import */ var _post_detail_post_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./post-detail/post-detail.component */ "./src/app/post/post-detail/post-detail.component.ts");







let PostModule = class PostModule {
};
PostModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_post_create_post_create_component__WEBPACK_IMPORTED_MODULE_4__["PostCreateComponent"], _post_detail_post_detail_component__WEBPACK_IMPORTED_MODULE_6__["PostDetailComponent"]],
        imports: [_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"], _post_routing_module__WEBPACK_IMPORTED_MODULE_3__["PostRoutingModule"], _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_5__["FontAwesomeModule"]]
    })
], PostModule);



/***/ }),

/***/ "./src/app/shared/reusable-functions/sweet-alerts.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/reusable-functions/sweet-alerts.ts ***!
  \***********************************************************/
/*! exports provided: confirmUpdate, confirmDelete, customConfirm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirmUpdate", function() { return confirmUpdate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirmDelete", function() { return confirmDelete; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customConfirm", function() { return customConfirm; });
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_0__);

// Confirm Update Function
function confirmUpdate() {
    return sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
        title: 'Update',
        text: 'Are you sure you want to update changes?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Confirm',
        cancelButtonText: 'Cancel',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
    });
}
// Confirm Delete Function
function confirmDelete(text) {
    return sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
        title: 'Delete',
        text: `Are you sure you want to delete ${text}?`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Confirm',
        cancelButtonText: 'Cancel',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
    });
}
// Confirm Function
function customConfirm(title, text) {
    return sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
        title: `${title}`,
        text: `Are you sure you want to ${title} ${text}?`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Confirm',
        cancelButtonText: 'Cancel',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
    });
}


/***/ })

}]);
//# sourceMappingURL=post-post-module-es2015.js.map
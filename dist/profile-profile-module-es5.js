(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/profile-edit/profile-edit.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/profile-edit/profile-edit.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"edit-profile card\" *ngIf=\"profile$ | async as profile\">\r\n  <div class=\"title\">Edit Profile</div>\r\n  <br />\r\n  <form novalidate=\"\" [formGroup]=\"form\" (ngSubmit)=\"onEditProfile(form)\">\r\n    <div class=\"grid center wrap\">\r\n      <div class=\"left\">\r\n        <img id=\"profile\" *ngIf=\"profile$\" class=\"image\" [src]=\"getUrl(profile.image)\" />\r\n        <label class=\"file\">\r\n          <input\r\n            formControlName=\"image\"\r\n            (change)=\"onSelectImage($event)\"\r\n            accept=\"image/*\"\r\n            type=\"file\"\r\n            onchange=\"document.getElementById('profile').src = window.URL.createObjectURL(this.files[0])\"\r\n          />\r\n          Change\r\n        </label>\r\n\r\n        <div\r\n          class=\"alert-danger\"\r\n          *ngIf=\"\r\n            (!form.get('image').valid && form.get('image').touched) ||\r\n            (form.get('image').untouched && formSubmitAttempt)\r\n          \"\r\n        >\r\n          This field is required!\r\n        </div>\r\n        <br />\r\n        <label class=\"label\">First Name</label>\r\n        <input type=\"text\" class=\"input\" placeholder=\"First Name\" formControlName=\"fname\" />\r\n        <div\r\n          class=\"alert-danger\"\r\n          *ngIf=\"\r\n            (!form.get('fname').valid && form.get('fname').touched) ||\r\n            (form.get('fname').untouched && formSubmitAttempt)\r\n          \"\r\n        >\r\n          This field is required!\r\n        </div>\r\n        <label class=\"label\">Email address</label>\r\n        <input type=\"text\" class=\"input\" placeholder=\"Email\" formControlName=\"email\" readonly />\r\n      </div>\r\n      <div class=\"right\">\r\n        <img id=\"cover_photo\" class=\"image cover\" [src]=\"getUrl(profile.cover_photo)\" />\r\n        <label class=\"file\">\r\n          <input\r\n            type=\"file\"\r\n            formControlName=\"cover_photo\"\r\n            (change)=\"onSelectCoverPhoto($event)\"\r\n            accept=\"image/*\"\r\n            onchange=\"document.getElementById('cover_photo').src = window.URL.createObjectURL(this.files[0])\"\r\n          />\r\n          Change\r\n        </label>\r\n\r\n        <div\r\n          class=\"alert-danger\"\r\n          *ngIf=\"\r\n            (!form.get('cover_photo').valid && form.get('cover_photo').touched) ||\r\n            (form.get('cover_photo').untouched && formSubmitAttempt)\r\n          \"\r\n        >\r\n          This field is required!\r\n        </div>\r\n        <br />\r\n        <label class=\"label\">Last Name</label>\r\n        <input type=\"text\" class=\"input\" placeholder=\"Last Name\" formControlName=\"lname\" />\r\n        <div\r\n          class=\"alert-danger\"\r\n          *ngIf=\"\r\n            (!form.get('lname').valid && form.get('lname').touched) ||\r\n            (form.get('lname').untouched && formSubmitAttempt)\r\n          \"\r\n        >\r\n          This field is required!\r\n        </div>\r\n        <label class=\"label\">Country</label>\r\n        <ng-select\r\n          placeholder=\"-- Select Country --\"\r\n          class=\"input custom\"\r\n          [items]=\"countries$ | async\"\r\n          bindLabel=\"country_name\"\r\n          bindValue=\"id\"\r\n          formControlName=\"country_id\"\r\n        ></ng-select>\r\n        <div\r\n          class=\"alert-danger\"\r\n          *ngIf=\"\r\n            (!form.get('country_id').valid && form.get('country_id').touched) ||\r\n            (form.get('country_id').untouched && formSubmitAttempt)\r\n          \"\r\n        >\r\n          This field is required!\r\n        </div>\r\n      </div>\r\n      <div class=\"about-you\">\r\n        <label class=\"label\">About you</label>\r\n        <textarea rows=\"4\" class=\"input\" formControlName=\"description\"></textarea>\r\n        <div\r\n          class=\"alert-danger\"\r\n          *ngIf=\"\r\n            (!form.get('description').valid && form.get('description').touched) ||\r\n            (form.get('description').untouched && formSubmitAttempt)\r\n          \"\r\n        >\r\n          This field is required!\r\n        </div>\r\n      </div>\r\n      <button type=\"submit\" class=\"login-btn button\" [disabled]=\"!getSubmitButton()\">\r\n        Update\r\n      </button>\r\n      &nbsp;&nbsp;\r\n      <button class=\"login-btn button\" (click)=\"openExportPrivateKey()\">\r\n        Export Private Key\r\n      </button>\r\n      &nbsp;&nbsp;\r\n      <button class=\"login-btn button\" (click)=\"openSiriusIdModal()\" *ngIf=\"!profile.session_token\">\r\n        Link Account with Sirius ID\r\n      </button>\r\n      &nbsp;&nbsp;\r\n      <button class=\"login-btn button\" (click)=\"setAccountAsPrivate()\" *ngIf=\"profile.is_private != 1\">\r\n        Set Account as Private\r\n      </button>\r\n      <button class=\"login-btn button\" (click)=\"setAccountAsPublic()\" *ngIf=\"profile.is_private == 1\">\r\n        Set Account to Public\r\n      </button>\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/profile-user-view/profile-user-view.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/profile-user-view/profile-user-view.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"profile\" *ngIf=\"profile$ | async as profile\">\r\n  <div class=\"cover\">\r\n    <!-- <img [src]=\"profile.ima ? getUrl(profile.cover_photo) : 'assets/images/no-image.png'\" alt=\"\" /> -->\r\n    <img [src]=\"profile.image !== null ? decode(profile.image) : 'assets/images/no-image.png'\" />\r\n    <div class=\"photo\">\r\n      <!-- <img [src]=\"profile.image ? getUrl(profile.image) : 'assets/images/no-image.png'\" alt=\"\" /> -->\r\n      <img [src]=\"profile.image !== null ? decode(profile.image) : 'assets/images/no-image.png'\" alt=\" \" />\r\n    </div>\r\n    <div class=\"name\" *ngIf=\"provider == null\">\r\n      {{ profile.fname }} {{ profile.lname }} &nbsp;\r\n      <button [routerLink]=\"['edit']\" class=\"edit-button\" *ngIf=\"profile.is_private == 0\">\r\n        <fa-icon [icon]=\"faFollow\"></fa-icon>\r\n      </button>\r\n      <button (click)=\"followUserPrivate(profile.id)\" class=\"edit-button\" *ngIf=\"profile.is_private == 1\">\r\n        <fa-icon [icon]=\"faFollow2\"></fa-icon>\r\n      </button>\r\n      <div class=\"stats\">{{ profile.email }}</div>\r\n    </div>\r\n    <div class=\"name\" *ngIf=\"provider !== null\">\r\n      {{ profile.fname }}\r\n      <div class=\"stats\">{{ profile.email }}</div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"description\">\r\n    {{ profile.description }}\r\n  </div>\r\n  <div class=\"place\">\r\n    {{ profile?.country?.country_name }}\r\n  </div>\r\n\r\n  <!-- <div class=\"tabs\">\r\n    <button class=\"tab\" type=\"submit\" (click)=\"gotoFollowers(profile.id)\">Followers</button>\r\n    <button class=\"tab\" type=\"submit\" (click)=\"gotoFollowings(profile.id)\">Following</button> \r\n  </div> -->\r\n\r\n  <!-- <ngb-tabset type=\"pills\" (tabChange)=\"onTabChange($event)\">\r\n    <ng-container *ngFor=\"let option of options\">\r\n      <ngb-tab id=\"{{ option.id }}\">\r\n        <ng-template ngbTabTitle>\r\n          <div class=\"link\">{{ option.name }}</div>\r\n        </ng-template>\r\n        <ng-template ngbTabContent>\r\n          <div class=\"card\">\r\n            <div *ngFor=\"let follower of profiles$ | async\">\r\n              <div class=\"notif-link\">\r\n                <small class=\"date\"></small>\r\n                <img\r\n                  height=\"30px\"\r\n                  width=\"30px\"\r\n                  style=\"border-radius: 10rem\"\r\n                  class=\"mr-4\"\r\n                  [src]=\"decode(follower.image)\"\r\n                  alt=\"\"\r\n                />\r\n                {{ follower.fname }} {{ follower.lname }}\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </ng-template>\r\n      </ngb-tab>\r\n    </ng-container>\r\n  </ngb-tabset> -->\r\n\r\n  <!-- <div class=\"place\">Manila, Philippines</div> -->\r\n  <!-- <div class=\"navigation\">\r\n      <div class=\"link\">Top Posts</div>\r\n      <div class=\"link\">Activity</div>\r\n      <div class=\"link\">Gallery</div>\r\n      <div class=\"link\">Followers</div>\r\n      <div class=\"link\">Following</div>\r\n    </div> -->\r\n\r\n  <!-- <div class=\"posts\">\r\n      <div class=\"post\" *ngFor=\"let post of posts$ | async\">\r\n        <img *ngIf=\"post.type !== 'video'\" [src]=\"getUrl(post.image)\" class=\"avatar\" [routerLink]=\"['/post/', post.id]\" />\r\n        <div class=\"video-overlay\" *ngIf=\"post.type === 'video'\" [routerLink]=\"['/post/', post.id]\">\r\n          <video>\r\n            <source [src]=\"getUrl(post.image)\" />\r\n          </video>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n\r\n  <!-- <div class=\"posts\">\r\n      <div class=\"post\">\r\n        <div class=\"stats\">\r\n          <div class=\"icon\">\r\n            <i class=\"fa fa-star\"></i>\r\n            5648\r\n          </div>\r\n          <div class=\"description\">I got defeated :(</div>\r\n        </div>\r\n        <img src=\"assets/images/cover.jpg\" alt=\"\" />\r\n      </div>\r\n    </div> -->\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/profile-view/profile-view.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/profile-view/profile-view.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"profile\" *ngIf=\"profile$ | async as profile\">\r\n  <div class=\"cover\">\r\n    <img [src]=\"profile.cover_photo ? getUrl(profile.cover_photo) : 'assets/images/no-image.png'\" alt=\"\" />\r\n    <div class=\"photo\">\r\n      <img [src]=\"profile.image ? getUrl(profile.image) : 'assets/images/no-image.png'\" alt=\"\" />\r\n    </div>\r\n    <div class=\"name\" *ngIf=\"provider == null\">\r\n      {{ profile.fname }} {{ profile.lname }}\r\n      <button [routerLink]=\"['edit']\" class=\"edit-button\">\r\n        <fa-icon [icon]=\"faPen\"></fa-icon>\r\n      </button>\r\n      <div class=\"stats\">{{ profile.email }}</div>\r\n    </div>\r\n    <div class=\"name\" *ngIf=\"provider !== null\">\r\n      {{ profile.fname }}\r\n      <button [routerLink]=\"['edit']\" class=\"edit-button\">\r\n        <fa-icon [icon]=\"faPen\"></fa-icon>\r\n      </button>\r\n      <div class=\"stats\">{{ profile.email }}</div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"description\">\r\n    {{ profile.description }}\r\n  </div>\r\n  <div class=\"place\">\r\n    {{ profile?.country?.country_name }}\r\n  </div>\r\n  <!-- <ngb-tabset type=\"pills\">\r\n    <ng-container *ngFor=\"let option of options\">\r\n      <ngb-tab id=\"{{ option.id }}\">\r\n        <ng-template ngbTabTitle>\r\n          <div class=\"link\">{{ option.name }}</div>\r\n        </ng-template>\r\n        <ng-template ngbTabContent>\r\n          <div class=\"layout\">\r\n            <div class=\"item\" *ngFor=\"let post of posts$ | async\">\r\n              <img\r\n                *ngIf=\"post.type !== 'video'\"\r\n                [src]=\"getUrl(post.image)\"\r\n                class=\"avatar\"\r\n                [routerLink]=\"['/post/', post.id]\"\r\n              />\r\n              <div class=\"video-overlay\" *ngIf=\"post.type === 'video'\" [routerLink]=\"['/post/', post.id]\">\r\n                <video>\r\n                  <source [src]=\"getUrl(post.image)\" />\r\n                </video>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </ng-template>\r\n      </ngb-tab>\r\n    </ng-container>\r\n  </ngb-tabset> -->\r\n\r\n  <!-- <div class=\"place\">Manila, Philippines</div> -->\r\n  <!-- <div class=\"navigation\">\r\n    <div class=\"link\">Top Posts</div>\r\n    <div class=\"link\">Activity</div>\r\n    <div class=\"link\">Gallery</div>\r\n    <div class=\"link\">Followers</div>\r\n    <div class=\"link\">Following</div>\r\n  </div> -->\r\n\r\n  <div class=\"posts\">\r\n    <div class=\"post\" *ngFor=\"let post of posts$ | async\">\r\n      <img *ngIf=\"post.type !== 'video'\" [src]=\"getUrl(post.image)\" class=\"avatar\" [routerLink]=\"['/post/', post.id]\" />\r\n      <div class=\"video-overlay\" *ngIf=\"post.type === 'video'\" [routerLink]=\"['/post/', post.id]\">\r\n        <video>\r\n          <source [src]=\"getUrl(post.image)\" />\r\n        </video>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <!-- <div class=\"posts\">\r\n    <div class=\"post\">\r\n      <div class=\"stats\">\r\n        <div class=\"icon\">\r\n          <i class=\"fa fa-star\"></i>\r\n          5648\r\n        </div>\r\n        <div class=\"description\">I got defeated :(</div>\r\n      </div>\r\n      <img src=\"assets/images/cover.jpg\" alt=\"\" />\r\n    </div>\r\n  </div> -->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/core/guards/edit-profile-guard.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/core/guards/edit-profile-guard.service.ts ***!
  \***********************************************************/
/*! exports provided: EditProfileGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfileGuard", function() { return EditProfileGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm5/ngxs-store.js");
/* harmony import */ var _auth_store_auth_state__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");



// NGXS


var EditProfileGuard = /** @class */ (function () {
    function EditProfileGuard(store, router) {
        this.store = store;
        this.router = router;
    }
    EditProfileGuard.prototype.canActivate = function () {
        var provider = this.store.selectSnapshot(_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_4__["AuthState"].getProvider);
        return true;
        // if (provider !== null || provider == null) {
        //   this.router.navigate(['/profile']);
        //   return false;
        // } else {
        //   return true;
        // }
    };
    EditProfileGuard.ctorParameters = function () { return [
        { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Store"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    EditProfileGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Store"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], EditProfileGuard);
    return EditProfileGuard;
}());



/***/ }),

/***/ "./src/app/profile/profile-edit/profile-edit.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/profile/profile-edit/profile-edit.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS1lZGl0L3Byb2ZpbGUtZWRpdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/profile/profile-edit/profile-edit.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/profile/profile-edit/profile-edit.component.ts ***!
  \****************************************************************/
/*! exports provided: ProfileEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileEditComponent", function() { return ProfileEditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_shared_reusable_custom_validators_set_as_touched_validator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/shared/reusable-custom-validators/set-as-touched.validator */ "./src/app/shared/reusable-custom-validators/set-as-touched.validator.ts");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm5/ngxs-store.js");
/* harmony import */ var _store_profile_state__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store/profile.state */ "./src/app/profile/store/profile.state.ts");
/* harmony import */ var _store_profile_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @app/post/store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app/post/store/post.state */ "./src/app/post/store/post.state.ts");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm5/ngxs-router-plugin.js");
/* harmony import */ var _app_shared_store_country_action__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app/shared/store/country.action */ "./src/app/shared/store/country.action.ts");
/* harmony import */ var _app_shared_store_country_state__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app/shared/store/country.state */ "./src/app/shared/store/country.state.ts");
/* harmony import */ var _app_shared_modals_sirius_id_modal_sirius_id_modal_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @app/shared/modals/sirius-id-modal/sirius-id-modal.component */ "./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.ts");
/* harmony import */ var _app_shared_modals_privatekey_export_privatekey_export_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @app/shared/modals/privatekey-export/privatekey-export.component */ "./src/app/shared/modals/privatekey-export/privatekey-export.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_18__);






// NGXS













//Sirius ID Sdk
var ProfileEditComponent = /** @class */ (function () {
    function ProfileEditComponent(store, formBuilder, modalService) {
        var _this = this;
        this.store = store;
        this.formBuilder = formBuilder;
        this.modalService = modalService;
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        this.currentNode = "" + _env_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].nodeUrl;
        this.session_token = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_17__["AuthState"].getUserSessionToken);
        this.form = this.formBuilder.group({
            fname: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            lname: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            country_id: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            description: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            image: [null],
            cover_photo: [null]
        });
        this.store.dispatch(new _app_shared_store_country_action__WEBPACK_IMPORTED_MODULE_12__["GetAllCountries"]());
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_8__["GetProfile"]()).subscribe(function (val) {
            console.log(val);
            Object(_app_shared_reusable_custom_validators_set_as_touched_validator__WEBPACK_IMPORTED_MODULE_4__["setAsTouched"])(_this.form);
            // this.form.patchValue(val.profile.profile);
            _this.form.get('fname').setValue(val.categories.profile.fname);
            _this.form.get('lname').setValue(val.categories.profile.lname);
            _this.form.get('email').setValue(val.categories.profile.email);
            _this.form.get('country_id').setValue(val.categories.profile.country_id);
            _this.form.get('description').setValue(val.categories.profile.description);
        });
        this.session_token = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_17__["AuthState"].getUserSessionToken);
    }
    ProfileEditComponent.prototype.getUrl = function (url) {
        return "" + _env_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].urlForImage + url;
    };
    ProfileEditComponent.prototype.onSelectImage = function (event) {
        console.log(event.target);
        if (this.selectedImage === null || this.selectedImage === undefined) {
            this.selectedImage = event.target.files[0];
            this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_9__["PreviewImageVideo"](event, this.url));
        }
    };
    ProfileEditComponent.prototype.openSiriusIdModal = function () {
        var modal = this.modalService.open(_app_shared_modals_sirius_id_modal_sirius_id_modal_component__WEBPACK_IMPORTED_MODULE_14__["SiriusIdModalComponent"]);
        modal.result.then(function () { });
        // const modal = this.modalService.open(TipModalComponent);
        // modal.componentInstance.userWallet = this.userWallet;
        // modal.result.then(() => {});
    };
    ProfileEditComponent.prototype.openExportPrivateKey = function () {
        var modal = this.modalService.open(_app_shared_modals_privatekey_export_privatekey_export_component__WEBPACK_IMPORTED_MODULE_15__["PrivatekeyExportComponent"]);
        modal.result.then(function () { });
    };
    ProfileEditComponent.prototype.onSelectCoverPhoto = function (event) {
        console.log(event.target);
        if (this.selectedCoverPhoto === null || this.selectedCoverPhoto === undefined) {
            this.selectedCoverPhoto = event.target.files[0];
            this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_9__["PreviewImageVideo"](event, this.url));
        }
    };
    ProfileEditComponent.prototype.setSubmitButton = function (flag) {
        this.submitButtonStatus = flag;
    };
    ProfileEditComponent.prototype.getSubmitButton = function () {
        return this.submitButtonStatus;
    };
    ProfileEditComponent.prototype.setAccountAsPrivate = function () {
        var _this = this;
        var formData = new FormData();
        formData.append('is_private', '1');
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_8__["EditProfile"](formData)).subscribe(function () {
            _this.setSubmitButton(true);
            sweetalert2__WEBPACK_IMPORTED_MODULE_18___default.a.fire('Sucess', 'Account set to Private', 'success');
            _this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_8__["GetProfile"]());
        }, function (err) {
            console.log(err);
            _this.setSubmitButton(true);
        });
    };
    ProfileEditComponent.prototype.setAccountAsPublic = function () {
        var _this = this;
        var formData = new FormData();
        formData.append('is_private', '0');
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_8__["EditProfile"](formData)).subscribe(function () {
            _this.setSubmitButton(true);
            sweetalert2__WEBPACK_IMPORTED_MODULE_18___default.a.fire('Sucesss', 'Account set to Public', 'success');
            _this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_8__["GetProfile"]());
        }, function (err) {
            console.log(err);
            _this.setSubmitButton(true);
        });
    };
    ProfileEditComponent.prototype.onEditProfile = function (form) {
        var _this = this;
        console.log(form.value);
        var formData = new FormData();
        formData.append('fname', form.value.fname);
        formData.append('lname', form.value.lname);
        formData.append('description', form.value.description);
        formData.append('country_id', form.value.country_id);
        if (this.selectedImage) {
            formData.append('image', this.selectedImage);
        }
        if (this.selectedCoverPhoto) {
            formData.append('cover_photo', this.selectedCoverPhoto);
        }
        console.log(formData);
        this.formSubmitAttempt = true;
        if (form.valid) {
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            console.log('yeah');
            this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_8__["EditProfile"](formData)).subscribe(function () {
                _this.setSubmitButton(true);
                _this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_11__["Navigate"](['/profile']));
            }, function (err) {
                console.log(err);
                _this.setSubmitButton(true);
            });
        }
    };
    ProfileEditComponent.ctorParameters = function () { return [
        { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Store"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_16__["NgbModal"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Select"])(_store_profile_state__WEBPACK_IMPORTED_MODULE_7__["ProfileState"].getProfile),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
    ], ProfileEditComponent.prototype, "profile$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Select"])(_app_post_store_post_state__WEBPACK_IMPORTED_MODULE_10__["PostState"].getFileUrl),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
    ], ProfileEditComponent.prototype, "url$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Select"])(_app_shared_store_country_state__WEBPACK_IMPORTED_MODULE_13__["CountryState"].getAllCountries),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
    ], ProfileEditComponent.prototype, "countries$", void 0);
    ProfileEditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile-edit',
            template: __webpack_require__(/*! raw-loader!./profile-edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/profile/profile-edit/profile-edit.component.html"),
            styles: [__webpack_require__(/*! ./profile-edit.component.scss */ "./src/app/profile/profile-edit/profile-edit.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_6__["Store"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_16__["NgbModal"]])
    ], ProfileEditComponent);
    return ProfileEditComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/profile/profile-routing.module.ts ***!
  \***************************************************/
/*! exports provided: ProfileRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileRoutingModule", function() { return ProfileRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _profile_edit_profile_edit_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile-edit/profile-edit.component */ "./src/app/profile/profile-edit/profile-edit.component.ts");
/* harmony import */ var _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./profile-view/profile-view.component */ "./src/app/profile/profile-view/profile-view.component.ts");
/* harmony import */ var _profile_profile_user_view_profile_user_view_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../profile/profile-user-view/profile-user-view.component */ "./src/app/profile/profile-user-view/profile-user-view.component.ts");
/* harmony import */ var _app_core_guards_edit_profile_guard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @app/core/guards/edit-profile-guard.service */ "./src/app/core/guards/edit-profile-guard.service.ts");







var routes = [
    {
        path: '',
        component: _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_4__["ProfileViewComponent"]
    },
    {
        path: 'profile-view/:id',
        component: _profile_profile_user_view_profile_user_view_component__WEBPACK_IMPORTED_MODULE_5__["ProfileUserViewComponent"]
    },
    {
        path: 'edit',
        component: _profile_edit_profile_edit_component__WEBPACK_IMPORTED_MODULE_3__["ProfileEditComponent"],
        canActivate: [_app_core_guards_edit_profile_guard_service__WEBPACK_IMPORTED_MODULE_6__["EditProfileGuard"]]
    }
];
var ProfileRoutingModule = /** @class */ (function () {
    function ProfileRoutingModule() {
    }
    ProfileRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ProfileRoutingModule);
    return ProfileRoutingModule;
}());



/***/ }),

/***/ "./src/app/profile/profile-user-view/profile-user-view.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/profile/profile-user-view/profile-user-view.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS11c2VyLXZpZXcvcHJvZmlsZS11c2VyLXZpZXcuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/profile/profile-user-view/profile-user-view.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/profile/profile-user-view/profile-user-view.component.ts ***!
  \**************************************************************************/
/*! exports provided: ProfileUserViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileUserViewComponent", function() { return ProfileUserViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm5/ngxs-store.js");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm5/ngxs-router-plugin.js");
/* harmony import */ var _store_profile_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _store_profile_state__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../store/profile.state */ "./src/app/profile/store/profile.state.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app/post/store/post.state */ "./src/app/post/store/post.state.ts");













var ProfileUserViewComponent = /** @class */ (function () {
    function ProfileUserViewComponent(store, route) {
        this.store = store;
        this.route = route;
        this.ENDPOINT_URL = _env_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].urlForImage;
        this.options = [
            {
                id: '1',
                name: 'Followers'
            },
            {
                id: '2',
                name: 'Following'
            }
        ];
        this.user_id = this.route.snapshot.params['id'];
        this.faPen = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faPen"];
        this.faFollow = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faUserCheck"];
        this.faFollow2 = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faUserAltSlash"];
        this.provider = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_10__["AuthState"].getProvider);
    }
    ProfileUserViewComponent.prototype.ngOnInit = function () {
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_4__["ViewUserProfile"](this.user_id));
        // this.store.dispatch(new UserPostById(this.user_id));
    };
    ProfileUserViewComponent.prototype.followUserPrivate = function (id) {
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_4__["FollowUserPrivate"](this.user_id)).subscribe(function (user) {
            console.log(user);
        });
    };
    ProfileUserViewComponent.prototype.getFollowers = function () {
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_4__["GetFollowers"](this.user_id));
    };
    ProfileUserViewComponent.prototype.getFollowings = function () {
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_4__["GetFollowings"](this.user_id));
    };
    ProfileUserViewComponent.prototype.decode = function (image) {
        return "" + this.ENDPOINT_URL + decodeURI(image);
    };
    ProfileUserViewComponent.prototype.gotoProfile = function (id) {
        this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_3__["Navigate"](["/profile/profile-view/" + id]));
    };
    ProfileUserViewComponent.prototype.gotoFollowers = function (id) {
        this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_3__["Navigate"](["/followers/" + id]));
    };
    ProfileUserViewComponent.prototype.gotoFollowings = function (id) {
        var _this = this;
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_4__["ViewUserProfile"](this.user_id)).subscribe(function () {
            _this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_3__["Navigate"](["/profile/profile-view/" + id]));
        });
    };
    ProfileUserViewComponent.prototype.onTabChange = function (event) {
        switch (event.nextId) {
            case '1':
                // this.getFollowings();
                break;
            case '2':
                // this.getFollowings();
                break;
            default:
                console.log('none');
        }
    };
    ProfileUserViewComponent.ctorParameters = function () { return [
        { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Select"])(_store_profile_state__WEBPACK_IMPORTED_MODULE_6__["ProfileState"].getFollowers),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_7__["Observable"])
    ], ProfileUserViewComponent.prototype, "profiles$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Select"])(_store_profile_state__WEBPACK_IMPORTED_MODULE_6__["ProfileState"].viewUserProfile),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_7__["Observable"])
    ], ProfileUserViewComponent.prototype, "profile$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Select"])(_app_post_store_post_state__WEBPACK_IMPORTED_MODULE_11__["PostState"].userPostById),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_7__["Observable"])
    ], ProfileUserViewComponent.prototype, "posts$", void 0);
    ProfileUserViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile-user-view',
            template: __webpack_require__(/*! raw-loader!./profile-user-view.component.html */ "./node_modules/raw-loader/index.js!./src/app/profile/profile-user-view/profile-user-view.component.html"),
            styles: [__webpack_require__(/*! ./profile-user-view.component.scss */ "./src/app/profile/profile-user-view/profile-user-view.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], ProfileUserViewComponent);
    return ProfileUserViewComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile-view/profile-view.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/profile/profile-view/profile-view.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS12aWV3L3Byb2ZpbGUtdmlldy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/profile/profile-view/profile-view.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/profile/profile-view/profile-view.component.ts ***!
  \****************************************************************/
/*! exports provided: ProfileViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileViewComponent", function() { return ProfileViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm5/ngxs-store.js");
/* harmony import */ var _store_profile_state__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../store/profile.state */ "./src/app/profile/store/profile.state.ts");
/* harmony import */ var _store_profile_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app/post/store/post.state */ "./src/app/post/store/post.state.ts");
/* harmony import */ var _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app/post/store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm5/ngxs-router-plugin.js");




// NGXS








var ProfileViewComponent = /** @class */ (function () {
    function ProfileViewComponent(store) {
        this.store = store;
        this.options = [
            {
                id: '1',
                name: 'Post'
            },
            {
                id: '2',
                name: 'Pins'
            }
        ];
        // NGXS
        this.provider = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_10__["AuthState"].getProvider);
        this.faPen = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_9__["faPen"];
        this.store.dispatch(new _store_profile_action__WEBPACK_IMPORTED_MODULE_6__["GetProfile"]());
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_8__["GetOwnPosts"]());
    }
    ProfileViewComponent.prototype.getUrl = function (url) {
        return "" + _env_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].urlForImage + url;
    };
    ProfileViewComponent.prototype.gotoEdit = function () {
        this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_11__["Navigate"](['profile/edit']));
    };
    ProfileViewComponent.ctorParameters = function () { return [
        { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Select"])(_store_profile_state__WEBPACK_IMPORTED_MODULE_5__["ProfileState"].getProfile),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProfileViewComponent.prototype, "profile$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Select"])(_app_post_store_post_state__WEBPACK_IMPORTED_MODULE_7__["PostState"].getOwnPost),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], ProfileViewComponent.prototype, "posts$", void 0);
    ProfileViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile-view',
            template: __webpack_require__(/*! raw-loader!./profile-view.component.html */ "./node_modules/raw-loader/index.js!./src/app/profile/profile-view/profile-view.component.html"),
            styles: [__webpack_require__(/*! ./profile-view.component.scss */ "./src/app/profile/profile-view/profile-view.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"]])
    ], ProfileViewComponent);
    return ProfileViewComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfileModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileModule", function() { return ProfileModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile-routing.module */ "./src/app/profile/profile-routing.module.ts");
/* harmony import */ var _app_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm5/ngxs-store.js");
/* harmony import */ var _store_profile_state__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./store/profile.state */ "./src/app/profile/store/profile.state.ts");
/* harmony import */ var _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile-view/profile-view.component */ "./src/app/profile/profile-view/profile-view.component.ts");
/* harmony import */ var _profile_edit_profile_edit_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./profile-edit/profile-edit.component */ "./src/app/profile/profile-edit/profile-edit.component.ts");
/* harmony import */ var _profile_user_view_profile_user_view_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./profile-user-view/profile-user-view.component */ "./src/app/profile/profile-user-view/profile-user-view.component.ts");




// NGXS





var ProfileModule = /** @class */ (function () {
    function ProfileModule() {
    }
    ProfileModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_6__["ProfileViewComponent"],
                _profile_edit_profile_edit_component__WEBPACK_IMPORTED_MODULE_7__["ProfileEditComponent"],
                _profile_user_view_profile_user_view_component__WEBPACK_IMPORTED_MODULE_8__["ProfileUserViewComponent"]
            ],
            imports: [_profile_routing_module__WEBPACK_IMPORTED_MODULE_2__["ProfileRoutingModule"], _app_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _ngxs_store__WEBPACK_IMPORTED_MODULE_4__["NgxsModule"].forFeature([_store_profile_state__WEBPACK_IMPORTED_MODULE_5__["ProfileState"]])]
        })
    ], ProfileModule);
    return ProfileModule;
}());



/***/ }),

/***/ "./src/app/shared/reusable-custom-validators/set-as-touched.validator.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/shared/reusable-custom-validators/set-as-touched.validator.ts ***!
  \*******************************************************************************/
/*! exports provided: setAsTouched */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setAsTouched", function() { return setAsTouched; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");

function setAsTouched(group) {
    group.markAsTouched();
    for (var i in group.controls) {
        if (group.controls[i] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"]) {
            group.controls[i].markAsTouched();
        }
        else {
            this.setAsTouched(group.controls[i]);
        }
    }
}


/***/ })

}]);
//# sourceMappingURL=profile-profile-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "../sirius-id-sdk/dist/index.js":
/*!**************************************!*\
  !*** ../sirius-id-sdk/dist/index.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./message/index */ "../sirius-id-sdk/dist/message/index.js"));
__export(__webpack_require__(/*! ./service/index */ "../sirius-id-sdk/dist/service/index.js"));


/***/ }),

/***/ "../sirius-id-sdk/dist/message/AppId.js":
/*!**********************************************!*\
  !*** ../sirius-id-sdk/dist/message/AppId.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var AppId = /** @class */ (function () {
    function AppId() {
    }
    /**
     * App Name
     */
    AppId.APP_NAME = 'SiriusID';
    /**
     * Version of SiriusID App/SDK
     */
    AppId.APP_VERSION = '0.0.1';
    return AppId;
}());
exports.AppId = AppId;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/AppMessage.js":
/*!***************************************************!*\
  !*** ../sirius-id-sdk/dist/message/AppMessage.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Message_1 = __webpack_require__(/*! ./Message */ "../sirius-id-sdk/dist/message/Message.js");
var MessageType_1 = __webpack_require__(/*! ./MessageType */ "../sirius-id-sdk/dist/message/MessageType.js");
var js_1 = __webpack_require__(/*! qrcode-generator-ts/js */ "../sirius-id-sdk/node_modules/qrcode-generator-ts/js/index.js");
var CompressData_1 = __webpack_require__(/*! ../service/CompressData */ "../sirius-id-sdk/dist/service/CompressData.js");
var AppMessage = /** @class */ (function (_super) {
    __extends(AppMessage, _super);
    function AppMessage(type, payload) {
        return _super.call(this, type, payload) || this;
    }
    AppMessage.prototype.generateQR = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, qrCode, dataStr, newData, numberLevel, url, url;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = this.createObject();
                        qrCode = new js_1.QRCode;
                        console.log(data.type);
                        dataStr = JSON.stringify(data);
                        console.log(dataStr.length);
                        return [4 /*yield*/, CompressData_1.CompressData.compress(dataStr)];
                    case 1:
                        newData = _a.sent();
                        console.log(newData.length);
                        numberLevel = 17;
                        if (data.type == MessageType_1.MessageType.CREDENTIAL) {
                            console.log("Types is Credential");
                            if (newData.length < 644) {
                                numberLevel = 17;
                            }
                            else if (newData.length < 792) {
                                numberLevel = 19;
                            }
                            else if (newData.length < 929) {
                                numberLevel = 21;
                            }
                            else if (newData.length < 1091) {
                                numberLevel = 23;
                            }
                            else if (newData.length < 1273) {
                                numberLevel = 25;
                            }
                            else if (newData.length < 1465) {
                                numberLevel = 27;
                            }
                            else if (newData.length < 1732) {
                                numberLevel = 30;
                            }
                            else if (newData.length < 2303) {
                                numberLevel = 35;
                            }
                            else if (newData.length < 2953) {
                                numberLevel = 40;
                            }
                            else {
                                console.log('Error:', 'string limit 2953 characters');
                                numberLevel = 0;
                                return [2 /*return*/];
                            }
                            qrCode.addData(newData);
                            qrCode.setTypeNumber(numberLevel);
                            qrCode.setErrorCorrectLevel(js_1.ErrorCorrectLevel.L);
                            qrCode.make();
                            url = qrCode.toDataURL();
                            return [2 /*return*/, url];
                        }
                        else {
                            if (newData.length > 644) {
                                console.log('Error:', 'string limit 644 characters');
                            }
                            else {
                                qrCode.addData(newData);
                                qrCode.setTypeNumber(17);
                                qrCode.setErrorCorrectLevel(js_1.ErrorCorrectLevel.L);
                                qrCode.make();
                                url = qrCode.toDataURL();
                                return [2 /*return*/, url];
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMessage.prototype.universalLink = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, dataStr, newData, url, replaceStr;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = this.createObject();
                        dataStr = JSON.stringify(data);
                        return [4 /*yield*/, CompressData_1.CompressData.compress(dataStr)];
                    case 1:
                        newData = _a.sent();
                        if (newData.length > 2000) {
                            console.log('Error:', 'string limit 2000 characters');
                        }
                        else {
                            url = "siriusid://siriusid.com/app/data/";
                            replaceStr = newData.replace(new RegExp('/', 'g'), '-');
                            return [2 /*return*/, url.concat(replaceStr)];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    AppMessage.prototype.createObject = function () {
        return {
            'version': this.version,
            'type': this.type,
            'payload': this.payload
        };
    };
    return AppMessage;
}(Message_1.Message));
exports.AppMessage = AppMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/CredentialConfirmMessage.js":
/*!*****************************************************************!*\
  !*** ../sirius-id-sdk/dist/message/CredentialConfirmMessage.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var MessageType_1 = __webpack_require__(/*! ./MessageType */ "../sirius-id-sdk/dist/message/MessageType.js");
var AppMessage_1 = __webpack_require__(/*! ./AppMessage */ "../sirius-id-sdk/dist/message/AppMessage.js");
var CredentialConfirmMessage = /** @class */ (function (_super) {
    __extends(CredentialConfirmMessage, _super);
    function CredentialConfirmMessage(appPublicKey, sessionToken, credential) {
        var _this = this;
        var type = MessageType_1.MessageType.CREDENTIAL_REQ;
        var payload = {
            appPublicKey: appPublicKey,
            sessionToken: sessionToken,
            credential: credential
        };
        _this = _super.call(this, type, payload) || this;
        _this.createTimestamp = Date.now();
        _this.token = payload.sessionToken;
        return _this;
    }
    /**
     * Create login request message
     * @param publicKey publicKey of dApp
     *
     * @param credentials array id of credentials dApp need
     */
    CredentialConfirmMessage.create = function (publicKey, credential) {
        var token = this.generateToken();
        return new CredentialConfirmMessage(publicKey, token, credential);
    };
    CredentialConfirmMessage.prototype.getSessionToken = function () {
        return this.token;
    };
    /**
     * Generate random string of sesion token
     */
    CredentialConfirmMessage.generateToken = function () {
        var outString = '';
        var inOptions = 'abcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 64; i++) {
            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
        }
        return outString;
    };
    return CredentialConfirmMessage;
}(AppMessage_1.AppMessage));
exports.CredentialConfirmMessage = CredentialConfirmMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/CredentialRequestMessage.js":
/*!*****************************************************************!*\
  !*** ../sirius-id-sdk/dist/message/CredentialRequestMessage.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var MessageType_1 = __webpack_require__(/*! ./MessageType */ "../sirius-id-sdk/dist/message/MessageType.js");
var AppMessage_1 = __webpack_require__(/*! ./AppMessage */ "../sirius-id-sdk/dist/message/AppMessage.js");
var CredentialRequestMessage = /** @class */ (function (_super) {
    __extends(CredentialRequestMessage, _super);
    function CredentialRequestMessage(credentials) {
        var _this = this;
        var payload = {
            credentials: credentials
        };
        var type = MessageType_1.MessageType.CREDENTIAL;
        _this = _super.call(this, type, payload) || this;
        return _this;
    }
    CredentialRequestMessage.create = function (credentials) {
        return new CredentialRequestMessage(credentials);
    };
    return CredentialRequestMessage;
}(AppMessage_1.AppMessage));
exports.CredentialRequestMessage = CredentialRequestMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/CredentialResponseMessage.js":
/*!******************************************************************!*\
  !*** ../sirius-id-sdk/dist/message/CredentialResponseMessage.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var MessageType_1 = __webpack_require__(/*! ./MessageType */ "../sirius-id-sdk/dist/message/MessageType.js");
var AppMessage_1 = __webpack_require__(/*! ./AppMessage */ "../sirius-id-sdk/dist/message/AppMessage.js");
var CredentialResponseMessage = /** @class */ (function (_super) {
    __extends(CredentialResponseMessage, _super);
    function CredentialResponseMessage(appPublicKey, sessionToken, credential) {
        var _this = this;
        var type = MessageType_1.MessageType.CREDENTIAL_RES;
        var payload = {
            appPublicKey: appPublicKey,
            sessionToken: sessionToken,
            credential: credential
        };
        _this = _super.call(this, type, payload) || this;
        _this.createTimestamp = Date.now();
        _this.token = payload.sessionToken;
        return _this;
    }
    /**
     * Create login request message
     * @param publicKey publicKey of dApp
     *
     * @param credentials array id of credentials dApp need
     */
    CredentialResponseMessage.create = function (publicKey, token, credential) {
        return new CredentialResponseMessage(publicKey, token, credential);
    };
    CredentialResponseMessage.prototype.getSessionToken = function () {
        return this.token;
    };
    return CredentialResponseMessage;
}(AppMessage_1.AppMessage));
exports.CredentialResponseMessage = CredentialResponseMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/CredentialStored.js":
/*!*********************************************************!*\
  !*** ../sirius-id-sdk/dist/message/CredentialStored.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var tsjs_xpx_chain_sdk_1 = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "../sirius-id-sdk/node_modules/tsjs-xpx-chain-sdk/dist/index.js");
var tsjs_chain_xipfs_sdk_1 = __webpack_require__(/*! tsjs-chain-xipfs-sdk */ "../sirius-id-sdk/node_modules/tsjs-chain-xipfs-sdk/build/main/src/index.js");
var ApiNode_1 = __webpack_require__(/*! ../service/ApiNode */ "../sirius-id-sdk/dist/service/ApiNode.js");
var CredentialStored = /** @class */ (function () {
    function CredentialStored(keyDecrypt, credentialHash) {
        this.keyDecrypt = keyDecrypt;
        this.credentialHash = credentialHash;
    }
    CredentialStored.create = function (credential, privateKey) {
        return __awaiter(this, void 0, void 0, function () {
            var account, keyEncrypt, keyDecrypt, credentialHash;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        account = tsjs_xpx_chain_sdk_1.Account.generateNewAccount(CredentialStored.NETWORK);
                        keyEncrypt = account.publicKey;
                        keyDecrypt = account.privateKey;
                        return [4 /*yield*/, CredentialStored.storedCredential(credential, keyEncrypt, privateKey)];
                    case 1:
                        credentialHash = _a.sent();
                        return [2 /*return*/, new CredentialStored(keyDecrypt, credentialHash)];
                }
            });
        });
    };
    CredentialStored.storedCredential = function (credential, keyEncrypt, privateKey) {
        return __awaiter(this, void 0, void 0, function () {
            var strCredential, publicAccount, encStr, sender, ipfsConnection, apiHost, blockchainConnection, conectionConfig, uploader, strParam, uploadParamBuilder, uploadParams;
            return __generator(this, function (_a) {
                strCredential = JSON.stringify(credential);
                publicAccount = tsjs_xpx_chain_sdk_1.PublicAccount.createFromPublicKey(keyEncrypt, CredentialStored.NETWORK);
                encStr = tsjs_xpx_chain_sdk_1.EncryptedMessage.create(strCredential, publicAccount, privateKey);
                sender = tsjs_xpx_chain_sdk_1.Account.createFromPrivateKey(privateKey, ApiNode_1.ApiNode.networkType);
                ipfsConnection = new tsjs_chain_xipfs_sdk_1.IpfsConnection(CredentialStored.ipfsDomain, // the host or multi address
                CredentialStored.ipfsPort, // the port number
                { protocol: CredentialStored.ipfsProtocol } // the optional protocol
                );
                apiHost = this.convertApiNode();
                blockchainConnection = new tsjs_chain_xipfs_sdk_1.BlockchainNetworkConnection(CredentialStored.convertNetworkType(), // the network type
                apiHost.apiDomain, // the rest api base endpoint
                apiHost.apiPort, // the optional websocket end point
                apiHost.apiProtocol);
                conectionConfig = tsjs_chain_xipfs_sdk_1.ConnectionConfig.createWithLocalIpfsConnection(blockchainConnection, ipfsConnection);
                uploader = new tsjs_chain_xipfs_sdk_1.Uploader(conectionConfig);
                strParam = tsjs_chain_xipfs_sdk_1.StringParameterData.create(encStr.payload);
                uploadParamBuilder = tsjs_chain_xipfs_sdk_1.UploadParameter.createForStringUpload(strParam, privateKey);
                uploadParams = uploadParamBuilder
                    .withRecipientPublicKey(sender.publicKey)
                    .withTransactionMosaics([])
                    .build();
                return [2 /*return*/, new Promise(function (resolve) {
                        uploader.upload(uploadParams).then(function (result) {
                            console.log("transaction hash: " + result.transactionHash);
                            resolve(result.transactionHash);
                        });
                    })];
            });
        });
    };
    CredentialStored.prototype.getKeyDecrypt = function () {
        return this.keyDecrypt;
    };
    CredentialStored.prototype.getCredentialHash = function () {
        return this.credentialHash;
    };
    CredentialStored.convertNetworkType = function () {
        switch (ApiNode_1.ApiNode.networkType) {
            case tsjs_xpx_chain_sdk_1.NetworkType.MAIN_NET:
                return tsjs_chain_xipfs_sdk_1.BlockchainNetworkType.MAIN_NET;
            case tsjs_xpx_chain_sdk_1.NetworkType.TEST_NET:
                return tsjs_chain_xipfs_sdk_1.BlockchainNetworkType.TEST_NET;
            case tsjs_xpx_chain_sdk_1.NetworkType.MIJIN:
                return tsjs_chain_xipfs_sdk_1.BlockchainNetworkType.MIJIN;
            case tsjs_xpx_chain_sdk_1.NetworkType.MIJIN_TEST:
                return tsjs_chain_xipfs_sdk_1.BlockchainNetworkType.MIJIN_TEST;
            case tsjs_xpx_chain_sdk_1.NetworkType.PRIVATE:
                return tsjs_chain_xipfs_sdk_1.BlockchainNetworkType.PRIVATE;
            case tsjs_xpx_chain_sdk_1.NetworkType.PRIVATE_TEST:
                return tsjs_chain_xipfs_sdk_1.BlockchainNetworkType.PRIVATE_TEST;
        }
    };
    CredentialStored.convertApiNode = function () {
        var url;
        var lastCharacter = ApiNode_1.ApiNode.apiNode[ApiNode_1.ApiNode.apiNode.length - 1];
        if (lastCharacter == "/") {
            url = ApiNode_1.ApiNode.apiNode.slice(0, ApiNode_1.ApiNode.apiNode.length - 1);
        }
        else
            url = ApiNode_1.ApiNode.apiNode;
        var httpsIs = url.indexOf('https');
        var apiProtocol;
        var apiPort;
        var apiDomain;
        if (httpsIs == -1) {
            apiProtocol = tsjs_chain_xipfs_sdk_1.Protocol.HTTP;
            if (url.indexOf(':') > -1) {
                apiDomain = url.slice(7, url.indexOf(':'));
                apiPort = Number(url.slice(url.indexOf(':') + 1));
            }
            else {
                apiDomain = url.slice(7);
                apiPort = 80;
            }
        }
        else {
            apiProtocol = tsjs_chain_xipfs_sdk_1.Protocol.HTTPS;
            apiPort = 443;
            apiDomain = url.slice(8);
        }
        console.log("=====================");
        console.log(apiProtocol + " " + apiDomain + " " + apiPort);
        console.log("=====================");
        return {
            'apiDomain': apiDomain,
            'apiPort': apiPort,
            'apiProtocol': apiProtocol
        };
    };
    CredentialStored.prototype.getCredential = function (credentialHash, keyDecrypt, senderPublicKey) {
        return __awaiter(this, void 0, void 0, function () {
            var publicAccount, ipfsConnection, apiHost, blockchainConnection, conectionConfig, dowloader, downloadParamBuilder, downloadParam;
            var _this = this;
            return __generator(this, function (_a) {
                publicAccount = tsjs_xpx_chain_sdk_1.PublicAccount.createFromPublicKey(senderPublicKey, ApiNode_1.ApiNode.networkType);
                ipfsConnection = new tsjs_chain_xipfs_sdk_1.IpfsConnection(CredentialStored.ipfsDomain, // the host or multi address
                CredentialStored.ipfsPort, // the port number
                { protocol: CredentialStored.ipfsProtocol } // the optional protocol
                );
                apiHost = CredentialStored.convertApiNode();
                blockchainConnection = new tsjs_chain_xipfs_sdk_1.BlockchainNetworkConnection(CredentialStored.convertNetworkType(), // the network type
                apiHost.apiDomain, // the rest api base endpoint
                apiHost.apiPort, // the optional websocket end point
                apiHost.apiProtocol);
                conectionConfig = tsjs_chain_xipfs_sdk_1.ConnectionConfig.createWithLocalIpfsConnection(blockchainConnection, ipfsConnection);
                dowloader = new tsjs_chain_xipfs_sdk_1.Downloader(conectionConfig);
                downloadParamBuilder = tsjs_chain_xipfs_sdk_1.DownloadParameter.create(credentialHash);
                downloadParam = downloadParamBuilder.build();
                return [2 /*return*/, new Promise(function (resolve) {
                        dowloader.download(downloadParam).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                            var content, encMess, decMess;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, result.data.getContentsAsString()];
                                    case 1:
                                        content = _a.sent();
                                        encMess = tsjs_xpx_chain_sdk_1.EncryptedMessage.createFromPayload(content);
                                        decMess = tsjs_xpx_chain_sdk_1.EncryptedMessage.decrypt(encMess, keyDecrypt, publicAccount);
                                        console.log("credential content: " + decMess);
                                        resolve(JSON.parse(decMess.payload));
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    CredentialStored.NETWORK = tsjs_xpx_chain_sdk_1.NetworkType.MAIN_NET;
    CredentialStored.ipfsDomain = "ipfs1-dev.xpxsirius.io";
    // static readonly ipfsPortUp = 5001;
    // static readonly ipfsProtocolUp = "http";
    // static readonly ipfsPortDown = 443;
    // static readonly ipfsProtocolDown = "https";
    CredentialStored.ipfsPort = 5443;
    CredentialStored.ipfsProtocol = "https";
    return CredentialStored;
}());
exports.CredentialStored = CredentialStored;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/Credentials.js":
/*!****************************************************!*\
  !*** ../sirius-id-sdk/dist/message/Credentials.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var md5_1 = __webpack_require__(/*! ts-md5/dist/md5 */ "../sirius-id-sdk/node_modules/ts-md5/dist/md5.js");
var tsjs_xpx_chain_sdk_1 = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "../sirius-id-sdk/node_modules/tsjs-xpx-chain-sdk/dist/index.js");
var ApiNode_1 = __webpack_require__(/*! ../service/ApiNode */ "../sirius-id-sdk/dist/service/ApiNode.js");
var Credentials = /** @class */ (function () {
    function Credentials(id, name, description, icon, documentHash, content, group, auth_origin, auth_owner) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.documentHash = documentHash;
        this.content = content;
        this.group = group;
        this.auth_origin = auth_origin;
        this.auth_owner = auth_owner;
    }
    Credentials.create = function (id, name, description, icon, documentHash, content, group, auth_origin, auth_owner) {
        var content_arr = Array.from(content);
        if (!auth_origin) {
            auth_origin = null;
        }
        if (!auth_owner) {
            auth_owner = null;
        }
        return new Credentials(id, name, description, icon, documentHash, content_arr, group, auth_origin, auth_owner);
    };
    Credentials.authCreate = function (content, privateKey) {
        var hashStr = md5_1.Md5.hashStr(JSON.stringify(content));
        var pubAccount = tsjs_xpx_chain_sdk_1.PublicAccount.createFromPublicKey(this.globalPublicKey, ApiNode_1.ApiNode.networkType);
        return tsjs_xpx_chain_sdk_1.EncryptedMessage.create(hashStr, pubAccount, privateKey).payload;
    };
    Credentials.compareHashStr = function (content, encryptedStr, publicKey) {
        var hashStr1 = md5_1.Md5.hashStr(JSON.stringify(content));
        var pubAccount = tsjs_xpx_chain_sdk_1.PublicAccount.createFromPublicKey(publicKey, ApiNode_1.ApiNode.networkType);
        var hashStr2 = tsjs_xpx_chain_sdk_1.EncryptedMessage.decrypt(tsjs_xpx_chain_sdk_1.EncryptedMessage.createFromPayload(encryptedStr), this.globalPrivateKey, pubAccount);
        if (hashStr1 == hashStr2.payload) {
            return true;
        }
        else
            return false;
    };
    Credentials.prototype.addAuthOrigin = function (auth) {
        this.auth_origin = auth;
    };
    Credentials.prototype.addAuthOwner = function (auth) {
        this.auth_owner = auth;
    };
    Credentials.prototype.getContent = function () {
        return this.content;
    };
    Credentials.prototype.getId = function () {
        return this.id;
    };
    Credentials.prototype.getName = function () {
        return this.name;
    };
    Credentials.prototype.getDescription = function () {
        return this.description;
    };
    Credentials.prototype.getIcon = function () {
        return this.icon;
    };
    Credentials.prototype.getDocumentHash = function () {
        return this.documentHash;
    };
    Credentials.prototype.getAuthOrigin = function () {
        return this.auth_origin;
    };
    Credentials.prototype.getAuthOwner = function () {
        return this.auth_owner;
    };
    Credentials.globalPrivateKey = "27745A12D8EE237BE979543F76FDDEFE8B266273DDCB4BC23C31DBAF7214F548";
    Credentials.globalPublicKey = "E0665BAB43304CF279F13C7EFB48A857DD03D5BB0372FDC7DAFE9DDAA01ECA5A";
    return Credentials;
}());
exports.Credentials = Credentials;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/LoginRequestMessage.js":
/*!************************************************************!*\
  !*** ../sirius-id-sdk/dist/message/LoginRequestMessage.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var MessageType_1 = __webpack_require__(/*! ./MessageType */ "../sirius-id-sdk/dist/message/MessageType.js");
var AppMessage_1 = __webpack_require__(/*! ./AppMessage */ "../sirius-id-sdk/dist/message/AppMessage.js");
var LoginRequestMessage = /** @class */ (function (_super) {
    __extends(LoginRequestMessage, _super);
    function LoginRequestMessage(
    /**
     * dApp
     */
    appPublicKey, 
    /**
     * token
     */
    sessionToken, 
    /**
     * list id
     */
    credentials) {
        var _this = this;
        var type = MessageType_1.MessageType.LOG_IN;
        var payload = {
            appPublicKey: appPublicKey,
            sessionToken: sessionToken,
            credentials: credentials
        };
        _this = _super.call(this, type, payload) || this;
        _this.createTimestamp = Date.now();
        _this.token = payload.sessionToken;
        return _this;
    }
    /**
     * Create login request message
     * @param publicKey publicKey of dApp
     *
     * @param credentials array id of credentials dApp need
     */
    LoginRequestMessage.create = function (publicKey, credentials) {
        var token = this.generateToken();
        return new LoginRequestMessage(publicKey, token, credentials);
    };
    /**
     * Generate random string of sesion token
     */
    LoginRequestMessage.generateToken = function () {
        var outString = '';
        var inOptions = 'abcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 64; i++) {
            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
        }
        return outString;
    };
    LoginRequestMessage.prototype.getSessionToken = function () {
        return this.token;
    };
    /**
     * Check if login request is approved
     */
    LoginRequestMessage.prototype.verify = function () {
        //TODO
    };
    return LoginRequestMessage;
}(AppMessage_1.AppMessage));
exports.LoginRequestMessage = LoginRequestMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/Message.js":
/*!************************************************!*\
  !*** ../sirius-id-sdk/dist/message/Message.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var AppId_1 = __webpack_require__(/*! ./AppId */ "../sirius-id-sdk/dist/message/AppId.js");
var Message = /** @class */ (function () {
    function Message(
    /**
     * Message type
     */
    type, 
    /**
     * Message payload
     */
    payload) {
        this.type = type;
        this.payload = payload;
        /**
         * Message version
         */
        this.version = AppId_1.AppId.APP_VERSION;
    }
    Message.prototype.toString = function () {
        var message = {
            type: this.type,
            version: this.version,
            payload: this.payload
        };
        return JSON.stringify(message);
    };
    return Message;
}());
exports.Message = Message;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/MessageType.js":
/*!****************************************************!*\
  !*** ../sirius-id-sdk/dist/message/MessageType.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var MessageType = /** @class */ (function () {
    function MessageType() {
    }
    /**
     * Login message
     * @type {number}
     */
    MessageType.LOG_IN = 1;
    /**
     * Sign transaction request message
     * @type {number}
     */
    MessageType.TRANSACTION = 2;
    /**
     * Send credential message
     * @type {number}
     */
    MessageType.CREDENTIAL = 3;
    /**
     * Credential request message
     * @type {number}
     */
    MessageType.CREDENTIAL_REQ = 4;
    /**
     * Credential request message
     * @type {number}
     */
    MessageType.CREDENTIAL_RES = 5;
    /**
     * Send credendential via Blockchain
     * @type {number}
     */
    MessageType.VERIFY_HARD = 6;
    /**
     * Send credendential via Socket io
     * @type {number}
     */
    MessageType.VERIFY_LIGHT = 7;
    /**
     * Send credendential via Blockchain and Socket io
     * @type {number}
     */
    MessageType.VERIFY_BOTH = 8;
    return MessageType;
}());
exports.MessageType = MessageType;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/SiriusIDTransactionMessage.js":
/*!*******************************************************************!*\
  !*** ../sirius-id-sdk/dist/message/SiriusIDTransactionMessage.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var MessageType_1 = __webpack_require__(/*! ./MessageType */ "../sirius-id-sdk/dist/message/MessageType.js");
var SiriusIdMessage_1 = __webpack_require__(/*! ./SiriusIdMessage */ "../sirius-id-sdk/dist/message/SiriusIdMessage.js");
var SiriusIDTransactionMessage = /** @class */ (function (_super) {
    __extends(SiriusIDTransactionMessage, _super);
    function SiriusIDTransactionMessage(encryptedSessionToken, credentials) {
        var _this = this;
        var messageType = MessageType_1.MessageType.LOG_IN;
        var payload = {
            encryptedSessionToken: encryptedSessionToken,
            credentials: credentials
        };
        _this = _super.call(this, messageType, payload) || this;
        return _this;
    }
    SiriusIDTransactionMessage.create = function (sessionToken, credentials) {
        return new SiriusIDTransactionMessage(this.encrypted(sessionToken), credentials);
    };
    SiriusIDTransactionMessage.encrypted = function (sessionToken) {
        return sessionToken;
    };
    SiriusIDTransactionMessage.prototype.toString = function () {
        var message = {
            appID: this.appID,
            messageType: this.type,
            appVersion: this.version,
            payload: this.payload
        };
        return JSON.stringify(message);
    };
    return SiriusIDTransactionMessage;
}(SiriusIdMessage_1.SiriusIdMessage));
exports.SiriusIDTransactionMessage = SiriusIDTransactionMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/SiriusIdMessage.js":
/*!********************************************************!*\
  !*** ../sirius-id-sdk/dist/message/SiriusIdMessage.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AppId_1 = __webpack_require__(/*! ./AppId */ "../sirius-id-sdk/dist/message/AppId.js");
var Message_1 = __webpack_require__(/*! ./Message */ "../sirius-id-sdk/dist/message/Message.js");
var SiriusIdMessage = /** @class */ (function (_super) {
    __extends(SiriusIdMessage, _super);
    function SiriusIdMessage(type, payload) {
        var _this = _super.call(this, type, payload) || this;
        /**
         * App name to determine app message in transaction
         */
        _this.appID = AppId_1.AppId.APP_NAME;
        return _this;
    }
    return SiriusIdMessage;
}(Message_1.Message));
exports.SiriusIdMessage = SiriusIdMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/TransactionRequestMessage.js":
/*!******************************************************************!*\
  !*** ../sirius-id-sdk/dist/message/TransactionRequestMessage.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var MessageType_1 = __webpack_require__(/*! ./MessageType */ "../sirius-id-sdk/dist/message/MessageType.js");
var AppMessage_1 = __webpack_require__(/*! ./AppMessage */ "../sirius-id-sdk/dist/message/AppMessage.js");
var TransactionRequestMessage = /** @class */ (function (_super) {
    __extends(TransactionRequestMessage, _super);
    function TransactionRequestMessage(message, transaction, transactionHash) {
        var _this = this;
        var payload = {
            message: message,
            transaction: transaction,
            transactionHash: transactionHash
        };
        var type = MessageType_1.MessageType.TRANSACTION;
        _this = _super.call(this, type, payload) || this;
        return _this;
    }
    return TransactionRequestMessage;
}(AppMessage_1.AppMessage));
exports.TransactionRequestMessage = TransactionRequestMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/VerifyRequestMessage.js":
/*!*************************************************************!*\
  !*** ../sirius-id-sdk/dist/message/VerifyRequestMessage.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AppMessage_1 = __webpack_require__(/*! ./AppMessage */ "../sirius-id-sdk/dist/message/AppMessage.js");
var VerifyRequestMessage = /** @class */ (function (_super) {
    __extends(VerifyRequestMessage, _super);
    function VerifyRequestMessage(
    /**
     * dApp
     */
    appPublicKey, 
    /**
     * token
     */
    sessionToken, 
    /**
     * list id
     */
    credentials, 
    /**
     * VERIFY_HARD or VERIFY_LIGHT or VERIFY_BOTH
     */
    mode, 
    /**
     * Use for VERIFY_LIGHT or VERIFY_BOTH
     */
    url) {
        var _this = this;
        var type = mode;
        var payload = {
            appPublicKey: appPublicKey,
            sessionToken: sessionToken,
            credentials: credentials,
            url: url
        };
        _this = _super.call(this, type, payload) || this;
        _this.createTimestamp = Date.now();
        _this.token = payload.sessionToken;
        _this.url = url;
        return _this;
    }
    /**
     * Create verify request message
     * @param publicKey publicKey of dApp
     *
     * @param credentials array id of credentials dApp need
     */
    VerifyRequestMessage.create = function (publicKey, credentials, mode, url) {
        var token = this.generateToken();
        return new VerifyRequestMessage(publicKey, token, credentials, mode, url);
    };
    /**
     * Generate random string of sesion token
     */
    VerifyRequestMessage.generateToken = function () {
        var outString = '';
        var inOptions = 'abcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 64; i++) {
            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
        }
        return outString;
    };
    VerifyRequestMessage.prototype.getSessionToken = function () {
        return this.token;
    };
    VerifyRequestMessage.prototype.getUrl = function () {
        return this.url;
    };
    /**
     * Check if login request is approved
     */
    VerifyRequestMessage.prototype.verify = function () {
        //TODO
    };
    return VerifyRequestMessage;
}(AppMessage_1.AppMessage));
exports.VerifyRequestMessage = VerifyRequestMessage;


/***/ }),

/***/ "../sirius-id-sdk/dist/message/index.js":
/*!**********************************************!*\
  !*** ../sirius-id-sdk/dist/message/index.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./AppId */ "../sirius-id-sdk/dist/message/AppId.js"));
__export(__webpack_require__(/*! ./MessageType */ "../sirius-id-sdk/dist/message/MessageType.js"));
__export(__webpack_require__(/*! ./Message */ "../sirius-id-sdk/dist/message/Message.js"));
__export(__webpack_require__(/*! ./AppMessage */ "../sirius-id-sdk/dist/message/AppMessage.js"));
__export(__webpack_require__(/*! ./SiriusIdMessage */ "../sirius-id-sdk/dist/message/SiriusIdMessage.js"));
__export(__webpack_require__(/*! ./LoginRequestMessage */ "../sirius-id-sdk/dist/message/LoginRequestMessage.js"));
__export(__webpack_require__(/*! ./SiriusIDTransactionMessage */ "../sirius-id-sdk/dist/message/SiriusIDTransactionMessage.js"));
__export(__webpack_require__(/*! ./TransactionRequestMessage */ "../sirius-id-sdk/dist/message/TransactionRequestMessage.js"));
__export(__webpack_require__(/*! ./Credentials */ "../sirius-id-sdk/dist/message/Credentials.js"));
__export(__webpack_require__(/*! ./CredentialRequestMessage */ "../sirius-id-sdk/dist/message/CredentialRequestMessage.js"));
__export(__webpack_require__(/*! ./CredentialConfirmMessage */ "../sirius-id-sdk/dist/message/CredentialConfirmMessage.js"));
__export(__webpack_require__(/*! ./CredentialResponseMessage */ "../sirius-id-sdk/dist/message/CredentialResponseMessage.js"));
__export(__webpack_require__(/*! ./CredentialStored */ "../sirius-id-sdk/dist/message/CredentialStored.js"));
__export(__webpack_require__(/*! ./VerifyRequestMessage */ "../sirius-id-sdk/dist/message/VerifyRequestMessage.js"));


/***/ }),

/***/ "../sirius-id-sdk/dist/service/AnnounceTransaction.js":
/*!************************************************************!*\
  !*** ../sirius-id-sdk/dist/service/AnnounceTransaction.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var tsjs_xpx_chain_sdk_1 = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "../sirius-id-sdk/node_modules/tsjs-xpx-chain-sdk/dist/index.js");
var AnnounceTransaction = /** @class */ (function () {
    function AnnounceTransaction() {
    }
    AnnounceTransaction.announce = function (transaction, privateKey, apiNode, networkType) {
        if (networkType === void 0) { networkType = tsjs_xpx_chain_sdk_1.NetworkType.TEST_NET; }
        return __awaiter(this, void 0, void 0, function () {
            var transactionHttp, wsAddress, listener, sender, generationHash, signedTx, address, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        transactionHttp = new tsjs_xpx_chain_sdk_1.TransactionHttp(apiNode);
                        wsAddress = apiNode.replace('https', 'wss').replace('http', 'ws');
                        listener = new tsjs_xpx_chain_sdk_1.Listener(wsAddress, WebSocket);
                        sender = tsjs_xpx_chain_sdk_1.Account.createFromPrivateKey(privateKey, networkType);
                        return [4 /*yield*/, this.getGenerationHash(apiNode)];
                    case 1:
                        generationHash = _b.sent();
                        signedTx = sender.sign(transaction, generationHash);
                        console.log("hash: " + signedTx.hash);
                        _a = transaction.type;
                        switch (_a) {
                            case tsjs_xpx_chain_sdk_1.TransactionType.TRANSFER: return [3 /*break*/, 2];
                            case tsjs_xpx_chain_sdk_1.TransactionType.AGGREGATE_COMPLETE: return [3 /*break*/, 4];
                            case tsjs_xpx_chain_sdk_1.TransactionType.AGGREGATE_BONDED: return [3 /*break*/, 6];
                            case tsjs_xpx_chain_sdk_1.TransactionType.ADDRESS_ALIAS: return [3 /*break*/, 8];
                            case tsjs_xpx_chain_sdk_1.TransactionType.LINK_ACCOUNT: return [3 /*break*/, 10];
                            case tsjs_xpx_chain_sdk_1.TransactionType.REGISTER_NAMESPACE: return [3 /*break*/, 12];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MOSAIC_ALIAS: return [3 /*break*/, 14];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MOSAIC_DEFINITION: return [3 /*break*/, 16];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MOSAIC_SUPPLY_CHANGE: return [3 /*break*/, 18];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MODIFY_MOSAIC_METADATA: return [3 /*break*/, 20];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MODIFY_NAMESPACE_METADATA: return [3 /*break*/, 22];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MODIFY_ACCOUNT_METADATA: return [3 /*break*/, 24];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MODIFY_CONTRACT: return [3 /*break*/, 26];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MODIFY_ACCOUNT_RESTRICTION_ADDRESS: return [3 /*break*/, 28];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MODIFY_ACCOUNT_RESTRICTION_MOSAIC: return [3 /*break*/, 30];
                            case tsjs_xpx_chain_sdk_1.TransactionType.MODIFY_ACCOUNT_RESTRICTION_OPERATION: return [3 /*break*/, 32];
                            case tsjs_xpx_chain_sdk_1.TransactionType.SECRET_LOCK: return [3 /*break*/, 34];
                            case tsjs_xpx_chain_sdk_1.TransactionType.SECRET_PROOF: return [3 /*break*/, 36];
                            case tsjs_xpx_chain_sdk_1.TransactionType.CHAIN_CONFIGURE: return [3 /*break*/, 38];
                            case tsjs_xpx_chain_sdk_1.TransactionType.CHAIN_UPGRADE: return [3 /*break*/, 40];
                        }
                        return [3 /*break*/, 42];
                    case 2:
                        console.log("transferrrrr");
                        address = transaction.recipient;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 3: return [2 /*return*/, _b.sent()];
                    case 4:
                        console.log("completeddddd");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 5: return [2 /*return*/, _b.sent()];
                    case 6:
                        console.log("bondeddddd");
                        address = sender.address;
                        return [4 /*yield*/, this.announceAggregateBonded(signedTx, listener, transactionHttp, address, sender, networkType, generationHash)];
                    case 7: return [2 /*return*/, _b.sent()];
                    case 8:
                        console.log("address aliasssss");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 9: return [2 /*return*/, _b.sent()];
                    case 10:
                        console.log("link accountttt");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 11: return [2 /*return*/, _b.sent()];
                    case 12:
                        console.log("register namespaceeee");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 13: return [2 /*return*/, _b.sent()];
                    case 14:
                        console.log("mosaic aliassss");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 15: return [2 /*return*/, _b.sent()];
                    case 16:
                        console.log("mosaic definitionnnn");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 17: return [2 /*return*/, _b.sent()];
                    case 18:
                        console.log("mosaic supply changeeee");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 19: return [2 /*return*/, _b.sent()];
                    case 20:
                        console.log("modify mosaic metadataaaa");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 21: return [2 /*return*/, _b.sent()];
                    case 22:
                        console.log("modify namespace metadataaaa");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 23: return [2 /*return*/, _b.sent()];
                    case 24:
                        console.log("modify account metadataaa");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 25: return [2 /*return*/, _b.sent()];
                    case 26:
                        console.log("modify contractttt");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 27: return [2 /*return*/, _b.sent()];
                    case 28:
                        console.log("modify account property addresssss");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 29: return [2 /*return*/, _b.sent()];
                    case 30:
                        console.log("modify account property mosaicccc");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 31: return [2 /*return*/, _b.sent()];
                    case 32:
                        console.log("modify account property entityyyy");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 33: return [2 /*return*/, _b.sent()];
                    case 34:
                        console.log("secret lockkk");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 35: return [2 /*return*/, _b.sent()];
                    case 36:
                        console.log("secret prooffff");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 37: return [2 /*return*/, _b.sent()];
                    case 38:
                        console.log("blockchain configggg");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 39: return [2 /*return*/, _b.sent()];
                    case 40:
                        console.log("blockchain upgradeeee");
                        address = sender.address;
                        return [4 /*yield*/, this.announceTransfer(signedTx, listener, transactionHttp, address)];
                    case 41: return [2 /*return*/, _b.sent()];
                    case 42: return [2 /*return*/];
                }
            });
        });
    };
    AnnounceTransaction.announceTransfer = function (signedTx, listener, transactionHttp, address) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        var checkTimeout = setTimeout(function () {
                            console.log("da qua 1 phut, check thu trang thai cua ma hash: " + signedTx.hash);
                            transactionHttp.getTransactionStatus(signedTx.hash).subscribe(function (result) {
                                console.log("co ket qua cua ma hash: " + signedTx.hash);
                                console.log(result);
                                if (result.status != "Success") {
                                    clearTimeout(checkTimeout);
                                    resolve(false);
                                }
                            }, function (error) {
                                resolve(false);
                            }, function () {
                                console.log("donne");
                            });
                        }, 60000);
                        listener.open().then(function () {
                            var subscription = listener.confirmed(address).subscribe(function (confirmed) {
                                if (confirmed && confirmed.transactionInfo && confirmed.transactionInfo.hash === signedTx.hash) {
                                    console.log('confirmed: ' + JSON.stringify(confirmed));
                                    resolve(true);
                                    subscription.unsubscribe();
                                    subscription2.unsubscribe();
                                    listener.close();
                                }
                            }, function (error) {
                                resolve(false);
                                console.error(error);
                            }, function () {
                                console.log('done.');
                            });
                            var subscription2 = listener.status(address).subscribe(function (status) {
                                if (status && status.hash === signedTx.hash) {
                                    console.log('status: ' + JSON.stringify(status));
                                    resolve(false);
                                    subscription2.unsubscribe();
                                    subscription.unsubscribe();
                                    listener.close();
                                }
                            }, function (error) {
                                console.error(error);
                            }, function () {
                                console.log('done2.');
                            });
                            transactionHttp.announce(signedTx);
                        });
                    })];
            });
        });
    };
    AnnounceTransaction.announceAggregateBonded = function (signedTx, listener, transactionHttp, address, sender, networkType, generationHash) {
        return __awaiter(this, void 0, void 0, function () {
            var lockFunds, lockFundsSigned;
            return __generator(this, function (_a) {
                lockFunds = tsjs_xpx_chain_sdk_1.LockFundsTransaction.create(tsjs_xpx_chain_sdk_1.Deadline.create(), new tsjs_xpx_chain_sdk_1.Mosaic(new tsjs_xpx_chain_sdk_1.NamespaceId('prx.xpx'), tsjs_xpx_chain_sdk_1.UInt64.fromUint(10 * 1000000)), tsjs_xpx_chain_sdk_1.UInt64.fromUint(50), signedTx, networkType);
                lockFundsSigned = sender.sign(lockFunds, generationHash);
                console.log("lockFunds Hash:", lockFundsSigned.hash);
                return [2 /*return*/, new Promise(function (resolve) {
                        var checkTimeout = setTimeout(function () {
                            transactionHttp.getTransactionStatus(signedTx.hash).subscribe(function (result) {
                                console.log(result);
                            }, function (error) {
                                resolve(false);
                            }, function () {
                                console.log("donne");
                            });
                        }, 60000);
                        listener.open().then(function () {
                            var lockFundsSub = listener.confirmed(address).subscribe(function (lockFundsConfirmed) {
                                if (lockFundsConfirmed && lockFundsConfirmed.transactionInfo && lockFundsConfirmed.transactionInfo.hash === lockFundsSigned.hash) {
                                    console.log('lockFuncdsConfirmed: ' + JSON.stringify(lockFundsConfirmed));
                                    var subscription_1 = listener.aggregateBondedAdded(address).subscribe(function (aggregateTx) {
                                        console.log("co vao nha");
                                        console.log(aggregateTx);
                                        if (aggregateTx && aggregateTx.transactionInfo && aggregateTx.transactionInfo.hash === signedTx.hash) {
                                            console.log('aggregateTx: ' + JSON.stringify(aggregateTx));
                                            resolve(true);
                                            subscription_1.unsubscribe();
                                            lockFundsSub.unsubscribe();
                                            listener.close();
                                        }
                                    }, function (error) {
                                        resolve(false);
                                        console.error(error);
                                    }, function () {
                                        console.log('done...');
                                    });
                                    transactionHttp.announceAggregateBonded(signedTx);
                                }
                            }, function (error) {
                                resolve(false);
                                console.error(error);
                            }, function () {
                                console.log('lockFunds Transaction Confirmed');
                            });
                            transactionHttp.announce(lockFundsSigned);
                        });
                    })];
            });
        });
    };
    AnnounceTransaction.announceCosignature = function (transactionHash, privateKey, apiNode, networkType) {
        if (networkType === void 0) { networkType = tsjs_xpx_chain_sdk_1.NetworkType.TEST_NET; }
        return __awaiter(this, void 0, void 0, function () {
            var sender, accountHttp, queryParams, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sender = tsjs_xpx_chain_sdk_1.Account.createFromPrivateKey(privateKey, networkType);
                        accountHttp = new tsjs_xpx_chain_sdk_1.AccountHttp(apiNode);
                        return [4 /*yield*/, this.getCosignatureTransaction(accountHttp, sender.publicAccount, queryParams, transactionHash)];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2:
                        if (false) {}
                        if (!(result instanceof tsjs_xpx_chain_sdk_1.AggregateTransaction)) return [3 /*break*/, 3];
                        console.log("da tim thay");
                        return [2 /*return*/, this.sendCosignatureTransaction(result, privateKey, apiNode, networkType)];
                    case 3:
                        if (!((typeof result) == 'string')) return [3 /*break*/, 5];
                        console.log("chua tim thay");
                        console.log(result);
                        queryParams = new tsjs_xpx_chain_sdk_1.QueryParams(10, result);
                        return [4 /*yield*/, this.getCosignatureTransaction(accountHttp, sender.publicAccount, queryParams, transactionHash)];
                    case 4:
                        result = _a.sent();
                        return [3 /*break*/, 6];
                    case 5: return [2 /*return*/, false];
                    case 6: return [3 /*break*/, 2];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    AnnounceTransaction.getCosignatureTransaction = function (accountHttp, publicAccount, queryParams, transactionHash) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("getCosignatureTransaction");
                return [2 /*return*/, new Promise(function (resolve) {
                        accountHttp.aggregateBondedTransactions(publicAccount, queryParams).subscribe(function (tx) {
                            console.log("all transactions");
                            console.log(tx);
                            console.log("hash: " + transactionHash);
                            for (var i = 0; i < tx.length; i++) {
                                var transaction = tx[i];
                                if (transaction.transactionInfo && transaction.transactionInfo.hash == transactionHash) {
                                    console.log("da vao day roi ma");
                                    resolve(transaction);
                                    break;
                                }
                                else
                                    console.log("khong phai roi");
                            }
                            if (tx.length > 0) {
                                var transactionLast = tx[tx.length - 1];
                                if (transactionLast.transactionInfo) {
                                    resolve(transactionLast.transactionInfo.hash);
                                }
                            }
                            else
                                resolve(null);
                        }, function (error) {
                            resolve(false);
                            console.log(error);
                        });
                    })];
            });
        });
    };
    AnnounceTransaction.sendCosignatureTransaction = function (transaction, privateKey, apiNode, networkType) {
        return __awaiter(this, void 0, void 0, function () {
            var transactionHttp, wsAddress, listener, sender, tx, signedTx;
            return __generator(this, function (_a) {
                transactionHttp = new tsjs_xpx_chain_sdk_1.TransactionHttp(apiNode);
                wsAddress = apiNode.replace('https', 'wss').replace('http', 'ws');
                listener = new tsjs_xpx_chain_sdk_1.Listener(wsAddress, WebSocket);
                sender = tsjs_xpx_chain_sdk_1.Account.createFromPrivateKey(privateKey, networkType);
                tx = tsjs_xpx_chain_sdk_1.CosignatureTransaction.create(transaction);
                signedTx = sender.signCosignatureTransaction(tx);
                console.log("hash: " + signedTx.parentHash);
                return [2 /*return*/, new Promise(function (resolve) {
                        listener.open().then(function () {
                            var subscription = listener.cosignatureAdded(sender.address).subscribe(function (cosinature) {
                                console.log("met r nha");
                                if (cosinature && cosinature.parentHash === signedTx.parentHash) {
                                    console.log('cosinature: ' + JSON.stringify(cosinature));
                                    resolve(true);
                                    subscription.unsubscribe();
                                    listener.close();
                                }
                            }, function (error) {
                                resolve(false);
                                console.error(error);
                            }, function () {
                                console.log('done...');
                            });
                            transactionHttp.announceAggregateBondedCosignature(signedTx);
                        });
                    })];
            });
        });
    };
    AnnounceTransaction.getGenerationHash = function (apiNode) {
        return __awaiter(this, void 0, void 0, function () {
            var networkHttp, blockHttp;
            return __generator(this, function (_a) {
                networkHttp = new tsjs_xpx_chain_sdk_1.NetworkHttp(apiNode);
                blockHttp = new tsjs_xpx_chain_sdk_1.BlockHttp(apiNode, networkHttp);
                return [2 /*return*/, new Promise(function (resolve, rejects) {
                        blockHttp.getBlockByHeight(1).subscribe(function (blockInfo) {
                            resolve(blockInfo.generationHash);
                        }, function (error) {
                            console.log(error);
                        }, function () {
                            console.log("Done");
                        });
                    })];
            });
        });
    };
    return AnnounceTransaction;
}());
exports.AnnounceTransaction = AnnounceTransaction;


/***/ }),

/***/ "../sirius-id-sdk/dist/service/ApiNode.js":
/*!************************************************!*\
  !*** ../sirius-id-sdk/dist/service/ApiNode.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ApiNode = /** @class */ (function () {
    function ApiNode() {
    }
    return ApiNode;
}());
exports.ApiNode = ApiNode;


/***/ }),

/***/ "../sirius-id-sdk/dist/service/Bip39.js":
/*!**********************************************!*\
  !*** ../sirius-id-sdk/dist/service/Bip39.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var bip39 = __webpack_require__(/*! bip39 */ "../sirius-id-sdk/node_modules/bip39/src/index.js");
var Bip39 = /** @class */ (function () {
    function Bip39() {
    }
    Bip39.EntropyToMnemonic = function (privateKey) {
        return bip39.entropyToMnemonic(privateKey);
    };
    Bip39.MnemonicToEntropy = function (passPhrase) {
        return bip39.mnemonicToEntropy(passPhrase);
    };
    return Bip39;
}());
exports.Bip39 = Bip39;


/***/ }),

/***/ "../sirius-id-sdk/dist/service/CompressData.js":
/*!*****************************************************!*\
  !*** ../sirius-id-sdk/dist/service/CompressData.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(Buffer) {
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var zlib = __webpack_require__(/*! zlib */ "./node_modules/browserify-zlib/lib/index.js");
var CompressData = /** @class */ (function () {
    function CompressData() {
    }
    CompressData.compress = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        zlib.deflate(data, function (err, buffer) {
                            if (!err) {
                                console.log(buffer.toString('base64'));
                                resolve(buffer.toString('base64'));
                            }
                            else {
                                console.log(err);
                            }
                        });
                    })];
            });
        });
    };
    CompressData.decompress = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var buffer;
            return __generator(this, function (_a) {
                buffer = Buffer.from(data, 'base64');
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        zlib.unzip(buffer, function (err, buffer) {
                            if (!err) {
                                console.log(buffer.toString());
                                resolve(buffer.toString());
                            }
                            else {
                                console.log(err);
                            }
                        });
                    })];
            });
        });
    };
    return CompressData;
}());
exports.CompressData = CompressData;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../sirius-livewall---frontend/node_modules/buffer/index.js */ "./node_modules/buffer/index.js").Buffer))

/***/ }),

/***/ "../sirius-id-sdk/dist/service/LoginTxResponse.js":
/*!********************************************************!*\
  !*** ../sirius-id-sdk/dist/service/LoginTxResponse.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var SiriusIDTransactionMessage_1 = __webpack_require__(/*! ../message/SiriusIDTransactionMessage */ "../sirius-id-sdk/dist/message/SiriusIDTransactionMessage.js");
var tsjs_xpx_chain_sdk_1 = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "../sirius-id-sdk/node_modules/tsjs-xpx-chain-sdk/dist/index.js");
var LoginTxResponse = /** @class */ (function () {
    function LoginTxResponse() {
    }
    //SiriusID announce a transfer transaction to dApp with response Login message after scan QRcode or get data from URL deeplink
    //The session Token is encrypted by function encrypt(sesstion);
    LoginTxResponse.message = function (Sender, apiNode, networkType, scannedCode) {
        var _this = this;
        // check form of api address 
        this.checkApiAddress(apiNode);
        var sender = tsjs_xpx_chain_sdk_1.Account.createFromPrivateKey(Sender, networkType);
        //const JSONscannedCode = JSON.parse(scannedCode)
        var recipient = tsjs_xpx_chain_sdk_1.Address.createFromPublicKey(scannedCode.message.payload.appPublicKey, networkType);
        var responseMessage = SiriusIDTransactionMessage_1.SiriusIDTransactionMessage.create(scannedCode.message.payload.sessionToken, scannedCode.message.payload.credentials);
        var wsAddress = apiNode.replace('https', 'wss').replace('http', 'ws');
        var listener = new tsjs_xpx_chain_sdk_1.Listener(wsAddress, WebSocket);
        var transactionHttp = new tsjs_xpx_chain_sdk_1.TransactionHttp(apiNode);
        var publicAccountRecipient = tsjs_xpx_chain_sdk_1.PublicAccount.createFromPublicKey(scannedCode.message.payload.appPublicKey, networkType);
        var encryptedMessage = tsjs_xpx_chain_sdk_1.EncryptedMessage.create(responseMessage.toString(), publicAccountRecipient, Sender);
        //transfer transaction contains data message 
        var tx = tsjs_xpx_chain_sdk_1.TransferTransaction.create(tsjs_xpx_chain_sdk_1.Deadline.create(), recipient, [], 
        //PlainMessage.create(responseMessage.toString()),
        encryptedMessage, networkType, new tsjs_xpx_chain_sdk_1.UInt64([0, 0]));
        return new Promise(function (resolve) {
            console.log('get network generation hash');
            var blockHttp = new tsjs_xpx_chain_sdk_1.BlockHttp(apiNode);
            // get network generation hash of node
            blockHttp.getBlockByHeight(1).subscribe(function (blockinfo) {
                var networkgenegationHash = blockinfo.generationHash;
                var signedTx = sender.sign(tx, networkgenegationHash);
                console.log(signedTx.hash);
                var checkTimeout = setTimeout(function () {
                    console.log("da qua 1 phut, check thu trang thai cua ma hash: " + signedTx.hash);
                    transactionHttp.getTransactionStatus(signedTx.hash).subscribe(function (result) {
                        console.log("co ket qua cua ma hash: " + signedTx.hash);
                        console.log(result);
                        if (result.status != "Success") {
                            clearTimeout(checkTimeout);
                            _this.successTx = false;
                            resolve(_this.successTx);
                        }
                    }, function (error) {
                        resolve(false);
                    }, function () {
                        console.log("donne");
                    });
                }, 60000);
                listener.open().then(function () {
                    // listen confirm transaction from recipient
                    var subscription = listener.confirmed(recipient).subscribe(function (confirmed) {
                        if (confirmed && confirmed.transactionInfo && confirmed.transactionInfo.hash === signedTx.hash) {
                            console.log('confirmed: ' + JSON.stringify(confirmed));
                            _this.successTx = true;
                            resolve(_this.successTx);
                            subscription.unsubscribe();
                            listener.close();
                        }
                    }, function (error) {
                        _this.successTx = false;
                        resolve(_this.successTx);
                        console.error(error);
                    }, function () {
                        console.log('done.');
                    });
                    // announ transfer transaction
                    transactionHttp.announce(signedTx);
                });
            });
        });
    };
    LoginTxResponse.encrypt = function (sessionToken) {
        return sessionToken;
    };
    LoginTxResponse.checkApiAddress = function (apiNode) {
        var urlPattern = RegExp(/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/igm);
        if (!urlPattern.test(apiNode))
            throw new Error('Invalid_Api_Node_Address');
    };
    return LoginTxResponse;
}());
exports.LoginTxResponse = LoginTxResponse;


/***/ }),

/***/ "../sirius-id-sdk/dist/service/MessageReceived.js":
/*!********************************************************!*\
  !*** ../sirius-id-sdk/dist/service/MessageReceived.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var MessageType_1 = __webpack_require__(/*! ../message/MessageType */ "../sirius-id-sdk/dist/message/MessageType.js");
var LoginRequestMessage_1 = __webpack_require__(/*! ../message/LoginRequestMessage */ "../sirius-id-sdk/dist/message/LoginRequestMessage.js");
var TransactionRequestMessage_1 = __webpack_require__(/*! ../message/TransactionRequestMessage */ "../sirius-id-sdk/dist/message/TransactionRequestMessage.js");
var CredentialRequestMessage_1 = __webpack_require__(/*! ../message/CredentialRequestMessage */ "../sirius-id-sdk/dist/message/CredentialRequestMessage.js");
var CredentialConfirmMessage_1 = __webpack_require__(/*! ../message/CredentialConfirmMessage */ "../sirius-id-sdk/dist/message/CredentialConfirmMessage.js");
var VerifyRequestMessage_1 = __webpack_require__(/*! ../message/VerifyRequestMessage */ "../sirius-id-sdk/dist/message/VerifyRequestMessage.js");
var MessageReceived = /** @class */ (function () {
    function MessageReceived(messageReceived) {
        var messageObj = JSON.parse(messageReceived);
        var result = null;
        switch (messageObj.type) {
            case MessageType_1.MessageType.LOG_IN:
                result = new LoginRequestMessage_1.LoginRequestMessage(messageObj.payload.appPublicKey, messageObj.payload.sessionToken, messageObj.payload.credentials);
                break;
            case MessageType_1.MessageType.TRANSACTION:
                if (messageObj.payload.transaction) {
                    result = new TransactionRequestMessage_1.TransactionRequestMessage(messageObj.payload.message, messageObj.payload.transaction.transaction, messageObj.payload.transactionHash);
                }
                else
                    result = new TransactionRequestMessage_1.TransactionRequestMessage(messageObj.payload.message, null, messageObj.payload.transactionHash);
                break;
            case MessageType_1.MessageType.CREDENTIAL:
                result = new CredentialRequestMessage_1.CredentialRequestMessage(messageObj.payload.credentials);
                break;
            case MessageType_1.MessageType.CREDENTIAL_REQ:
                result = new CredentialConfirmMessage_1.CredentialConfirmMessage(messageObj.payload.appPublicKey, messageObj.payload.sessionToken, messageObj.payload.credential);
                break;
            case MessageType_1.MessageType.VERIFY_HARD:
                result = new VerifyRequestMessage_1.VerifyRequestMessage(messageObj.payload.appPublicKey, messageObj.payload.sessionToken, messageObj.payload.credentials, MessageType_1.MessageType.VERIFY_HARD);
                break;
            case MessageType_1.MessageType.VERIFY_LIGHT:
                result = new VerifyRequestMessage_1.VerifyRequestMessage(messageObj.payload.appPublicKey, messageObj.payload.sessionToken, messageObj.payload.credentials, MessageType_1.MessageType.VERIFY_LIGHT, messageObj.payload.url);
                break;
            case MessageType_1.MessageType.VERIFY_BOTH:
                result = new VerifyRequestMessage_1.VerifyRequestMessage(messageObj.payload.appPublicKey, messageObj.payload.sessionToken, messageObj.payload.credentials, MessageType_1.MessageType.VERIFY_BOTH, messageObj.payload.url);
                break;
            // case MessageType.CREDENTIAL_RES:
            //     result = new CredentialRequestMessage(messageObj.type,messageObj.payload.credential);
            //     break;
        }
        this.message = result;
    }
    return MessageReceived;
}());
exports.MessageReceived = MessageReceived;


/***/ }),

/***/ "../sirius-id-sdk/dist/service/VerifytoConfirmCredential.js":
/*!******************************************************************!*\
  !*** ../sirius-id-sdk/dist/service/VerifytoConfirmCredential.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tsjs_xpx_chain_sdk_1 = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "../sirius-id-sdk/node_modules/tsjs-xpx-chain-sdk/dist/index.js");
var Credentials_1 = __webpack_require__(/*! ../message/Credentials */ "../sirius-id-sdk/dist/message/Credentials.js");
var ApiNode_1 = __webpack_require__(/*! ./ApiNode */ "../sirius-id-sdk/dist/service/ApiNode.js");
var VerifytoConfirmCredential = /** @class */ (function () {
    function VerifytoConfirmCredential(transaction, sessionToken, credential, privateKey) {
        var _a, _b;
        // Compare between sessionToken generate by dApp and encryptedSessionToken sent from SiriusID to verify
        this.success = false;
        var transferTx = transaction;
        var dAppAccount = tsjs_xpx_chain_sdk_1.Account.createFromPrivateKey(privateKey, ApiNode_1.ApiNode.networkType);
        if (transferTx.message && ((_a = transferTx.signer) === null || _a === void 0 ? void 0 : _a.publicKey) != dAppAccount.publicKey) { //don't listen tx was went from itself
            var transferTxMessage = tsjs_xpx_chain_sdk_1.EncryptedMessage.createFromPayload(transferTx.message.payload);
            var plainMessage = tsjs_xpx_chain_sdk_1.EncryptedMessage.decrypt(transferTxMessage, privateKey, transferTx.signer);
            //them buoc giai nen o dau day ne
            var JSONtransferTxmessage = JSON.parse(plainMessage.payload);
            VerifytoConfirmCredential.message = JSONtransferTxmessage;
            var credentialsUser = JSONtransferTxmessage.payload.credential;
            var step1 = Credentials_1.Credentials.compareHashStr(credential.getContent(), credentialsUser.auth_owner, (_b = transferTx.signer) === null || _b === void 0 ? void 0 : _b.publicKey);
            if (step1 == true && sessionToken == JSONtransferTxmessage.payload.sessionToken) {
                credential.addAuthOwner(credentialsUser.auth_owner);
                if (JSON.stringify(credential) == JSON.stringify(credentialsUser)) {
                    this.success = true;
                }
                else
                    this.success = false;
            }
            else
                this.success = false;
        }
        else
            this.success = false;
    }
    VerifytoConfirmCredential.verify = function (transactionReceived, sessionToken, credential, privateKey) {
        var verify = new VerifytoConfirmCredential(transactionReceived, sessionToken, credential, privateKey);
        return verify.success;
    };
    VerifytoConfirmCredential.getMessage = function () {
        return VerifytoConfirmCredential.message;
    };
    return VerifytoConfirmCredential;
}());
exports.VerifytoConfirmCredential = VerifytoConfirmCredential;


/***/ }),

/***/ "../sirius-id-sdk/dist/service/VerifytoLogin.js":
/*!******************************************************!*\
  !*** ../sirius-id-sdk/dist/service/VerifytoLogin.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var tsjs_xpx_chain_sdk_1 = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "../sirius-id-sdk/node_modules/tsjs-xpx-chain-sdk/dist/index.js");
var CredentialStored_1 = __webpack_require__(/*! ../message/CredentialStored */ "../sirius-id-sdk/dist/message/CredentialStored.js");
var VerifytoLogin = /** @class */ (function () {
    function VerifytoLogin(transaction, sessionToken, credentials, privateKey) {
        // const transferTx = transaction as TransferTransaction;
        // // const transferTxMessage = transferTx.message.payload;
        // const senderPublicKey = transferTx.signer?.publicKey;
        // const transferTxMessage = EncryptedMessage.createFromPayload(transferTx.message.payload);
        // const plainMessage = EncryptedMessage.decrypt(transferTxMessage,privateKey,<PublicAccount>transferTx.signer);
        // Compare between sessionToken generate by dApp and encryptedSessionToken sent from SiriusID to verify
        this.success = false;
        // const JSONtransferTxmessage = JSON.parse(plainMessage.payload);
        // VerifytoLogin.message = JSONtransferTxmessage;
        // let credentialsReceived = new Array();
        // for (let i=0;i<JSONtransferTxmessage.payload.credentials;i++){
        //     let el = JSONtransferTxmessage.payload.credentials[i] as CredentialStored;
        //     let credential = await el.getCredential(el.credentialHash,el.keyDecrypt,<string>senderPublicKey);
        //     credentialsReceived.push(credential);
        // }
        // let credentials_req = this.intersecArray(JSONtransferTxmessage.payload.credentials,credentials);
        // //Check if message received is a LOGIN message
        // if(JSONtransferTxmessage.messageType == 1){
        //     console.log('This is login response message');
        //     if (sessionToken == JSONtransferTxmessage.payload.encryptedSessionToken
        //         && credentials_req.length == credentials.length
        //     ){
        //         console.log("Login successssss");
        //         this.success = true;
        //     }
        //     else{
        //         console.log("Login faileddddd");
        //     }
        // }
    }
    VerifytoLogin.verify = function (transactionReceived, sessionToken, credentials, privateKey) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var success, transferTx, senderPublicKey, transferTxMessage, plainMessage, JSONtransferTxmessage, credentialsReceived, i, el, credential, credentials_req;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        success = false;
                        transferTx = transactionReceived;
                        senderPublicKey = (_a = transferTx.signer) === null || _a === void 0 ? void 0 : _a.publicKey;
                        transferTxMessage = tsjs_xpx_chain_sdk_1.EncryptedMessage.createFromPayload(transferTx.message.payload);
                        plainMessage = tsjs_xpx_chain_sdk_1.EncryptedMessage.decrypt(transferTxMessage, privateKey, transferTx.signer);
                        JSONtransferTxmessage = JSON.parse(plainMessage.payload);
                        console.log(JSONtransferTxmessage);
                        VerifytoLogin.message = JSONtransferTxmessage;
                        credentialsReceived = new Array();
                        i = 0;
                        _b.label = 1;
                    case 1:
                        if (!(i < JSONtransferTxmessage.payload.credentials.length)) return [3 /*break*/, 4];
                        el = new CredentialStored_1.CredentialStored(JSONtransferTxmessage.payload.credentials[i]['keyDecrypt'], JSONtransferTxmessage.payload.credentials[i]['credentialHash']);
                        console.log(el);
                        return [4 /*yield*/, el.getCredential(el.credentialHash, el.keyDecrypt, senderPublicKey)];
                    case 2:
                        credential = _b.sent();
                        credentialsReceived.push(credential);
                        _b.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4:
                        console.log("<<<<<<<<<<<");
                        console.log(credentialsReceived);
                        credentials_req = this.intersecArray(credentialsReceived, credentials);
                        //Check if message received is a LOGIN message
                        if (JSONtransferTxmessage.messageType == 1) {
                            console.log('This is login response message');
                            if (sessionToken == JSONtransferTxmessage.payload.encryptedSessionToken
                                && credentials_req.length == credentials.length) {
                                VerifytoLogin.credentials = credentialsReceived;
                                console.log("Login successssss");
                                success = true;
                            }
                            else {
                                console.log("Login faileddddd");
                            }
                        }
                        // const verify = new VerifytoLogin(transactionReceived, sessionToken,credentials, privateKey);
                        return [2 /*return*/, success];
                }
            });
        });
    };
    VerifytoLogin.intersecArray = function (arr1, arr2) {
        return arr1.filter(function (element1) { return arr2.includes(element1['id']); });
    };
    VerifytoLogin.getMessage = function () {
        return VerifytoLogin.message;
    };
    return VerifytoLogin;
}());
exports.VerifytoLogin = VerifytoLogin;


/***/ }),

/***/ "../sirius-id-sdk/dist/service/index.js":
/*!**********************************************!*\
  !*** ../sirius-id-sdk/dist/service/index.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./LoginTxResponse */ "../sirius-id-sdk/dist/service/LoginTxResponse.js"));
__export(__webpack_require__(/*! ./VerifytoLogin */ "../sirius-id-sdk/dist/service/VerifytoLogin.js"));
__export(__webpack_require__(/*! ./MessageReceived */ "../sirius-id-sdk/dist/service/MessageReceived.js"));
__export(__webpack_require__(/*! ./Bip39 */ "../sirius-id-sdk/dist/service/Bip39.js"));
__export(__webpack_require__(/*! ./AnnounceTransaction */ "../sirius-id-sdk/dist/service/AnnounceTransaction.js"));
__export(__webpack_require__(/*! ./CompressData */ "../sirius-id-sdk/dist/service/CompressData.js"));
__export(__webpack_require__(/*! ./VerifytoConfirmCredential */ "../sirius-id-sdk/dist/service/VerifytoConfirmCredential.js"));
__export(__webpack_require__(/*! ./ApiNode */ "../sirius-id-sdk/dist/service/ApiNode.js"));


/***/ }),

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-wrap\">\r\n  <app-loader></app-loader>\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/core/loader/loader.component.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/core/loader/loader.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [class.hidden]=\"!show\" class=\"spinner-wrapper\">\r\n  <div *ngIf=\"show\" class=\"loader\"></div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/core/navbar/navbar.component.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/core/navbar/navbar.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar\">\r\n  <div class=\"logo\" [routerLink]=\"['/home']\">\r\n    <img src=\"assets/images/logo.png\" alt=\"\" />\r\n  </div>\r\n\r\n  <div class=\"search\">\r\n    <form [formGroup]=\"searchForm\">\r\n      <i class=\"fa fa-search\"></i>\r\n      <input type=\"text\" placeholder=\"Search\" formControlName=\"name\" />\r\n    </form>\r\n  </div>\r\n  <div class=\"links\">\r\n    <div class=\"link\" routerLinkActive=\"active\" [routerLink]=\"['/home']\">Home</div>\r\n    <div class=\"link\" routerLinkActive=\"active\" [routerLink]=\"['/trends']\">Followings</div>\r\n    <div class=\"link\" routerLinkActive=\"active\" [routerLink]=\"['/trends']\">Trends</div>\r\n    <div class=\"link\" routerLinkActive=\"active\" [routerLink]=\"['/favorites']\">Favorites</div>\r\n    <div class=\"link\" routerLinkActive=\"active\" [routerLink]=\"['/boards']\">Boards</div>\r\n  </div>\r\n  <div class=\"icons\">\r\n    <!-- <div class=\"icon\">\r\n      <i class=\"fa fa-bell\" (click)=\"dropdownBellState = !dropdownBellState\"></i>\r\n      <div class=\"dropdown\" [ngClass]=\"{ 'active': dropdownBellState }\">\r\n        <div class=\"item\" [routerLink]=\"['/profile']\" (click)=\"dropdownBellState = !dropdownBellState\">Profile</div>\r\n        <div class=\"item\" (click)=\"onLogoutOut()\">Logout</div>\r\n      </div> -->\r\n    <div class=\"icon\">\r\n      <i\r\n        class=\"fa fa-bell\"\r\n        (click)=\"dropdownStateNotif = !dropdownStateNotif\"\r\n        (clickOutside)=\"closeDropdownNotif()\"\r\n      ></i>\r\n      <div class=\"dropdown notif\" [ngClass]=\"{ 'active': dropdownStateNotif }\">\r\n        <div\r\n          class=\"item\"\r\n          *ngFor=\"let notif of recentNotifications$ | async\"\r\n          [routerLink]=\"['/post/', notif.notifiable_id]\"\r\n        >\r\n          <small class=\"date\">\r\n            {{ notif.created_at | dateAgo }}\r\n          </small>\r\n          <img\r\n            height=\"30px\"\r\n            width=\"30px\"\r\n            style=\"border-radius: 10rem\"\r\n            class=\"mr-4\"\r\n            [src]=\"getImage(notif.sender.image)\"\r\n            alt=\"\"\r\n          />\r\n          {{ notif.message }}\r\n        </div>\r\n        <div *ngIf=\"recentNotifications$ | async as notif\">\r\n          <div\r\n            class=\"see_all\"\r\n            [routerLink]=\"['/notifications']\"\r\n            *ngIf=\"notif.length > 0\"\r\n            (click)=\"dropdownStateNotif = !dropdownStateNotif\"\r\n          >\r\n            See all\r\n          </div>\r\n          <div\r\n            class=\"no_notif\"\r\n            *ngIf=\"notif.length == 0\"\r\n            (click)=\"dropdownStateNotif = !dropdownStateNotif\"\r\n            [ngStyle]=\"{ 'cursor': 'auto' }\"\r\n          >\r\n            No notifications\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"icon\">\r\n      <i class=\"fa fa-heart\" (click)=\"dropdownFriendReqStateNotif = !dropdownFriendReqStateNotif\"></i>\r\n      <div class=\"dropdown notif\" [ngClass]=\"{ 'active': dropdownFriendReqStateNotif }\">\r\n        <div class=\"item\" *ngFor=\"let friend of friends$ | async\">\r\n          <img\r\n            height=\"30px\"\r\n            width=\"30px\"\r\n            style=\"border-radius: 10rem\"\r\n            class=\"mr-4\"\r\n            [src]=\"\r\n              friend.user.image !== null\r\n                ? getImage(friend.user.image)\r\n                : 'https://www.computerhope.com/jargon/g/guest-user.jpg'\r\n            \"\r\n            alt=\"\"\r\n          />\r\n          {{ friend.user.fname }} {{ friend.user.lname }}\r\n          <small class=\"date\" (click)=\"acceptRequest(friend.id)\">\r\n            Accept\r\n          </small>\r\n        </div>\r\n\r\n        <div *ngIf=\"friends$ | async as friend\">\r\n          <div\r\n            class=\"see_all\"\r\n            [routerLink]=\"['/requests']\"\r\n            *ngIf=\"friend.length > 0\"\r\n            (click)=\"dropdownFriendReqStateNotif = !dropdownFriendReqStateNotif\"\r\n          >\r\n            See all\r\n          </div>\r\n          <div\r\n            class=\"no_notif\"\r\n            *ngIf=\"friend.length == 0\"\r\n            (click)=\"dropdownFriendReqStateNotif = !dropdownFriendReqStateNotif\"\r\n            [ngStyle]=\"{ 'cursor': 'auto' }\"\r\n          >\r\n            No Requests\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"profile-shortcut\">\r\n      <img\r\n        [src]=\"profilePhoto ? getImage(profilePhoto) : 'assets/images/no-image.png'\"\r\n        alt=\"\"\r\n        (click)=\"dropdownState = !dropdownState\"\r\n        (clickOutside)=\"closeDropdownProfile()\"\r\n      />\r\n      <div class=\"dropdown\" [ngClass]=\"{ 'active': dropdownState }\">\r\n        <div class=\"item\" [routerLink]=\"['/profile']\" (click)=\"dropdownState = !dropdownState\">Profile</div>\r\n        <div class=\"item\" [routerLink]=\"['/wallet']\" (click)=\"dropdownState = !dropdownState\">Account</div>\r\n        <div class=\"item\" (click)=\"onLogoutOut()\">Logout</div>\r\n      </div>\r\n    </div>\r\n\r\n    <!-- <i class=\"fa fa-list-ul icon\"></i> -->\r\n  </div>\r\n</div>\r\n<button [routerLink]=\"['/post/new']\" class=\"create shortcut-button\" *ngIf=\"hasAccount == true\">+</button>\r\n<!-- <button class=\"info shortcut-button\">?</button> -->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"layout\" *ngIf=\"hasAccount\">\r\n  <div class=\"item\" *ngFor=\"let post of posts$ | async\">\r\n    <form [formGroup]=\"form\" (ngSubmit)=\"onPinPost(form, post.id)\">\r\n      <select class=\"boards\" formControlName=\"board_id\">\r\n        <option *ngFor=\"let board of boards$ | async\" [value]=\"board.id\">{{ board.name }}</option>\r\n        <option>\r\n          <button [routerLink]=\"['/post/new']\" class=\"button\" *ngIf=\"hasAccount == true\">+</button>\r\n        </option>\r\n      </select>\r\n      <button class=\"save\" type=\"submit\">Save</button>\r\n    </form>\r\n    <img *ngIf=\"post.type !== 'video'\" [src]=\"getUrl(post.image)\" class=\"avatar\" [routerLink]=\"['/post/', post.id]\" />\r\n    <div class=\"video-overlay\" *ngIf=\"post.type === 'video'\" [routerLink]=\"['/post/', post.id]\">\r\n      <video>\r\n        <source [src]=\"getUrl(post.image)\" />\r\n      </video>\r\n    </div>\r\n    <button class=\"add\" [routerLink]=\"['/boards']\">Add</button>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"wallet\" *ngIf=\"noAccount\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"accountCanvass\">\r\n        <div class=\"accountPanel\">\r\n          <img src=\"../../../assets/images/wallet_icon.png\" alt=\"wallet\" />\r\n          <p>Do you have ProximaX Wallet?</p>\r\n          <p class=\"modalFooter\">*Setup your wallet first to continue</p>\r\n          <div class=\"accountButton\">\r\n            <div class=\"create_button\" (click)=\"createWallet()\">\r\n              <button class=\"login-btn button\" type=\"submit\">\r\n                <i class=\"fa fa-plus\"></i>\r\n                Create Wallet\r\n              </button>\r\n            </div>\r\n            <div class=\"link_button\" (click)=\"linkWallet()\">\r\n              <button class=\"login-btn button\" type=\"submit\">\r\n                <i class=\"fa fa-link\"></i>\r\n                Link Wallet\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/private/private-layout.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/private/private-layout.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- With Navbar -->\r\n<app-navbar></app-navbar>\r\n<router-outlet></router-outlet>\r\n<!-- With Navbar -->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/public/public-layout.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/public/public-layout.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Without Navbar. Example: Login, Register etc...-->\r\n<router-outlet></router-outlet>\r\n<!-- Without Navbar. Example: Login, Register etc... -->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/modals/category-select/category-select.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/modals/category-select/category-select.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"qr-modal\">\n  <div class=\"qr-placeholder\">\n    <div class=\"modal-body\">\n      <div class=\"qrCanvass\">\n        <div class=\"qrPanel\">\n          <p class=\"modalTitle\">Select a Category</p>\n          <form novalidate [formGroup]=\"cat_form\" (ngSubmit)=\"submit()\">\n            <div *ngFor=\"let cat of categories$ | async\">\n              <label for=\"vehicle1\">{{ cat.name }}</label>\n              &nbsp;&nbsp;\n              <input type=\"checkbox\" class=\"input\" [value]=\"cat.id\" (change)=\"onCheckboxChange($event)\" />\n            </div>\n            <button type=\"submit\" class=\"google-btn button\">Submit Category</button>\n          </form>\n          &nbsp;\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"description\"></div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/modals/privatekey-export/privatekey-export.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/modals/privatekey-export/privatekey-export.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"qr-modal\">\n  <div class=\"qr-placeholder\">\n    <div class=\"modal-body\">\n      <div class=\"qrCanvass\">\n        <div class=\"qrPanel\">\n          <div class=\"modalImage\"><img src=\"../../../../assets/images/logo.png\" alt=\"wallet\" /></div>\n          <p class=\"modalTitle\">Export Private Key</p>\n        </div>\n      </div>\n    </div>\n    <div class=\"text-center\" style=\"padding-bottom:10px;\">\n      <button class=\"tip button\" (click)=\"exportAccount(account)\">\n        <i class=\"fa fa-qrcode\"></i>\n        Export Private Key\n      </button>\n    </div>\n  </div>\n  <div class=\"description\"></div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/modals/qr-code-id/qr-code-id.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/modals/qr-code-id/qr-code-id.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"qr-modal\">\n  <div class=\"qr-placeholder\">\n    <div class=\"modal-body\">\n      <div class=\"qrCanvass\">\n        <div class=\"qrPanel\">\n          <p class=\"modalTitle\">Sirius Id QR Code</p>\n          <div class=\"input-group\">\n            <input type=\"text\" class=\"form-control\" value=\"{{ userQrId }}\" readonly />\n          </div>\n          <ngx-qrcode [(qrc-value)]=\"userQrId\"></ngx-qrcode>\n          <p class=\"modalFooter\">Open your Imagerax Mobile to proceed</p>\n          <div class=\"modalImage\"><img src=\"../../../assets/images/wallet_icon.png\" alt=\"wallet\" /></div>\n        </div>\n      </div>\n    </div>\n    <!-- <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"modal.close('Save click')\">Close</button>\n      </div> -->\n  </div>\n  <div class=\"description\"></div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/modals/qr-modal/qr-modal.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/modals/qr-modal/qr-modal.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"qr-modal\">\r\n  <div class=\"qr-placeholder\">\r\n    <div class=\"modal-body\">\r\n      <div class=\"qrCanvass\">\r\n        <div class=\"qrPanel\">\r\n          <p class=\"modalTitle\">Scan Wallet Address</p>\r\n          <div class=\"input-group\">\r\n            <input type=\"text\" class=\"form-control\" value=\"{{ userWallet }}\" readonly />\r\n          </div>\r\n          <ngx-qrcode [(qrc-value)]=\"userWallet\"></ngx-qrcode>\r\n          <p class=\"modalFooter\">Open your ProximaX Mobile Wallet to proceed</p>\r\n          <div class=\"modalImage\"><img src=\"../../../assets/images/wallet_icon.png\" alt=\"wallet\" /></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- <div class=\"modal-footer\">\r\n      <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"modal.close('Save click')\">Close</button>\r\n    </div> -->\r\n  </div>\r\n  <div class=\"description\"></div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"qr-modal\">\n  <div class=\"qr-placeholder\">\n    <div class=\"modal-body\">\n      <div class=\"qrCanvass\">\n        <div class=\"qrPanel\">\n          <div class=\"modalImage\"><img src=\"../../../../assets/images/logo.png\" alt=\"wallet\" /></div>\n          <p class=\"modalTitle\">Link Account</p>\n          <div class=\"input-group\">\n            <input type=\"text\" class=\"form-control\" value=\"{{ wallet }}\" readonly />\n          </div>\n          <ngx-qrcode [(qrc-value)]=\"wallet\"></ngx-qrcode>\n          <p class=\"modalFooter\">Open your ProximaX Mobile Wallet to proceed</p>\n        </div>\n      </div>\n    </div>\n    <div class=\"text-center\">\n      <button class=\"tip button\" (click)=\"loginRequestAndVerify()\">\n        <i class=\"fa fa-qrcode\"></i>\n        Link Account\n      </button>\n    </div>\n  </div>\n  <div class=\"description\"></div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/modals/tip-modal/tip-modal.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/modals/tip-modal/tip-modal.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form novalidate=\"\" [formGroup]=\"form\" (ngSubmit)=\"generateQr()\">\r\n  <div class=\"tip-modal\">\r\n    <div class=\"text-center title\">Send tip</div>\r\n    <!-- <div class=\"row mt2\">\r\n      <div class=\"col-md-6\">\r\n        <span class=\"balance\">Balance:</span>\r\n        <span class=\"balance-value\">0 XPX</span>\r\n      </div>\r\n      <div class=\"col-md-6 text-center\">\r\n        <button class=\"qr button\" (click)=\"openQRModal()\">\r\n          <img src=\"assets/images/qr_code.png\" alt=\"\" />\r\n          Show QR Code\r\n        </button>\r\n      </div>\r\n    </div> -->\r\n    <div class=\"mt1 text-center\">\r\n      <img src=\"assets/images/treasure_chest.png\" class=\"treasure-img\" alt=\"\" />\r\n      <div class=\"tip-amount mx-auto\">\r\n        <input type=\"number\" formControlName=\"amount\" (input)=\"changeInputTipValue($event.target.value)\" />\r\n      </div>\r\n    </div>\r\n    <div class=\"slider-wrapper\">\r\n      <input type=\"range\" id=\"slider\" class=\"slider\" #slider formControlName=\"tip\" />\r\n      <div class=\"circle1\" id=\"circle1\" #circle1></div>\r\n      <div class=\"circle2\" id=\"circle2\" #circle2></div>\r\n      <div class=\"circle3\" id=\"circle3\" #circle3></div>\r\n      <div class=\"circle4\" id=\"circle4\" #circle4></div>\r\n      <div class=\"circle5\" id=\"circle5\" #circle5></div>\r\n    </div>\r\n\r\n    <input type=\"text\" class=\"comment\" placeholder=\"Write a comment...\" formControlName=\"message\" #message>\r\n\r\n    <div class=\"text-center\">\r\n      <button class=\"tip button\">\r\n        <i class=\"fa fa-qrcode\"></i>\r\n        Show QR Code\r\n      </button>\r\n    </div>\r\n    <div class=\"info text-center\">\r\n      Your tip will be sent to your favorite artist.\r\n    </div>\r\n  </div>\r\n</form>"

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _layouts_public_public_layout_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./layouts/public/public-layout.component */ "./src/app/layouts/public/public-layout.component.ts");
/* harmony import */ var _layouts_private_private_layout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layouts/private/private-layout.component */ "./src/app/layouts/private/private-layout.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _shared_routes_private_layout_routes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/routes/private-layout.routes */ "./src/app/shared/routes/private-layout.routes.ts");
/* harmony import */ var _shared_routes_public_layout_routes__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shared/routes/public-layout.routes */ "./src/app/shared/routes/public-layout.routes.ts");
/* harmony import */ var _app_core_guards_auth_guard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app/core/guards/auth-guard.service */ "./src/app/core/guards/auth-guard.service.ts");








const appRoutes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
    },
    {
        path: '',
        component: _layouts_private_private_layout_component__WEBPACK_IMPORTED_MODULE_2__["PrivateLayoutComponent"],
        children: _shared_routes_private_layout_routes__WEBPACK_IMPORTED_MODULE_5__["privateRoutes"],
        canActivate: [_app_core_guards_auth_guard_service__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    { path: '', component: _layouts_public_public_layout_component__WEBPACK_IMPORTED_MODULE_1__["PublicLayoutComponent"], children: _shared_routes_public_layout_routes__WEBPACK_IMPORTED_MODULE_6__["publicRoutes"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(appRoutes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_4__["PreloadAllModules"] })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");



// NGXS

let AppComponent = class AppComponent {
    constructor(router, store) {
        this.router = router;
        this.store = store;
        this.title = 'proxiwall';
    }
    ngOnInit() {
        this.router.events.subscribe(evt => {
            if (!(evt instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Store"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/shared.module */ "./src/app/shared/shared.module.ts");






let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]],
        imports: [_core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"]],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/auth/plugins/logout.plugin.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/plugins/logout.plugin.ts ***!
  \***********************************************/
/*! exports provided: logoutPlugin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logoutPlugin", function() { return logoutPlugin; });
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _store_auth_action__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../store/auth.action */ "./src/app/auth/store/auth.action.ts");


function logoutPlugin(state, action, next) {
    // Use the get action type helper to determine the type
    if (Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_0__["getActionTypeFromInstance"])(action) === _store_auth_action__WEBPACK_IMPORTED_MODULE_1__["Logout"].type) {
        // if we are a logout type, lets erase all the state
        state = {};
    }
    // return the next function with the empty state
    return next(state, action);
}


/***/ }),

/***/ "./src/app/auth/store/auth.action.ts":
/*!*******************************************!*\
  !*** ./src/app/auth/store/auth.action.ts ***!
  \*******************************************/
/*! exports provided: Login, Logout, LoginGoogle, LoginFacebook, SignUp, RequestResetPassword, ResetPassword, ReplaceProfilePhoto, SiriusRegister, LoginEmailToDb */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Login", function() { return Login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Logout", function() { return Logout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginGoogle", function() { return LoginGoogle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginFacebook", function() { return LoginFacebook; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUp", function() { return SignUp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestResetPassword", function() { return RequestResetPassword; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPassword", function() { return ResetPassword; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReplaceProfilePhoto", function() { return ReplaceProfilePhoto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiriusRegister", function() { return SiriusRegister; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginEmailToDb", function() { return LoginEmailToDb; });
class Login {
    constructor(payload) {
        this.payload = payload;
    }
}
Login.type = '[Login Page] LoginUser';
Login.ctorParameters = () => [
    { type: undefined }
];
class Logout {
}
Logout.type = '[Navbar] LogoutUser';
class LoginGoogle {
    constructor(payload) {
        this.payload = payload;
    }
}
LoginGoogle.type = '[Login Page] LoginWithGoogle';
LoginGoogle.ctorParameters = () => [
    { type: undefined }
];
class LoginFacebook {
    constructor(payload) {
        this.payload = payload;
    }
}
LoginFacebook.type = '[Login Page] LoginWithFacebook';
LoginFacebook.ctorParameters = () => [
    { type: undefined }
];
class SignUp {
    constructor(payload) {
        this.payload = payload;
    }
}
SignUp.type = '[Sign Up Page] SignUpUser';
SignUp.ctorParameters = () => [
    { type: undefined }
];
class RequestResetPassword {
    constructor(payload) {
        this.payload = payload;
    }
}
RequestResetPassword.type = '[Forgot Password Page] RequestResetPassword';
RequestResetPassword.ctorParameters = () => [
    { type: undefined }
];
class ResetPassword {
    constructor(payload) {
        this.payload = payload;
    }
}
ResetPassword.type = '[Reset Password Page] ResetPassword';
ResetPassword.ctorParameters = () => [
    { type: undefined }
];
class ReplaceProfilePhoto {
    constructor(payload) {
        this.payload = payload;
    }
}
ReplaceProfilePhoto.type = '[Edit Profile Page] ReplaceProfilePhoto';
ReplaceProfilePhoto.ctorParameters = () => [
    { type: String }
];
class SiriusRegister {
    constructor(payload) {
        this.payload = payload;
    }
}
SiriusRegister.type = '[ Sirius Register ] Sirius Register';
SiriusRegister.ctorParameters = () => [
    { type: undefined }
];
class LoginEmailToDb {
    constructor(payload) {
        this.payload = payload;
    }
}
LoginEmailToDb.type = '[ Email ] Email ';
LoginEmailToDb.ctorParameters = () => [
    { type: undefined }
];


/***/ }),

/***/ "./src/app/auth/store/auth.state.ts":
/*!******************************************!*\
  !*** ./src/app/auth/store/auth.state.ts ***!
  \******************************************/
/*! exports provided: AuthState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthState", function() { return AuthState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/core/models/auth.model */ "./src/app/core/models/auth.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _auth_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth.action */ "./src/app/auth/store/auth.action.ts");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);





// NGXS



// SweetAlert2

let AuthState = class AuthState {
    constructor(authService, store) {
        this.authService = authService;
        this.store = store;
    }
    static token(state) {
        return state.token;
    }
    static wallet_public_key(state) {
        return state.wallet_public_key;
    }
    static getUserId(state) {
        return state.id;
    }
    static getUserWalletAddress(state) {
        return state.wallet_address;
    }
    static getUserSessionToken(state) {
        return state.session_token;
    }
    static getUserWalletPublicKey(state) {
        return state.wallet_public_key;
    }
    static getUserWallet(state) {
        return state.wallet['name'];
    }
    static getProvider(state) {
        return state.provider;
    }
    static getProfilePhoto(state) {
        return state.profilePhoto;
    }
    loginFacebook({ patchState }, { payload }) {
        return this.authService.loginWithFacebook(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            patchState({
                token: result.token.access_token,
                id: result.profile.id,
                profilePhoto: result.profile.image,
                provider: result.profile.linked_social_accounts.provider_name,
                wallet_public_key: result.profile.wallet_public_key
            });
            this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/home']));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            patchState({
                token: null
            });
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    siriusRegister({ patchState }, { payload }) {
        return this.authService.siriusRegister(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            patchState({});
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            patchState({
                token: null
            });
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    loginGoogle({ patchState }, { payload }) {
        return this.authService.loginWithGoogle(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            patchState({
                token: result.token.access_token,
                id: result.profile.id,
                provider: result.profile.linked_social_accounts.provider_name,
                profilePhoto: result.profile.image
            });
            this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/home']));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            patchState({
                token: null
            });
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    loginEmailToDb({ patchState }, { payload }) {
        return this.authService.loginEmailToDb(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            patchState({
                token: result.token.access_token,
                id: result.profile.id,
                provider: null,
                profilePhoto: result.profile.image,
                wallet_address: result.profile.wallet_address,
                wallet_public_key: result.profile.wallet_public_key,
                wallet: JSON.parse(result.profile.wallet),
                session_token: result.profile.session_token
            });
            this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/home']));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    login({ patchState }, { payload }) {
        return this.authService.login(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            console.log(result);
            patchState({
                token: result.token.access_token,
                id: result.profile.id,
                provider: null,
                profilePhoto: result.profile.image,
                wallet_address: result.profile.wallet_address,
                wallet_public_key: result.profile.wallet_public_key,
                wallet: JSON.parse(result.profile.wallet),
                session_token: result.profile.session_token
            });
            this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/home']));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            patchState({
                token: null
            });
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    logout() {
        return this.authService.logout().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(() => {
            this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/login']));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    signUp({ patchState }, { payload }) {
        return this.authService.signUp(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            console.log(result);
            patchState({
                token: result.token.access_token
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            patchState({
                token: null
            });
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    requestResetPassword({}, { payload }) {
        return this.authService.requestForResetPassword(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Success', result.message, 'success').then(() => {
                this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/login']));
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    resetPassword({}, { payload }) {
        return this.authService.resetPassword(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Success', result.message, 'success').then(() => {
                this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_7__["Navigate"](['/login']));
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    replaceProfilePhoto({ patchState }, { payload }) {
        patchState({
            profilePhoto: payload
        });
    }
};
AuthState.ctorParameters = () => [
    { type: _app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["LoginFacebook"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["LoginFacebook"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "loginFacebook", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["SiriusRegister"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["SiriusRegister"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "siriusRegister", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["LoginGoogle"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["LoginGoogle"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "loginGoogle", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["LoginEmailToDb"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["LoginEmailToDb"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "loginEmailToDb", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["Login"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["Login"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "login", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["Logout"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "logout", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["SignUp"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["SignUp"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "signUp", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["RequestResetPassword"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["RequestResetPassword"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "requestResetPassword", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["ResetPassword"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["ResetPassword"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "resetPassword", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_auth_action__WEBPACK_IMPORTED_MODULE_6__["ReplaceProfilePhoto"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _auth_action__WEBPACK_IMPORTED_MODULE_6__["ReplaceProfilePhoto"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState.prototype, "replaceProfilePhoto", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__["AuthStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "token", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__["AuthStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "wallet_public_key", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__["AuthStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "getUserId", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__["AuthStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "getUserWalletAddress", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__["AuthStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "getUserSessionToken", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__["AuthStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "getUserWalletPublicKey", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__["AuthStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "getUserWallet", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "getProvider", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_auth_model__WEBPACK_IMPORTED_MODULE_2__["AuthStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], AuthState, "getProfilePhoto", null);
AuthState = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["State"])({
        name: 'auth',
        defaults: {
            token: null,
            id: null,
            profilePhoto: null,
            provider: null,
            wallet_address: null,
            wallet_public_key: null,
            wallet: null,
            session_token: null
        }
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
], AuthState);



/***/ }),

/***/ "./src/app/core/core.module.ts":
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/*! exports provided: COMPONENTS, CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COMPONENTS", function() { return COMPONENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _layouts_public_public_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../layouts/public/public-layout.component */ "./src/app/layouts/public/public-layout.component.ts");
/* harmony import */ var _layouts_private_private_layout_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../layouts/private/private-layout.component */ "./src/app/layouts/private/private-layout.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/core/navbar/navbar.component.ts");
/* harmony import */ var _interceptors_http_request_interceptor__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./interceptors/http-request.interceptor */ "./src/app/core/interceptors/http-request.interceptor.ts");
/* harmony import */ var _app_home_home_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @app/home/home.module */ "./src/app/home/home.module.ts");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _loader_loader_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./loader/loader.component */ "./src/app/core/loader/loader.component.ts");
/* harmony import */ var _angular_service_worker__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/service-worker */ "./node_modules/@angular/service-worker/fesm2015/service-worker.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _pipes_date_ago_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pipes/date-ago.pipe */ "./src/app/core/pipes/date-ago.pipe.ts");
/* harmony import */ var _directives_click_outside_directive__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./directives/click-outside.directive */ "./src/app/core/directives/click-outside.directive.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");
/* harmony import */ var _app_auth_plugins_logout_plugin__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @app/auth/plugins/logout.plugin */ "./src/app/auth/plugins/logout.plugin.ts");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var _ngxs_storage_plugin__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ngxs/storage-plugin */ "./node_modules/@ngxs/storage-plugin/fesm2015/ngxs-storage-plugin.js");
/* harmony import */ var _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @app/post/store/post.state */ "./src/app/post/store/post.state.ts");
/* harmony import */ var _app_notifications_store_notification_state__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @app/notifications/store/notification.state */ "./src/app/notifications/store/notification.state.ts");
/* harmony import */ var _app_profile_store_profile_state__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @app/profile/store/profile.state */ "./src/app/profile/store/profile.state.ts");
/* harmony import */ var _app_post_store_board_state__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @app/post/store/board.state */ "./src/app/post/store/board.state.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");

















// NGXS












const COMPONENTS = [
    _layouts_private_private_layout_component__WEBPACK_IMPORTED_MODULE_5__["PrivateLayoutComponent"],
    _layouts_public_public_layout_component__WEBPACK_IMPORTED_MODULE_4__["PublicLayoutComponent"],
    _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_7__["NavbarComponent"],
    _loader_loader_component__WEBPACK_IMPORTED_MODULE_11__["LoaderComponent"],
    _pipes_date_ago_pipe__WEBPACK_IMPORTED_MODULE_14__["DateAgoPipe"],
    _directives_click_outside_directive__WEBPACK_IMPORTED_MODULE_15__["ClickOutsideDirective"]
];
let CoreModule = class CoreModule {
};
CoreModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [COMPONENTS],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__["BrowserModule"],
            ngx_bootstrap__WEBPACK_IMPORTED_MODULE_26__["ModalModule"].forRoot(),
            _app_home_home_module__WEBPACK_IMPORTED_MODULE_9__["HomeModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
            _ngxs_store__WEBPACK_IMPORTED_MODULE_16__["NgxsModule"].forRoot([_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_19__["AuthState"], _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_21__["PostState"], _app_notifications_store_notification_state__WEBPACK_IMPORTED_MODULE_22__["NotificationState"], _app_profile_store_profile_state__WEBPACK_IMPORTED_MODULE_23__["ProfileState"], _app_post_store_board_state__WEBPACK_IMPORTED_MODULE_24__["BoardState"]]),
            _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_17__["NgxsRouterPluginModule"].forRoot(),
            // NgxsLoggerPluginModule.forRoot({
            //   disabled: environment.production
            // }),
            _ngxs_storage_plugin__WEBPACK_IMPORTED_MODULE_20__["NgxsStoragePluginModule"].forRoot({
                key: [
                    'auth.token',
                    'auth.id',
                    'auth.profilePhoto',
                    'auth.provider',
                    'auth.wallet_public_key',
                    'auth.session_token',
                    'auth.wallet'
                ]
            }),
            _angular_service_worker__WEBPACK_IMPORTED_MODULE_12__["ServiceWorkerModule"].register('ngsw-worker.js', { enabled: _env_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].production })
        ],
        exports: [COMPONENTS],
        providers: [
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_25__["BsModalService"],
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"], useClass: _interceptors_http_request_interceptor__WEBPACK_IMPORTED_MODULE_8__["HttpRequestInterceptor"], multi: true },
            {
                provide: _ngxs_store__WEBPACK_IMPORTED_MODULE_16__["NGXS_PLUGINS"],
                useValue: _app_auth_plugins_logout_plugin__WEBPACK_IMPORTED_MODULE_18__["logoutPlugin"],
                multi: true
            }
        ]
    })
], CoreModule);



/***/ }),

/***/ "./src/app/core/directives/click-outside.directive.ts":
/*!************************************************************!*\
  !*** ./src/app/core/directives/click-outside.directive.ts ***!
  \************************************************************/
/*! exports provided: ClickOutsideDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClickOutsideDirective", function() { return ClickOutsideDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ClickOutsideDirective = class ClickOutsideDirective {
    constructor(elementRef) {
        this.elementRef = elementRef;
        this.clickOutside = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    onDocumentClick(event) {
        const targetElement = event.target;
        // Check if the click was outside the element
        if (targetElement && !this.elementRef.nativeElement.contains(targetElement)) {
            this.clickOutside.emit(event);
        }
    }
};
ClickOutsideDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ClickOutsideDirective.prototype, "clickOutside", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event']),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [MouseEvent]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ClickOutsideDirective.prototype, "onDocumentClick", null);
ClickOutsideDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[clickOutside]'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
], ClickOutsideDirective);



/***/ }),

/***/ "./src/app/core/guards/auth-guard.service.ts":
/*!***************************************************!*\
  !*** ./src/app/core/guards/auth-guard.service.ts ***!
  \***************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _auth_store_auth_state__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");



// NGXS


let AuthGuard = class AuthGuard {
    constructor(store, router) {
        this.store = store;
        this.router = router;
    }
    canActivate() {
        const token = this.store.selectSnapshot(_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_4__["AuthState"].token);
        if (token == 'null' || !token || token == undefined) {
            this.router.navigate(['/login']);
            return false;
        }
        else {
            return true;
        }
    }
};
AuthGuard.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Store"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
];
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Store"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], AuthGuard);



/***/ }),

/***/ "./src/app/core/interceptors/http-request.interceptor.ts":
/*!***************************************************************!*\
  !*** ./src/app/core/interceptors/http-request.interceptor.ts ***!
  \***************************************************************/
/*! exports provided: HttpRequestInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpRequestInterceptor", function() { return HttpRequestInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/loader.service */ "./src/app/core/services/loader.service.ts");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _app_auth_store_auth_action__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @app/auth/store/auth.action */ "./src/app/auth/store/auth.action.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");







// NGXS



// Sweet Alert


let HttpRequestInterceptor = class HttpRequestInterceptor {
    constructor(router, store, loaderService) {
        this.router = router;
        this.store = store;
        this.loaderService = loaderService;
    }
    showLoader() {
        this.loaderService.show();
    }
    hideLoader() {
        this.loaderService.hide();
    }
    intercept(req, next) {
        const token = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_7__["AuthState"].token);
        if (token) {
            req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });
        }
        if (!req.headers.has('Content-Type')) {
            if (req.url == `${_env_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].url}/users/edit` || req.url == `${_env_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].url}/posts`) {
                delete req['Content-Type'];
            }
            else {
                req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
            }
        }
        this.showLoader();
        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((event) => {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]) {
                this.hideLoader();
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpErrorResponse"]) {
                this.hideLoader();
                if (err.status === 401) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a
                        .fire('Session Expired!', 'Sorry, your session has expired, you will be redirected to the login page', 'warning')
                        .then(() => {
                        this.store.dispatch(new _app_auth_store_auth_action__WEBPACK_IMPORTED_MODULE_9__["Logout"]());
                    });
                }
                // if (err.status === 500 && req.method === 'GET') {
                //   this.router.navigate(['error']);
                // }
                // if (!window.navigator.onLine) {
                //   this.router.navigate(['no-internet-connection']);
                // }
                if (err.status !== 401) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire('Error', 'Invalid Email/Password', 'error');
                }
                if (err.status == 400) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire('Error', 'Email Address Exist!', 'error');
                }
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
            }
        }));
    }
};
HttpRequestInterceptor.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_8__["Store"] },
    { type: _services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"] }
];
HttpRequestInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ngxs_store__WEBPACK_IMPORTED_MODULE_8__["Store"], _services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]])
], HttpRequestInterceptor);



/***/ }),

/***/ "./src/app/core/loader/loader.component.scss":
/*!***************************************************!*\
  !*** ./src/app/core/loader/loader.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hidden {\n  visibility: hidden;\n}\n\n.spinner-wrapper {\n  position: fixed;\n  z-index: 2000 !important;\n  width: 100%;\n  height: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  top: 0;\n  left: 0;\n  background-color: rgba(255, 255, 255, 0.5);\n  z-index: 998;\n}\n\n.loader {\n  border: 16px solid #f3f3f3;\n  border-radius: 50%;\n  border-top: 16px solid #f36621;\n  width: 100px;\n  height: 100px;\n  -webkit-animation: spin 2s linear infinite;\n  /* Safari */\n  animation: spin 2s linear infinite;\n}\n\n/* Safari */\n\n@-webkit-keyframes spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n  }\n}\n\n@keyframes spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9sb2FkZXIvQzpcXFVzZXJzXFxBU1VTXFxEZXNrdG9wXFxQcm9qZWN0XFxpbWFnZXJheC1zaXJpdXMtZnJvbnRlbmRcXHNpcml1cy1saXZld2FsbC0tLWZyb250ZW5kL3NyY1xcYXBwXFxjb3JlXFxsb2FkZXJcXGxvYWRlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29yZS9sb2FkZXIvbG9hZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7QUNDRjs7QURFQTtFQUNFLGVBQUE7RUFDQSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLDBDQUFBO0VBQ0EsWUFBQTtBQ0NGOztBREVBO0VBQ0UsMEJBQUE7RUFDQSxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSwwQ0FBQTtFQUE0QyxXQUFBO0VBQzVDLGtDQUFBO0FDRUY7O0FEQ0EsV0FBQTs7QUFDQTtFQUNFO0lBQ0UsK0JBQUE7RUNFRjtFREFBO0lBQ0UsaUNBQUE7RUNFRjtBQUNGOztBRENBO0VBQ0U7SUFDRSwrQkFBQTtZQUFBLHVCQUFBO0VDQ0Y7RURDQTtJQUNFLGlDQUFBO1lBQUEseUJBQUE7RUNDRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvY29yZS9sb2FkZXIvbG9hZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhpZGRlbiB7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG59XHJcblxyXG4uc3Bpbm5lci13cmFwcGVyIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgei1pbmRleDogMjAwMCAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xyXG4gIHotaW5kZXg6IDk5ODtcclxufVxyXG5cclxuLmxvYWRlciB7XHJcbiAgYm9yZGVyOiAxNnB4IHNvbGlkICNmM2YzZjM7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGJvcmRlci10b3A6IDE2cHggc29saWQgI2YzNjYyMTtcclxuICB3aWR0aDogMTAwcHg7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICAtd2Via2l0LWFuaW1hdGlvbjogc3BpbiAycyBsaW5lYXIgaW5maW5pdGU7IC8qIFNhZmFyaSAqL1xyXG4gIGFuaW1hdGlvbjogc3BpbiAycyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi8qIFNhZmFyaSAqL1xyXG5ALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7XHJcbiAgMCUge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICB9XHJcbiAgMTAwJSB7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XHJcbiAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIHNwaW4ge1xyXG4gIDAlIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gIH1cclxuICAxMDAlIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XHJcbiAgfVxyXG59XHJcbiIsIi5oaWRkZW4ge1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG59XG5cbi5zcGlubmVyLXdyYXBwZXIge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDIwMDAgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xuICB6LWluZGV4OiA5OTg7XG59XG5cbi5sb2FkZXIge1xuICBib3JkZXI6IDE2cHggc29saWQgI2YzZjNmMztcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXItdG9wOiAxNnB4IHNvbGlkICNmMzY2MjE7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgLXdlYmtpdC1hbmltYXRpb246IHNwaW4gMnMgbGluZWFyIGluZmluaXRlO1xuICAvKiBTYWZhcmkgKi9cbiAgYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTtcbn1cblxuLyogU2FmYXJpICovXG5ALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7XG4gIDAlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xuICB9XG4gIDEwMCUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbiAgfVxufVxuQGtleWZyYW1lcyBzcGluIHtcbiAgMCUge1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xuICB9XG4gIDEwMCUge1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/core/loader/loader.component.ts":
/*!*************************************************!*\
  !*** ./src/app/core/loader/loader.component.ts ***!
  \*************************************************/
/*! exports provided: LoaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderComponent", function() { return LoaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/loader.service */ "./src/app/core/services/loader.service.ts");



let LoaderComponent = class LoaderComponent {
    constructor(loaderService) {
        this.loaderService = loaderService;
        this.show = false;
    }
    ngOnInit() {
        this.subscription = this.loaderService.loaderState.subscribe((state) => {
            this.show = state.show;
        });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
};
LoaderComponent.ctorParameters = () => [
    { type: _services_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"] }
];
LoaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-loader',
        template: __webpack_require__(/*! raw-loader!./loader.component.html */ "./node_modules/raw-loader/index.js!./src/app/core/loader/loader.component.html"),
        styles: [__webpack_require__(/*! ./loader.component.scss */ "./src/app/core/loader/loader.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"]])
], LoaderComponent);



/***/ }),

/***/ "./src/app/core/models/auth.model.ts":
/*!*******************************************!*\
  !*** ./src/app/core/models/auth.model.ts ***!
  \*******************************************/
/*! exports provided: AuthStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthStateModel", function() { return AuthStateModel; });
class AuthStateModel {
}


/***/ }),

/***/ "./src/app/core/models/board.model.ts":
/*!********************************************!*\
  !*** ./src/app/core/models/board.model.ts ***!
  \********************************************/
/*! exports provided: BoardStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardStateModel", function() { return BoardStateModel; });
class BoardStateModel {
}


/***/ }),

/***/ "./src/app/core/models/categories.model.ts":
/*!*************************************************!*\
  !*** ./src/app/core/models/categories.model.ts ***!
  \*************************************************/
/*! exports provided: CategoriesStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesStateModel", function() { return CategoriesStateModel; });
class CategoriesStateModel {
}


/***/ }),

/***/ "./src/app/core/models/comment.model.ts":
/*!**********************************************!*\
  !*** ./src/app/core/models/comment.model.ts ***!
  \**********************************************/
/*! exports provided: Comment, CommentStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Comment", function() { return Comment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentStateModel", function() { return CommentStateModel; });
class Comment {
    constructor(comment) {
        this.id = comment.id;
        this.post_id = comment.post_id;
        this.comment = comment.comment;
    }
}
Comment.ctorParameters = () => [
    { type: Comment }
];
class CommentStateModel {
}


/***/ }),

/***/ "./src/app/core/models/country.model.ts":
/*!**********************************************!*\
  !*** ./src/app/core/models/country.model.ts ***!
  \**********************************************/
/*! exports provided: CountryStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryStateModel", function() { return CountryStateModel; });
class CountryStateModel {
}


/***/ }),

/***/ "./src/app/core/models/notification.model.ts":
/*!***************************************************!*\
  !*** ./src/app/core/models/notification.model.ts ***!
  \***************************************************/
/*! exports provided: NotificationStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationStateModel", function() { return NotificationStateModel; });
class NotificationStateModel {
}


/***/ }),

/***/ "./src/app/core/models/post.model.ts":
/*!*******************************************!*\
  !*** ./src/app/core/models/post.model.ts ***!
  \*******************************************/
/*! exports provided: PostStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostStateModel", function() { return PostStateModel; });
class PostStateModel {
}


/***/ }),

/***/ "./src/app/core/models/profile.model.ts":
/*!**********************************************!*\
  !*** ./src/app/core/models/profile.model.ts ***!
  \**********************************************/
/*! exports provided: ProfileStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileStateModel", function() { return ProfileStateModel; });
class ProfileStateModel {
}


/***/ }),

/***/ "./src/app/core/models/request.model.ts":
/*!**********************************************!*\
  !*** ./src/app/core/models/request.model.ts ***!
  \**********************************************/
/*! exports provided: RequestsStateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestsStateModel", function() { return RequestsStateModel; });
class RequestsStateModel {
}


/***/ }),

/***/ "./src/app/core/navbar/navbar.component.scss":
/*!***************************************************!*\
  !*** ./src/app/core/navbar/navbar.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#home-header {\n  position: fixed;\n  width: 100%;\n  background: transparent;\n  top: 0;\n  left: 0;\n  right: 0;\n  padding: 22px 0;\n  transition: all 0.5s ease-in-out;\n  -webkit-transition: all 0.5s ease-in-out;\n  z-index: 9999;\n}\n\na {\n  color: #fff;\n  text-decoration: none;\n  background-color: transparent;\n  -webkit-text-decoration-skip: objects;\n}\n\n.mobile_menu {\n  height: 60px;\n  align-items: center;\n}\n\n#home-header > .container {\n  height: auto;\n}\n\n.hometop-btn:hover ::ng-deep .mat-button-focus-overlay {\n  display: none;\n}\n\n#home-header:not(.header-fixed) .hometop-btn ::ng-deep .mat-button-ripple {\n  top: -22px;\n  bottom: -22px;\n}\n\n.header-fixed {\n  position: fixed !important;\n  left: 0;\n  right: 0;\n  top: 0;\n  padding: 0 !important;\n  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);\n  -webkit-animation: 0.6s slideDown forwards;\n          animation: 0.6s slideDown forwards;\n  transition: all 0.5s ease-in-out !important;\n}\n\n.header-fixed .container {\n  height: 60px !important;\n}\n\n.header-fixed .hometop-btn {\n  line-height: 60px !important;\n}\n\n.home-logo {\n  display: flex;\n  align-items: center;\n}\n\n.home-logo a {\n  display: inherit;\n}\n\n#menu-toggle {\n  display: none;\n  position: absolute;\n  right: 5px;\n  top: 10px;\n}\n\n.icon-button .mat-icon {\n  font-size: 20px;\n}\n\nnav ul {\n  padding: 0;\n  margin: 0;\n  list-style: none;\n  position: relative;\n}\n\nnav ul li {\n  display: inline-block;\n}\n\nnav ul li a {\n  display: block;\n  color: #fff;\n  cursor: pointer;\n}\n\nnav ul li ul {\n  text-align: left;\n}\n\nnav ul li ul li {\n  box-shadow: 0 1px 15px 1px rgba(0, 0, 0, 0.04), 0 1px 6px rgba(0, 0, 0, 0.04);\n  text-align: center;\n  padding: 10px;\n  cursor: pointer;\n}\n\nnav ul li ul li:first-child {\n  border-top-right-radius: 10px;\n  border-top-left-radius: 10px;\n}\n\nnav ul li ul li:last-child {\n  border-bottom-right-radius: 10px;\n  border-bottom-left-radius: 10px;\n}\n\nnav ul li ul li ul {\n  text-align: left;\n  background: transparent;\n}\n\n/* Hide Dropdowns by Default */\n\nnav ul ul {\n  display: none;\n  position: absolute;\n  top: 50px;\n  /* the height of the main nav */\n}\n\n/* Display Dropdowns on Hover */\n\nnav ul li:hover > ul {\n  display: inherit;\n}\n\n/* Fisrt Tier Dropdown */\n\nnav ul ul li {\n  width: 170px;\n  float: none;\n  display: list-item;\n  position: relative;\n}\n\n/* Second, Third and more Tiers\t*/\n\nnav ul ul ul li {\n  position: relative;\n  top: -50px;\n  left: 145px;\n}\n\n/* Change this in order to change the Dropdown symbol */\n\nli > a:after {\n  content: \" +\";\n}\n\nli > a:only-child:after {\n  content: \"\";\n}\n\n@media (max-width: 991px) {\n  #home-header {\n    padding: 0;\n  }\n\n  #home-header > .container {\n    height: 60px;\n    padding: 0;\n  }\n\n  .menu-opened:host .header-mobile-top {\n    padding: 13px 0;\n  }\n\n  #menu-toggle {\n    display: block;\n  }\n\n  .menu-opened:host .container {\n    height: auto !important;\n  }\n\n  .home-logo {\n    margin-left: 15px;\n  }\n\n  .top-menu {\n    display: none !important;\n  }\n\n  .menu-opened:host .top-menu {\n    display: block !important;\n    width: 100% !important;\n    flex: none !important;\n    background: #f3f3f3;\n  }\n\n  .menu-opened:host .top-menu button {\n    display: block;\n    width: 100%;\n    line-height: 56px !important;\n  }\n\n  .navbar-collapse {\n    flex-basis: 100%;\n    flex-grow: 1;\n    align-items: center;\n    margin-top: 0px;\n    text-align: center;\n  }\n\n  li.nav-item {\n    text-align: center;\n  }\n\n  nav ul ul {\n    display: none;\n    position: relative;\n    top: 10px;\n    /* the height of the main nav */\n  }\n\n  nav ul ul ul li {\n    position: relative;\n    top: 10px;\n    left: 5px;\n  }\n\n  nav ul li:hover > ul {\n    display: inline-block;\n  }\n}\n\n.navbar-toggler:hover,\n.navbar-toggler:focus {\n  text-decoration: none;\n  outline: none;\n}\n\nspan.navbar-toggler-icon .eva {\n  -webkit-text-fill-color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9uYXZiYXIvQzpcXFVzZXJzXFxBU1VTXFxEZXNrdG9wXFxQcm9qZWN0XFxpbWFnZXJheC1zaXJpdXMtZnJvbnRlbmRcXHNpcml1cy1saXZld2FsbC0tLWZyb250ZW5kL3NyY1xcYXBwXFxjb3JlXFxuYXZiYXJcXG5hdmJhci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29yZS9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLGVBQUE7RUFHQSxnQ0FBQTtFQUNBLHdDQUFBO0VBQ0EsYUFBQTtBQ0RKOztBRElBO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0VBQ0EsNkJBQUE7RUFDQSxxQ0FBQTtBQ0RKOztBRElBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0FDREo7O0FESUE7RUFDSSxZQUFBO0FDREo7O0FESUE7RUFDSSxhQUFBO0FDREo7O0FESUE7RUFDSSxVQUFBO0VBQ0EsYUFBQTtBQ0RKOztBRElBO0VBQ0ksMEJBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLE1BQUE7RUFPQSxxQkFBQTtFQUNBLHNFQUFBO0VBQ0EsMENBQUE7VUFBQSxrQ0FBQTtFQUNBLDJDQUFBO0FDUEo7O0FEVUE7RUFDSSx1QkFBQTtBQ1BKOztBRFVBO0VBQ0ksNEJBQUE7QUNQSjs7QURVQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQ1BKOztBRFFJO0VBQ0ksZ0JBQUE7QUNOUjs7QURVQTtFQUNJLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FDUEo7O0FEV0k7RUFDSSxlQUFBO0FDUlI7O0FEYUE7RUFDSSxVQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNWSjs7QURXSTtFQU1JLHFCQUFBO0FDZFI7O0FEU1E7RUFDSSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUNQWjs7QURVUTtFQUNJLGdCQUFBO0FDUlo7O0FEWVk7RUFHSSw2RUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7QUNaaEI7O0FEYWdCO0VBQ0ksNkJBQUE7RUFDQSw0QkFBQTtBQ1hwQjs7QURhZ0I7RUFDSSxnQ0FBQTtFQUNBLCtCQUFBO0FDWHBCOztBRGFnQjtFQUNJLGdCQUFBO0VBQ0EsdUJBQUE7QUNYcEI7O0FEc0JBLDhCQUFBOztBQUVBO0VBQ0ksYUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLCtCQUFBO0FDcEJKOztBRHdCQSwrQkFBQTs7QUFFQTtFQUNJLGdCQUFBO0FDdEJKOztBRDBCQSx3QkFBQTs7QUFFQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ3hCSjs7QUQ0QkEsaUNBQUE7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FDMUJKOztBRDhCQSx1REFBQTs7QUFFQTtFQUNJLGFBQUE7QUM1Qko7O0FEK0JBO0VBQ0ksV0FBQTtBQzVCSjs7QUQrQkE7RUFDSTtJQUNJLFVBQUE7RUM1Qk47O0VEOEJFO0lBQ0ksWUFBQTtJQUNBLFVBQUE7RUMzQk47O0VENkJFO0lBQ0ksZUFBQTtFQzFCTjs7RUQ0QkU7SUFDSSxjQUFBO0VDekJOOztFRDJCRTtJQUNJLHVCQUFBO0VDeEJOOztFRDBCRTtJQUNJLGlCQUFBO0VDdkJOOztFRHlCRTtJQUNJLHdCQUFBO0VDdEJOOztFRHdCRTtJQUNJLHlCQUFBO0lBQ0Esc0JBQUE7SUFDQSxxQkFBQTtJQUNBLG1CQUFBO0VDckJOOztFRHVCRTtJQUNJLGNBQUE7SUFDQSxXQUFBO0lBQ0EsNEJBQUE7RUNwQk47O0VEc0JFO0lBQ0ksZ0JBQUE7SUFDQSxZQUFBO0lBQ0EsbUJBQUE7SUFLQSxlQUFBO0lBQ0Esa0JBQUE7RUN2Qk47O0VEeUJFO0lBQ0ksa0JBQUE7RUN0Qk47O0VEd0JFO0lBQ0ksYUFBQTtJQUNBLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLCtCQUFBO0VDckJOOztFRHVCRTtJQUNJLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLFNBQUE7RUNwQk47O0VEc0JFO0lBQ0kscUJBQUE7RUNuQk47QUFDRjs7QURzQkE7O0VBRUkscUJBQUE7RUFDQSxhQUFBO0FDcEJKOztBRHVCQTtFQUNJLDZCQUFBO0FDcEJKIiwiZmlsZSI6InNyYy9hcHAvY29yZS9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2hvbWUtaGVhZGVyIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBwYWRkaW5nOiAyMnB4IDA7XHJcbiAgICAvLyB0cmFuc2l0aW9uOiBwYWRkaW5nIDAuM3MgbGluZWFyO1xyXG4gICAgLy8gLXdlYmtpdC10cmFuc2l0aW9uOiBwYWRkaW5nIDAuM3MgbGluZWFyO1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC41cyBlYXNlLWluLW91dDtcclxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC41cyBlYXNlLWluLW91dDtcclxuICAgIHotaW5kZXg6IDk5OTk7XHJcbn1cclxuXHJcbmEge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uLXNraXA6IG9iamVjdHM7XHJcbn1cclxuXHJcbi5tb2JpbGVfbWVudSB7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4jaG9tZS1oZWFkZXI+LmNvbnRhaW5lciB7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbn1cclxuXHJcbi5ob21ldG9wLWJ0bjpob3ZlciA6Om5nLWRlZXAgLm1hdC1idXR0b24tZm9jdXMtb3ZlcmxheSB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4jaG9tZS1oZWFkZXI6bm90KC5oZWFkZXItZml4ZWQpIC5ob21ldG9wLWJ0biA6Om5nLWRlZXAgLm1hdC1idXR0b24tcmlwcGxlIHtcclxuICAgIHRvcDogLTIycHg7XHJcbiAgICBib3R0b206IC0yMnB4O1xyXG59XHJcblxyXG4uaGVhZGVyLWZpeGVkIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZCAhaW1wb3J0YW50O1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgLy8gYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIC8vIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMjlkZWcsIHJnYmEoMTkzLCAxNSwgNjUsIDAuOCkgMCwgcmdiYSgzNSwgNTksIDEzNiwgMC44KSAxMDAlKSAhaW1wb3J0YW50O1xyXG4gICAgLy8gYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC0yOWRlZywgcmdiYSgxOTMsIDE1LCA2NSwgMC44NykgMCwgcmdiKDM1LCA1OSwgMTM2KSAxMDAlKSAhaW1wb3J0YW50O1xyXG4gICAgLy8gYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQoLTI5ZGVnLCByZ2JhKDE5MywgMTUsIDY1LCAwLjg3KSAwLCByZ2IoMzUsIDU5LCAxMzYpIDEwMCUpICFpbXBvcnRhbnQ7XHJcbiAgICAvLyBiYWNrZ3JvdW5kOiAtbXMtbGluZWFyLWdyYWRpZW50KC0yOWRlZywgcmdiYSgxOTMsIDE1LCA2NSwgMC44NykgMCwgcmdiKDM1LCA1OSwgMTM2KSAxMDAlKSAhaW1wb3J0YW50O1xyXG4gICAgLy8gYmFja2dyb3VuZDogLW8tbGluZWFyLWdyYWRpZW50KC0yOWRlZywgcmdiYSgxOTMsIDE1LCA2NSwgMC44NykgMCwgcmdiKDM1LCA1OSwgMTM2KSAxMDAlKSAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDRweCByZ2JhKDAsIDAsIDAsIC4xNCksIDAgNHB4IDhweCByZ2JhKDAsIDAsIDAsIC4yOCk7XHJcbiAgICBhbmltYXRpb246IC42cyBzbGlkZURvd24gZm9yd2FyZHM7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjVzIGVhc2UtaW4tb3V0ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5oZWFkZXItZml4ZWQgLmNvbnRhaW5lciB7XHJcbiAgICBoZWlnaHQ6IDYwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmhlYWRlci1maXhlZCAuaG9tZXRvcC1idG4ge1xyXG4gICAgbGluZS1oZWlnaHQ6IDYwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmhvbWUtbG9nbyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGEge1xyXG4gICAgICAgIGRpc3BsYXk6IGluaGVyaXQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbiNtZW51LXRvZ2dsZSB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDVweDtcclxuICAgIHRvcDogMTBweDtcclxufVxyXG5cclxuLmljb24tYnV0dG9uIHtcclxuICAgIC5tYXQtaWNvbiB7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4vLyBkcm9wZG93biBtZW51XHJcbm5hdiB1bCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxpIHtcclxuICAgICAgICBhIHtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICB1bCB7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6ICMzMzM7XHJcbiAgICAgICAgICAgIC8vIHBhZGRpbmc6IDEwcHggMjBweDtcclxuICAgICAgICAgICAgLy8gYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICAgICAgbGkge1xyXG4gICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZDogIzMzMztcclxuICAgICAgICAgICAgICAgIC8vIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMXB4IDE1cHggMXB4IHJnYmEoMCwgMCwgMCwgMC4wNCksIDAgMXB4IDZweCByZ2JhKDAsIDAsIDAsIDAuMDQpO1xyXG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB1bCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgICAgICAgICAvLyBwYWRkaW5nOiAxMHB4IDIwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICAvLyB0b3A6IDI1cHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4vKiBIaWRlIERyb3Bkb3ducyBieSBEZWZhdWx0ICovXHJcblxyXG5uYXYgdWwgdWwge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTBweDtcclxuICAgIC8qIHRoZSBoZWlnaHQgb2YgdGhlIG1haW4gbmF2ICovXHJcbn1cclxuXHJcblxyXG4vKiBEaXNwbGF5IERyb3Bkb3ducyBvbiBIb3ZlciAqL1xyXG5cclxubmF2IHVsIGxpOmhvdmVyPnVsIHtcclxuICAgIGRpc3BsYXk6IGluaGVyaXQ7XHJcbn1cclxuXHJcblxyXG4vKiBGaXNydCBUaWVyIERyb3Bkb3duICovXHJcblxyXG5uYXYgdWwgdWwgbGkge1xyXG4gICAgd2lkdGg6IDE3MHB4O1xyXG4gICAgZmxvYXQ6IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBsaXN0LWl0ZW07XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcblxyXG4vKiBTZWNvbmQsIFRoaXJkIGFuZCBtb3JlIFRpZXJzXHQqL1xyXG5cclxubmF2IHVsIHVsIHVsIGxpIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLTUwcHg7XHJcbiAgICBsZWZ0OiAxNDVweDtcclxufVxyXG5cclxuXHJcbi8qIENoYW5nZSB0aGlzIGluIG9yZGVyIHRvIGNoYW5nZSB0aGUgRHJvcGRvd24gc3ltYm9sICovXHJcblxyXG5saT5hOmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6ICcgKyc7XHJcbn1cclxuXHJcbmxpPmE6b25seS1jaGlsZDphZnRlciB7XHJcbiAgICBjb250ZW50OiAnJztcclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDk5MXB4KSB7XHJcbiAgICAjaG9tZS1oZWFkZXIge1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICB9XHJcbiAgICAjaG9tZS1oZWFkZXI+LmNvbnRhaW5lciB7XHJcbiAgICAgICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICB9XHJcbiAgICAubWVudS1vcGVuZWQ6aG9zdCAuaGVhZGVyLW1vYmlsZS10b3Age1xyXG4gICAgICAgIHBhZGRpbmc6IDEzcHggMDtcclxuICAgIH1cclxuICAgICNtZW51LXRvZ2dsZSB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcbiAgICAubWVudS1vcGVuZWQ6aG9zdCAuY29udGFpbmVyIHtcclxuICAgICAgICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcclxuICAgIH1cclxuICAgIC5ob21lLWxvZ28ge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gICAgfVxyXG4gICAgLnRvcC1tZW51IHtcclxuICAgICAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICAubWVudS1vcGVuZWQ6aG9zdCAudG9wLW1lbnUge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuICAgICAgICBmbGV4OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2YzZjNmMztcclxuICAgIH1cclxuICAgIC5tZW51LW9wZW5lZDpob3N0IC50b3AtbWVudSBidXR0b24ge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiA1NnB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICAubmF2YmFyLWNvbGxhcHNlIHtcclxuICAgICAgICBmbGV4LWJhc2lzOiAxMDAlO1xyXG4gICAgICAgIGZsZXgtZ3JvdzogMTtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIC8vIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMjlkZWcsIHJnYigxOTMsIDE1LCA2NSkgMCwgcmdiKDM1LCA1OSwgMTM2KSAxMDAlKSAhaW1wb3J0YW50O1xyXG4gICAgICAgIC8vIGJhY2tncm91bmQ6IC1tb3otbGluZWFyLWdyYWRpZW50KC0yOWRlZywgcmdiKDE5MywgMTUsIDY1KSAwLCByZ2IoMzUsIDU5LCAxMzYpIDEwMCUpICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZDogLW1zLWxpbmVhci1ncmFkaWVudCgtMjlkZWcsIHJnYigxOTMsIDE1LCA2NSkgMCwgcmdiKDM1LCA1OSwgMTM2KSAxMDAlKSAhaW1wb3J0YW50O1xyXG4gICAgICAgIC8vIGJhY2tncm91bmQ6IC1vLWxpbmVhci1ncmFkaWVudCgtMjlkZWcsIHJnYigxOTMsIDE1LCA2NSkgMCwgcmdiKDM1LCA1OSwgMTM2KSAxMDAlKSAhaW1wb3J0YW50O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBsaS5uYXYtaXRlbSB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgbmF2IHVsIHVsIHtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgLyogdGhlIGhlaWdodCBvZiB0aGUgbWFpbiBuYXYgKi9cclxuICAgIH1cclxuICAgIG5hdiB1bCB1bCB1bCBsaSB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDogMTBweDtcclxuICAgICAgICBsZWZ0OiA1cHg7XHJcbiAgICB9XHJcbiAgICBuYXYgdWwgbGk6aG92ZXI+dWwge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIH1cclxufVxyXG5cclxuLm5hdmJhci10b2dnbGVyOmhvdmVyLFxyXG4ubmF2YmFyLXRvZ2dsZXI6Zm9jdXMge1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuc3Bhbi5uYXZiYXItdG9nZ2xlci1pY29uIC5ldmEge1xyXG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzQ2cHgpIHt9IiwiI2hvbWUtaGVhZGVyIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHBhZGRpbmc6IDIycHggMDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZS1pbi1vdXQ7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZS1pbi1vdXQ7XG4gIHotaW5kZXg6IDk5OTk7XG59XG5cbmEge1xuICBjb2xvcjogI2ZmZjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb24tc2tpcDogb2JqZWN0cztcbn1cblxuLm1vYmlsZV9tZW51IHtcbiAgaGVpZ2h0OiA2MHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4jaG9tZS1oZWFkZXIgPiAuY29udGFpbmVyIHtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuXG4uaG9tZXRvcC1idG46aG92ZXIgOjpuZy1kZWVwIC5tYXQtYnV0dG9uLWZvY3VzLW92ZXJsYXkge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4jaG9tZS1oZWFkZXI6bm90KC5oZWFkZXItZml4ZWQpIC5ob21ldG9wLWJ0biA6Om5nLWRlZXAgLm1hdC1idXR0b24tcmlwcGxlIHtcbiAgdG9wOiAtMjJweDtcbiAgYm90dG9tOiAtMjJweDtcbn1cblxuLmhlYWRlci1maXhlZCB7XG4gIHBvc2l0aW9uOiBmaXhlZCAhaW1wb3J0YW50O1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gIGJveC1zaGFkb3c6IDAgMCA0cHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCA0cHggOHB4IHJnYmEoMCwgMCwgMCwgMC4yOCk7XG4gIGFuaW1hdGlvbjogMC42cyBzbGlkZURvd24gZm9yd2FyZHM7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0ICFpbXBvcnRhbnQ7XG59XG5cbi5oZWFkZXItZml4ZWQgLmNvbnRhaW5lciB7XG4gIGhlaWdodDogNjBweCAhaW1wb3J0YW50O1xufVxuXG4uaGVhZGVyLWZpeGVkIC5ob21ldG9wLWJ0biB7XG4gIGxpbmUtaGVpZ2h0OiA2MHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5ob21lLWxvZ28ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmhvbWUtbG9nbyBhIHtcbiAgZGlzcGxheTogaW5oZXJpdDtcbn1cblxuI21lbnUtdG9nZ2xlIHtcbiAgZGlzcGxheTogbm9uZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogNXB4O1xuICB0b3A6IDEwcHg7XG59XG5cbi5pY29uLWJ1dHRvbiAubWF0LWljb24ge1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbm5hdiB1bCB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxubmF2IHVsIGxpIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxubmF2IHVsIGxpIGEge1xuICBkaXNwbGF5OiBibG9jaztcbiAgY29sb3I6ICNmZmY7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbm5hdiB1bCBsaSB1bCB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5uYXYgdWwgbGkgdWwgbGkge1xuICBib3gtc2hhZG93OiAwIDFweCAxNXB4IDFweCByZ2JhKDAsIDAsIDAsIDAuMDQpLCAwIDFweCA2cHggcmdiYSgwLCAwLCAwLCAwLjA0KTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAxMHB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5uYXYgdWwgbGkgdWwgbGk6Zmlyc3QtY2hpbGQge1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTBweDtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTBweDtcbn1cbm5hdiB1bCBsaSB1bCBsaTpsYXN0LWNoaWxkIHtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG59XG5uYXYgdWwgbGkgdWwgbGkgdWwge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLyogSGlkZSBEcm9wZG93bnMgYnkgRGVmYXVsdCAqL1xubmF2IHVsIHVsIHtcbiAgZGlzcGxheTogbm9uZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwcHg7XG4gIC8qIHRoZSBoZWlnaHQgb2YgdGhlIG1haW4gbmF2ICovXG59XG5cbi8qIERpc3BsYXkgRHJvcGRvd25zIG9uIEhvdmVyICovXG5uYXYgdWwgbGk6aG92ZXIgPiB1bCB7XG4gIGRpc3BsYXk6IGluaGVyaXQ7XG59XG5cbi8qIEZpc3J0IFRpZXIgRHJvcGRvd24gKi9cbm5hdiB1bCB1bCBsaSB7XG4gIHdpZHRoOiAxNzBweDtcbiAgZmxvYXQ6IG5vbmU7XG4gIGRpc3BsYXk6IGxpc3QtaXRlbTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4vKiBTZWNvbmQsIFRoaXJkIGFuZCBtb3JlIFRpZXJzXHQqL1xubmF2IHVsIHVsIHVsIGxpIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IC01MHB4O1xuICBsZWZ0OiAxNDVweDtcbn1cblxuLyogQ2hhbmdlIHRoaXMgaW4gb3JkZXIgdG8gY2hhbmdlIHRoZSBEcm9wZG93biBzeW1ib2wgKi9cbmxpID4gYTphZnRlciB7XG4gIGNvbnRlbnQ6IFwiICtcIjtcbn1cblxubGkgPiBhOm9ubHktY2hpbGQ6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogOTkxcHgpIHtcbiAgI2hvbWUtaGVhZGVyIHtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG5cbiAgI2hvbWUtaGVhZGVyID4gLmNvbnRhaW5lciB7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cblxuICAubWVudS1vcGVuZWQ6aG9zdCAuaGVhZGVyLW1vYmlsZS10b3Age1xuICAgIHBhZGRpbmc6IDEzcHggMDtcbiAgfVxuXG4gICNtZW51LXRvZ2dsZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cblxuICAubWVudS1vcGVuZWQ6aG9zdCAuY29udGFpbmVyIHtcbiAgICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcbiAgfVxuXG4gIC5ob21lLWxvZ28ge1xuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xuICB9XG5cbiAgLnRvcC1tZW51IHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gIH1cblxuICAubWVudS1vcGVuZWQ6aG9zdCAudG9wLW1lbnUge1xuICAgIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBmbGV4OiBub25lICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZDogI2YzZjNmMztcbiAgfVxuXG4gIC5tZW51LW9wZW5lZDpob3N0IC50b3AtbWVudSBidXR0b24ge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGxpbmUtaGVpZ2h0OiA1NnB4ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAubmF2YmFyLWNvbGxhcHNlIHtcbiAgICBmbGV4LWJhc2lzOiAxMDAlO1xuICAgIGZsZXgtZ3JvdzogMTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICBsaS5uYXYtaXRlbSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgbmF2IHVsIHVsIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDEwcHg7XG4gICAgLyogdGhlIGhlaWdodCBvZiB0aGUgbWFpbiBuYXYgKi9cbiAgfVxuXG4gIG5hdiB1bCB1bCB1bCBsaSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogMTBweDtcbiAgICBsZWZ0OiA1cHg7XG4gIH1cblxuICBuYXYgdWwgbGk6aG92ZXIgPiB1bCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB9XG59XG4ubmF2YmFyLXRvZ2dsZXI6aG92ZXIsXG4ubmF2YmFyLXRvZ2dsZXI6Zm9jdXMge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbnNwYW4ubmF2YmFyLXRvZ2dsZXItaWNvbiAuZXZhIHtcbiAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6ICNmZmY7XG59Il19 */"

/***/ }),

/***/ "./src/app/core/navbar/navbar.component.ts":
/*!*************************************************!*\
  !*** ./src/app/core/navbar/navbar.component.ts ***!
  \*************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_services_proximax_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/services/proximax.service */ "./src/app/core/services/proximax.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_auth_store_auth_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @app/auth/store/auth.action */ "./src/app/auth/store/auth.action.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _app_notifications_store_notification_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app/notifications/store/notification.action */ "./src/app/notifications/store/notification.action.ts");
/* harmony import */ var _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @app/post/store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _app_notifications_store_notification_state__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app/notifications/store/notification.state */ "./src/app/notifications/store/notification.state.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var _app_profile_store_profile_action__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app/profile/store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _app_profile_store_profile_state__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @app/profile/store/profile.state */ "./src/app/profile/store/profile.state.ts");






// NGXS











let NavbarComponent = class NavbarComponent {
    constructor(store, fb, href, proximax) {
        this.store = store;
        this.fb = fb;
        this.href = href;
        this.proximax = proximax;
        this.dropdownState = false;
        this.dropdownStateNotif = false;
        this.dropdownFriendReqStateNotif = false;
        this.hasAccount = false;
        this.ENDPOINT_URL = _env_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].urlForImage;
        this.store.dispatch(new _app_profile_store_profile_action__WEBPACK_IMPORTED_MODULE_13__["GetProfile"]()).subscribe(user => {
            console.log('-----Profile from Wallet------', user);
            let _addr = user.categories.profile.wallet_address;
            let _pub = user.categories.profile.wallet_public_key;
            // check if account has wallet
            if (this.proximax.getAccountInfo(_addr) == true) {
                this.hasAccount = false;
            }
            else {
                this.hasAccount = true;
            }
        });
        this.searchForm = this.fb.group({
            name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]]
        });
        this.profilePhoto$.subscribe(val => {
            this.profilePhoto = val;
        });
        this.store.dispatch(new _app_notifications_store_notification_action__WEBPACK_IMPORTED_MODULE_8__["GetRecent10"]());
        this.searchForm
            .get('name')
            .valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["debounceTime"])(500))
            .subscribe((name) => {
            this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_9__["SearchPost"]({ val: name }));
        });
        this.store.dispatch(new _app_profile_store_profile_action__WEBPACK_IMPORTED_MODULE_13__["GetAllRequest"]());
    }
    getImage(url) {
        return `${_env_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].urlForImage}${url}`;
    }
    onLogoutOut() {
        this.store.dispatch(new _app_auth_store_auth_action__WEBPACK_IMPORTED_MODULE_6__["Logout"]());
    }
    closeDropdownNotif() {
        this.dropdownStateNotif = false;
    }
    closeDropdownFriendReqNotif() {
        this.dropdownFriendReqStateNotif = false;
    }
    closeDropdownProfile() {
        this.dropdownState = false;
    }
    decode(image) {
        return `${this.ENDPOINT_URL}${decodeURI(image)}`;
    }
    acceptRequest(id) {
        this.store.dispatch(new _app_profile_store_profile_action__WEBPACK_IMPORTED_MODULE_13__["AcceptRequest"](id)).subscribe(response => {
            console.log(response);
        });
    }
};
NavbarComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Store"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"] },
    { type: _app_core_services_proximax_service__WEBPACK_IMPORTED_MODULE_1__["ProximaxService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Select"])(_app_notifications_store_notification_state__WEBPACK_IMPORTED_MODULE_10__["NotificationState"].GetRecent10),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
], NavbarComponent.prototype, "recentNotifications$", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Select"])(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_12__["AuthState"].getProfilePhoto),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
], NavbarComponent.prototype, "profilePhoto$", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Select"])(_app_profile_store_profile_state__WEBPACK_IMPORTED_MODULE_14__["ProfileState"].getAllRequest),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
], NavbarComponent.prototype, "friends$", void 0);
NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-navbar',
        template: __webpack_require__(/*! raw-loader!./navbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/core/navbar/navbar.component.html"),
        styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/core/navbar/navbar.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Store"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"],
        _app_core_services_proximax_service__WEBPACK_IMPORTED_MODULE_1__["ProximaxService"]])
], NavbarComponent);



/***/ }),

/***/ "./src/app/core/pipes/date-ago.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/core/pipes/date-ago.pipe.ts ***!
  \*********************************************/
/*! exports provided: DateAgoPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateAgoPipe", function() { return DateAgoPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DateAgoPipe = class DateAgoPipe {
    transform(d) {
        const currentDate = new Date(new Date().toUTCString());
        const date = new Date(d + 'Z');
        const year = currentDate.getFullYear() - date.getFullYear();
        const month = currentDate.getMonth() - date.getMonth();
        const day = currentDate.getDate() - date.getDate();
        const hour = currentDate.getHours() - date.getHours();
        const minute = currentDate.getMinutes() - date.getMinutes();
        const second = currentDate.getSeconds() - date.getSeconds();
        const createdSecond = (year * 31556926) + (month * 2629746) + (day * 86400) + (hour * 3600) + (minute * 60) + second;
        if (createdSecond >= 31556926) {
            const yearAgo = Math.floor(createdSecond / 31556926);
            return yearAgo > 1 ? yearAgo + ' years ago' : yearAgo + ' year ago';
        }
        else if (createdSecond >= 2629746) {
            const monthAgo = Math.floor(createdSecond / 2629746);
            return monthAgo > 1 ? monthAgo + ' months ago' : monthAgo + ' month ago';
        }
        else if (createdSecond >= 86400) {
            const dayAgo = Math.floor(createdSecond / 86400);
            return dayAgo > 1 ? dayAgo + ' days ago' : dayAgo + ' day ago';
        }
        else if (createdSecond >= 3600) {
            const hourAgo = Math.floor(createdSecond / 3600);
            return hourAgo > 1 ? hourAgo + ' hours ago' : hourAgo + ' hour ago';
        }
        else if (createdSecond >= 60) {
            const minuteAgo = Math.floor(createdSecond / 60);
            return minuteAgo > 1 ? minuteAgo + ' minutes ago' : minuteAgo + ' minute ago';
        }
        else if (createdSecond < 60) {
            return createdSecond > 1 ? createdSecond + ' seconds ago' : createdSecond + ' second ago';
        }
        else if (createdSecond < 0) {
            return '0 second ago';
        }
    }
};
DateAgoPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'dateAgo',
        pure: true
    })
], DateAgoPipe);



/***/ }),

/***/ "./src/app/core/services/auth.service.ts":
/*!***********************************************!*\
  !*** ./src/app/core/services/auth.service.ts ***!
  \***********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");





let AuthService = class AuthService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}/auth`;
        this.sirius = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}`;
    }
    login(payload) {
        return this.httpClient.post(`${this.url}/login`, JSON.stringify(payload));
    }
    logout() {
        localStorage.clear();
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(null);
    }
    signUp(payload) {
        return this.httpClient.post(`${this.url}/register`, JSON.stringify(payload));
    }
    getProfile() {
        return this.httpClient.get(`${this.url}/me`);
    }
    loginWithGoogle(payload) {
        return this.httpClient.post(`${this.url}/socialLogin`, JSON.stringify(payload));
    }
    loginWithFacebook(payload) {
        return this.httpClient.post(`${this.url}/socialLogin`, JSON.stringify(payload));
    }
    editProfile(payload) {
        return this.httpClient.put(`${this.url}/me`, JSON.stringify(payload));
    }
    requestForResetPassword(payload) {
        return this.httpClient.post(`${this.url}/password/email`, JSON.stringify(payload));
    }
    resetPassword(payload) {
        return this.httpClient.post(`${this.url}/password/reset`, JSON.stringify(payload));
    }
    siriusRegister(payload) {
        return this.httpClient.post(`${this.sirius}/sirius/register`, JSON.stringify(payload));
    }
    loginEmailToDb(payload) {
        return this.httpClient.post(`${this.url}/validate`, JSON.stringify(payload));
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], AuthService);



/***/ }),

/***/ "./src/app/core/services/board.service.ts":
/*!************************************************!*\
  !*** ./src/app/core/services/board.service.ts ***!
  \************************************************/
/*! exports provided: BoardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardService", function() { return BoardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let BoardService = class BoardService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].url}`;
    }
    createBoard(payload) {
        return this.httpClient.post(`${this.url}/boards`, JSON.stringify(payload));
    }
    getMyBoards() {
        return this.httpClient.get(`${this.url}/boards/owned`);
    }
};
BoardService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
BoardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], BoardService);



/***/ }),

/***/ "./src/app/core/services/categories.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/core/services/categories.service.ts ***!
  \*****************************************************/
/*! exports provided: CategoriesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesService", function() { return CategoriesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");




let CategoriesService = class CategoriesService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}`;
    }
    getAllCategories() {
        return this.httpClient.get(`${this.url}/categories/all`);
    }
    selectCategories(payload) {
        return this.httpClient.post(`${this.url}/user-categories`, payload);
    }
};
CategoriesService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CategoriesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], CategoriesService);



/***/ }),

/***/ "./src/app/core/services/change-data.service.ts":
/*!******************************************************!*\
  !*** ./src/app/core/services/change-data.service.ts ***!
  \******************************************************/
/*! exports provided: ChangeDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangeDataService", function() { return ChangeDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "./node_modules/tsjs-xpx-chain-sdk/dist/index.js");
/* harmony import */ var tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_2__);



let ChangeDataService = class ChangeDataService {
    constructor() {
        this.pubickeySiriusid = '';
        this.addressSiriusid = '';
        this.name = ''; // Name of user when him sign up
        /**
         * For TEST_NET
         */
        this.publicKeydApp = '10F4337C7A862A247435167AEF4BD37BF5FAE8BCB247A8B3B3B1A616F9224708';
        this.privateKeydApp = 'FC4979544C6D504AE8181FB0CD23C68FB345FB3FDC3DC232D419AAF2D3581248';
        this.apiNode = '';
        this.ws = 'wss://demo-sc-api-1.ssi.xpxsirius.io';
        this.addressdApp = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_2__["Address"].createFromPublicKey(this.publicKeydApp, tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_2__["NetworkType"].PRIVATE_TEST);
    }
    /**
     * For local testing
     */
    // ws = 'ws://192.168.1.23:3000'; //local
    // publicKeydApp = '0750E2716F2CBADD19F9F5314944ABFDF005A5D3D6DAAB4D08E97F188AAA9300'; // privateKey: 4EBACFE2B723FDA8B4DFC7E7B8AA26AF9C8638DCC353463AFF89F03230AFD38E
    updateWebsocket() {
        this.ws = 'wss://' + this.apiNode;
    }
};
ChangeDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ChangeDataService);



/***/ }),

/***/ "./src/app/core/services/comments.service.ts":
/*!***************************************************!*\
  !*** ./src/app/core/services/comments.service.ts ***!
  \***************************************************/
/*! exports provided: CommentsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentsService", function() { return CommentsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");




let CommentsService = class CommentsService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}/comments`;
    }
    getAll() {
        return this.httpClient.get(`${this.url}/all`);
    }
    addComment(payload) {
        return this.httpClient.post(`${this.url}`, JSON.stringify(payload));
    }
    deleteComment(payload) {
        return this.httpClient.delete(`${this.url}/${payload.id}`);
    }
    likeComment(payload) {
        return this.httpClient.get(`${this.url}/like/${payload.id}`);
    }
};
CommentsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CommentsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], CommentsService);



/***/ }),

/***/ "./src/app/core/services/countries.service.ts":
/*!****************************************************!*\
  !*** ./src/app/core/services/countries.service.ts ***!
  \****************************************************/
/*! exports provided: CountriesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountriesService", function() { return CountriesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");




let CountriesService = class CountriesService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}/countries`;
    }
    getAll() {
        return this.httpClient.get(`${this.url}/all`);
    }
};
CountriesService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CountriesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], CountriesService);



/***/ }),

/***/ "./src/app/core/services/loader.service.ts":
/*!*************************************************!*\
  !*** ./src/app/core/services/loader.service.ts ***!
  \*************************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



let LoaderService = class LoaderService {
    constructor() {
        this.loaderSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.loaderState = this.loaderSubject.asObservable();
    }
    show() {
        this.loaderSubject.next({ show: true });
    }
    hide() {
        this.loaderSubject.next({ show: false });
    }
};
LoaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], LoaderService);



/***/ }),

/***/ "./src/app/core/services/notifications.service.ts":
/*!********************************************************!*\
  !*** ./src/app/core/services/notifications.service.ts ***!
  \********************************************************/
/*! exports provided: NotificationsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsService", function() { return NotificationsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");




let NotificationsService = class NotificationsService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}/users`;
    }
    getRecent10() {
        return this.httpClient.get(`${this.url}/notifications`);
    }
    getAll() {
        return this.httpClient.get(`${this.url}/all_notifications`);
    }
};
NotificationsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
NotificationsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], NotificationsService);



/***/ }),

/***/ "./src/app/core/services/posts.service.ts":
/*!************************************************!*\
  !*** ./src/app/core/services/posts.service.ts ***!
  \************************************************/
/*! exports provided: PostsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostsService", function() { return PostsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");




let PostsService = class PostsService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}/posts`;
        this.url2 = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}`;
    }
    getAll() {
        return this.httpClient.get(`${this.url}/all`);
    }
    getPostsPerPage(key, pageNumber) {
        return this.httpClient.get(`${this.url}?key=${key}&page=${pageNumber}`);
    }
    getPost(id) {
        return this.httpClient.get(this.getPostUrl(id));
    }
    getPostUrl(id) {
        return `${this.url}/${id}`;
    }
    getOwn() {
        return this.httpClient.get(`${this.url}/own`);
    }
    getAllTrends() {
        return this.httpClient.get(`${this.url}/trends`);
    }
    getAllTrendsToday() {
        return this.httpClient.get(`${this.url}/trends/today`);
    }
    getAllTrendsWeek() {
        return this.httpClient.get(`${this.url}/trends/week`);
    }
    getAllTrendsMonth() {
        return this.httpClient.get(`${this.url}/trends/month`);
    }
    getTrendsPerPage(key, pageNumber) {
        return this.httpClient.get(`${this.url}?key=${key}&page=${pageNumber}`);
    }
    getAllFavorites() {
        return this.httpClient.get(`${this.url}/favorites`);
    }
    getAllFavoritesToday() {
        return this.httpClient.get(`${this.url}/favorites/today`);
    }
    getAllFavoritesWeek() {
        return this.httpClient.get(`${this.url}/favorites/week`);
    }
    getAllFavoritesMonth() {
        return this.httpClient.get(`${this.url}/favorites/month`);
    }
    getFavoritesPerPage(key, pageNumber) {
        return this.httpClient.get(`${this.url}?key=${key}&page=${pageNumber}`);
    }
    addPost(payload) {
        return this.httpClient.post(`${this.url}`, payload);
    }
    deletePost(payload) {
        return this.httpClient.delete(`${this.url}/${payload.id}`);
    }
    likePost(payload) {
        return this.httpClient.get(`${this.url}/like/${payload.id}`);
    }
    getAllBoards() {
        return this.httpClient.get(`${this.url2}/boards/all`);
    }
    pinPost(payload) {
        return this.httpClient.post(`${this.url}/pin`, JSON.stringify(payload));
    }
    getPostImageById(id) {
        return this.httpClient.get(`${this.url}/${id}`);
    }
};
PostsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
PostsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], PostsService);



/***/ }),

/***/ "./src/app/core/services/proximax.service.ts":
/*!***************************************************!*\
  !*** ./src/app/core/services/proximax.service.ts ***!
  \***************************************************/
/*! exports provided: ProximaxService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProximaxService", function() { return ProximaxService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var siriusid_sdk__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! siriusid-sdk */ "../sirius-id-sdk/dist/index.js");
/* harmony import */ var siriusid_sdk__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(siriusid_sdk__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "./node_modules/tsjs-xpx-chain-sdk/dist/index.js");
/* harmony import */ var tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../profile/store/profile.action */ "./src/app/profile/store/profile.action.ts");







// NGXS


let ProximaxService = class ProximaxService {
    constructor(http, store) {
        this.http = http;
        this.store = store;
        this.node = _env_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].nodeUrl;
        this.credential = undefined;
        this.account = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["Account"].createFromPrivateKey('C3C94C048F45BD3E6F400AAE8D39FDE1C6DCA0892085C236AB2A67691F365C09', tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["NetworkType"].TEST_NET);
        this.generateNewAccount = () => {
            const newAccount = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["Account"].generateNewAccount(this.NETWORK_TYPE);
            return newAccount;
        };
        this.createWallet = (_wallet_name, _wallet_password) => {
            const password = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["Password"](_wallet_password);
            const wallet = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["SimpleWallet"].create(_wallet_name, password, tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["NetworkType"].TEST_NET);
            const account = wallet.open(password);
            // store address and public_key
            return this.onEditProfile(account.address.pretty(), account.publicKey, JSON.stringify(wallet));
        };
        this.createSessionToken = (_session_token) => {
            const session_token = _session_token;
            return this.onEditProfileSessionToken(JSON.stringify(session_token));
        };
        this.linkWallet = (_wallet_privateKey, _wallet_name, _wallet_password) => {
            const password = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["Password"]('password');
            const privateKey = _wallet_privateKey;
            // const wallet = SimpleWallet.createFromPrivateKey(_wallet_name, password, privateKey, NetworkType.TEST_NET);
            const wallet = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["SimpleWallet"].createFromPrivateKey(_wallet_name, password, privateKey, tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["NetworkType"].MAIN_NET);
            const account = wallet.open(password);
            return this.onEditProfile(account.address.pretty(), account.publicKey, JSON.stringify(wallet));
        };
        this.generateNewAccountTest = () => {
            // const accountHttp = new AccountHttp(this.node);
            const wallet_address = 'VDCCN6IQZ6CXLUY2EGXTITWWVPLI7TQC6A435UFB';
            const rawAddress = wallet_address;
            const address = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["Address"].createFromRawAddress(rawAddress);
            const accountHttp = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["AccountHttp"](this.node);
            const mosaicHttp = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["MosaicHttp"](this.node);
            const mosaicService = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["MosaicService"](accountHttp, mosaicHttp);
            mosaicService
                .mosaicsAmountViewFromAddress(address)
                .subscribe(mosaic => console.log('Balance:', mosaic[0].amount.compact()), err => console.error(err));
        };
        this.NETWORK_TYPE = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["NetworkType"].TEST_NET;
    }
    createCredential(form) {
        this.credential = siriusid_sdk__WEBPACK_IMPORTED_MODULE_4__["Credentials"].create('1', 'Imegerax', 'Imagerax LoginCredentials', 'test', [], form);
        console.log(this.credential);
        return siriusid_sdk__WEBPACK_IMPORTED_MODULE_4__["CredentialRequestMessage"].create(this.credential);
    }
    generateLoginMessage(public_key, credential) {
        const loginRequestMessage = siriusid_sdk__WEBPACK_IMPORTED_MODULE_4__["LoginRequestMessage"].create(public_key, credential);
        console.log(loginRequestMessage);
        return loginRequestMessage;
    }
    generateTransactionsList(_public_key) {
        const accountHttp = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["AccountHttp"](this.node);
        const rawPublicKey = _public_key;
        const publicAccount = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["PublicAccount"].createFromPublicKey(rawPublicKey, this.NETWORK_TYPE);
        const pageSize = 100; // Page size between 10 and 100, otherwise 10
        return new rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"](observer => {
            accountHttp.transactions(publicAccount, new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["QueryParams"](pageSize)).subscribe((transactions) => {
                observer.next(transactions);
            }, err => {
                console.error('Error Here', err);
                observer.next(null);
            });
        });
    }
    getBalance(wallet_address) {
        const url = this.node;
        const accountHttp = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["AccountHttp"](url);
        const mosaicHttp = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["MosaicHttp"](url);
        const mosaicService = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["MosaicService"](accountHttp, mosaicHttp);
        const rawAddress = wallet_address;
        const address = tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["Address"].createFromRawAddress(rawAddress);
        return new rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"](observer => {
            mosaicService.mosaicsAmountViewFromAddress(address).subscribe(mosaic => {
                const relativeAmount = this.getRelaiveAmount(mosaic[0].amount.compact());
                observer.next(relativeAmount);
            }, err => {
                console.error(err);
                observer.next(0);
            });
        });
    }
    getRelaiveAmount(amount) {
        return amount / Math.pow(10, 6);
    }
    getPriceUSD() {
        const coinId = 'proximax';
        const url = 'https://api.coingecko.com/api/v3';
        return new rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"](observer => {
            this.http.get(`${url}/coins/${coinId}`).subscribe((details) => {
                observer.next(details.market_data.current_price.usd);
            });
        });
    }
    onEditProfile(address, public_key, wallet) {
        const formData = new FormData();
        formData.append('wallet_address', address);
        formData.append('wallet_public_key', public_key);
        formData.append('wallet', wallet);
        this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_8__["EditProfile"](formData)).subscribe(() => {
            location.reload();
        }, err => {
            console.log(err);
        });
    }
    onEditProfileSessionToken(session_token) {
        const formData = new FormData();
        formData.append('session_token', session_token);
        this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_8__["EditProfile"](formData)).subscribe(() => {
            // location.reload();
        }, err => {
            console.log(err);
        });
    }
    getAccountInfo(_addr) {
        if (_addr == null) {
            // console.log('---------I have NO ACCOUNT');
            return true;
        }
        else {
            // console.log('---------I have ACCOUNT, ');
            return false;
        }
    }
    getSessionToken(_session_token) {
        if (_session_token == null) {
            return true;
        }
        else {
            return false;
        }
    }
};
ProximaxService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Store"] }
];
ProximaxService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Store"]])
], ProximaxService);



/***/ }),

/***/ "./src/app/core/services/users.service.ts":
/*!************************************************!*\
  !*** ./src/app/core/services/users.service.ts ***!
  \************************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");




let UsersService = class UsersService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}/users`;
        this.url2 = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}`;
        this.urlFollowPublic = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}/followers/follow`;
        this.urlFollowPrivate = `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].url}/request/follow`;
    }
    updateUser(payload) {
        return this.httpClient.post(`${this.url}/edit`, payload);
    }
    viewUser(id) {
        return this.httpClient.get(`${this.url}/${id}`);
    }
    getFollowers(id) {
        return this.httpClient.get(`${this.url}/${id}/followers`);
    }
    getFollowing(id) {
        return this.httpClient.get(`${this.url}/${id}/following`);
    }
    getAllFriendRequest() {
        return this.httpClient.get(`${this.url2}/request`);
    }
    confirmRequest(id) {
        const payload = {
            response: 1
        };
        return this.httpClient.post(`${this.url2}/response/${id}`, payload);
    }
    blockUser(id) {
        return this.httpClient.get(`${this.url}/block/${id}`);
    }
    unblockUser(id) {
        return this.httpClient.get(`${this.url}/unblock/${id}`);
    }
    followUserPublicAccount(id) {
        return this.httpClient.post(`${this.urlFollowPublic}/${id}`, JSON.stringify(id));
    }
    followUserPrivateAccount(id) {
        const payload = {
            id: id
        };
        return this.httpClient.post(`${this.urlFollowPrivate}/${id}`, payload);
    }
};
UsersService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
UsersService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], UsersService);



/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".wallet {\n  width: 60vw;\n  margin: 2rem auto 0 auto;\n}\n@media screen and (max-width: 1024px) {\n  .wallet {\n    width: 80vw;\n  }\n}\n@media screen and (max-width: 460px) {\n  .wallet {\n    width: 90vw;\n  }\n}\n.wallet .accountCanvass {\n  padding: 5rem 5rem;\n  display: table;\n  width: 100%;\n  box-shadow: 0 0 0.5rem 0.5rem rgba(0, 0, 0, 0.1);\n  border-radius: 0.8rem;\n}\n.wallet .accountCanvass .accountPanel {\n  text-align: center;\n  vertical-align: middle;\n  display: table-cell;\n  font-size: 25px;\n}\n.wallet .accountCanvass .accountPanel img {\n  width: 15rem;\n}\n.wallet .accountCanvass .accountPanel p {\n  padding: 2rem;\n}\n.wallet .accountCanvass .accountPanel .accountButton {\n  display: flex;\n  margin-top: 10rem;\n}\n.wallet .accountCanvass .accountPanel .accountButton .create_button {\n  flex: 1;\n  padding: 1rem;\n}\n.wallet .accountCanvass .accountPanel .accountButton .create_button button {\n  background-color: #2cb6c4;\n}\n.wallet .accountCanvass .accountPanel .accountButton .link_button {\n  flex: 1;\n  padding: 1rem;\n}\n.wallet .accountCanvass .accountPanel .accountButton .link_button button {\n  background-color: #2cb6c4;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXEFTVVNcXERlc2t0b3BcXFByb2plY3RcXGltYWdlcmF4LXNpcml1cy1mcm9udGVuZFxcc2lyaXVzLWxpdmV3YWxsLS0tZnJvbnRlbmQvc3JjXFxhcHBcXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSx3QkFBQTtBQ0NKO0FER0k7RUFOSjtJQU9RLFdBQUE7RUNBTjtBQUNGO0FERUk7RUFWSjtJQVdRLFdBQUE7RUNDTjtBQUNGO0FEQUk7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0RBQUE7RUFDQSxxQkFBQTtBQ0VSO0FEQVE7RUFDSSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FDRVo7QURBWTtFQUNJLFlBQUE7QUNFaEI7QURDWTtFQUNJLGFBQUE7QUNDaEI7QURFWTtFQUVJLGFBQUE7RUFDQSxpQkFBQTtBQ0FoQjtBREVnQjtFQUVJLE9BQUE7RUFDQSxhQUFBO0FDQXBCO0FERW9CO0VBQ0kseUJBQUE7QUNBeEI7QURJZ0I7RUFFSSxPQUFBO0VBQ0EsYUFBQTtBQ0ZwQjtBRElvQjtFQUNJLHlCQUFBO0FDRnhCIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndhbGxldCB7XHJcbiAgICB3aWR0aDogNjB2dztcclxuICAgIG1hcmdpbjogMnJlbSBhdXRvIDAgYXV0bztcclxuICAgIC8vIGNvbG9yOiAkY29sb3ItcHJpbWFyeTtcclxuICAgIC8vIEBpbmNsdWRlIGNsZWFyZml4O1xyXG5cclxuICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xyXG4gICAgICAgIHdpZHRoOiA4MHZ3O1xyXG4gICAgfVxyXG5cclxuICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ2MHB4KSB7XHJcbiAgICAgICAgd2lkdGg6IDkwdnc7XHJcbiAgICB9XHJcbiAgICAuYWNjb3VudENhbnZhc3Mge1xyXG4gICAgICAgIHBhZGRpbmc6IDVyZW0gNXJlbTtcclxuICAgICAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMC41cmVtIDAuNXJlbSByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMC44cmVtO1xyXG5cclxuICAgICAgICAuYWNjb3VudFBhbmVsIHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDI1cHg7XHJcblxyXG4gICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1cmVtO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBwIHtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDJyZW07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC5hY2NvdW50QnV0dG9uIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcmVtO1xyXG5cclxuICAgICAgICAgICAgICAgIC5jcmVhdGVfYnV0dG9uIHtcclxuICAgICAgICAgICAgICAgICAgICAtbXMtZmxleDogMTtcclxuICAgICAgICAgICAgICAgICAgICBmbGV4OiAxO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDFyZW07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyY2I2YzQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAubGlua19idXR0b24ge1xyXG4gICAgICAgICAgICAgICAgICAgIC1tcy1mbGV4OiAxO1xyXG4gICAgICAgICAgICAgICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMXJlbTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzJjYjZjNDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIud2FsbGV0IHtcbiAgd2lkdGg6IDYwdnc7XG4gIG1hcmdpbjogMnJlbSBhdXRvIDAgYXV0bztcbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xuICAud2FsbGV0IHtcbiAgICB3aWR0aDogODB2dztcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDYwcHgpIHtcbiAgLndhbGxldCB7XG4gICAgd2lkdGg6IDkwdnc7XG4gIH1cbn1cbi53YWxsZXQgLmFjY291bnRDYW52YXNzIHtcbiAgcGFkZGluZzogNXJlbSA1cmVtO1xuICBkaXNwbGF5OiB0YWJsZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaGFkb3c6IDAgMCAwLjVyZW0gMC41cmVtIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgYm9yZGVyLXJhZGl1czogMC44cmVtO1xufVxuLndhbGxldCAuYWNjb3VudENhbnZhc3MgLmFjY291bnRQYW5lbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuLndhbGxldCAuYWNjb3VudENhbnZhc3MgLmFjY291bnRQYW5lbCBpbWcge1xuICB3aWR0aDogMTVyZW07XG59XG4ud2FsbGV0IC5hY2NvdW50Q2FudmFzcyAuYWNjb3VudFBhbmVsIHAge1xuICBwYWRkaW5nOiAycmVtO1xufVxuLndhbGxldCAuYWNjb3VudENhbnZhc3MgLmFjY291bnRQYW5lbCAuYWNjb3VudEJ1dHRvbiB7XG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW4tdG9wOiAxMHJlbTtcbn1cbi53YWxsZXQgLmFjY291bnRDYW52YXNzIC5hY2NvdW50UGFuZWwgLmFjY291bnRCdXR0b24gLmNyZWF0ZV9idXR0b24ge1xuICAtbXMtZmxleDogMTtcbiAgZmxleDogMTtcbiAgcGFkZGluZzogMXJlbTtcbn1cbi53YWxsZXQgLmFjY291bnRDYW52YXNzIC5hY2NvdW50UGFuZWwgLmFjY291bnRCdXR0b24gLmNyZWF0ZV9idXR0b24gYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJjYjZjNDtcbn1cbi53YWxsZXQgLmFjY291bnRDYW52YXNzIC5hY2NvdW50UGFuZWwgLmFjY291bnRCdXR0b24gLmxpbmtfYnV0dG9uIHtcbiAgLW1zLWZsZXg6IDE7XG4gIGZsZXg6IDE7XG4gIHBhZGRpbmc6IDFyZW07XG59XG4ud2FsbGV0IC5hY2NvdW50Q2FudmFzcyAuYWNjb3VudFBhbmVsIC5hY2NvdW50QnV0dG9uIC5saW5rX2J1dHRvbiBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmNiNmM0O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @app/post/store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app/post/store/post.state */ "./src/app/post/store/post.state.ts");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");
/* harmony import */ var _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../profile/store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _app_core_services_proximax_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app/core/services/proximax.service */ "./src/app/core/services/proximax.service.ts");
/* harmony import */ var _app_shared_modals_category_select_category_select_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app/shared/modals/category-select/category-select.component */ "./src/app/shared/modals/category-select/category-select.component.ts");





// import { } from '@siriusid-sdk';
// NGXS








//Modals

//Forms
let HomeComponent = class HomeComponent {
    constructor(store, proximax, formBuilder, modalService) {
        this.store = store;
        this.proximax = proximax;
        this.formBuilder = formBuilder;
        this.modalService = modalService;
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        this.hasAccount = false;
        this.noAccount = false;
        this.closeResult = '';
        this.form = this.formBuilder.group({
            board_id: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_10__["Validators"].required],
            post_id: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_10__["Validators"].required]
        });
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_6__["GetAllPosts"]());
        this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_9__["GetProfile"]()).subscribe(user => {
            console.log('-----Profile from Wallet------', user);
            // this.openSelectCatModal();
            if (user.categories.profile.categories.length == 0 && user.categories.profile.wallet_address !== null) {
                this.openSelectCatModal();
            }
            else {
                console.log('error');
            }
            let _addr = user.categories.profile.wallet_address;
            let _pub = user.categories.profile.wallet_public_key;
            // check if account has wallet
            if (this.proximax.getAccountInfo(_addr) == true) {
                this.hasAccount = false;
                this.noAccount = true;
            }
            else {
                this.hasAccount = true;
                this.noAccount = false;
            }
        });
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_6__["GetAllBoards"]()).subscribe(user => {
            console.log(user);
        });
    }
    getUrl(url) {
        return `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urlForImage}${url}`;
    }
    setSubmitButton(flag) {
        this.submitButtonStatus = flag;
    }
    getSubmitButton() {
        return this.submitButtonStatus;
    }
    onPinPost(form, id) {
        console.log(id);
        this.form.get('post_id').setValue(id);
        console.log(form.value);
        if (form.valid) {
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_6__["PinPost"](form.value)).subscribe(() => {
                this.setSubmitButton(true);
                this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_8__["Navigate"](['/home']));
            }, err => {
                console.log(err);
                this.setSubmitButton(true);
            });
        }
    }
    createWallet() {
        sweetalert2__WEBPACK_IMPORTED_MODULE_11___default.a.mixin({
            progressSteps: ['1', '2', '3']
        })
            .queue([
            {
                input: 'text',
                inputPlaceholder: 'wallet-name',
                title: 'Create Wallet',
                text: 'Setup wallet Name ',
                confirmButtonText: 'Next &rarr;',
                showCancelButton: true,
                inputValidator: (value) => {
                    return new Promise(resolve => {
                        if (value.length <= 2) {
                            resolve('Enter atleast 3 characters');
                        }
                        else {
                            resolve();
                        }
                    });
                }
            },
            {
                input: 'password',
                inputPlaceholder: 'wallet-password',
                title: 'Create Wallet',
                text: 'Setup wallet password',
                confirmButtonText: 'Next &rarr;',
                showCancelButton: true,
                inputValidator: (value) => {
                    return new Promise(resolve => {
                        if (value.length <= 7) {
                            resolve('Enter atleast 8 characters');
                        }
                        else {
                            resolve();
                        }
                    });
                }
            },
            {
                title: 'Create Wallet',
                text: 'Are you sure to create wallet?',
                confirmButtonText: 'Create Wallet'
            }
        ])
            .then(result => {
            if (result.value) {
                const _name = result.value[0];
                const _password = result.value[1];
                const _confirm = result.value[1];
                if (_confirm) {
                    this.proximax.createWallet(_name, _password);
                }
            }
        });
    }
    linkWallet() {
        sweetalert2__WEBPACK_IMPORTED_MODULE_11___default.a.mixin({
            progressSteps: ['1', '2', '3', '4']
        })
            .queue([
            {
                input: 'text',
                inputPlaceholder: 'wallet-private-key',
                title: 'Add you Private Key',
                confirmButtonText: 'Next &rarr;',
                showCancelButton: true,
                inputValidator: (value) => {
                    return new Promise(resolve => {
                        if (value.length < 64 || value.length > 64) {
                            resolve('Enter a valid Private Key');
                        }
                        else {
                            resolve();
                        }
                    });
                }
            },
            {
                input: 'text',
                inputPlaceholder: 'wallet-name',
                title: 'Setup wallet name',
                confirmButtonText: 'Next &rarr;',
                showCancelButton: true,
                inputValidator: (value) => {
                    return new Promise(resolve => {
                        if (value.length <= 2) {
                            resolve('Enter atleast 3 characters');
                        }
                        else {
                            resolve();
                        }
                    });
                }
            },
            {
                input: 'password',
                inputPlaceholder: 'wallet-password',
                title: 'Setup wallet password',
                confirmButtonText: 'Next &rarr;',
                showCancelButton: true,
                inputValidator: (value) => {
                    return new Promise(resolve => {
                        if (value.length <= 7) {
                            resolve('Enter atleast 8 characters');
                        }
                        else {
                            resolve();
                        }
                    });
                }
            },
            {
                title: 'Are you sure to link wallet?',
                confirmButtonText: 'Link Wallet'
            }
        ])
            .then(result => {
            if (result.value) {
                const _publickey = result.value[0];
                const _name = result.value[1];
                const _password = result.value[2];
                const _confirm = result.value[3];
                if (_confirm) {
                    this.proximax.linkWallet(_publickey, _name, _password);
                }
            }
        });
    }
    openSelectCatModal() {
        const modal = this.modalService.open(_app_shared_modals_category_select_category_select_component__WEBPACK_IMPORTED_MODULE_13__["CategorySelectComponent"]);
        modal.result.then(() => { });
    }
};
HomeComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"] },
    { type: _app_core_services_proximax_service__WEBPACK_IMPORTED_MODULE_12__["ProximaxService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormBuilder"] },
    { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Select"])(_app_post_store_post_state__WEBPACK_IMPORTED_MODULE_7__["PostState"].filteredPosts),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
], HomeComponent.prototype, "posts$", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Select"])(_app_post_store_post_state__WEBPACK_IMPORTED_MODULE_7__["PostState"].getAllBoards),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
], HomeComponent.prototype, "boards$", void 0);
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"],
        _app_core_services_proximax_service__WEBPACK_IMPORTED_MODULE_12__["ProximaxService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormBuilder"],
        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]])
], HomeComponent);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");






let HomeModule = class HomeModule {
};
HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"]]
    })
], HomeModule);



/***/ }),

/***/ "./src/app/layouts/private/private-layout.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/layouts/private/private-layout.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvcHJpdmF0ZS9wcml2YXRlLWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layouts/private/private-layout.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/layouts/private/private-layout.component.ts ***!
  \*************************************************************/
/*! exports provided: PrivateLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivateLayoutComponent", function() { return PrivateLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PrivateLayoutComponent = class PrivateLayoutComponent {
    constructor() { }
    ngOnInit() { }
};
PrivateLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-private-layout',
        template: __webpack_require__(/*! raw-loader!./private-layout.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/private/private-layout.component.html"),
        styles: [__webpack_require__(/*! ./private-layout.component.scss */ "./src/app/layouts/private/private-layout.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PrivateLayoutComponent);



/***/ }),

/***/ "./src/app/layouts/public/public-layout.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layouts/public/public-layout.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvcHVibGljL3B1YmxpYy1sYXlvdXQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layouts/public/public-layout.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layouts/public/public-layout.component.ts ***!
  \***********************************************************/
/*! exports provided: PublicLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicLayoutComponent", function() { return PublicLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PublicLayoutComponent = class PublicLayoutComponent {
    constructor() { }
    ngOnInit() {
    }
};
PublicLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-public-layout',
        template: __webpack_require__(/*! raw-loader!./public-layout.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/public/public-layout.component.html"),
        styles: [__webpack_require__(/*! ./public-layout.component.scss */ "./src/app/layouts/public/public-layout.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PublicLayoutComponent);



/***/ }),

/***/ "./src/app/notifications/store/notification.action.ts":
/*!************************************************************!*\
  !*** ./src/app/notifications/store/notification.action.ts ***!
  \************************************************************/
/*! exports provided: GetRecent10, GetAllNotifications */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRecent10", function() { return GetRecent10; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllNotifications", function() { return GetAllNotifications; });
class GetRecent10 {
}
GetRecent10.type = '[Navbar] GetRecent10';
class GetAllNotifications {
}
GetAllNotifications.type = '[Notificatons Page] GetAllNotifications';


/***/ }),

/***/ "./src/app/notifications/store/notification.state.ts":
/*!***********************************************************!*\
  !*** ./src/app/notifications/store/notification.state.ts ***!
  \***********************************************************/
/*! exports provided: NotificationState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationState", function() { return NotificationState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_services_notifications_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/services/notifications.service */ "./src/app/core/services/notifications.service.ts");
/* harmony import */ var _app_core_models_notification_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/core/models/notification.model */ "./src/app/core/models/notification.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _notification_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notification.action */ "./src/app/notifications/store/notification.action.ts");





// NGXS


let NotificationState = class NotificationState {
    constructor(notificationsService) {
        this.notificationsService = notificationsService;
    }
    static GetRecent10(state) {
        return state.recentNotifications;
    }
    static GetAllNotifications(state) {
        return state.notifications;
    }
    getRecent10({ patchState }, {}) {
        return this.notificationsService.getRecent10().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            console.log(result);
            patchState({
                recentNotifications: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllNotifications({ patchState }, {}) {
        return this.notificationsService.getAll().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            console.log(result);
            patchState({
                notifications: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
};
NotificationState.ctorParameters = () => [
    { type: _app_core_services_notifications_service__WEBPACK_IMPORTED_MODULE_1__["NotificationsService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_notification_action__WEBPACK_IMPORTED_MODULE_6__["GetRecent10"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _notification_action__WEBPACK_IMPORTED_MODULE_6__["GetRecent10"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], NotificationState.prototype, "getRecent10", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_notification_action__WEBPACK_IMPORTED_MODULE_6__["GetAllNotifications"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _notification_action__WEBPACK_IMPORTED_MODULE_6__["GetAllNotifications"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], NotificationState.prototype, "getAllNotifications", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_notification_model__WEBPACK_IMPORTED_MODULE_2__["NotificationStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], NotificationState, "GetRecent10", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_notification_model__WEBPACK_IMPORTED_MODULE_2__["NotificationStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], NotificationState, "GetAllNotifications", null);
NotificationState = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["State"])({
        name: 'notification',
        defaults: {
            recentNotifications: [],
            notifications: []
        }
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_services_notifications_service__WEBPACK_IMPORTED_MODULE_1__["NotificationsService"]])
], NotificationState);



/***/ }),

/***/ "./src/app/post/store/board.action.ts":
/*!********************************************!*\
  !*** ./src/app/post/store/board.action.ts ***!
  \********************************************/
/*! exports provided: GetAllBoards, CreateBoard, GetOwnBoard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllBoards", function() { return GetAllBoards; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateBoard", function() { return CreateBoard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOwnBoard", function() { return GetOwnBoard; });
class GetAllBoards {
}
GetAllBoards.type = '[ Post New ] GetAllBoards';
class CreateBoard {
    constructor(payload) {
        this.payload = payload;
    }
}
CreateBoard.type = '[ Create Board Modal ] CreateBoard';
CreateBoard.ctorParameters = () => [
    { type: undefined }
];
class GetOwnBoard {
}
GetOwnBoard.type = '[ View My Own Board ] GetOwnBoard';


/***/ }),

/***/ "./src/app/post/store/board.state.ts":
/*!*******************************************!*\
  !*** ./src/app/post/store/board.state.ts ***!
  \*******************************************/
/*! exports provided: BoardState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardState", function() { return BoardState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_models_board_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/models/board.model */ "./src/app/core/models/board.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _app_core_services_board_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/core/services/board.service */ "./src/app/core/services/board.service.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _board_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./board.action */ "./src/app/post/store/board.action.ts");





//NGXS


let BoardState = class BoardState {
    constructor(boardService, store) {
        this.boardService = boardService;
        this.store = store;
    }
    static getAllBoards(state) {
        return state.boards;
    }
    static getOwnBoard(state) {
        return state.boards;
    }
    createBoard({ patchState }, { payload }) {
        return this.boardService.createBoard(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])((result) => { }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getOwnBoard({ patchState }, {}) {
        return this.boardService.getMyBoards().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])((result) => {
            patchState({
                boards: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
};
BoardState.ctorParameters = () => [
    { type: _app_core_services_board_service__WEBPACK_IMPORTED_MODULE_4__["BoardService"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_board_action__WEBPACK_IMPORTED_MODULE_6__["CreateBoard"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _board_action__WEBPACK_IMPORTED_MODULE_6__["CreateBoard"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], BoardState.prototype, "createBoard", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_board_action__WEBPACK_IMPORTED_MODULE_6__["GetOwnBoard"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _board_action__WEBPACK_IMPORTED_MODULE_6__["GetOwnBoard"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], BoardState.prototype, "getOwnBoard", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_board_model__WEBPACK_IMPORTED_MODULE_1__["BoardStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], BoardState, "getAllBoards", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_board_model__WEBPACK_IMPORTED_MODULE_1__["BoardStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], BoardState, "getOwnBoard", null);
BoardState = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["State"])({
        name: 'board',
        defaults: {
            board: null,
            boards: [],
            post: []
        }
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_services_board_service__WEBPACK_IMPORTED_MODULE_4__["BoardService"], _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
], BoardState);



/***/ }),

/***/ "./src/app/post/store/post.action.ts":
/*!*******************************************!*\
  !*** ./src/app/post/store/post.action.ts ***!
  \*******************************************/
/*! exports provided: GetAllPosts, GetPostsPerPage, GetPost, GetOwnPosts, SearchPost, GetAllTrends, GetAllTrendsToday, GetAllTrendsWeek, GetAllTrendsMonth, GetTrendsPerPage, GetAllFavorites, GetAllFavoritesToday, GetAllFavoritesWeek, GetAllFavoritesMonth, GetFavoritesPerPage, AddPost, DeletePost, EditPost, LikePost, PreviewImageVideo, GetAllBoards, PinPost, UserPostById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllPosts", function() { return GetAllPosts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetPostsPerPage", function() { return GetPostsPerPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetPost", function() { return GetPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOwnPosts", function() { return GetOwnPosts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPost", function() { return SearchPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllTrends", function() { return GetAllTrends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllTrendsToday", function() { return GetAllTrendsToday; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllTrendsWeek", function() { return GetAllTrendsWeek; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllTrendsMonth", function() { return GetAllTrendsMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetTrendsPerPage", function() { return GetTrendsPerPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllFavorites", function() { return GetAllFavorites; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllFavoritesToday", function() { return GetAllFavoritesToday; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllFavoritesWeek", function() { return GetAllFavoritesWeek; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllFavoritesMonth", function() { return GetAllFavoritesMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetFavoritesPerPage", function() { return GetFavoritesPerPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPost", function() { return AddPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeletePost", function() { return DeletePost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPost", function() { return EditPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LikePost", function() { return LikePost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreviewImageVideo", function() { return PreviewImageVideo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllBoards", function() { return GetAllBoards; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinPost", function() { return PinPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPostById", function() { return UserPostById; });
class GetAllPosts {
}
GetAllPosts.type = '[Home Page] GetAllPosts';
class GetPostsPerPage {
    constructor(key, pageNumber) {
        this.key = key;
        this.pageNumber = pageNumber;
    }
}
GetPostsPerPage.type = '[Home Page] GetPostsPerPage';
GetPostsPerPage.ctorParameters = () => [
    { type: String },
    { type: String }
];
class GetPost {
    constructor(id) {
        this.id = id;
    }
}
GetPost.type = '[Home Page Modal] GetPost';
GetPost.ctorParameters = () => [
    { type: Number }
];
class GetOwnPosts {
}
GetOwnPosts.type = '[Profile Page] GetOwnPosts';
class SearchPost {
    constructor(payload) {
        this.payload = payload;
    }
}
SearchPost.type = '[Navbar] SearchPost';
SearchPost.ctorParameters = () => [
    { type: undefined }
];
class GetAllTrends {
}
GetAllTrends.type = '[Trends Page] GetAllTrends';
class GetAllTrendsToday {
}
GetAllTrendsToday.type = '[Trends Page] GetAllTrendsToday';
class GetAllTrendsWeek {
}
GetAllTrendsWeek.type = '[Trends Page] GetAllTrendsWeek';
class GetAllTrendsMonth {
}
GetAllTrendsMonth.type = '[Trends Page] GetAllTrendsMonth';
class GetTrendsPerPage {
    constructor(key, pageNumber) {
        this.key = key;
        this.pageNumber = pageNumber;
    }
}
GetTrendsPerPage.type = '[Trends Page] GetTrendsPerPage';
GetTrendsPerPage.ctorParameters = () => [
    { type: String },
    { type: String }
];
class GetAllFavorites {
}
GetAllFavorites.type = '[Favorites Page] GetAllFavorites';
class GetAllFavoritesToday {
}
GetAllFavoritesToday.type = '[Favorites Page] GetAllFavoritesToday';
class GetAllFavoritesWeek {
}
GetAllFavoritesWeek.type = '[Favorites Page] GetAllFavoritesToday';
class GetAllFavoritesMonth {
}
GetAllFavoritesMonth.type = '[Favorites Page] GetAllFavoritesMonth';
class GetFavoritesPerPage {
    constructor(key, pageNumber) {
        this.key = key;
        this.pageNumber = pageNumber;
    }
}
GetFavoritesPerPage.type = '[Favorites Page] GetFavoritesPerPage';
GetFavoritesPerPage.ctorParameters = () => [
    { type: String },
    { type: String }
];
class AddPost {
    constructor(payload) {
        this.payload = payload;
    }
}
AddPost.type = '[Post Create Page] AddPost';
AddPost.ctorParameters = () => [
    { type: FormData }
];
class DeletePost {
    constructor(payload) {
        this.payload = payload;
    }
}
DeletePost.type = '[Home Page] DeletePost';
DeletePost.ctorParameters = () => [
    { type: undefined }
];
class EditPost {
    constructor(payload) {
        this.payload = payload;
    }
}
EditPost.type = '[Home Page] EditPost';
EditPost.ctorParameters = () => [
    { type: undefined }
];
class LikePost {
    constructor(payload) {
        this.payload = payload;
    }
}
LikePost.type = '[Home Page] LikePost';
LikePost.ctorParameters = () => [
    { type: undefined }
];
class PreviewImageVideo {
    constructor(event, url) {
        this.event = event;
        this.url = url;
    }
}
PreviewImageVideo.type = '[Post Create Page] PreviewImageVideo';
PreviewImageVideo.ctorParameters = () => [
    { type: Event },
    { type: undefined }
];
class GetAllBoards {
}
GetAllBoards.type = '[ Post Create Page ] GetAllBoards';
class PinPost {
    constructor(payload) {
        this.payload = payload;
    }
}
PinPost.type = '[ Pin Post Modal Page ] Pin Post';
PinPost.ctorParameters = () => [
    { type: undefined }
];
class UserPostById {
    constructor(id) {
        this.id = id;
    }
}
UserPostById.type = '[ UserViewProfilePage ]  userPostById';
UserPostById.ctorParameters = () => [
    { type: Number }
];


/***/ }),

/***/ "./src/app/post/store/post.state.ts":
/*!******************************************!*\
  !*** ./src/app/post/store/post.state.ts ***!
  \******************************************/
/*! exports provided: PostState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostState", function() { return PostState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_services_posts_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/services/posts.service */ "./src/app/core/services/posts.service.ts");
/* harmony import */ var _app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/core/models/post.model */ "./src/app/core/models/post.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _post_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _app_core_models_board_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @app/core/models/board.model */ "./src/app/core/models/board.model.ts");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");







// NGXS




let PostState = class PostState {
    constructor(postsService, store) {
        this.postsService = postsService;
        this.store = store;
    }
    static getAllPosts(state) {
        return state.posts;
    }
    static userPostById(state) {
        return state.posts;
    }
    static filteredPosts(state) {
        return state.posts
            .filter((p) => {
            console.log(p);
            if (state.searchText) {
                if (p.caption.toLowerCase().includes(state.searchText.toLowerCase())) {
                    return p;
                }
            }
            else {
                return p;
            }
        })
            .sort((a, b) => {
            return a - b;
        });
    }
    static getPost(state) {
        return state.post;
    }
    static getOwnPost(state) {
        return state.posts;
    }
    static getFileUrl(state) {
        return state.fileUrl;
    }
    static getFormat(state) {
        return state.format;
    }
    static getTotalPages(state) {
        return state.totalPages;
    }
    static getItemsPerPage(state) {
        return state.itemsPerPage;
    }
    static getCurrentPage(state) {
        return state.currentPage;
    }
    static getAllBoards(state) {
        return state.boards;
    }
    userPostById({ patchState }, { id }) {
        return this.postsService.getPostImageById(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            patchState({
                posts: result
            }),
                Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
                });
        }));
    }
    getAllBoards({ patchState }, {}) {
        const userId = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_5__["AuthState"].getUserId);
        return this.postsService.getAllBoards().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            patchState({
                boards: result.filter(boards => boards.user_id == userId)
            });
            if (result.length == 0) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Success', 'Board is Empty', 'success').then(() => { });
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllPosts({ patchState }, {}) {
        return this.postsService.getAll().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getPostsPerPage(ctx, { key, pageNumber }) {
        return this.postsService.getPostsPerPage(key, pageNumber).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            ctx.patchState({
                posts: result['data'],
                totalPages: result['total'],
                itemsPerPage: result['per_page'],
                currentPage: result['current_page']
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getPost(ctx, { id }) {
        return this.postsService.getPost(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            ctx.patchState({
                post: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getOwnPosts({ patchState }, {}) {
        return this.postsService.getOwn().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    pinPost({ patchState }, { payload }) {
        return this.postsService.pinPost(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Pinned Post', 'You have pin this post', 'success').then(() => {
                this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_10__["Navigate"](['/home']));
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    setFilter({ patchState }, { payload }) {
        patchState({
            searchText: payload.val
        });
    }
    getAllTrends({ patchState }, {}) {
        return this.postsService.getAll().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllTrendsToday({ patchState }, {}) {
        return this.postsService.getAllTrendsToday().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllTrendsWeek({ patchState }, {}) {
        return this.postsService.getAllTrendsWeek().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllTrendsMonth({ patchState }, {}) {
        return this.postsService.getAllTrendsMonth().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getTrendsPerPage(ctx, { key, pageNumber }) {
        return this.postsService.getTrendsPerPage(key, pageNumber).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            ctx.patchState({
                posts: result['data'],
                totalPages: result['total'],
                itemsPerPage: result['per_page'],
                currentPage: result['current_page']
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllFavorites({ patchState }, {}) {
        return this.postsService.getAllFavorites().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllFavoritesToday({ patchState }, {}) {
        return this.postsService.getAllFavoritesToday().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllFavoritesWeek({ patchState }, {}) {
        return this.postsService.getAllFavoritesWeek().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getAllFavoritesMonth({ patchState }, {}) {
        return this.postsService.getAllFavoritesMonth().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                posts: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    getFavoritesPerPage(ctx, { key, pageNumber }) {
        return this.postsService.getFavoritesPerPage(key, pageNumber).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            ctx.patchState({
                posts: result['data'],
                totalPages: result['total'],
                itemsPerPage: result['per_page'],
                currentPage: result['current_page']
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    addPost(ctx, { payload }) {
        return this.postsService.addPost(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(() => {
            // return ctx.dispatch(new GetBranchesPerPage('', '1'));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    deletePost({ getState, setState }, { payload }) {
        return this.postsService.deletePost(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(() => {
            const state = getState();
            const filteredArray = state.posts.filter(item => item.id !== payload.id);
            setState(Object.assign({}, state, { posts: filteredArray }));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    likePost(ctx, { payload }) {
        return this.postsService.likePost(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(() => { }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    previewImageVideo(ctx, { event, url }) {
        const file = event.target.files && event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            if (file.type.indexOf('image') > -1) {
                ctx.patchState({
                    format: 'image'
                });
            }
            else if (file.type.indexOf('video') > -1) {
                ctx.patchState({
                    format: 'video'
                });
            }
            reader.onload = event2 => {
                url = event2.target.result;
                ctx.patchState({
                    fileUrl: url
                });
            };
        }
    }
};
PostState.ctorParameters = () => [
    { type: _app_core_services_posts_service__WEBPACK_IMPORTED_MODULE_1__["PostsService"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["UserPostById"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["UserPostById"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "userPostById", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllBoards"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllBoards"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllBoards", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllPosts"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllPosts"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllPosts", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetPostsPerPage"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetPostsPerPage"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getPostsPerPage", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetPost"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetPost"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getPost", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetOwnPosts"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetOwnPosts"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getOwnPosts", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["PinPost"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["PinPost"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "pinPost", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["SearchPost"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["SearchPost"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "setFilter", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllTrends"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllTrends"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllTrends", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllTrendsToday"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllTrendsToday"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllTrendsToday", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllTrendsWeek"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllTrendsWeek"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllTrendsWeek", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllTrendsMonth"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllTrendsMonth"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllTrendsMonth", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetTrendsPerPage"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetTrendsPerPage"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getTrendsPerPage", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllFavorites"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllFavorites"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllFavorites", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllFavoritesToday"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllFavoritesToday"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllFavoritesToday", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllFavoritesWeek"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllFavoritesWeek"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllFavoritesWeek", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllFavoritesMonth"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetAllFavoritesMonth"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getAllFavoritesMonth", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["GetFavoritesPerPage"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["GetFavoritesPerPage"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "getFavoritesPerPage", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["AddPost"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["AddPost"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "addPost", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["DeletePost"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["DeletePost"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "deletePost", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["LikePost"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["LikePost"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "likePost", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Action"])(_post_action__WEBPACK_IMPORTED_MODULE_8__["PreviewImageVideo"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _post_action__WEBPACK_IMPORTED_MODULE_8__["PreviewImageVideo"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState.prototype, "previewImageVideo", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getAllPosts", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "userPostById", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "filteredPosts", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getPost", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getOwnPost", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getFileUrl", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getFormat", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getTotalPages", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getItemsPerPage", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_post_model__WEBPACK_IMPORTED_MODULE_2__["PostStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getCurrentPage", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_board_model__WEBPACK_IMPORTED_MODULE_9__["BoardStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], PostState, "getAllBoards", null);
PostState = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["State"])({
        name: 'post',
        defaults: {
            posts: [],
            post: null,
            fileUrl: null,
            format: null,
            totalPages: null,
            itemsPerPage: null,
            currentPage: null,
            searchText: null
        }
    }),
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_7__["State"])({
        name: 'board',
        defaults: {
            board: null,
            boards: [],
            post: []
        }
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_services_posts_service__WEBPACK_IMPORTED_MODULE_1__["PostsService"], _ngxs_store__WEBPACK_IMPORTED_MODULE_7__["Store"]])
], PostState);



/***/ }),

/***/ "./src/app/profile/store/profile.action.ts":
/*!*************************************************!*\
  !*** ./src/app/profile/store/profile.action.ts ***!
  \*************************************************/
/*! exports provided: GetProfile, EditProfile, ViewUserProfile, GetFollowers, GetFollowings, GetAllRequest, BlockUser, FollowUserPublic, FollowUserPrivate, AcceptRequest, GetAllCategoriesProfile, SelectCategory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetProfile", function() { return GetProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfile", function() { return EditProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewUserProfile", function() { return ViewUserProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetFollowers", function() { return GetFollowers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetFollowings", function() { return GetFollowings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllRequest", function() { return GetAllRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlockUser", function() { return BlockUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FollowUserPublic", function() { return FollowUserPublic; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FollowUserPrivate", function() { return FollowUserPrivate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceptRequest", function() { return AcceptRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllCategoriesProfile", function() { return GetAllCategoriesProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectCategory", function() { return SelectCategory; });
class GetProfile {
}
GetProfile.type = '[Profile Page] GetProfile';
class EditProfile {
    constructor(payload) {
        this.payload = payload;
    }
}
EditProfile.type = '[Edit Profile Page] EditProfile';
EditProfile.ctorParameters = () => [
    { type: FormData }
];
class ViewUserProfile {
    constructor(id) {
        this.id = id;
    }
}
ViewUserProfile.type = '[ View User Profile Page ] ViewUserProfile';
ViewUserProfile.ctorParameters = () => [
    { type: Number }
];
class GetFollowers {
    constructor(id) {
        this.id = id;
    }
}
GetFollowers.type = '[ Followers Page ]  GetFollowers';
GetFollowers.ctorParameters = () => [
    { type: Number }
];
class GetFollowings {
    constructor(id) {
        this.id = id;
    }
}
GetFollowings.type = '[ Followers Page ]  GetFollowings';
GetFollowings.ctorParameters = () => [
    { type: Number }
];
class GetAllRequest {
}
GetAllRequest.type = ' [ Get All Request ] GetAllRequest';
class BlockUser {
    constructor(id) {
        this.id = id;
    }
}
BlockUser.type = '[ Block User ] BlockUser';
BlockUser.ctorParameters = () => [
    { type: Number }
];
class FollowUserPublic {
    constructor(id) {
        this.id = id;
    }
}
FollowUserPublic.type = '[ Follow User Public Account ] FollowUserPublic';
FollowUserPublic.ctorParameters = () => [
    { type: Number }
];
class FollowUserPrivate {
    constructor(id) {
        this.id = id;
    }
}
FollowUserPrivate.type = '[ Follow User Private Account ] FollowUserPrivate';
FollowUserPrivate.ctorParameters = () => [
    { type: Number }
];
class AcceptRequest {
    constructor(id) {
        this.id = id;
    }
}
AcceptRequest.type = '[ Accept Friend Request ]  AcceptRequest';
AcceptRequest.ctorParameters = () => [
    { type: Number }
];
class GetAllCategoriesProfile {
}
GetAllCategoriesProfile.type = ' [ All Category ] GetAllCategoriesProfile ';
class SelectCategory {
    constructor(payload) {
        this.payload = payload;
    }
}
SelectCategory.type = '[ Select Cateogry ]';
SelectCategory.ctorParameters = () => [
    { type: undefined }
];


/***/ }),

/***/ "./src/app/profile/store/profile.state.ts":
/*!************************************************!*\
  !*** ./src/app/profile/store/profile.state.ts ***!
  \************************************************/
/*! exports provided: ProfileState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileState", function() { return ProfileState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _app_core_services_categories_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/core/services/categories.service */ "./src/app/core/services/categories.service.ts");
/* harmony import */ var _app_core_models_profile_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/core/models/profile.model */ "./src/app/core/models/profile.model.ts");
/* harmony import */ var _app_core_models_request_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/core/models/request.model */ "./src/app/core/models/request.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _app_core_services_users_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app/core/services/users.service */ "./src/app/core/services/users.service.ts");
/* harmony import */ var _app_core_models_categories_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app/core/models/categories.model */ "./src/app/core/models/categories.model.ts");
/* harmony import */ var _post_store_post_action__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../post/store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _profile_action__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _app_auth_store_auth_action__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app/auth/store/auth.action */ "./src/app/auth/store/auth.action.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_13__);










// NGXS




let ProfileState = class ProfileState {
    constructor(authService, usersService, store, categoriesService) {
        this.authService = authService;
        this.usersService = usersService;
        this.store = store;
        this.categoriesService = categoriesService;
    }
    static getProfile(state) {
        return state.profile;
    }
    static getAllCategories(state) {
        return state.cats;
    }
    static getSessionToken(state) {
        return state.session_token;
    }
    static viewUserProfile(state) {
        return state.profile;
    }
    static getFollowers(state) {
        return state.profiles;
    }
    static getAllRequest(state) {
        return state.requests;
    }
    selectCategory({ patchState }, { payload }) {
        return this.categoriesService.selectCategories(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            this.store.dispatch(new _post_store_post_action__WEBPACK_IMPORTED_MODULE_9__["GetAllPosts"]());
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    getAllCategories({ patchState }, {}) {
        return this.categoriesService.getAllCategories().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            patchState({
                cats: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    getFollowers({ patchState }, { id }) {
        return this.usersService.getFollowers(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            patchState({
                profiles: result
            });
            console.log(result);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    acceptRequest({ patchState }, { id }) {
        return this.usersService.confirmRequest(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            patchState({});
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    getAllRequest({ patchState }, {}) {
        return this.usersService.getAllFriendRequest().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            patchState({
                requests: result['requests']
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    getFollowings({ patchState }, { id }) {
        return this.usersService.getFollowing(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            patchState({
                profiles: result
            });
            console.log(result);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    viewUserProfile({ patchState }, { id }) {
        return this.usersService.viewUser(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            patchState({
                profile: result
            });
            console.log(result);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    getProfile({ patchState }, {}) {
        return this.authService.getProfile().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            this.store.dispatch(new _app_auth_store_auth_action__WEBPACK_IMPORTED_MODULE_12__["ReplaceProfilePhoto"](result.image));
            console.log(result);
            patchState({
                profile: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    editProfile({ patchState }, { payload }) {
        return this.usersService.updateUser(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            console.log(result);
            patchState({
                profile: result,
                session_token: result.session_token
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
    followUserPrivate({ patchState }, { id }) {
        return this.usersService.followUserPrivateAccount(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])((result) => {
            patchState({});
            sweetalert2__WEBPACK_IMPORTED_MODULE_13___default.a.fire('Successfully', 'Request Sent', 'success').then(() => {
                this.store.dispatch(new _profile_action__WEBPACK_IMPORTED_MODULE_11__["ViewUserProfile"](id));
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    }
};
ProfileState.ctorParameters = () => [
    { type: _app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] },
    { type: _app_core_services_users_service__WEBPACK_IMPORTED_MODULE_7__["UsersService"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Store"] },
    { type: _app_core_services_categories_service__WEBPACK_IMPORTED_MODULE_2__["CategoriesService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["SelectCategory"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["SelectCategory"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "selectCategory", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["GetAllCategoriesProfile"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["GetAllCategoriesProfile"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "getAllCategories", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["GetFollowers"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["GetFollowers"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "getFollowers", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["AcceptRequest"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["AcceptRequest"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "acceptRequest", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["GetAllRequest"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["GetAllRequest"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "getAllRequest", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["GetFollowings"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["GetFollowings"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "getFollowings", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["ViewUserProfile"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["ViewUserProfile"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "viewUserProfile", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["GetProfile"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["GetProfile"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "getProfile", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["EditProfile"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["EditProfile"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "editProfile", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Action"])(_profile_action__WEBPACK_IMPORTED_MODULE_11__["FollowUserPrivate"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _profile_action__WEBPACK_IMPORTED_MODULE_11__["FollowUserPrivate"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState.prototype, "followUserPrivate", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_profile_model__WEBPACK_IMPORTED_MODULE_3__["ProfileStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState, "getProfile", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_categories_model__WEBPACK_IMPORTED_MODULE_8__["CategoriesStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState, "getAllCategories", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_profile_model__WEBPACK_IMPORTED_MODULE_3__["ProfileStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState, "getSessionToken", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_profile_model__WEBPACK_IMPORTED_MODULE_3__["ProfileStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState, "viewUserProfile", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_profile_model__WEBPACK_IMPORTED_MODULE_3__["ProfileStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState, "getFollowers", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_request_model__WEBPACK_IMPORTED_MODULE_4__["RequestsStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], ProfileState, "getAllRequest", null);
ProfileState = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["State"])({
        name: 'categories',
        defaults: {
            cat: null,
            cats: []
        }
    }),
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["State"])({
        name: 'profile',
        defaults: {
            profile: null,
            profiles: [],
            session_token: null
        }
    }),
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_10__["State"])({
        name: 'request',
        defaults: {
            request: null,
            requests: []
        }
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
        _app_core_services_users_service__WEBPACK_IMPORTED_MODULE_7__["UsersService"],
        _ngxs_store__WEBPACK_IMPORTED_MODULE_10__["Store"],
        _app_core_services_categories_service__WEBPACK_IMPORTED_MODULE_2__["CategoriesService"]])
], ProfileState);



/***/ }),

/***/ "./src/app/shared/modals/category-select/category-select.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/shared/modals/category-select/category-select.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2RhbHMvY2F0ZWdvcnktc2VsZWN0L2NhdGVnb3J5LXNlbGVjdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/shared/modals/category-select/category-select.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/shared/modals/category-select/category-select.component.ts ***!
  \****************************************************************************/
/*! exports provided: CategorySelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategorySelectComponent", function() { return CategorySelectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm2015/Subject.js");
/* harmony import */ var _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../profile/store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _profile_store_profile_state__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../profile/store/profile.state */ "./src/app/profile/store/profile.state.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");









let CategorySelectComponent = class CategorySelectComponent {
    constructor(store, formBuilder, modalService, bsModalRef) {
        this.store = store;
        this.formBuilder = formBuilder;
        this.modalService = modalService;
        this.bsModalRef = bsModalRef;
        this.cat_form = this.formBuilder.group({
            ids: this.formBuilder.array([]),
            id: null
        });
        this.onClose = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    ngOnInit() {
        this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_3__["GetAllCategoriesProfile"]());
    }
    onCheckboxChange(e) {
        const ids = this.cat_form.get('ids');
        if (e.target.checked) {
            ids.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"](e.target.value));
        }
        else {
            let i = 0;
            ids.controls.forEach((item) => {
                if (item.value == e.target.value) {
                    ids.removeAt(i);
                    return;
                }
                i++;
            });
        }
    }
    submit() {
        this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_3__["SelectCategory"](this.cat_form.value)).subscribe(response => {
            this.onClose.next(true);
            this.modalRef.hide();
        }, err => { });
    }
};
CategorySelectComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"] },
    { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"] },
    { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Select"])(_profile_store_profile_state__WEBPACK_IMPORTED_MODULE_4__["ProfileState"].getAllCategories),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"])
], CategorySelectComponent.prototype, "categories$", void 0);
CategorySelectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-select',
        template: __webpack_require__(/*! raw-loader!./category-select.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/modals/category-select/category-select.component.html"),
        styles: [__webpack_require__(/*! ./category-select.component.scss */ "./src/app/shared/modals/category-select/category-select.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
        ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"],
        ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"]])
], CategorySelectComponent);



/***/ }),

/***/ "./src/app/shared/modals/privatekey-export/privatekey-export.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/shared/modals/privatekey-export/privatekey-export.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2RhbHMvcHJpdmF0ZWtleS1leHBvcnQvcHJpdmF0ZWtleS1leHBvcnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/shared/modals/privatekey-export/privatekey-export.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/shared/modals/privatekey-export/privatekey-export.component.ts ***!
  \********************************************************************************/
/*! exports provided: PrivatekeyExportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivatekeyExportComponent", function() { return PrivatekeyExportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _profile_store_profile_state__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../profile/store/profile.state */ "./src/app/profile/store/profile.state.ts");
/* harmony import */ var _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../profile/store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");







let PrivatekeyExportComponent = class PrivatekeyExportComponent {
    constructor(store) {
        this.store = store;
    }
    ngOnInit() {
        this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_5__["GetProfile"]()).subscribe(val => {
            this.wallet_name = val.auth.wallet.name;
        });
    }
    /**
     * Method to export account
     * @param {any} account
     * @memberof ViewAllAccountsComponent
     */
    exportAccount(account) {
        const acc = Object.assign({}, account);
        const accounts = [];
        accounts.push(acc);
        const wallet = {
            name: this.wallet_name,
            accounts
        };
        wallet.accounts[0].name = 'Primary_Account';
        wallet.accounts[0].firstAccount = true;
        wallet.accounts[0].default = true;
        const wordArray = crypto_js__WEBPACK_IMPORTED_MODULE_2__["enc"].Utf8.parse(JSON.stringify(wallet));
        const file = crypto_js__WEBPACK_IMPORTED_MODULE_2__["enc"].Base64.stringify(wordArray);
        // Word array to base64
        const now = Date.now();
        const date = new Date(now);
        const year = date.getFullYear();
        const month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
        const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        const blob = new Blob([file], { type: '' });
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        // the filename you want
        let networkTypeName = 'imagerax';
        networkTypeName = networkTypeName.includes(' ') ? networkTypeName.split(' ').join('') : networkTypeName;
        a.download = `${wallet.name}_${networkTypeName}_${year}-${month}-${day}.wlt`;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
    }
};
PrivatekeyExportComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Select"])(_profile_store_profile_state__WEBPACK_IMPORTED_MODULE_4__["ProfileState"].getProfile),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"])
], PrivatekeyExportComponent.prototype, "profile$", void 0);
PrivatekeyExportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-privatekey-export',
        template: __webpack_require__(/*! raw-loader!./privatekey-export.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/modals/privatekey-export/privatekey-export.component.html"),
        styles: [__webpack_require__(/*! ./privatekey-export.component.scss */ "./src/app/shared/modals/privatekey-export/privatekey-export.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
], PrivatekeyExportComponent);



/***/ }),

/***/ "./src/app/shared/modals/qr-code-id/qr-code-id.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/shared/modals/qr-code-id/qr-code-id.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".qr-modal {\n  padding: 3rem 5rem;\n}\n.qr-modal .qrCanvass {\n  padding: 2rem 5rem;\n  display: table;\n  width: 100%;\n}\n.qr-modal .qrCanvass .qrPanel {\n  text-align: center;\n  vertical-align: middle;\n  display: table-cell;\n  font-size: 25px;\n}\n.qr-modal .qrCanvass .qrPanel .modalTitle {\n  text-align: center;\n  font-size: 26px;\n  color: #f36621;\n}\n.qr-modal .qrCanvass .qrPanel .qrcode img {\n  height: 200px;\n  width: 200px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vZGFscy9xci1jb2RlLWlkL0M6XFxVc2Vyc1xcQVNVU1xcRGVza3RvcFxcUHJvamVjdFxcaW1hZ2VyYXgtc2lyaXVzLWZyb250ZW5kXFxzaXJpdXMtbGl2ZXdhbGwtLS1mcm9udGVuZC9zcmNcXGFwcFxcc2hhcmVkXFxtb2RhbHNcXHFyLWNvZGUtaWRcXHFyLWNvZGUtaWQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2RhbHMvcXItY29kZS1pZC9xci1jb2RlLWlkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7QUNDRjtBRENFO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtBQ0NKO0FEQ0k7RUFDRSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FDQ047QURDTTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNDUjtBRElRO0VBQ0UsYUFBQTtFQUNBLFlBQUE7QUNGViIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2RhbHMvcXItY29kZS1pZC9xci1jb2RlLWlkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnFyLW1vZGFsIHtcclxuICBwYWRkaW5nOiAzcmVtIDVyZW07XHJcblxyXG4gIC5xckNhbnZhc3Mge1xyXG4gICAgcGFkZGluZzogMnJlbSA1cmVtO1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAucXJQYW5lbCB7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgZGlzcGxheTogdGFibGUtY2VsbDtcclxuICAgICAgZm9udC1zaXplOiAyNXB4O1xyXG5cclxuICAgICAgLm1vZGFsVGl0bGUge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICAgICAgY29sb3I6ICNmMzY2MjE7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIG5neC1xcmNvZGUgY3NzIHByb3BlcnR5XHJcbiAgICAgIC5xcmNvZGUge1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDIwMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCIucXItbW9kYWwge1xuICBwYWRkaW5nOiAzcmVtIDVyZW07XG59XG4ucXItbW9kYWwgLnFyQ2FudmFzcyB7XG4gIHBhZGRpbmc6IDJyZW0gNXJlbTtcbiAgZGlzcGxheTogdGFibGU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnFyLW1vZGFsIC5xckNhbnZhc3MgLnFyUGFuZWwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIGRpc3BsYXk6IHRhYmxlLWNlbGw7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cbi5xci1tb2RhbCAucXJDYW52YXNzIC5xclBhbmVsIC5tb2RhbFRpdGxlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIGNvbG9yOiAjZjM2NjIxO1xufVxuLnFyLW1vZGFsIC5xckNhbnZhc3MgLnFyUGFuZWwgLnFyY29kZSBpbWcge1xuICBoZWlnaHQ6IDIwMHB4O1xuICB3aWR0aDogMjAwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/shared/modals/qr-code-id/qr-code-id.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/shared/modals/qr-code-id/qr-code-id.component.ts ***!
  \******************************************************************/
/*! exports provided: QrCodeIdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrCodeIdComponent", function() { return QrCodeIdComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let QrCodeIdComponent = class QrCodeIdComponent {
    constructor() { }
    ngOnInit() {
        this.userQrId = this.payload;
        console.log(this.userQrId);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], QrCodeIdComponent.prototype, "payload", void 0);
QrCodeIdComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-qr-code-id',
        template: __webpack_require__(/*! raw-loader!./qr-code-id.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/modals/qr-code-id/qr-code-id.component.html"),
        styles: [__webpack_require__(/*! ./qr-code-id.component.scss */ "./src/app/shared/modals/qr-code-id/qr-code-id.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], QrCodeIdComponent);



/***/ }),

/***/ "./src/app/shared/modals/qr-modal/qr-modal.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/shared/modals/qr-modal/qr-modal.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".qr-modal {\n  padding: 3rem 5rem;\n}\n.qr-modal .qrCanvass {\n  padding: 2rem 5rem;\n  display: table;\n  width: 100%;\n}\n.qr-modal .qrCanvass .qrPanel {\n  text-align: center;\n  vertical-align: middle;\n  display: table-cell;\n  font-size: 25px;\n}\n.qr-modal .qrCanvass .qrPanel .modalTitle {\n  text-align: center;\n  font-size: 26px;\n  color: #f36621;\n}\n.qr-modal .qrCanvass .qrPanel .qrcode img {\n  height: 200px;\n  width: 200px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vZGFscy9xci1tb2RhbC9DOlxcVXNlcnNcXEFTVVNcXERlc2t0b3BcXFByb2plY3RcXGltYWdlcmF4LXNpcml1cy1mcm9udGVuZFxcc2lyaXVzLWxpdmV3YWxsLS0tZnJvbnRlbmQvc3JjXFxhcHBcXHNoYXJlZFxcbW9kYWxzXFxxci1tb2RhbFxccXItbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2RhbHMvcXItbW9kYWwvcXItbW9kYWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtBQ0NGO0FEQ0U7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FDQ0o7QURDSTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNDUjtBRENRO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQ0NaO0FESVk7RUFDSSxhQUFBO0VBQ0EsWUFBQTtBQ0ZoQiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2RhbHMvcXItbW9kYWwvcXItbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucXItbW9kYWwge1xyXG4gIHBhZGRpbmc6IDNyZW0gNXJlbTtcclxuXHJcbiAgLnFyQ2FudmFzcyB7XHJcbiAgICBwYWRkaW5nOiAycmVtIDVyZW07XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC5xclBhbmVsIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuXHJcbiAgICAgICAgLm1vZGFsVGl0bGUge1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjZweDtcclxuICAgICAgICAgICAgY29sb3I6ICNmMzY2MjE7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBuZ3gtcXJjb2RlIGNzcyBwcm9wZXJ0eVxyXG4gICAgICAgIC5xcmNvZGUge1xyXG4gICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyMDBweDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAyMDBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiLnFyLW1vZGFsIHtcbiAgcGFkZGluZzogM3JlbSA1cmVtO1xufVxuLnFyLW1vZGFsIC5xckNhbnZhc3Mge1xuICBwYWRkaW5nOiAycmVtIDVyZW07XG4gIGRpc3BsYXk6IHRhYmxlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5xci1tb2RhbCAucXJDYW52YXNzIC5xclBhbmVsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xuICBmb250LXNpemU6IDI1cHg7XG59XG4ucXItbW9kYWwgLnFyQ2FudmFzcyAucXJQYW5lbCAubW9kYWxUaXRsZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBjb2xvcjogI2YzNjYyMTtcbn1cbi5xci1tb2RhbCAucXJDYW52YXNzIC5xclBhbmVsIC5xcmNvZGUgaW1nIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgd2lkdGg6IDIwMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/shared/modals/qr-modal/qr-modal.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/modals/qr-modal/qr-modal.component.ts ***!
  \**************************************************************/
/*! exports provided: QrModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrModalComponent", function() { return QrModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let QrModalComponent = class QrModalComponent {
    constructor() { }
    ngOnInit() {
        this.userWallet = this.payload;
        // console.log("TCL: QrModalComponent -> payload", this.payload)
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], QrModalComponent.prototype, "payload", void 0);
QrModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-qr-modal',
        template: __webpack_require__(/*! raw-loader!./qr-modal.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/modals/qr-modal/qr-modal.component.html"),
        styles: [__webpack_require__(/*! ./qr-modal.component.scss */ "./src/app/shared/modals/qr-modal/qr-modal.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], QrModalComponent);



/***/ }),

/***/ "./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".qr-modal {\n  padding: 3rem 5rem;\n}\n.qr-modal .qrCanvass {\n  padding: 2rem 5rem;\n  display: table;\n  width: 100%;\n}\n.qr-modal .qrCanvass .qrPanel {\n  text-align: center;\n  vertical-align: middle;\n  display: table-cell;\n  font-size: 25px;\n}\n.qr-modal .qrCanvass .qrPanel .modalTitle {\n  text-align: center;\n  font-size: 26px;\n  color: #f36621;\n}\n.qr-modal .qrCanvass .qrPanel .qrcode img {\n  height: 200px;\n  width: 200px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vZGFscy9zaXJpdXMtaWQtbW9kYWwvQzpcXFVzZXJzXFxBU1VTXFxEZXNrdG9wXFxQcm9qZWN0XFxpbWFnZXJheC1zaXJpdXMtZnJvbnRlbmRcXHNpcml1cy1saXZld2FsbC0tLWZyb250ZW5kL3NyY1xcYXBwXFxzaGFyZWRcXG1vZGFsc1xcc2lyaXVzLWlkLW1vZGFsXFxzaXJpdXMtaWQtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2RhbHMvc2lyaXVzLWlkLW1vZGFsL3Npcml1cy1pZC1tb2RhbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0FDQ0Y7QURDRTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUNDSjtBRENJO0VBQ0Usa0JBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0NOO0FEQ007RUFDRSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDQ1I7QURJUTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FDRlYiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvbW9kYWxzL3Npcml1cy1pZC1tb2RhbC9zaXJpdXMtaWQtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucXItbW9kYWwge1xyXG4gIHBhZGRpbmc6IDNyZW0gNXJlbTtcclxuXHJcbiAgLnFyQ2FudmFzcyB7XHJcbiAgICBwYWRkaW5nOiAycmVtIDVyZW07XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgIC5xclBhbmVsIHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xyXG4gICAgICBmb250LXNpemU6IDI1cHg7XHJcblxyXG4gICAgICAubW9kYWxUaXRsZSB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjZweDtcclxuICAgICAgICBjb2xvcjogI2YzNjYyMTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gbmd4LXFyY29kZSBjc3MgcHJvcGVydHlcclxuICAgICAgLnFyY29kZSB7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgICB3aWR0aDogMjAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsIi5xci1tb2RhbCB7XG4gIHBhZGRpbmc6IDNyZW0gNXJlbTtcbn1cbi5xci1tb2RhbCAucXJDYW52YXNzIHtcbiAgcGFkZGluZzogMnJlbSA1cmVtO1xuICBkaXNwbGF5OiB0YWJsZTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ucXItbW9kYWwgLnFyQ2FudmFzcyAucXJQYW5lbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuLnFyLW1vZGFsIC5xckNhbnZhc3MgLnFyUGFuZWwgLm1vZGFsVGl0bGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgY29sb3I6ICNmMzY2MjE7XG59XG4ucXItbW9kYWwgLnFyQ2FudmFzcyAucXJQYW5lbCAucXJjb2RlIGltZyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAyMDBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.ts ***!
  \****************************************************************************/
/*! exports provided: SiriusIdModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SiriusIdModalComponent", function() { return SiriusIdModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/auth/store/auth.state */ "./src/app/auth/store/auth.state.ts");
/* harmony import */ var siriusid_sdk__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! siriusid-sdk */ "../sirius-id-sdk/dist/index.js");
/* harmony import */ var siriusid_sdk__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(siriusid_sdk__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tsjs-xpx-chain-sdk */ "./node_modules/tsjs-xpx-chain-sdk/dist/index.js");
/* harmony import */ var tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_core_services_change_data_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app/core/services/change-data.service */ "./src/app/core/services/change-data.service.ts");
/* harmony import */ var _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../profile/store/profile.action */ "./src/app/profile/store/profile.action.ts");
/* harmony import */ var _core_services_proximax_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../core/services/proximax.service */ "./src/app/core/services/proximax.service.ts");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");
/* harmony import */ var _app_profile_store_profile_state__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app/profile/store/profile.state */ "./src/app/profile/store/profile.state.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_13__);

var SiriusIdModalComponent_1;














let SiriusIdModalComponent = SiriusIdModalComponent_1 = class SiriusIdModalComponent {
    constructor(store, changedDateServices, proximaService, ngbModalService) {
        this.store = store;
        this.changedDateServices = changedDateServices;
        this.proximaService = proximaService;
        this.ngbModalService = ngbModalService;
        this.currentNode = `${_env_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].nodeUrl}`;
        this.connect = false;
        this.credential_id = ['passport'];
        this.changedDateServices.apiNode = this.currentNode;
        this.changedDateServices.updateWebsocket();
        this.listener = new tsjs_xpx_chain_sdk__WEBPACK_IMPORTED_MODULE_5__["Listener"](this.changedDateServices.ws, WebSocket);
        this.wallet = this.store.selectSnapshot(_app_auth_store_auth_state__WEBPACK_IMPORTED_MODULE_3__["AuthState"].wallet_public_key);
        this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_8__["GetProfile"]()).subscribe(user => {
            console.log('---User Profile---', user);
        });
        this.session_token = this.store.selectSnapshot(_app_profile_store_profile_state__WEBPACK_IMPORTED_MODULE_11__["ProfileState"].getSessionToken);
    }
    ngOnInit() { }
    loginRequestAndVerify() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loginRequestMessage = siriusid_sdk__WEBPACK_IMPORTED_MODULE_4__["LoginRequestMessage"].create(this.wallet, this.credential_id);
            const sessionToken = loginRequestMessage.getSessionToken();
            const formData = new FormData();
            formData.append('session_token', sessionToken);
            this.store.dispatch(new _profile_store_profile_action__WEBPACK_IMPORTED_MODULE_8__["EditProfile"](formData)).subscribe(() => {
                // this.store.dispatch(new Navigate(['/home']));
                sweetalert2__WEBPACK_IMPORTED_MODULE_13___default.a.fire('Success', 'Linked Account', 'success').then(() => {
                    this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_10__["Navigate"](['/profile']));
                });
                this.ngbModalService.dismissAll(SiriusIdModalComponent_1);
            }, err => {
                console.log(err);
            });
        });
    }
};
SiriusIdModalComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] },
    { type: _app_core_services_change_data_service__WEBPACK_IMPORTED_MODULE_7__["ChangeDataService"] },
    { type: _core_services_proximax_service__WEBPACK_IMPORTED_MODULE_9__["ProximaxService"] },
    { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_12__["NgbModal"] }
];
SiriusIdModalComponent = SiriusIdModalComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sirius-id-modal',
        template: __webpack_require__(/*! raw-loader!./sirius-id-modal.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.html"),
        styles: [__webpack_require__(/*! ./sirius-id-modal.component.scss */ "./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"],
        _app_core_services_change_data_service__WEBPACK_IMPORTED_MODULE_7__["ChangeDataService"],
        _core_services_proximax_service__WEBPACK_IMPORTED_MODULE_9__["ProximaxService"],
        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_12__["NgbModal"]])
], SiriusIdModalComponent);



/***/ }),

/***/ "./src/app/shared/modals/tip-modal/tip-modal.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/shared/modals/tip-modal/tip-modal.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2RhbHMvdGlwLW1vZGFsL3RpcC1tb2RhbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/shared/modals/tip-modal/tip-modal.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/modals/tip-modal/tip-modal.component.ts ***!
  \****************************************************************/
/*! exports provided: TipModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TipModalComponent", function() { return TipModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _qr_modal_qr_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../qr-modal/qr-modal.component */ "./src/app/shared/modals/qr-modal/qr-modal.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");




// NG-Bootstrap Modal

let TipModalComponent = class TipModalComponent {
    constructor(formBuilder, modalService) {
        this.formBuilder = formBuilder;
        this.modalService = modalService;
        this.form = this.formBuilder.group({
            amount: [1, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            tip: [1, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            message: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.form.get('tip').valueChanges.subscribe(val => {
            this.form.get('amount').setValue(val);
        });
    }
    changeInputTipValue(value) {
        this.form.get('tip').setValue(+value);
        this.setSliderValues();
    }
    setSliderValues() {
        const slider = this.slider.nativeElement;
        const sliderValue = slider;
        slider.style.background =
            'linear-gradient(to right, #f36621 0%, #f36621 ' +
                sliderValue.value +
                '%, #eeeeee ' +
                sliderValue.value +
                '%, #eeeeee)';
        parseInt(sliderValue.value, 10) >= 20
            ? (this.circle1.nativeElement.style.background = '#f36621')
            : (this.circle1.nativeElement.style.background = '#cbcbcb');
        parseInt(sliderValue.value, 10) >= 40
            ? (this.circle2.nativeElement.style.background = '#f36621')
            : (this.circle2.nativeElement.style.background = '#cbcbcb');
        parseInt(sliderValue.value, 10) >= 60
            ? (this.circle3.nativeElement.style.background = '#f36621')
            : (this.circle3.nativeElement.style.background = '#cbcbcb');
        parseInt(sliderValue.value, 10) >= 80
            ? (this.circle4.nativeElement.style.background = '#f36621')
            : (this.circle4.nativeElement.style.background = '#cbcbcb');
        parseInt(sliderValue.value, 10) >= 100
            ? (this.circle5.nativeElement.style.background = '#f36621')
            : (this.circle5.nativeElement.style.background = '#cbcbcb');
    }
    ngAfterViewInit() {
        const slider = this.slider.nativeElement;
        slider.oninput = () => {
            this.setSliderValues();
        };
    }
    get formValue() { return this.form.controls; }
    generateQr() {
        // Generate payload
        const amount = this.formValue.amount.value;
        const message = this.formValue.message.value;
        const address = this.userWallet;
        const urlScheme = "proximax://send";
        const payload = `${urlScheme}?address=${address}&amount=${amount}&message=${message}`;
        const modal = this.modalService.open(_qr_modal_qr_modal_component__WEBPACK_IMPORTED_MODULE_3__["QrModalComponent"]);
        modal.componentInstance.payload = payload;
        modal.result.then(() => { });
    }
};
TipModalComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slider', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], TipModalComponent.prototype, "slider", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('circle1', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], TipModalComponent.prototype, "circle1", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('circle2', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], TipModalComponent.prototype, "circle2", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('circle3', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], TipModalComponent.prototype, "circle3", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('circle4', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], TipModalComponent.prototype, "circle4", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('circle5', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], TipModalComponent.prototype, "circle5", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], TipModalComponent.prototype, "userWallet", void 0);
TipModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tip-modal',
        template: __webpack_require__(/*! raw-loader!./tip-modal.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/modals/tip-modal/tip-modal.component.html"),
        styles: [__webpack_require__(/*! ./tip-modal.component.scss */ "./src/app/shared/modals/tip-modal/tip-modal.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]])
], TipModalComponent);



/***/ }),

/***/ "./src/app/shared/pipes/date-ago.pipe.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/pipes/date-ago.pipe.ts ***!
  \***********************************************/
/*! exports provided: DateAgoPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DateAgoPipe", function() { return DateAgoPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DateAgoPipe = class DateAgoPipe {
    transform(d) {
        const currentDate = new Date(new Date().toUTCString());
        const date = new Date(d + 'Z');
        const year = currentDate.getFullYear() - date.getFullYear();
        const month = currentDate.getMonth() - date.getMonth();
        const day = currentDate.getDate() - date.getDate();
        const hour = currentDate.getHours() - date.getHours();
        const minute = currentDate.getMinutes() - date.getMinutes();
        const second = currentDate.getSeconds() - date.getSeconds();
        const createdSecond = (year * 31556926) + (month * 2629746) + (day * 86400) + (hour * 3600) + (minute * 60) + second;
        if (createdSecond >= 31556926) {
            const yearAgo = Math.floor(createdSecond / 31556926);
            return yearAgo > 1 ? yearAgo + ' years ago' : yearAgo + ' year ago';
        }
        else if (createdSecond >= 2629746) {
            const monthAgo = Math.floor(createdSecond / 2629746);
            return monthAgo > 1 ? monthAgo + ' months ago' : monthAgo + ' month ago';
        }
        else if (createdSecond >= 86400) {
            const dayAgo = Math.floor(createdSecond / 86400);
            return dayAgo > 1 ? dayAgo + ' days ago' : dayAgo + ' day ago';
        }
        else if (createdSecond >= 3600) {
            const hourAgo = Math.floor(createdSecond / 3600);
            return hourAgo > 1 ? hourAgo + ' hours ago' : hourAgo + ' hour ago';
        }
        else if (createdSecond >= 60) {
            const minuteAgo = Math.floor(createdSecond / 60);
            return minuteAgo > 1 ? minuteAgo + ' minutes ago' : minuteAgo + ' minute ago';
        }
        else if (createdSecond < 60) {
            return createdSecond > 1 ? createdSecond + ' seconds ago' : createdSecond + ' second ago';
        }
        else if (createdSecond < 0) {
            return '0 second ago';
        }
    }
};
DateAgoPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'dateAgo',
        pure: true
    })
], DateAgoPipe);



/***/ }),

/***/ "./src/app/shared/routes/private-layout.routes.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/routes/private-layout.routes.ts ***!
  \********************************************************/
/*! exports provided: privateRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "privateRoutes", function() { return privateRoutes; });
/* harmony import */ var _app_home_home_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/home/home.component */ "./src/app/home/home.component.ts");

// Route for content layout with navbar, footer etc...
const privateRoutes = [
    {
        path: 'home',
        component: _app_home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"]
    },
    {
        path: 'wallet',
        loadChildren: () => __webpack_require__.e(/*! import() | wallet-wallet-module */ "wallet-wallet-module").then(__webpack_require__.bind(null, /*! ../../wallet/wallet.module */ "./src/app/wallet/wallet.module.ts")).then(m => m.WalletModule)
    },
    {
        path: 'profile',
        loadChildren: () => Promise.all(/*! import() | profile-profile-module */[__webpack_require__.e("default~boards-boards-module~post-post-module~profile-profile-module"), __webpack_require__.e("profile-profile-module")]).then(__webpack_require__.bind(null, /*! ../../profile/profile.module */ "./src/app/profile/profile.module.ts")).then(m => m.ProfileModule)
    },
    {
        path: 'post',
        loadChildren: () => Promise.all(/*! import() | post-post-module */[__webpack_require__.e("default~boards-boards-module~post-post-module~profile-profile-module"), __webpack_require__.e("post-post-module")]).then(__webpack_require__.bind(null, /*! ../../post/post.module */ "./src/app/post/post.module.ts")).then(m => m.PostModule)
    },
    {
        path: 'trends',
        loadChildren: () => __webpack_require__.e(/*! import() | trends-trends-module */ "trends-trends-module").then(__webpack_require__.bind(null, /*! ../../trends/trends.module */ "./src/app/trends/trends.module.ts")).then(m => m.TrendsModule)
    },
    {
        path: 'favorites',
        loadChildren: () => __webpack_require__.e(/*! import() | favorites-favorites-module */ "favorites-favorites-module").then(__webpack_require__.bind(null, /*! ../../favorites/favorites.module */ "./src/app/favorites/favorites.module.ts")).then(m => m.FavoritesModule)
    },
    {
        path: 'notifications',
        loadChildren: () => __webpack_require__.e(/*! import() | notifications-notifications-module */ "notifications-notifications-module").then(__webpack_require__.bind(null, /*! ../../notifications/notifications.module */ "./src/app/notifications/notifications.module.ts")).then(m => m.NotificationsModule)
    },
    {
        path: 'boards',
        loadChildren: () => Promise.all(/*! import() | boards-boards-module */[__webpack_require__.e("default~boards-boards-module~post-post-module~profile-profile-module"), __webpack_require__.e("boards-boards-module")]).then(__webpack_require__.bind(null, /*! ../../boards/boards.module */ "./src/app/boards/boards.module.ts")).then(m => m.BoardsModule)
    },
    {
        path: 'requests',
        loadChildren: () => __webpack_require__.e(/*! import() | requests-requests-module */ "requests-requests-module").then(__webpack_require__.bind(null, /*! ../../requests/requests.module */ "./src/app/requests/requests.module.ts")).then(m => m.RequestsModule)
    }
];


/***/ }),

/***/ "./src/app/shared/routes/public-layout.routes.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/routes/public-layout.routes.ts ***!
  \*******************************************************/
/*! exports provided: publicRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "publicRoutes", function() { return publicRoutes; });
// Route for content layout without navbar and footer etc... for pages like Login, Registration etc...
const publicRoutes = [
    {
        path: '',
        loadChildren: () => __webpack_require__.e(/*! import() | auth-auth-module */ "auth-auth-module").then(__webpack_require__.bind(null, /*! ../../auth/auth.module */ "./src/app/auth/auth.module.ts")).then(m => m.AuthModule)
    }
];


/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: COMPONENTS, MODULES, SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COMPONENTS", function() { return COMPONENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MODULES", function() { return MODULES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _modals_tip_modal_tip_modal_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modals/tip-modal/tip-modal.component */ "./src/app/shared/modals/tip-modal/tip-modal.component.ts");
/* harmony import */ var _modals_qr_modal_qr_modal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modals/qr-modal/qr-modal.component */ "./src/app/shared/modals/qr-modal/qr-modal.component.ts");
/* harmony import */ var _modals_qr_code_id_qr_code_id_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modals/qr-code-id/qr-code-id.component */ "./src/app/shared/modals/qr-code-id/qr-code-id.component.ts");
/* harmony import */ var _pipes_date_ago_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pipes/date-ago.pipe */ "./src/app/shared/pipes/date-ago.pipe.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _store_comment_state__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./store/comment.state */ "./src/app/shared/store/comment.state.ts");
/* harmony import */ var _store_country_state__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./store/country.state */ "./src/app/shared/store/country.state.ts");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm2015/ng-select-ng-select.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm2015/angular-fontawesome.js");
/* harmony import */ var ng_inline_svg__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ng-inline-svg */ "./node_modules/ng-inline-svg/lib/index.js");
/* harmony import */ var ng_inline_svg__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(ng_inline_svg__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var ngx_qrcode2__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-qrcode2 */ "./node_modules/ngx-qrcode2/index.js");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-clipboard */ "./node_modules/ngx-clipboard/fesm2015/ngx-clipboard.js");
/* harmony import */ var _modals_sirius_id_modal_sirius_id_modal_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./modals/sirius-id-modal/sirius-id-modal.component */ "./src/app/shared/modals/sirius-id-modal/sirius-id-modal.component.ts");
/* harmony import */ var _modals_privatekey_export_privatekey_export_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./modals/privatekey-export/privatekey-export.component */ "./src/app/shared/modals/privatekey-export/privatekey-export.component.ts");
/* harmony import */ var _modals_category_select_category_select_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./modals/category-select/category-select.component */ "./src/app/shared/modals/category-select/category-select.component.ts");









// NGXS



// NG-Select

// NG-Bootstrap


// Font Awesome

// NG-Inline-SVG






const COMPONENTS = [
    _modals_tip_modal_tip_modal_component__WEBPACK_IMPORTED_MODULE_5__["TipModalComponent"],
    _modals_qr_modal_qr_modal_component__WEBPACK_IMPORTED_MODULE_6__["QrModalComponent"],
    _modals_qr_code_id_qr_code_id_component__WEBPACK_IMPORTED_MODULE_7__["QrCodeIdComponent"],
    _modals_sirius_id_modal_sirius_id_modal_component__WEBPACK_IMPORTED_MODULE_18__["SiriusIdModalComponent"],
    _modals_privatekey_export_privatekey_export_component__WEBPACK_IMPORTED_MODULE_19__["PrivatekeyExportComponent"],
    _modals_category_select_category_select_component__WEBPACK_IMPORTED_MODULE_20__["CategorySelectComponent"]
];
const MODULES = [
    _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
    _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbTabsetModule"],
    _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_14__["FontAwesomeModule"],
    _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_12__["NgSelectModule"],
    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModalModule"],
    ng_inline_svg__WEBPACK_IMPORTED_MODULE_15__["InlineSVGModule"],
    ngx_qrcode2__WEBPACK_IMPORTED_MODULE_16__["NgxQRCodeModule"],
    ngx_clipboard__WEBPACK_IMPORTED_MODULE_17__["ClipboardModule"]
];
let SharedModule = class SharedModule {
};
SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [MODULES, _ngxs_store__WEBPACK_IMPORTED_MODULE_9__["NgxsModule"].forFeature([_store_country_state__WEBPACK_IMPORTED_MODULE_11__["CountryState"], _store_comment_state__WEBPACK_IMPORTED_MODULE_10__["CommentState"]])],
        exports: [MODULES, COMPONENTS, _pipes_date_ago_pipe__WEBPACK_IMPORTED_MODULE_8__["DateAgoPipe"]],
        declarations: [
            COMPONENTS,
            _pipes_date_ago_pipe__WEBPACK_IMPORTED_MODULE_8__["DateAgoPipe"],
            _modals_qr_code_id_qr_code_id_component__WEBPACK_IMPORTED_MODULE_7__["QrCodeIdComponent"],
            _modals_qr_code_id_qr_code_id_component__WEBPACK_IMPORTED_MODULE_7__["QrCodeIdComponent"],
            _modals_sirius_id_modal_sirius_id_modal_component__WEBPACK_IMPORTED_MODULE_18__["SiriusIdModalComponent"],
            _modals_privatekey_export_privatekey_export_component__WEBPACK_IMPORTED_MODULE_19__["PrivatekeyExportComponent"],
            _modals_privatekey_export_privatekey_export_component__WEBPACK_IMPORTED_MODULE_19__["PrivatekeyExportComponent"],
            _modals_category_select_category_select_component__WEBPACK_IMPORTED_MODULE_20__["CategorySelectComponent"]
        ],
        entryComponents: [COMPONENTS]
    })
], SharedModule);



/***/ }),

/***/ "./src/app/shared/store/comment.action.ts":
/*!************************************************!*\
  !*** ./src/app/shared/store/comment.action.ts ***!
  \************************************************/
/*! exports provided: AddComment, DeleteComment, LikeComment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComment", function() { return AddComment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteComment", function() { return DeleteComment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LikeComment", function() { return LikeComment; });
/* harmony import */ var _app_core_models_comment_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/core/models/comment.model */ "./src/app/core/models/comment.model.ts");

class AddComment {
    constructor(payload) {
        this.payload = payload;
    }
}
AddComment.type = '[Home Page] AddComment';
AddComment.ctorParameters = () => [
    { type: _app_core_models_comment_model__WEBPACK_IMPORTED_MODULE_0__["Comment"] }
];
class DeleteComment {
    constructor(payload) {
        this.payload = payload;
    }
}
DeleteComment.type = '[Home Page Modal] DeleteComment';
DeleteComment.ctorParameters = () => [
    { type: _app_core_models_comment_model__WEBPACK_IMPORTED_MODULE_0__["Comment"] }
];
class LikeComment {
    constructor(payload) {
        this.payload = payload;
    }
}
LikeComment.type = '[Home Page Modal] LikeComment';
LikeComment.ctorParameters = () => [
    { type: _app_core_models_comment_model__WEBPACK_IMPORTED_MODULE_0__["Comment"] }
];


/***/ }),

/***/ "./src/app/shared/store/comment.state.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/store/comment.state.ts ***!
  \***********************************************/
/*! exports provided: CommentState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentState", function() { return CommentState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_services_comments_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/services/comments.service */ "./src/app/core/services/comments.service.ts");
/* harmony import */ var _app_core_models_comment_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/core/models/comment.model */ "./src/app/core/models/comment.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _comment_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./comment.action */ "./src/app/shared/store/comment.action.ts");





// NGXS


let CommentState = class CommentState {
    constructor(commentsService) {
        this.commentsService = commentsService;
    }
    static getAllComments(state) {
        return state.comments;
    }
    addComment(ctx, { payload }) {
        return this.commentsService.addComment(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(() => { }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    deleteComment(ctx, { payload }) {
        return this.commentsService.deleteComment(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(() => { }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
    likeComment(ctx, { payload }) {
        return this.commentsService.likeComment(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(() => { }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
};
CommentState.ctorParameters = () => [
    { type: _app_core_services_comments_service__WEBPACK_IMPORTED_MODULE_1__["CommentsService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_comment_action__WEBPACK_IMPORTED_MODULE_6__["AddComment"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _comment_action__WEBPACK_IMPORTED_MODULE_6__["AddComment"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], CommentState.prototype, "addComment", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_comment_action__WEBPACK_IMPORTED_MODULE_6__["DeleteComment"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _comment_action__WEBPACK_IMPORTED_MODULE_6__["DeleteComment"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], CommentState.prototype, "deleteComment", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_comment_action__WEBPACK_IMPORTED_MODULE_6__["LikeComment"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _comment_action__WEBPACK_IMPORTED_MODULE_6__["LikeComment"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], CommentState.prototype, "likeComment", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_comment_model__WEBPACK_IMPORTED_MODULE_2__["CommentStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], CommentState, "getAllComments", null);
CommentState = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["State"])({
        name: 'comment',
        defaults: {
            comments: []
        }
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_services_comments_service__WEBPACK_IMPORTED_MODULE_1__["CommentsService"]])
], CommentState);



/***/ }),

/***/ "./src/app/shared/store/country.action.ts":
/*!************************************************!*\
  !*** ./src/app/shared/store/country.action.ts ***!
  \************************************************/
/*! exports provided: GetAllCountries */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllCountries", function() { return GetAllCountries; });
class GetAllCountries {
}
GetAllCountries.type = '[Sign Up Page] GetAllCountries';


/***/ }),

/***/ "./src/app/shared/store/country.state.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/store/country.state.ts ***!
  \***********************************************/
/*! exports provided: CountryState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryState", function() { return CountryState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_core_services_countries_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/core/services/countries.service */ "./src/app/core/services/countries.service.ts");
/* harmony import */ var _app_core_models_country_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/core/models/country.model */ "./src/app/core/models/country.model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _country_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./country.action */ "./src/app/shared/store/country.action.ts");





// NGXS


let CountryState = class CountryState {
    constructor(countriesService, store) {
        this.countriesService = countriesService;
        this.store = store;
    }
    static getAllCountries(state) {
        return state.countries;
    }
    getAllCountries({ patchState }, {}) {
        return this.countriesService.getAll().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((result) => {
            // console.log(result);
            patchState({
                countries: result
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(err => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
};
CountryState.ctorParameters = () => [
    { type: _app_core_services_countries_service__WEBPACK_IMPORTED_MODULE_1__["CountriesService"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Action"])(_country_action__WEBPACK_IMPORTED_MODULE_6__["GetAllCountries"]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _country_action__WEBPACK_IMPORTED_MODULE_6__["GetAllCountries"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], CountryState.prototype, "getAllCountries", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Selector"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_models_country_model__WEBPACK_IMPORTED_MODULE_2__["CountryStateModel"]]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], CountryState, "getAllCountries", null);
CountryState = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_5__["State"])({
        name: 'country',
        defaults: {
            countries: []
        }
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_core_services_countries_service__WEBPACK_IMPORTED_MODULE_1__["CountriesService"], _ngxs_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
], CountryState);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    url: 'https://imagerax-api-staging.proofsys.io/api/v1',
    urlForImage: 'https://imagerax-api-staging.proofsys.io/',
    nodeUrl: 'https://bctestnet2.brimstone.xpxsirius.io'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    if (window) {
        window.console.log = () => { };
    }
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\ASUS\Desktop\Project\imagerax-sirius-frontend\sirius-livewall---frontend\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 10:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 11:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 12:
/*!**********************!*\
  !*** glob (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 13:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 14:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 15:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 16:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 17:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 18:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 19:
/*!************************!*\
  !*** buffer (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 20:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 21:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** buffer (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 4:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 5:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 6:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 7:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 8:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 9:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map
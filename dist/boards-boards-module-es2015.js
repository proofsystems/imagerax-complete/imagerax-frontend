(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["boards-boards-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/boards/boards-all/boards-all.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/boards/boards-all/boards-all.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>boards-all works!</p>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/boards/boards.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/boards/boards.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"post-create\">\r\n  <button class=\"back\" [routerLink]=\"['/post/new']\">\r\n    <fa-icon [icon]=\"faArrowAltCircleLeft\"></fa-icon>\r\n  </button>\r\n  <form [formGroup]=\"form\" (ngSubmit)=\"onCreateBoard(form)\">\r\n    <div>\r\n      <textarea formControlName=\"name\" class=\"caption\" placeholder=\"Board Name\"></textarea>\r\n      <textarea formControlName=\"category\" class=\"caption\" placeholder=\"Category\"></textarea>\r\n      <div\r\n        class=\"alert danger\"\r\n        *ngIf=\"\r\n          (!form.get('name').valid && form.get('name').touched) || (form.get('name').untouched && formSubmitAttempt)\r\n        \"\r\n      >\r\n        This field is required!\r\n      </div>\r\n    </div>\r\n    <div>\r\n      <select formControlName=\"visibility\" class=\"caption\">\r\n        <option value=\"\" selected>--Select your Visibility--</option>\r\n        <option value=\"1\">Public</option>\r\n        <option value=\"0\">Private</option>\r\n      </select>\r\n    </div>\r\n    <!-- <button class=\"google button\" [routerLink]=\"['/boards/boards-all']\">\r\n      View All Board\r\n    </button> -->\r\n    <button class=\"google-btn button\" [routerLink]=\"['/boards/boards-all']\">\r\n      View All Board\r\n    </button>\r\n    &nbsp;&nbsp;\r\n    <button type=\"submit\" class=\"login-btn button\" [disabled]=\"!getSubmitButton()\">\r\n      Create Board\r\n    </button>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/boards/boards-all/boards-all.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/boards/boards-all/boards-all.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkcy9ib2FyZHMtYWxsL2JvYXJkcy1hbGwuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/boards/boards-all/boards-all.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/boards/boards-all/boards-all.component.ts ***!
  \***********************************************************/
/*! exports provided: BoardsAllComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardsAllComponent", function() { return BoardsAllComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _post_store_board_state__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../post/store/board.state */ "./src/app/post/store/board.state.ts");
/* harmony import */ var _post_store_board_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../post/store/board.action */ "./src/app/post/store/board.action.ts");







let BoardsAllComponent = class BoardsAllComponent {
    constructor(store) {
        this.store = store;
    }
    ngOnInit() {
        this.store.dispatch(new _post_store_board_action__WEBPACK_IMPORTED_MODULE_6__["GetOwnBoard"]());
    }
    getUrl(url) {
        return `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urlForImage}${url}`;
    }
};
BoardsAllComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Select"])(_post_store_board_state__WEBPACK_IMPORTED_MODULE_5__["BoardState"].getOwnBoard),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
], BoardsAllComponent.prototype, "boards$", void 0);
BoardsAllComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-boards-all',
        template: __webpack_require__(/*! raw-loader!./boards-all.component.html */ "./node_modules/raw-loader/index.js!./src/app/boards/boards-all/boards-all.component.html"),
        styles: [__webpack_require__(/*! ./boards-all.component.scss */ "./src/app/boards/boards-all/boards-all.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"]])
], BoardsAllComponent);



/***/ }),

/***/ "./src/app/boards/boards-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/boards/boards-routing.module.ts ***!
  \*************************************************/
/*! exports provided: BoardsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardsRoutingModule", function() { return BoardsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _boards_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./boards.component */ "./src/app/boards/boards.component.ts");
/* harmony import */ var _boards_all_boards_all_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./boards-all/boards-all.component */ "./src/app/boards/boards-all/boards-all.component.ts");





const routes = [
    {
        path: '',
        component: _boards_component__WEBPACK_IMPORTED_MODULE_3__["BoardsComponent"]
    },
    {
        path: 'boards-all',
        component: _boards_all_boards_all_component__WEBPACK_IMPORTED_MODULE_4__["BoardsAllComponent"]
    }
];
let BoardsRoutingModule = class BoardsRoutingModule {
};
BoardsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], BoardsRoutingModule);



/***/ }),

/***/ "./src/app/boards/boards.component.scss":
/*!**********************************************!*\
  !*** ./src/app/boards/boards.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkcy9ib2FyZHMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/boards/boards.component.ts":
/*!********************************************!*\
  !*** ./src/app/boards/boards.component.ts ***!
  \********************************************/
/*! exports provided: BoardsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardsComponent", function() { return BoardsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _app_post_store_board_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/post/store/board.action */ "./src/app/post/store/board.action.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");







let BoardsComponent = class BoardsComponent {
    constructor(formBuilder, store) {
        this.formBuilder = formBuilder;
        this.store = store;
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        this.faArrowAltCircleLeft = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_6__["faArrowAltCircleLeft"];
        this.form = this.formBuilder.group({
            name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            visibility: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            category: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    ngOnInit() { }
    setSubmitButton(flag) {
        this.submitButtonStatus = flag;
    }
    getSubmitButton() {
        return this.submitButtonStatus;
    }
    back() {
        this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_5__["Navigate"](['/home']));
    }
    onCreateBoard(form) {
        console.log(form.value);
        this.formSubmitAttempt = true;
        if (form.valid) {
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            this.store.dispatch(new _app_post_store_board_action__WEBPACK_IMPORTED_MODULE_3__["CreateBoard"](form.value)).subscribe(() => {
                this.setSubmitButton(true);
                this.store.dispatch(new _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_5__["Navigate"](['/home']));
            }, err => {
                console.log(err);
                this.setSubmitButton(true);
            });
        }
    }
};
BoardsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"] }
];
BoardsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-boards',
        template: __webpack_require__(/*! raw-loader!./boards.component.html */ "./node_modules/raw-loader/index.js!./src/app/boards/boards.component.html"),
        styles: [__webpack_require__(/*! ./boards.component.scss */ "./src/app/boards/boards.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"]])
], BoardsComponent);



/***/ }),

/***/ "./src/app/boards/boards.module.ts":
/*!*****************************************!*\
  !*** ./src/app/boards/boards.module.ts ***!
  \*****************************************/
/*! exports provided: BoardsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardsModule", function() { return BoardsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _boards_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./boards.component */ "./src/app/boards/boards.component.ts");
/* harmony import */ var _app_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _boards_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./boards-routing.module */ "./src/app/boards/boards-routing.module.ts");
/* harmony import */ var _boards_all_boards_all_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./boards-all/boards-all.component */ "./src/app/boards/boards-all/boards-all.component.ts");






let BoardsModule = class BoardsModule {
};
BoardsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_boards_component__WEBPACK_IMPORTED_MODULE_2__["BoardsComponent"], _boards_all_boards_all_component__WEBPACK_IMPORTED_MODULE_5__["BoardsAllComponent"]],
        imports: [_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _boards_routing_module__WEBPACK_IMPORTED_MODULE_4__["BoardsRoutingModule"]]
    })
], BoardsModule);



/***/ })

}]);
//# sourceMappingURL=boards-boards-module-es2015.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["trends-trends-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/trends/trends.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/trends/trends.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"trends\">\r\n  <div class=\"filter\">\r\n    <div class=\"link\">Today</div>\r\n    <div class=\"link\">This Week</div>\r\n    <div class=\"link\">This Month</div>\r\n    <div class=\"link\">All</div>\r\n  </div>\r\n  <div class=\"title text-center\">Most Viewed</div>\r\n  <div class=\"hashtags\">\r\n    <div class=\"link\">#beauty</div>\r\n    <div class=\"link\">#ugly</div>\r\n    <div class=\"link\">#proxigood</div>\r\n    <div class=\"link\">#proxied</div>\r\n    <div class=\"link\">#proxilife</div>\r\n    <div class=\"link\">#food</div>\r\n    <div class=\"link\">#water</div>\r\n    <div class=\"link\">#car</div>\r\n    <div class=\"link\">#mustanggt</div>\r\n    <div class=\"link\">#bmw3</div>\r\n  </div>\r\n</div> -->\r\n<ngb-tabset type=\"pills\" (tabChange)=\"onTabChange($event)\">\r\n  <ng-container *ngFor=\"let option of options\">\r\n    <ngb-tab id=\"{{ option.id }}\">\r\n      <ng-template ngbTabTitle>\r\n        <div class=\"link\">{{ option.name }}</div>\r\n      </ng-template>\r\n      <ng-template ngbTabContent>\r\n        <div class=\"layout\">\r\n          <div class=\"item\" *ngFor=\"let post of posts$ | async\">\r\n            <img\r\n              *ngIf=\"post.type !== 'video'\"\r\n              [src]=\"getUrl(post.image)\"\r\n              class=\"avatar\"\r\n              [routerLink]=\"['/post/', post.id]\"\r\n            />\r\n            <div class=\"video-overlay\" *ngIf=\"post.type === 'video'\" [routerLink]=\"['/post/', post.id]\">\r\n              <video>\r\n                <source [src]=\"getUrl(post.image)\" />\r\n              </video>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </ng-template>\r\n    </ngb-tab>\r\n  </ng-container>\r\n</ngb-tabset>\r\n"

/***/ }),

/***/ "./src/app/trends/trends-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/trends/trends-routing.module.ts ***!
  \*************************************************/
/*! exports provided: TrendsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrendsRoutingModule", function() { return TrendsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _trends_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./trends.component */ "./src/app/trends/trends.component.ts");




const routes = [
    {
        path: '',
        component: _trends_component__WEBPACK_IMPORTED_MODULE_3__["TrendsComponent"]
    }
];
let TrendsRoutingModule = class TrendsRoutingModule {
};
TrendsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], TrendsRoutingModule);



/***/ }),

/***/ "./src/app/trends/trends.component.scss":
/*!**********************************************!*\
  !*** ./src/app/trends/trends.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RyZW5kcy90cmVuZHMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/trends/trends.component.ts":
/*!********************************************!*\
  !*** ./src/app/trends/trends.component.ts ***!
  \********************************************/
/*! exports provided: TrendsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrendsComponent", function() { return TrendsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @env/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
/* harmony import */ var _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @app/post/store/post.action */ "./src/app/post/store/post.action.ts");
/* harmony import */ var _app_post_store_post_state__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @app/post/store/post.state */ "./src/app/post/store/post.state.ts");




// NGXS



let TrendsComponent = class TrendsComponent {
    constructor(store) {
        this.store = store;
        this.options = [
            {
                id: '1',
                name: 'Today'
            },
            {
                id: '2',
                name: 'This Week'
            },
            {
                id: '3',
                name: 'This Month'
            },
            {
                id: '4',
                name: 'All'
            }
        ];
        this.getToday();
    }
    getToday() {
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__["GetAllTrendsToday"]());
    }
    getWeek() {
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__["GetAllTrendsWeek"]());
    }
    getMonth() {
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__["GetAllTrendsMonth"]());
    }
    getAll() {
        this.store.dispatch(new _app_post_store_post_action__WEBPACK_IMPORTED_MODULE_5__["GetAllTrends"]());
    }
    getUrl(url) {
        return `${_env_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urlForImage}${url}`;
    }
    // openModal(id: number) {
    //   this.store.dispatch(new GetPost(id)).subscribe(() => {
    //     this.modalService.open(PostModalComponent);
    //   });
    // }
    onTabChange(event) {
        console.log(event.nextId);
        switch (event.nextId) {
            case '1':
                this.getToday();
                break;
            case '2':
                this.getWeek();
                break;
            case '3':
                this.getMonth();
                break;
            case '4':
                this.getAll();
                break;
            default:
                console.log('none');
        }
    }
};
TrendsComponent.ctorParameters = () => [
    { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Select"])(_app_post_store_post_state__WEBPACK_IMPORTED_MODULE_6__["PostState"].filteredPosts),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"])
], TrendsComponent.prototype, "posts$", void 0);
TrendsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-trends',
        template: __webpack_require__(/*! raw-loader!./trends.component.html */ "./node_modules/raw-loader/index.js!./src/app/trends/trends.component.html"),
        styles: [__webpack_require__(/*! ./trends.component.scss */ "./src/app/trends/trends.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngxs_store__WEBPACK_IMPORTED_MODULE_4__["Store"]])
], TrendsComponent);



/***/ }),

/***/ "./src/app/trends/trends.module.ts":
/*!*****************************************!*\
  !*** ./src/app/trends/trends.module.ts ***!
  \*****************************************/
/*! exports provided: TrendsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrendsModule", function() { return TrendsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _trends_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./trends.component */ "./src/app/trends/trends.component.ts");
/* harmony import */ var _trends_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./trends-routing.module */ "./src/app/trends/trends-routing.module.ts");





let TrendsModule = class TrendsModule {
};
TrendsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_trends_component__WEBPACK_IMPORTED_MODULE_3__["TrendsComponent"]],
        imports: [_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"], _trends_routing_module__WEBPACK_IMPORTED_MODULE_4__["TrendsRoutingModule"]]
    })
], TrendsModule);



/***/ })

}]);
//# sourceMappingURL=trends-trends-module-es2015.js.map
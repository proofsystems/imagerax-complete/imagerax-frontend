import { Component, OnInit } from '@angular/core';
import { Requests } from '@app/core/models/request.model';
import { Select, Store } from '@ngxs/store';
import { GetAllRequest } from '@app/profile/store/profile.action';
import { ProfileState } from '@app/profile/store/profile.state';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {
  @Select(ProfileState.getAllRequest) friends$: Observable<Requests[]>;
  readonly ENDPOINT_URL = environment.urlForImage;

  constructor(private store: Store) {}

  ngOnInit() {
    this.store.dispatch(new GetAllRequest());
  }

  getImage(url: string) {
    return `${environment.urlForImage}${url}`;
  }
}

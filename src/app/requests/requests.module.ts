import { NgModule } from '@angular/core';
import { RequestsComponent } from './requests.component';
import { SharedModule } from '@app/shared/shared.module';
import { RequestsRoutingModule } from './requests-routing.module';

@NgModule({
  declarations: [RequestsComponent],
  imports: [RequestsRoutingModule, SharedModule]
})
export class RequestsModule {}

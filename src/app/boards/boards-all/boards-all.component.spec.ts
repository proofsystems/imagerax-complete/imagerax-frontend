import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardsAllComponent } from './boards-all.component';

describe('BoardsAllComponent', () => {
  let component: BoardsAllComponent;
  let fixture: ComponentFixture<BoardsAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardsAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardsAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

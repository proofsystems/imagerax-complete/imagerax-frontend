import { Component, OnInit } from '@angular/core';
import { Board, BoardStateModel } from '@app/core/models/board.model';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { Store, Select } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { BoardState } from '../../post/store/board.state';
import { GetOwnBoard } from '../../post/store/board.action';

@Component({
  selector: 'app-boards-all',
  templateUrl: './boards-all.component.html',
  styleUrls: ['./boards-all.component.scss']
})
export class BoardsAllComponent implements OnInit {
  @Select(BoardState.getOwnBoard) boards$: Observable<Board[]>;

  constructor(private store: Store) {}

  ngOnInit() {
    this.store.dispatch(new GetOwnBoard());
  }

  getUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }
}

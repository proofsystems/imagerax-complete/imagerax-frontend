import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Board } from '@app/core/models/board.model';
import { CreateBoard } from '@app/post/store/board.action';
import { BoardState } from '@app/post/store/board.state';
import { Store, Select } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { faArrowAltCircleUp, faArrowAltCircleLeft, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  faArrowAltCircleLeft = faArrowAltCircleLeft;

  constructor(private formBuilder: FormBuilder, private store: Store) {
    this.form = this.formBuilder.group({
      name: [null, Validators.required],
      visibility: [null, Validators.required],
      category: [null, Validators.required]
    });
  }

  ngOnInit() {}

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  back() {
    this.store.dispatch(new Navigate(['/home']));
  }

  onCreateBoard(form: FormGroup) {
    console.log(form.value);
    this.formSubmitAttempt = true;
    if (form.valid) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new CreateBoard(form.value)).subscribe(
        () => {
          this.setSubmitButton(true);
          this.store.dispatch(new Navigate(['/home']));
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }
}

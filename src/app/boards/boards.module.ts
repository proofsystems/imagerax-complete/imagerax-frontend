import { NgModule } from '@angular/core';
import { BoardsComponent } from './boards.component';
import { SharedModule } from '@app/shared/shared.module';
import { BoardsRoutingModule } from './boards-routing.module';
import { BoardsAllComponent } from './boards-all/boards-all.component';

@NgModule({
  declarations: [BoardsComponent, BoardsAllComponent],
  imports: [SharedModule, BoardsRoutingModule]
})
export class BoardsModule {}

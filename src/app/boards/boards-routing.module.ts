import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardsComponent } from './boards.component';
import { BoardsAllComponent } from './boards-all/boards-all.component';

const routes: Routes = [
  {
    path: '',
    component: BoardsComponent
  },
  {
    path: 'boards-all',
    component: BoardsAllComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardsRoutingModule {}

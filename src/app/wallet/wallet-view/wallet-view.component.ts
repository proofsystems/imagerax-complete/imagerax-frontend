import { environment } from '@env/environment';
import { Component, OnInit } from '@angular/core';
import { ProximaxService } from '../../core/services/proximax.service';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

// SweetAlert
import Swal from 'sweetalert2';
import { TransferTransaction, Transaction, TransactionType, Address } from 'tsjs-xpx-chain-sdk';

// NGXS
import { Store, Select } from '@ngxs/store';
import { WalletState } from '../store/wallet.state'
import { AuthState } from '@app/auth/store/auth.state';

import { Observable } from 'rxjs';
import { Profile } from '@app/core/models/profile.model';

import { GetProfile } from './../../profile/store/profile.action';
import { Navigate } from '@ngxs/router-plugin';


@Component({
  selector: 'app-wallet-view',
  templateUrl: './wallet-view.component.html',
  styleUrls: ['./wallet-view.component.scss']
})
export class WalletViewComponent implements OnInit {

  // NGXS
  @Select(WalletState.getProfile) profile$: Observable<Profile>;

  // NGXS
  userId = this.store.selectSnapshot(AuthState.getUserId);

  ADDRESS: any;
  PUBLIC_KEY: any;
  PRIVATE_KEY: any;
  newAccount: any;
  xpxBalance: any;
  xpxBalanceUSD: any;
  transactions: any = [];
  transferTransactions: any[] = [];
  transactionDetail: any;
  transSign: any;
  RAW_ADDRESS: any;
  RAW_PUBLIC_KEY: any;

  closeResult: string;

  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  hasAccount: boolean = false;
  noAccount: boolean = false;

  createWalletPassword: any;
  linkWalletPassword: any;

  hasTransactions: boolean = false;
  noTransactions: boolean = false;

  profileDetails: any = [];
  wallet_object: any = [];

  userWalletAddress: string;
  userWalletPublicKey: string;


  constructor(
    private proximax: ProximaxService,
    private modalService: NgbModal,
    private store: Store,
  ) {
  }

  ngOnInit() {

    this.store.dispatch(new GetProfile()).subscribe(user => {
      console.log('-----Profile from Wallet------', user);

      let _addr = user.profile.profile.wallet_address;
      let _pub = user.profile.profile.wallet_public_key;

      let noAccount = this.proximax.getAccountInfo(_addr);

      // check if account has wallet
      if (noAccount == true) {
        this.store.dispatch(new Navigate(['/home']));
        // console.log('no account');
        this.proximax.generateNewAccountTest();
      } else {
        // console.log('has account');
        this.hasAccount = true;
        this.noAccount = false;
        this.ADDRESS = _addr;
        this.RAW_PUBLIC_KEY = _pub;

        this.RAW_ADDRESS = _addr.replace(/-/g, '');
        this.PUBLIC_KEY = (_pub).match(new RegExp('.{1,6}', 'g')).join('-');

        // Load Data
        this.getBalance(_addr);
        this.getTransactions(this.RAW_PUBLIC_KEY);
      }
    });

  }

  getBalance(_addr) {
    this.proximax.getBalance(_addr).subscribe(amount => {
      // console.log('Amount:', amount);
      this.xpxBalance = parseFloat(amount.toString()).toFixed(6);

      this.proximax.getPriceUSD().subscribe(price => {
        // console.log('Price:', price);
        const balance = this.xpxBalance * price;
        this.xpxBalanceUSD = parseFloat(balance.toString()).toFixed(2);
      });

    });
  }

  getTransactions(public_key) {
    this.proximax.generateTransactionsList(public_key)
      .subscribe(transactionList => {
        // console.log('transactions....', transactionList.length);
        if (transactionList.length == 0) {
          this.hasTransactions = false;
          this.noTransactions = true;
          // console.log('No transactions yet....');

        } else {
          const _transactionList = transactionList
            .filter(tx => tx.type == TransactionType.TRANSFER) // Filter transfer transactions only
            .map(tx => {

              const _tx = tx as TransferTransaction;
              const _address = (_tx.recipient as Address).plain().toString().match(new RegExp('.{1,6}', 'g')).join('-');
              const _date = new Date(_tx.deadline.value.toString());
              const _fee = parseFloat((_tx.maxFee.compact() / Math.pow(10, 6)).toString()).toFixed(6);
              const _amount = parseFloat((_tx.mosaics[0].amount.compact() / Math.pow(10, 6)).toString()).toFixed(6);

              return {
                address: _address,
                type: (_tx.recipient as Address).plain(),
                sender: _tx.signer.address.plain(),
                signer: _tx.signer.publicKey.toString(),
                signature: _tx.signature.toString(),
                message: _tx.message.payload.toString(),
                amount: _amount,
                fee: _fee,
                month: this.months[_date.getMonth()],
                day: (_date.getUTCDate() < 10 ? ('0' + _date.getUTCDate()) : ('' + _date.getUTCDate())),
                year: _date.getUTCFullYear(),
                time: _date.toLocaleTimeString()
              }
            })
          this.transferTransactions = _transactionList;
          this.hasTransactions = true;
          this.noTransactions = false;
          // console.log('transactions....', JSON.stringify(this.transferTransactions));
          console.log('Has transactions....');
        }
      })
  }

  copied() {
    Swal.fire({
      position: 'bottom-end',
      title: 'Copied to clipboard',
      type: 'success',
      showConfirmButton: false,
      timer: 1000,
      backdrop: 'transparent'
    });
  }

  showKeys(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  showQrCode(content) {
    this.modalService.open(content, { size: 'sm' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  showTransDetails(content, transactionDetail) {
    // console.log("TCL: showTransDetails -> transactionDetail", transactionDetail)
    this.transactionDetail = transactionDetail;

    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}

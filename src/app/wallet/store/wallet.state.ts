import { AuthService } from '@app/core/services/auth.service';
import { Profile, ProfileStateModel } from '@app/core/models/profile.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UsersService } from '@app/core/services/users.service';

// NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { GetWallet } from './wallet.action';
import { ReplaceProfilePhoto } from '@app/auth/store/auth.action';

@State<ProfileStateModel>({
  name: 'wallet',
  defaults: {
    profile: null,
    profiles: [],
    session_token: null
  }
})
export class WalletState {
  @Selector()
  static getProfile(state: ProfileStateModel) {
    return state.profile;
  }

  constructor(private authService: AuthService, private usersService: UsersService, private store: Store) {}

  @Action(GetWallet)
  getProfile({ patchState }: StateContext<ProfileStateModel>, {  }: GetWallet) {
    return this.authService.getProfile().pipe(
      tap((result: Profile) => {
        this.store.dispatch(new ReplaceProfilePhoto(result.image));
        console.log(result);
        patchState({
          profile: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}

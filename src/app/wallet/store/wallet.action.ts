export class GetWallet {
  static readonly type = '[Wallet Page] GetWallet';
}

export class EditWallet {
  static readonly type = '[Edit Wallet Page] EditWallet';

  constructor(public payload: FormData) { }
}

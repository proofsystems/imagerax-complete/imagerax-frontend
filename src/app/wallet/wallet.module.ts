import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { CommonModule } from '@angular/common';

import { WalletRoutingModule } from './wallet-routing.module';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { ClipboardModule } from 'ngx-clipboard';
import { TooltipModule } from 'ng2-tooltip-directive';

// NGXS
import { NgxsModule } from '@ngxs/store';
import { WalletState } from './store/wallet.state';
import { WalletViewComponent } from './wallet-view/wallet-view.component';

@NgModule({
  declarations: [WalletViewComponent],
  imports: [
    WalletRoutingModule,
    NgxQRCodeModule,
    ClipboardModule,
    TooltipModule,
    CommonModule,
    NgbModalModule,
    NgxsModule.forFeature([WalletState])
  ]
})
export class WalletModule { }

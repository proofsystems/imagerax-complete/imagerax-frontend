import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WalletViewComponent } from './wallet-view/wallet-view.component'

const routes: Routes = [
  {
    path: '',
    component: WalletViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WalletRoutingModule { }

import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { TrendsComponent } from './trends.component';
import { TrendsRoutingModule } from './trends-routing.module';

@NgModule({
  declarations: [TrendsComponent],
  imports: [SharedModule, TrendsRoutingModule]
})
export class TrendsModule {}

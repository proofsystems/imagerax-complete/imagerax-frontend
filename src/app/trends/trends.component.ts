import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '@app/core/models/post.model';
import { environment } from '@env/environment';
// import { PostModalComponent } from '@app/core/modals/post-modal/post-modal.component';

// NG-Bootstrap
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';

// NGXS
import { Store, Select } from '@ngxs/store';
import { GetAllTrends, GetAllTrendsToday, GetAllTrendsWeek, GetAllTrendsMonth } from '@app/post/store/post.action';
import { PostState } from '@app/post/store/post.state';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent {
  options = [
    {
      id: '1',
      name: 'Today'
    },
    {
      id: '2',
      name: 'This Week'
    },
    {
      id: '3',
      name: 'This Month'
    },
    {
      id: '4',
      name: 'All'
    }
  ];

  // NGXS
  @Select(PostState.filteredPosts) posts$: Observable<Post[]>;

  constructor(private store: Store) {
    this.getToday();
  }

  getToday() {
    this.store.dispatch(new GetAllTrendsToday());
  }

  getWeek() {
    this.store.dispatch(new GetAllTrendsWeek());
  }

  getMonth() {
    this.store.dispatch(new GetAllTrendsMonth());
  }

  getAll() {
    this.store.dispatch(new GetAllTrends());
  }

  getUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  // openModal(id: number) {
  //   this.store.dispatch(new GetPost(id)).subscribe(() => {
  //     this.modalService.open(PostModalComponent);
  //   });
  // }

  onTabChange(event: NgbTabChangeEvent) {
    console.log(event.nextId);
    switch (event.nextId) {
      case '1':
        this.getToday();
        break;
      case '2':
        this.getWeek();
        break;
      case '3':
        this.getMonth();
        break;
      case '4':
        this.getAll();
        break;
      default:
        console.log('none');
    }
  }
}

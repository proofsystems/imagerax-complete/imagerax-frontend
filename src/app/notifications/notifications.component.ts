import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Notification } from '@app/core/models/notification.model';
import { environment } from '@env/environment';

// NGXS
import { NotificationState } from './store/notification.state';
import { Select, Store } from '@ngxs/store';
import { GetAllNotifications } from './store/notification.action';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent {
  // NGXS
  @Select(NotificationState.GetAllNotifications) notifications$: Observable<Notification[]>;

  constructor(private store: Store) {
    this.store.dispatch(new GetAllNotifications());
  }

  getImage(url: string) {
    return `${environment.urlForImage}${url}`;
  }
}

import { NgModule } from '@angular/core';
import { NotificationsComponent } from './notifications.component';
import { SharedModule } from '@app/shared/shared.module';
import { NotificationsRoutingModule } from './notifications-routing.module';

@NgModule({
  declarations: [NotificationsComponent],
  imports: [SharedModule, NotificationsRoutingModule]
})
export class NotificationsModule {}

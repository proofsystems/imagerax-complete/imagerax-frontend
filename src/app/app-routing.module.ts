import { PublicLayoutComponent } from './layouts/public/public-layout.component';
import { PrivateLayoutComponent } from './layouts/private/private-layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { privateRoutes } from './shared/routes/private-layout.routes';
import { publicRoutes } from './shared/routes/public-layout.routes';
import { AuthGuard } from '@app/core/guards/auth-guard.service';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: '',
    component: PrivateLayoutComponent,
    children: privateRoutes,
    canActivate: [AuthGuard]
  },
  { path: '', component: PublicLayoutComponent, children: publicRoutes }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

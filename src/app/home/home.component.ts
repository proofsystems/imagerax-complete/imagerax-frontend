import { Component, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '@app/core/models/post.model';
import { environment } from '@env/environment';
import swal from 'sweetalert2';
//Proxima
import { LoginRequestMessage, VerifytoLogin } from 'siriusid-sdk';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { } from '@siriusid-sdk';

// NGXS
import { Store, Select } from '@ngxs/store';
import { GetAllPosts, GetAllBoards, PinPost } from '@app/post/store/post.action';
import { PostState } from '@app/post/store/post.state';
import { Navigate } from '@ngxs/router-plugin';
import { GetProfile } from '../profile/store/profile.action';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ProximaxService } from '@app/core/services/proximax.service';
import { Board } from '@app/core/models/board.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

//Modals
import { CategorySelectComponent } from '@app/shared/modals/category-select/category-select.component';
//Forms

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  keyWallet: any;
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  hasAccount: boolean = false;
  noAccount: boolean = false;
  closeResult = '';
  content: any;
  modalRef: BsModalRef;
  template: any;

  // NGXS
  @Select(PostState.filteredPosts) posts$: Observable<Post[]>;
  @Select(PostState.getAllBoards) boards$: Observable<Board[]>;

  constructor(
    private store: Store,
    private proximax: ProximaxService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal
  ) {
    this.form = this.formBuilder.group({
      board_id: [null, Validators.required],
      post_id: [null, Validators.required]
    });

    this.store.dispatch(new GetAllPosts());
    this.store.dispatch(new GetProfile()).subscribe(user => {
      console.log('-----Profile from Wallet------', user);
      // this.openSelectCatModal();
      if (user.categories.profile.categories.length == 0 && user.categories.profile.wallet_address !== null) {
        this.openSelectCatModal();
      } else {
        console.log('error');
      }
      let _addr = user.categories.profile.wallet_address;
      let _pub = user.categories.profile.wallet_public_key;
      // check if account has wallet
      if (this.proximax.getAccountInfo(_addr) == true) {
        this.hasAccount = false;
        this.noAccount = true;
      } else {
        this.hasAccount = true;
        this.noAccount = false;
      }
    });
    this.store.dispatch(new GetAllBoards()).subscribe(user => {
      console.log(user);
    });
  }

  getUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onPinPost(form: FormGroup, id: any) {
    console.log(id);
    this.form.get('post_id').setValue(id);
    console.log(form.value);
    if (form.valid) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new PinPost(form.value)).subscribe(
        () => {
          this.setSubmitButton(true);
          this.store.dispatch(new Navigate(['/home']));
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }

  createWallet() {
    Swal.mixin({
      progressSteps: ['1', '2', '3']
    })
      .queue([
        {
          input: 'text',
          inputPlaceholder: 'wallet-name',
          title: 'Create Wallet',
          text: 'Setup wallet Name ',
          confirmButtonText: 'Next &rarr;',
          showCancelButton: true,
          inputValidator: (value: any) => {
            return new Promise(resolve => {
              if (value.length <= 2) {
                resolve('Enter atleast 3 characters');
              } else {
                resolve();
              }
            });
          }
        },
        {
          input: 'password',
          inputPlaceholder: 'wallet-password',
          title: 'Create Wallet',
          text: 'Setup wallet password',
          confirmButtonText: 'Next &rarr;',
          showCancelButton: true,
          inputValidator: (value: any) => {
            return new Promise(resolve => {
              if (value.length <= 7) {
                resolve('Enter atleast 8 characters');
              } else {
                resolve();
              }
            });
          }
        },
        {
          title: 'Create Wallet',
          text: 'Are you sure to create wallet?',
          confirmButtonText: 'Create Wallet'
        }
      ])
      .then(result => {
        if (result.value) {
          const _name = result.value[0];
          const _password = result.value[1];
          const _confirm = result.value[1];

          if (_confirm) {
            this.proximax.createWallet(_name, _password);
          }
        }
      });
  }

  linkWallet() {
    Swal.mixin({
      progressSteps: ['1', '2', '3', '4']
    })
      .queue([
        {
          input: 'text',
          inputPlaceholder: 'wallet-private-key',
          title: 'Add you Private Key',
          confirmButtonText: 'Next &rarr;',
          showCancelButton: true,
          inputValidator: (value: any) => {
            return new Promise(resolve => {
              if (value.length < 64 || value.length > 64) {
                resolve('Enter a valid Private Key');
              } else {
                resolve();
              }
            });
          }
        },
        {
          input: 'text',
          inputPlaceholder: 'wallet-name',
          title: 'Setup wallet name',
          confirmButtonText: 'Next &rarr;',
          showCancelButton: true,
          inputValidator: (value: any) => {
            return new Promise(resolve => {
              if (value.length <= 2) {
                resolve('Enter atleast 3 characters');
              } else {
                resolve();
              }
            });
          }
        },
        {
          input: 'password',
          inputPlaceholder: 'wallet-password',
          title: 'Setup wallet password',
          confirmButtonText: 'Next &rarr;',
          showCancelButton: true,
          inputValidator: (value: any) => {
            return new Promise(resolve => {
              if (value.length <= 7) {
                resolve('Enter atleast 8 characters');
              } else {
                resolve();
              }
            });
          }
        },
        {
          title: 'Are you sure to link wallet?',
          confirmButtonText: 'Link Wallet'
        }
      ])
      .then(result => {
        if (result.value) {
          const _publickey = result.value[0];
          const _name = result.value[1];
          const _password = result.value[2];
          const _confirm = result.value[3];

          if (_confirm) {
            this.proximax.linkWallet(_publickey, _name, _password);
          }
        }
      });
  }

  openSelectCatModal() {
    const modal = this.modalService.open(CategorySelectComponent);
    modal.result.then(() => {});
  }
}

import { Comment } from './comment.model';

export interface Post {
  id: number;
  caption: string;
  comments: Comment;
  image: any;
  likes: [];
  user_id: number;
  type: string;

  post_by: {
    id: number;
    email: string;
    fname: string;
    image: string;
    is_followed: number;
    following_count: number;
    updated_at: Date;
  };
}

export class PostStateModel {
  posts: Post[];
  post: Post;
  fileUrl?: string | ArrayBuffer;
  format?: string;
  totalPages: number;
  itemsPerPage: number;
  currentPage: number;
  searchText: string;
}

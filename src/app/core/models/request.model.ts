import { Profile } from './profile.model';

export interface Requests {
  id: number;
  user_id: number;
  following_user_id: number;
  user: {
    id: number;
    fname: string;
    lname: string;
    image: string;
    linked_social_accounts: {
      id: number;
      provider_id: string;
      provider_name: string;
      user_id: string;
    };
    wallet_address: string;
    wallet_public_key: string;
    wallet: string;
    session_token: string;
  };
}

export class RequestsStateModel {
  request: Requests;
  requests: Requests[];
}

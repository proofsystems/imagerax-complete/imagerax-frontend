export interface Auth {
  email: string;
  password: string;

  profile: {
    id: number;
    fname: string;
    lname: string;
    image: string;
    linked_social_accounts: {
      id: number;
      provider_id: string;
      provider_name: string;
      user_id: string;
    };
    wallet_address: string;
    wallet_public_key: string;
    wallet: string;
    session_token: string;
  };

  token: {
    access_token: string;
  };

  fname: string;
  lname: string;
  country_id: number;
}

export interface SocialAuth {
  access_token: string;
  provider: string;
}

export class AuthStateModel {
  token: string;
  id: number;
  profilePhoto: string;
  provider: string;
  wallet_address: string;
  wallet_public_key: string;
  wallet: string;
  session_token: string;
}

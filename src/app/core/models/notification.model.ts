export interface Notification {
  id: number;
  notifiable_id: number;
  sender: {
    id: number;
    fname: string;
    lname: string;
    image: string;
  };
  notifiable: {
    id: number;
    post_id: number;
  };
  message: string;
  created_at: string;
}

export class NotificationStateModel {
  recentNotifications: Notification[];
  notifications: Notification[];
}

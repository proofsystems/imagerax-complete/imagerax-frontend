import { Country } from './country.model';

export interface Profile {
  id: number;
  country: Country;
  email: string;
  fname: string;
  lname: string;
  image: string;
  provider: string;
  description: string;
  followers_count: number;
  followings_count: number;
  wallet_address: string;
  wallet_public_key: string;
  wallet: string;
  is_followerd: number;
  num_post: number;
  session_token: string;
  is_private: number;
}

export class ProfileStateModel {
  profile: Profile;
  profiles: Profile[];
  session_token: string;
}

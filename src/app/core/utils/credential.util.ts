export class CredentialUtil {
  public static toMap(object: object): Map<string, string> {
    console.log(typeof object);
    const map = new Map<string, string>();
    if (!object) {
      return map;
    }

    Object.keys(object).forEach(key => {
      map.set(this.toTitleCase(key), object[key].toString());
    });
    return map;
  }

  private static toTitleCase(text: string) {
    var result = text.replace(/([A-Z])/g, ' $1');
    return result.charAt(0).toUpperCase() + result.slice(1);
  }
}

import { ProximaxService } from '@app/core/services/proximax.service';
import { Component, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Notification } from '@app/core/models/notification.model';
import { Profile } from '@app/core/models/profile.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { environment } from '@env/environment';

// NGXS
import { Logout } from '@app/auth/store/auth.action';
import { Select, Store } from '@ngxs/store';
import { GetRecent10 } from '@app/notifications/store/notification.action';
import { SearchPost } from '@app/post/store/post.action';
import { NotificationState } from '@app/notifications/store/notification.state';
import { debounceTime } from 'rxjs/operators';
import { AuthState } from '@app/auth/store/auth.state';
import { GetAllRequest } from '@app/profile/store/profile.action';
import { ProfileState } from '@app/profile/store/profile.state';
import { GetProfile } from '@app/profile/store/profile.action';
import { Requests } from '@app/core/models/request.model';
import swal from 'sweetalert2';
import { AcceptRequest } from '@app/profile/store/profile.action';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  dropdownState = false;
  dropdownStateNotif = false;
  dropdownFriendReqStateNotif = false;
  searchForm: FormGroup;
  profilePhoto: string;
  hasAccount: boolean = false;
  readonly ENDPOINT_URL = environment.urlForImage;

  // NGXS
  @Select(NotificationState.GetRecent10) recentNotifications$: Observable<Notification[]>;
  @Select(AuthState.getProfilePhoto) profilePhoto$: Observable<string>;
  @Select(ProfileState.getAllRequest) friends$: Observable<Requests[]>;

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private href: ElementRef,
    private proximax: ProximaxService
  ) {
    this.store.dispatch(new GetProfile()).subscribe(user => {
      console.log('-----Profile from Wallet------', user);

      let _addr = user.categories.profile.wallet_address;
      let _pub = user.categories.profile.wallet_public_key;
      // check if account has wallet
      if (this.proximax.getAccountInfo(_addr) == true) {
        this.hasAccount = false;
      } else {
        this.hasAccount = true;
      }
    });

    this.searchForm = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.profilePhoto$.subscribe(val => {
      this.profilePhoto = val;
    });
    this.store.dispatch(new GetRecent10());

    this.searchForm
      .get('name')
      .valueChanges.pipe(debounceTime(500))
      .subscribe((name: string) => {
        this.store.dispatch(new SearchPost({ val: name }));
      });

    this.store.dispatch(new GetAllRequest());
  }

  getImage(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  onLogoutOut() {
    this.store.dispatch(new Logout());
  }

  closeDropdownNotif() {
    this.dropdownStateNotif = false;
  }

  closeDropdownFriendReqNotif() {
    this.dropdownFriendReqStateNotif = false;
  }

  closeDropdownProfile() {
    this.dropdownState = false;
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  acceptRequest(id: any) {
    this.store.dispatch(new AcceptRequest(id)).subscribe(response => {
      console.log(response);
    });
  }
}

import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { LoaderService } from '../services/loader.service';

// NGXS
import { AuthState } from '@app/auth/store/auth.state';
import { Store } from '@ngxs/store';
import { Logout } from '@app/auth/store/auth.action';

// Sweet Alert
import swal from 'sweetalert2';
import { environment } from '@env/environment';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  constructor(private router: Router, private store: Store, private loaderService: LoaderService) {}

  private showLoader(): void {
    this.loaderService.show();
  }
  private hideLoader(): void {
    this.loaderService.hide();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.store.selectSnapshot(AuthState.token);

    if (token) {
      req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });
    }

    if (!req.headers.has('Content-Type')) {
      if (req.url == `${environment.url}/users/edit` || req.url == `${environment.url}/posts`) {
        delete req['Content-Type'];
      } else {
        req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
      }
    }

    this.showLoader();

    req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.hideLoader();
        }
      }),
      catchError(err => {
        if (err instanceof HttpErrorResponse) {
          this.hideLoader();
          if (err.status === 401) {
            swal
              .fire(
                'Session Expired!',
                'Sorry, your session has expired, you will be redirected to the login page',
                'warning'
              )
              .then(() => {
                this.store.dispatch(new Logout());
              });
          }
          // if (err.status === 500 && req.method === 'GET') {
          //   this.router.navigate(['error']);
          // }
          // if (!window.navigator.onLine) {
          //   this.router.navigate(['no-internet-connection']);
          // }
          if (err.status !== 401) {
            swal.fire('Error', 'Invalid Email/Password', 'error');
          }

          if (err.status == 400) {
            swal.fire('Error', 'Email Address Exist!', 'error');
          }

          return throwError(err);
        }
      })
    );
  }
}

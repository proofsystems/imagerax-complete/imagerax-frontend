import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

// NGXS
import { Store } from '@ngxs/store';
import { AuthState } from '../../auth/store/auth.state';

@Injectable({
  providedIn: 'root'
})
export class EditProfileGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}

  canActivate() {
    const provider = this.store.selectSnapshot(AuthState.getProvider);
    return true;
    // if (provider !== null || provider == null) {
    //   this.router.navigate(['/profile']);
    //   return false;
    // } else {
    //   return true;
    // }
  }
}

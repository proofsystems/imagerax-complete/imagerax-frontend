import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Post } from '@app/core/models/post.model';
import { Board } from '@app/core/models/board.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  url = `${environment.url}/posts`;
  url2 = `${environment.url}`;

  constructor(private httpClient: HttpClient) {}

  getAll() {
    return this.httpClient.get<Post[]>(`${this.url}/all`);
  }

  getPostsPerPage(key: string, pageNumber: string) {
    return this.httpClient.get<Post[]>(`${this.url}?key=${key}&page=${pageNumber}`);
  }

  getPost(id: number) {
    return this.httpClient.get(this.getPostUrl(id));
  }

  private getPostUrl(id: number) {
    return `${this.url}/${id}`;
  }

  getOwn() {
    return this.httpClient.get<Post[]>(`${this.url}/own`);
  }

  getAllTrends() {
    return this.httpClient.get<Post[]>(`${this.url}/trends`);
  }

  getAllTrendsToday() {
    return this.httpClient.get<Post[]>(`${this.url}/trends/today`);
  }

  getAllTrendsWeek() {
    return this.httpClient.get<Post[]>(`${this.url}/trends/week`);
  }

  getAllTrendsMonth() {
    return this.httpClient.get<Post[]>(`${this.url}/trends/month`);
  }

  getTrendsPerPage(key: string, pageNumber: string) {
    return this.httpClient.get<Post[]>(`${this.url}?key=${key}&page=${pageNumber}`);
  }

  getAllFavorites() {
    return this.httpClient.get<Post[]>(`${this.url}/favorites`);
  }

  getAllFavoritesToday() {
    return this.httpClient.get<Post[]>(`${this.url}/favorites/today`);
  }

  getAllFavoritesWeek() {
    return this.httpClient.get<Post[]>(`${this.url}/favorites/week`);
  }

  getAllFavoritesMonth() {
    return this.httpClient.get<Post[]>(`${this.url}/favorites/month`);
  }

  getFavoritesPerPage(key: string, pageNumber: string) {
    return this.httpClient.get<Post[]>(`${this.url}?key=${key}&page=${pageNumber}`);
  }

  addPost(payload: FormData) {
    return this.httpClient.post(`${this.url}`, payload);
  }

  deletePost(payload: Post) {
    return this.httpClient.delete(`${this.url}/${payload.id}`);
  }

  likePost(payload: Post) {
    return this.httpClient.get(`${this.url}/like/${payload.id}`);
  }

  getAllBoards() {
    return this.httpClient.get<Board[]>(`${this.url2}/boards/all`);
  }

  pinPost(payload: Post) {
    return this.httpClient.post(`${this.url}/pin`, JSON.stringify(payload));
  }

  getPostImageById(id: number) {
    return this.httpClient.get(`${this.url}/${id}`);
  }
}

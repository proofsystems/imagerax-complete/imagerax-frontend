import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Categories } from '@app/core/models/categories.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  url = `${environment.url}`;

  constructor(private httpClient: HttpClient) {}

  getAllCategories() {
    return this.httpClient.get<Categories[]>(`${this.url}/categories/all`);
  }

  selectCategories(payload) {
    return this.httpClient.post(`${this.url}/user-categories`, payload);
  }
}

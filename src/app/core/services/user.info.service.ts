import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  email: string = '';
  first_name: string = '';
  last_name: string = '';

  userInfo: any;

  haveCredential = false;
}

import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CredentialRequestMessage, Credentials, LoginRequestMessage } from 'siriusid-sdk';
import {
  Account,
  NetworkType,
  Address,
  AccountHttp,
  QueryParams,
  PublicAccount,
  MosaicHttp,
  MosaicService,
  TransferTransaction,
  Transaction,
  Password,
  SimpleWallet
} from 'tsjs-xpx-chain-sdk';
import { Observable, Observer } from 'rxjs';

// NGXS
import { Store } from '@ngxs/store';
import { EditProfile } from '../../profile/store/profile.action';
import { Navigate } from '@ngxs/router-plugin';
import { GetProfile } from '../../profile/store/profile.action';
import { mergeMap } from 'rxjs/operators';
import { Content } from '@angular/compiler/src/render3/r3_ast';

@Injectable({
  providedIn: 'root'
})
export class ProximaxService {
  node: any = environment.nodeUrl;
  NETWORK_TYPE: NetworkType;
  credential = undefined;
  address: any;
  publicKey: any;
  private account = Account.createFromPrivateKey(
    'C3C94C048F45BD3E6F400AAE8D39FDE1C6DCA0892085C236AB2A67691F365C09',
    NetworkType.TEST_NET
  );
  name: string;
  description: string;
  id: string;
  icon: string;

  constructor(private http: HttpClient, private store: Store) {
    this.NETWORK_TYPE = NetworkType.TEST_NET;
  }

  createCredential(form: Map<string, string>) {
    this.credential = Credentials.create('1', 'Imegerax', 'Imagerax LoginCredentials', 'test', [], form);
    console.log(this.credential);
    return CredentialRequestMessage.create(this.credential);
  }

  public generateNewAccount = () => {
    const newAccount = Account.generateNewAccount(this.NETWORK_TYPE);
    return newAccount;
  };

  public generateLoginMessage(public_key: string, credential) {
    const loginRequestMessage = LoginRequestMessage.create(public_key, credential);
    console.log(loginRequestMessage);
    return loginRequestMessage;
  }

  public createWallet = (_wallet_name: any, _wallet_password: any) => {
    const password = new Password(_wallet_password);
    const wallet = SimpleWallet.create(_wallet_name, password, NetworkType.TEST_NET);
    const account = wallet.open(password);
    // store address and public_key
    return this.onEditProfile(account.address.pretty(), account.publicKey, JSON.stringify(wallet));
  };

  public createSessionToken = (_session_token: string) => {
    const session_token = _session_token;
    return this.onEditProfileSessionToken(JSON.stringify(session_token));
  };

  public linkWallet = (_wallet_privateKey: any, _wallet_name: any, _wallet_password: any) => {
    const password = new Password('password');
    const privateKey = _wallet_privateKey as string;

    // const wallet = SimpleWallet.createFromPrivateKey(_wallet_name, password, privateKey, NetworkType.TEST_NET);
    const wallet = SimpleWallet.createFromPrivateKey(_wallet_name, password, privateKey, NetworkType.MAIN_NET);

    const account = wallet.open(password);

    return this.onEditProfile(account.address.pretty(), account.publicKey, JSON.stringify(wallet));
  };

  public generateTransactionsList(_public_key): Observable<Transaction[]> {
    const accountHttp = new AccountHttp(this.node);

    const rawPublicKey = _public_key as string;
    const publicAccount = PublicAccount.createFromPublicKey(rawPublicKey, this.NETWORK_TYPE);

    const pageSize = 100; // Page size between 10 and 100, otherwise 10

    return new Observable(observer => {
      accountHttp.transactions(publicAccount, new QueryParams(pageSize)).subscribe(
        (transactions: Transaction[]) => {
          observer.next(transactions);
        },
        err => {
          console.error('Error Here', err);
          observer.next(null);
        }
      );
    });
  }

  public getBalance(wallet_address): Observable<number> {
    const url = this.node;
    const accountHttp = new AccountHttp(url);
    const mosaicHttp = new MosaicHttp(url);
    const mosaicService = new MosaicService(accountHttp, mosaicHttp);

    const rawAddress = wallet_address as string;
    const address = Address.createFromRawAddress(rawAddress);

    return new Observable(observer => {
      mosaicService.mosaicsAmountViewFromAddress(address).subscribe(
        mosaic => {
          const relativeAmount: number = this.getRelaiveAmount(mosaic[0].amount.compact());
          observer.next(relativeAmount);
        },
        err => {
          console.error(err);
          observer.next(0);
        }
      );
    });
  }

  getRelaiveAmount(amount) {
    return amount / Math.pow(10, 6);
  }

  getPriceUSD(): Observable<number> {
    const coinId = 'proximax';
    const url = 'https://api.coingecko.com/api/v3';

    return new Observable(observer => {
      this.http.get(`${url}/coins/${coinId}`).subscribe((details: any) => {
        observer.next(details.market_data.current_price.usd);
      });
    });
  }

  onEditProfile(address, public_key, wallet) {
    const formData = new FormData();

    formData.append('wallet_address', address);
    formData.append('wallet_public_key', public_key);
    formData.append('wallet', wallet);

    this.store.dispatch(new EditProfile(formData)).subscribe(
      () => {
        location.reload();
      },
      err => {
        console.log(err);
      }
    );
  }

  onEditProfileSessionToken(session_token) {
    const formData = new FormData();
    formData.append('session_token', session_token);
    this.store.dispatch(new EditProfile(formData)).subscribe(
      () => {
        // location.reload();
      },
      err => {
        console.log(err);
      }
    );
  }

  getAccountInfo(_addr) {
    if (_addr == null) {
      // console.log('---------I have NO ACCOUNT');
      return true;
    } else {
      // console.log('---------I have ACCOUNT, ');
      return false;
    }
  }

  getSessionToken(_session_token) {
    if (_session_token == null) {
      return true;
    } else {
      return false;
    }
  }

  generateNewAccountTest = () => {
    // const accountHttp = new AccountHttp(this.node);
    const wallet_address = 'VDCCN6IQZ6CXLUY2EGXTITWWVPLI7TQC6A435UFB';

    const rawAddress = wallet_address as string;
    const address = Address.createFromRawAddress(rawAddress);

    const accountHttp = new AccountHttp(this.node);
    const mosaicHttp = new MosaicHttp(this.node);
    const mosaicService = new MosaicService(accountHttp, mosaicHttp);

    mosaicService
      .mosaicsAmountViewFromAddress(address)
      .subscribe(mosaic => console.log('Balance:', mosaic[0].amount.compact()), err => console.error(err));
  };
}

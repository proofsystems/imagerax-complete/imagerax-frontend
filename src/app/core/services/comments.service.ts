import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Comment } from '@app/core/models/comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  url = `${environment.url}/comments`;

  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<Comment[]>(`${this.url}/all`);
  }

  addComment(payload: Comment) {
    return this.httpClient.post(`${this.url}`, JSON.stringify(payload));
  }

  deleteComment(payload: Comment) {
    return this.httpClient.delete(`${this.url}/${payload.id}`);
  }

  likeComment(payload: Comment) {
    return this.httpClient.get(`${this.url}/like/${payload.id}`);
  }
}

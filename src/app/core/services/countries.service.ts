import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Country } from '@app/core/models/country.model';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  url = `${environment.url}/countries`;

  constructor(private httpClient: HttpClient) {}

  getAll() {
    return this.httpClient.get<Country[]>(`${this.url}/all`);
  }
}

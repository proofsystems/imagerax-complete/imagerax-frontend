import { Injectable } from '@angular/core';
import { Board } from '@app/core/models/board.model';
import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  url = `${environment.url}`;

  constructor(private httpClient: HttpClient) {}

  createBoard(payload: Board) {
    return this.httpClient.post(`${this.url}/boards`, JSON.stringify(payload));
  }

  getMyBoards() {
    return this.httpClient.get<Board[]>(`${this.url}/boards/owned`);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Auth } from '@app/core/models/auth.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  url = `${environment.url}/users`;
  url2 = `${environment.url}`;
  urlFollowPublic = `${environment.url}/followers/follow`;
  urlFollowPrivate = `${environment.url}/request/follow`;

  constructor(private httpClient: HttpClient) {}

  updateUser(payload: FormData) {
    return this.httpClient.post(`${this.url}/edit`, payload);
  }

  viewUser(id: number) {
    return this.httpClient.get(`${this.url}/${id}`);
  }

  getFollowers(id: number) {
    return this.httpClient.get(`${this.url}/${id}/followers`);
  }

  getFollowing(id: number) {
    return this.httpClient.get(`${this.url}/${id}/following`);
  }

  getAllFriendRequest() {
    return this.httpClient.get(`${this.url2}/request`);
  }

  confirmRequest(id: number) {
    const payload = {
      response: 1
    };
    return this.httpClient.post(`${this.url2}/response/${id}`, payload);
  }

  blockUser(id: number) {
    return this.httpClient.get(`${this.url}/block/${id}`);
  }

  unblockUser(id: number) {
    return this.httpClient.get(`${this.url}/unblock/${id}`);
  }

  followUserPublicAccount(id: number) {
    return this.httpClient.post(`${this.urlFollowPublic}/${id}`, JSON.stringify(id));
  }

  followUserPrivateAccount(id: number) {
    const payload = {
      id: id
    };
    return this.httpClient.post(`${this.urlFollowPrivate}/${id}`, payload);
  }
}

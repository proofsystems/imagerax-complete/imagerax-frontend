import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { PublicLayoutComponent } from '../layouts/public/public-layout.component';
import { PrivateLayoutComponent } from '../layouts/private/private-layout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpRequestInterceptor } from './interceptors/http-request.interceptor';
import { HomeModule } from '@app/home/home.module';
import { environment } from '@env/environment';
import { LoaderComponent } from './loader/loader.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserModule } from '@angular/platform-browser';
import { DateAgoPipe } from './pipes/date-ago.pipe';
import { ClickOutsideDirective } from './directives/click-outside.directive';

// NGXS
import { NgxsModule } from '@ngxs/store';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NGXS_PLUGINS } from '@ngxs/store';
import { logoutPlugin } from '@app/auth/plugins/logout.plugin';
import { AuthState } from '@app/auth/store/auth.state';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { PostState } from '@app/post/store/post.state';
import { NotificationState } from '@app/notifications/store/notification.state';
import { ProfileState } from '@app/profile/store/profile.state';
import { PostStateModel } from './models/post.model';
import { BoardState } from '@app/post/store/board.state';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalModule } from 'ngx-bootstrap';

export const COMPONENTS = [
  PrivateLayoutComponent,
  PublicLayoutComponent,
  NavbarComponent,
  LoaderComponent,
  DateAgoPipe,
  ClickOutsideDirective
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    BrowserModule,
    ModalModule.forRoot(),
    HomeModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    NgxsModule.forRoot([AuthState, PostState, NotificationState, ProfileState, BoardState]),
    NgxsRouterPluginModule.forRoot(),
    // NgxsLoggerPluginModule.forRoot({
    //   disabled: environment.production
    // }),
    NgxsStoragePluginModule.forRoot({
      key: [
        'auth.token',
        'auth.id',
        'auth.profilePhoto',
        'auth.provider',
        'auth.wallet_public_key',
        'auth.session_token',
        'auth.wallet'
      ]
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  exports: [COMPONENTS],
  providers: [
    BsModalService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    {
      provide: NGXS_PLUGINS,
      useValue: logoutPlugin,
      multi: true
    }
  ]
})
export class CoreModule {}

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '@app/core/models/post.model';
import { environment } from '@env/environment';
// import { PostModalComponent } from '@app/core/modals/post-modal/post-modal.component';

// NG-Bootstrap
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';

// NGXS
import { Store, Select } from '@ngxs/store';
import {
  GetAllFavorites,
  GetAllFavoritesToday,
  GetAllFavoritesWeek,
  GetAllFavoritesMonth
} from '@app/post/store/post.action';
import { PostState } from '@app/post/store/post.state';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent {
  options = [
    {
      id: '1',
      name: 'Today'
    },
    {
      id: '2',
      name: 'This Week'
    },
    {
      id: '3',
      name: 'This Month'
    },
    {
      id: '4',
      name: 'All'
    }
  ];

  // NGXS
  @Select(PostState.filteredPosts) posts$: Observable<Post[]>;

  constructor(private store: Store) {
    this.getToday();
  }

  getToday() {
    this.store.dispatch(new GetAllFavoritesToday());
  }

  getWeek() {
    this.store.dispatch(new GetAllFavoritesWeek());
  }

  getMonth() {
    this.store.dispatch(new GetAllFavoritesMonth());
  }

  getAll() {
    this.store.dispatch(new GetAllFavorites());
  }

  getUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  // openModal(id: number) {
  //   this.store.dispatch(new GetPost(id)).subscribe(() => {
  //     this.modalService.open(PostModalComponent);
  //   });
  // }

  onTabChange(event: NgbTabChangeEvent) {
    switch (event.nextId) {
      case '1':
        this.getToday();
        break;
      case '2':
        this.getWeek();
        break;
      case '3':
        this.getMonth();
        break;
      case '4':
        this.getAll();
        break;
      default:
        console.log('none');
    }
  }
}

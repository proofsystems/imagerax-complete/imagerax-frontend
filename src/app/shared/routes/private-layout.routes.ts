import { Routes } from '@angular/router';
import { HomeComponent } from '@app/home/home.component';
import { AuthModule } from '@app/auth/auth.module';

// Route for content layout with navbar, footer etc...
export const privateRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'wallet',
    loadChildren: () => import('../../wallet/wallet.module').then(m => m.WalletModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('../../profile/profile.module').then(m => m.ProfileModule)
  },
  {
    path: 'post',
    loadChildren: () => import('../../post/post.module').then(m => m.PostModule)
  },
  {
    path: 'trends',
    loadChildren: () => import('../../trends/trends.module').then(m => m.TrendsModule)
  },
  {
    path: 'favorites',
    loadChildren: () => import('../../favorites/favorites.module').then(m => m.FavoritesModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('../../notifications/notifications.module').then(m => m.NotificationsModule)
  },
  {
    path: 'boards',
    loadChildren: () => import('../../boards/boards.module').then(m => m.BoardsModule)
  },

  {
    path: 'requests',
    loadChildren: () => import('../../requests/requests.module').then(m => m.RequestsModule)
  }
];

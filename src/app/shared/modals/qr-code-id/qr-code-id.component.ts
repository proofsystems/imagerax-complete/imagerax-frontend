import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-qr-code-id',
  templateUrl: './qr-code-id.component.html',
  styleUrls: ['./qr-code-id.component.scss']
})
export class QrCodeIdComponent implements OnInit {
  userQrId: any;

  constructor() {}

  @Input() payload: any;

  ngOnInit() {
    this.userQrId = this.payload;
    console.log(this.userQrId);
  }
}

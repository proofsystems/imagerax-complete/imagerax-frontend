import { Component, OnInit, Input } from '@angular/core';
import { environment } from '@env/environment';

// SweetAlert
import Swal from 'sweetalert2';

@Component({
  selector: 'app-qr-modal',
  templateUrl: './qr-modal.component.html',
  styleUrls: ['./qr-modal.component.scss']
})
export class QrModalComponent implements OnInit {
  userWallet: any;


  @Input() payload: any;

  constructor() { }

  ngOnInit() {
    this.userWallet = this.payload;
    // console.log("TCL: QrModalComponent -> payload", this.payload)
  }

}

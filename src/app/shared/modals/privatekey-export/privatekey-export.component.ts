import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { Store, Select } from '@ngxs/store';
import { ProfileState } from '../../../profile/store/profile.state';
import { GetProfile } from '../../../profile/store/profile.action';
import { Observable } from 'rxjs';
import { Profile } from '@app/core/models/profile.model';
import { AuthState } from '../../../auth/store/auth.state';

@Component({
  selector: 'app-privatekey-export',
  templateUrl: './privatekey-export.component.html',
  styleUrls: ['./privatekey-export.component.scss']
})
export class PrivatekeyExportComponent implements OnInit {
  @Select(ProfileState.getProfile) profile$: Observable<Profile>;
  wallet_name: string;
  constructor(private store: Store) {}

  ngOnInit() {
    this.store.dispatch(new GetProfile()).subscribe(val => {
      this.wallet_name = val.auth.wallet.name;
    });
  }

  /**
   * Method to export account
   * @param {any} account
   * @memberof ViewAllAccountsComponent
   */
  exportAccount(account) {
    const acc = Object.assign({}, account);
    const accounts = [];
    accounts.push(acc);
    const wallet = {
      name: this.wallet_name,
      accounts
    };

    wallet.accounts[0].name = 'Primary_Account';
    wallet.accounts[0].firstAccount = true;
    wallet.accounts[0].default = true;

    const wordArray = CryptoJS.enc.Utf8.parse(JSON.stringify(wallet));
    const file = CryptoJS.enc.Base64.stringify(wordArray);
    // Word array to base64
    const now = Date.now();
    const date = new Date(now);
    const year = date.getFullYear();
    const month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
    const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();

    const blob = new Blob([file], { type: '' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    // the filename you want
    let networkTypeName = 'imagerax';
    networkTypeName = networkTypeName.includes(' ') ? networkTypeName.split(' ').join('') : networkTypeName;
    a.download = `${wallet.name}_${networkTypeName}_${year}-${month}-${day}.wlt`;
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivatekeyExportComponent } from './privatekey-export.component';

describe('PrivatekeyExportComponent', () => {
  let component: PrivatekeyExportComponent;
  let fixture: ComponentFixture<PrivatekeyExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivatekeyExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivatekeyExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

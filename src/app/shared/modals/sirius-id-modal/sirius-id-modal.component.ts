import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AuthState } from '@app/auth/store/auth.state';
import { LoginRequestMessage, VerifytoLogin } from 'siriusid-sdk';
import { Listener, TransferTransaction, EncryptedMessage, PublicAccount } from 'tsjs-xpx-chain-sdk';
import { environment } from '@env/environment';
import { ChangeDataService } from '@app/core/services/change-data.service';
import { EditProfile } from '../../../profile/store/profile.action';
import { ProximaxService } from '../../../core/services/proximax.service';
import { GetProfile } from '../../../profile/store/profile.action';
import { Navigate } from '@ngxs/router-plugin';
import { ProfileState } from '@app/profile/store/profile.state';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Component({
  selector: 'app-sirius-id-modal',
  templateUrl: './sirius-id-modal.component.html',
  styleUrls: ['./sirius-id-modal.component.scss']
})
export class SiriusIdModalComponent implements OnInit {
  wallet: any;
  form: FormGroup;
  currentNode = `${environment.nodeUrl}`;
  listener: Listener;
  connect = false;
  credential_id = ['passport'];
  session_token: string;

  constructor(
    private store: Store,
    private changedDateServices: ChangeDataService,
    private proximaService: ProximaxService,
    private ngbModalService: NgbModal
  ) {
    this.changedDateServices.apiNode = this.currentNode;
    this.changedDateServices.updateWebsocket();
    this.listener = new Listener(this.changedDateServices.ws, WebSocket);
    this.wallet = this.store.selectSnapshot(AuthState.wallet_public_key);
    this.store.dispatch(new GetProfile()).subscribe(user => {
      console.log('---User Profile---', user);
    });
    this.session_token = this.store.selectSnapshot(ProfileState.getSessionToken);
  }

  ngOnInit() {}

  async loginRequestAndVerify() {
    const loginRequestMessage = LoginRequestMessage.create(this.wallet, this.credential_id);
    const sessionToken = loginRequestMessage.getSessionToken();
    const formData = new FormData();
    formData.append('session_token', sessionToken);
    this.store.dispatch(new EditProfile(formData)).subscribe(
      () => {
        // this.store.dispatch(new Navigate(['/home']));
        swal.fire('Success', 'Linked Account', 'success').then(() => {
          this.store.dispatch(new Navigate(['/profile']));
        });
        this.ngbModalService.dismissAll(SiriusIdModalComponent);
      },
      err => {
        console.log(err);
      }
    );
  }
}

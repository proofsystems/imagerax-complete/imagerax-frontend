import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiriusIdModalComponent } from './sirius-id-modal.component';

describe('SiriusIdModalComponent', () => {
  let component: SiriusIdModalComponent;
  let fixture: ComponentFixture<SiriusIdModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiriusIdModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiriusIdModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Categories } from '@app/core/models/categories.model';
import { GetAllCategoriesProfile, SelectCategory } from '../../../profile/store/profile.action';
import { ProfileState } from '../../../profile/store/profile.state';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { faGrinTongueSquint } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-category-select',
  templateUrl: './category-select.component.html',
  styleUrls: ['./category-select.component.scss']
})
export class CategorySelectComponent implements OnInit {
  cat_form: FormGroup;
  modalRef: BsModalRef;
  public onClose: Subject<boolean>;
  @Select(ProfileState.getAllCategories) categories$: Observable<Categories[]>;
  constructor(
    private store: Store,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private bsModalRef: BsModalService
  ) {
    this.cat_form = this.formBuilder.group({
      ids: this.formBuilder.array([]),
      id: null
    });
    this.onClose = new Subject();
  }

  ngOnInit() {
    this.store.dispatch(new GetAllCategoriesProfile());
  }

  onCheckboxChange(e) {
    const ids: FormArray = this.cat_form.get('ids') as FormArray;
    if (e.target.checked) {
      ids.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      ids.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          ids.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  submit() {
    this.store.dispatch(new SelectCategory(this.cat_form.value)).subscribe(
      response => {
        this.onClose.next(true);
        this.modalRef.hide();
      },
      err => {}
    );
  }
}

import { Component, ViewChild, ElementRef, AfterViewInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { QrModalComponent } from '../qr-modal/qr-modal.component';

// NG-Bootstrap Modal
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-tip-modal',
  templateUrl: './tip-modal.component.html',
  styleUrls: ['./tip-modal.component.scss']
})
export class TipModalComponent implements AfterViewInit {
  @ViewChild('slider', { static: false }) slider: ElementRef;
  @ViewChild('circle1', { static: false }) circle1: ElementRef;
  @ViewChild('circle2', { static: false }) circle2: ElementRef;
  @ViewChild('circle3', { static: false }) circle3: ElementRef;
  @ViewChild('circle4', { static: false }) circle4: ElementRef;
  @ViewChild('circle5', { static: false }) circle5: ElementRef;

  @Input() userWallet: any;

  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private modalService: NgbModal) {
    this.form = this.formBuilder.group({
      amount: [1, Validators.required],
      tip: [1, Validators.required],
      message: [null, Validators.required]
    });

    this.form.get('tip').valueChanges.subscribe(val => {
      this.form.get('amount').setValue(val);
    });
  }

  changeInputTipValue(value: number) {
    this.form.get('tip').setValue(+value);
    this.setSliderValues();
  }

  setSliderValues() {
    const slider = this.slider.nativeElement as HTMLElement;
    const sliderValue = slider as HTMLInputElement;
    slider.style.background =
      'linear-gradient(to right, #f36621 0%, #f36621 ' +
      sliderValue.value +
      '%, #eeeeee ' +
      sliderValue.value +
      '%, #eeeeee)';

    parseInt(sliderValue.value, 10) >= 20
      ? (this.circle1.nativeElement.style.background = '#f36621')
      : (this.circle1.nativeElement.style.background = '#cbcbcb');

    parseInt(sliderValue.value, 10) >= 40
      ? (this.circle2.nativeElement.style.background = '#f36621')
      : (this.circle2.nativeElement.style.background = '#cbcbcb');

    parseInt(sliderValue.value, 10) >= 60
      ? (this.circle3.nativeElement.style.background = '#f36621')
      : (this.circle3.nativeElement.style.background = '#cbcbcb');

    parseInt(sliderValue.value, 10) >= 80
      ? (this.circle4.nativeElement.style.background = '#f36621')
      : (this.circle4.nativeElement.style.background = '#cbcbcb');

    parseInt(sliderValue.value, 10) >= 100
      ? (this.circle5.nativeElement.style.background = '#f36621')
      : (this.circle5.nativeElement.style.background = '#cbcbcb');
  }

  ngAfterViewInit() {
    const slider = this.slider.nativeElement as HTMLElement;
    slider.oninput = () => {
      this.setSliderValues();
    };
  }

  get formValue() { return this.form.controls }

  generateQr() {
    // Generate payload
    const amount = this.formValue.amount.value;
    const message = this.formValue.message.value;
    const address = this.userWallet;
    const urlScheme = "proximax://send";

    const payload = `${urlScheme}?address=${address}&amount=${amount}&message=${message}`;

    const modal = this.modalService.open(QrModalComponent);
    modal.componentInstance.payload = payload;
    modal.result.then(() => { });
  }
}

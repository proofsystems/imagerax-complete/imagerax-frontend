import { FormGroup, FormArray, FormControl } from '@angular/forms';

export function setAsTouched(group: FormGroup | FormArray) {
  group.markAsTouched();
  for (const i in group.controls) {
    if (group.controls[i] instanceof FormControl) {
      group.controls[i].markAsTouched();
    } else {
      this.setAsTouched(group.controls[i]);
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { TipModalComponent } from './modals/tip-modal/tip-modal.component';
import { QrModalComponent } from './modals/qr-modal/qr-modal.component';
import { QrCodeIdComponent } from './modals/qr-code-id/qr-code-id.component';
import { DateAgoPipe } from './pipes/date-ago.pipe';
// NGXS
import { NgxsModule } from '@ngxs/store';
import { CommentState } from './store/comment.state';
import { CountryState } from './store/country.state';

// NG-Select
import { NgSelectModule } from '@ng-select/ng-select';
// NG-Bootstrap
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
// Font Awesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// NG-Inline-SVG
import { InlineSVGModule } from 'ng-inline-svg';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { ClipboardModule } from 'ngx-clipboard';
import { SiriusIdModalComponent } from './modals/sirius-id-modal/sirius-id-modal.component';
import { PrivatekeyExportComponent } from './modals/privatekey-export/privatekey-export.component';
import { CategorySelectComponent } from './modals/category-select/category-select.component';

export const COMPONENTS = [
  TipModalComponent,
  QrModalComponent,
  QrCodeIdComponent,
  SiriusIdModalComponent,
  PrivatekeyExportComponent,
  CategorySelectComponent
];

export const MODULES = [
  CommonModule,
  RouterModule,
  ReactiveFormsModule,
  NgbTabsetModule,
  FontAwesomeModule,
  NgSelectModule,
  NgbModalModule,
  InlineSVGModule,
  NgxQRCodeModule,
  ClipboardModule
];

@NgModule({
  imports: [MODULES, NgxsModule.forFeature([CountryState, CommentState])],
  exports: [MODULES, COMPONENTS, DateAgoPipe],
  declarations: [
    COMPONENTS,
    DateAgoPipe,
    QrCodeIdComponent,
    QrCodeIdComponent,
    SiriusIdModalComponent,
    PrivatekeyExportComponent,
    PrivatekeyExportComponent,
    CategorySelectComponent
  ],
  entryComponents: [COMPONENTS]
})
export class SharedModule {}

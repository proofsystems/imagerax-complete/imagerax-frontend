import { CountriesService } from '@app/core/services/countries.service';
import { Country, CountryStateModel } from '@app/core/models/country.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

// NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { GetAllCountries } from './country.action';

@State<CountryStateModel>({
  name: 'country',
  defaults: {
    countries: []
  }
})
export class CountryState {
  @Selector()
  static getAllCountries(state: CountryStateModel) {
    return state.countries;
  }

  constructor(private countriesService: CountriesService, private store: Store) {}

  @Action(GetAllCountries)
  getAllCountries({ patchState }: StateContext<CountryStateModel>, {  }: GetAllCountries) {
    return this.countriesService.getAll().pipe(
      tap((result: Country[]) => {
        // console.log(result);
        patchState({
          countries: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}

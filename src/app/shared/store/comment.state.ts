import { CommentsService } from '@app/core/services/comments.service';
import { Comment, CommentStateModel } from '@app/core/models/comment.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

// NGXS
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { AddComment, DeleteComment, LikeComment } from './comment.action';

@State<CommentStateModel>({
  name: 'comment',
  defaults: {
    comments: []
  }
})
export class CommentState {
  @Selector()
  static getAllComments(state: CommentStateModel) {
    return state.comments;
  }

  constructor(private commentsService: CommentsService) {}

  @Action(AddComment)
  addComment(ctx: StateContext<CommentStateModel>, { payload }: AddComment) {
    return this.commentsService.addComment(payload).pipe(
      tap(() => {}),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(DeleteComment)
  deleteComment(ctx: StateContext<CommentStateModel>, { payload }: DeleteComment) {
    return this.commentsService.deleteComment(payload).pipe(
      tap(() => {}),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(LikeComment)
  likeComment(ctx: StateContext<CommentStateModel>, { payload }: LikeComment) {
    return this.commentsService.likeComment(payload).pipe(
      tap(() => {}),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }
}

import { Comment } from '@app/core/models/comment.model';

export class AddComment {
  static readonly type = '[Home Page] AddComment';
  constructor(public payload: Comment) { }
}

export class DeleteComment {
  static readonly type = '[Home Page Modal] DeleteComment';
  constructor(public payload: Comment) { }
}

export class LikeComment {
  static readonly type = '[Home Page Modal] LikeComment';
  constructor(public payload: Comment) { }
}

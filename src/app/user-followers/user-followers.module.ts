import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { UserFollowersComponent } from './user-followers.component';
import { UserFollowersRoutingModule } from './user-followers.routing.module';

@NgModule({
  declarations: [UserFollowersComponent],
  imports: [SharedModule, UserFollowersRoutingModule]
})
export class UserFollowersModule {}

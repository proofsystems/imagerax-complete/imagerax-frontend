import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Login, LoginGoogle, LoginFacebook } from '../store/auth.action';
import { AuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { environment } from '@env/environment';
import { Listener } from 'tsjs-xpx-chain-sdk';
import { ChangeDataService } from '../../core/services/change-data.service';
import { LoginRequestMessage, VerifytoLogin } from 'siriusid-sdk';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { QrCodeIdComponent } from '../../shared/modals/qr-code-id/qr-code-id.component';
import { SiriusIdModalComponent } from '../../shared/modals/sirius-id-modal/sirius-id-modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  public user: any = SocialUser;
  platform: any;
  currentNode = `${environment.nodeUrl};`;
  img = null;
  status = null;
  lived = false;
  connect = false;
  credential_id = ['imagerax'];
  listener: Listener;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private authService: AuthService,
    private changeDataService: ChangeDataService,
    private modalService: NgbModal
  ) {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.changeDataService.apiNode = this.currentNode;
    console.log(this.changeDataService);
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  // async onLoginRequest() {
  //   const loginRequestMessage = LoginRequestMessage.create(this.changeDataService.publicKeydApp, this.credential_id);
  //   const sessionToken = loginRequestMessage.getSessionToken();

  //   this.img = await loginRequestMessage.generateQR();
  // }

  openSiriusIdModal() {
    const modal = this.modalService.open(SiriusIdModalComponent);
    modal.result.then(() => {});
    // const modal = this.modalService.open(TipModalComponent);
    // modal.componentInstance.userWallet = this.userWallet;
    // modal.result.then(() => {});
  }

  onLogIn(form: FormGroup) {
    this.formSubmitAttempt = true;
    if (form.valid) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new Login(form.value)).subscribe(
        () => {
          this.setSubmitButton(true);
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }

  signInWithGoogle(platform: any): void {
    platform = GoogleLoginProvider.PROVIDER_ID;
    this.authService.signIn(platform).then(response => {
      this.user = response.authToken;
      const payload = {
        access_token: response.authToken,
        provider: 'google'
      };
      this.store.dispatch(new LoginGoogle(payload)).subscribe(
        () => {
          console.log('success');
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  signOut(): void {
    this.authService.signOut();
  }

  signInWithFacebook(platform: any): void {
    // this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    platform = FacebookLoginProvider.PROVIDER_ID;
    this.authService.signIn(platform).then(response => {
      this.user = response.authToken;
      const payload = {
        access_token: response.authToken,
        provider: response.provider
      };
      this.store.dispatch(new LoginFacebook(payload)).subscribe(
        () => {
          console.log('success');
        },
        err => {
          console.log(err);
        }
      );
    });
  }
}

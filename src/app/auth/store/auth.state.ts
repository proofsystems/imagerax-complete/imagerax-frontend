import { AuthService } from '@app/core/services/auth.service';
import { Auth, AuthStateModel, SocialAuth } from '@app/core/models/auth.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

// NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import {
  Login,
  Logout,
  LoginGoogle,
  SignUp,
  ResetPassword,
  RequestResetPassword,
  LoginFacebook,
  ReplaceProfilePhoto,
  SiriusRegister,
  LoginEmailToDb
} from './auth.action';
import { Navigate } from '@ngxs/router-plugin';

// SweetAlert2
import swal from 'sweetalert2';

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    token: null,
    id: null,
    profilePhoto: null,
    provider: null,
    wallet_address: null,
    wallet_public_key: null,
    wallet: null,
    session_token: null
  }
})
export class AuthState {
  @Selector()
  static token(state: AuthStateModel) {
    return state.token;
  }

  @Selector()
  static wallet_public_key(state: AuthStateModel) {
    return state.wallet_public_key;
  }

  @Selector()
  static getUserId(state: AuthStateModel) {
    return state.id;
  }

  @Selector()
  static getUserWalletAddress(state: AuthStateModel) {
    return state.wallet_address;
  }

  @Selector()
  static getUserSessionToken(state: AuthStateModel) {
    return state.session_token;
  }

  @Selector()
  static getUserWalletPublicKey(state: AuthStateModel) {
    return state.wallet_public_key;
  }

  @Selector()
  static getUserWallet(state: AuthStateModel) {
    return state.wallet['name'];
  }

  @Selector()
  static getProvider(state: SocialAuth) {
    return state.provider;
  }

  @Selector()
  static getProfilePhoto(state: AuthStateModel) {
    return state.profilePhoto;
  }

  constructor(private authService: AuthService, private store: Store) {}

  @Action(LoginFacebook)
  loginFacebook({ patchState }: StateContext<AuthStateModel>, { payload }: LoginFacebook) {
    return this.authService.loginWithFacebook(payload).pipe(
      tap((result: Auth) => {
        patchState({
          token: result.token.access_token,
          id: result.profile.id,
          profilePhoto: result.profile.image,
          provider: result.profile.linked_social_accounts.provider_name,
          wallet_public_key: result.profile.wallet_public_key
        });
        this.store.dispatch(new Navigate(['/home']));
      }),
      catchError(err => {
        patchState({
          token: null
        });
        return throwError(err);
      })
    );
  }

  @Action(SiriusRegister)
  siriusRegister({ patchState }: StateContext<AuthStateModel>, { payload }: SiriusRegister) {
    return this.authService.siriusRegister(payload).pipe(
      tap((result: any) => {
        patchState({});
      }),
      catchError(err => {
        patchState({
          token: null
        });
        return throwError(err);
      })
    );
  }

  @Action(LoginGoogle)
  loginGoogle({ patchState }: StateContext<AuthStateModel>, { payload }: LoginGoogle) {
    return this.authService.loginWithGoogle(payload).pipe(
      tap((result: Auth) => {
        patchState({
          token: result.token.access_token,
          id: result.profile.id,
          provider: result.profile.linked_social_accounts.provider_name,
          profilePhoto: result.profile.image
        });
        this.store.dispatch(new Navigate(['/home']));
      }),
      catchError(err => {
        patchState({
          token: null
        });
        return throwError(err);
      })
    );
  }

  @Action(LoginEmailToDb)
  loginEmailToDb({ patchState }: StateContext<AuthStateModel>, { payload }: LoginEmailToDb) {
    return this.authService.loginEmailToDb(payload).pipe(
      tap((result: Auth) => {
        patchState({
          token: result.token.access_token,
          id: result.profile.id,
          provider: null,
          profilePhoto: result.profile.image,
          wallet_address: result.profile.wallet_address,
          wallet_public_key: result.profile.wallet_public_key,
          wallet: JSON.parse(result.profile.wallet),
          session_token: result.profile.session_token
        });
        this.store.dispatch(new Navigate(['/home']));
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(Login)
  login({ patchState }: StateContext<AuthStateModel>, { payload }: Login) {
    return this.authService.login(payload).pipe(
      tap((result: Auth) => {
        console.log(result);
        patchState({
          token: result.token.access_token,
          id: result.profile.id,
          provider: null,
          profilePhoto: result.profile.image,
          wallet_address: result.profile.wallet_address,
          wallet_public_key: result.profile.wallet_public_key,
          wallet: JSON.parse(result.profile.wallet),
          session_token: result.profile.session_token
        });
        this.store.dispatch(new Navigate(['/home']));
      }),
      catchError(err => {
        patchState({
          token: null
        });
        return throwError(err);
      })
    );
  }

  @Action(Logout)
  logout() {
    return this.authService.logout().pipe(
      tap(() => {
        this.store.dispatch(new Navigate(['/login']));
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(SignUp)
  signUp({ patchState }: StateContext<AuthStateModel>, { payload }: SignUp) {
    return this.authService.signUp(payload).pipe(
      tap((result: Auth) => {
        console.log(result);
        patchState({
          token: result.token.access_token
        });
      }),
      catchError(err => {
        patchState({
          token: null
        });
        return throwError(err);
      })
    );
  }

  @Action(RequestResetPassword)
  requestResetPassword({  }: StateContext<AuthStateModel>, { payload }: RequestResetPassword) {
    return this.authService.requestForResetPassword(payload).pipe(
      tap((result: any) => {
        swal.fire('Success', result.message, 'success').then(() => {
          this.store.dispatch(new Navigate(['/login']));
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(ResetPassword)
  resetPassword({  }: StateContext<AuthStateModel>, { payload }: ResetPassword) {
    return this.authService.resetPassword(payload).pipe(
      tap((result: any) => {
        swal.fire('Success', result.message, 'success').then(() => {
          this.store.dispatch(new Navigate(['/login']));
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(ReplaceProfilePhoto)
  replaceProfilePhoto({ patchState }: StateContext<AuthStateModel>, { payload }: ReplaceProfilePhoto) {
    patchState({
      profilePhoto: payload
    });
  }
}

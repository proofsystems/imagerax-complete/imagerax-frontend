import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Country } from '@app/core/models/country.model';
import { checkIfMatchingPasswords } from '@app/shared/reusable-custom-validators/matching-passwords.validator';

// NGXS
import { Store, Select } from '@ngxs/store';
import { SignUp } from '../store/auth.action';
import { CountryState } from '@app/shared/store/country.state';
import { GetAllCountries } from './../../shared/store/country.action';
import { Navigate } from '@ngxs/router-plugin';

// SweetAlert
import swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  form: FormGroup;
  emailRegEx = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
  submitButtonStatus = true;
  formSubmitAttempt = false;

  // NGXS
  @Select(CountryState.getAllCountries) countries$: Observable<Country[]>;

  constructor(private formBuilder: FormBuilder, private store: Store) {
    this.form = this.formBuilder.group(
      {
        email: [null, [Validators.required, Validators.pattern(this.emailRegEx)]],
        password: [null, [Validators.required, Validators.minLength(8)]],
        confirm_password: [null, [Validators.required, Validators.minLength(8)]],
        fname: [null, Validators.required],
        lname: [null, Validators.required],
        country_id: [null, Validators.required]
      },
      { validator: checkIfMatchingPasswords('password', 'confirm_password') }
    );

    this.store.dispatch(new GetAllCountries());
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onSignUp(form: FormGroup) {
    console.log(form.value);
    this.formSubmitAttempt = true;
    if (form.valid) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new SignUp(form.value)).subscribe(
        () => {
          swal.fire('Success', 'Registered Successfully', 'success').then(() => {
            this.store.dispatch(new Navigate(['/home']));
          });
          this.setSubmitButton(true);
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }
}

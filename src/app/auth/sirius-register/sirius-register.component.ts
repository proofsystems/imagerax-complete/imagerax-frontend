import { Component, OnInit } from '@angular/core';
import { Profile } from '@app/core/models/profile.model';
import { Observable } from 'rxjs';
import { GetProfile } from '../../profile/store/profile.action';
import { Navigate } from '@ngxs/router-plugin';
import { ProximaxService } from '@app/core/services/proximax.service';
import { Store, Select } from '@ngxs/store';
import { ProfileState } from '../../profile/store/profile.state';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CredentialUtil } from '@app/core/utils/credential.util';
import { UserInfoService } from '@app/core/services/user.info.service';
import { CredentialRequestMessage, Credentials } from 'siriusid-sdk';
import { ChangeDataService } from '@app/core/services/change-data.service';
import { SiriusRegister } from '../store/auth.action';

@Component({
  selector: 'app-sirius-register',
  templateUrl: './sirius-register.component.html',
  styleUrls: ['./sirius-register.component.scss']
})
export class SiriusRegisterComponent implements OnInit {
  img = null;
  connect = false;
  continue = true;
  flag = true;
  //Personal Credentials
  form: FormGroup;

  constructor(
    private userInfo: UserInfoService,
    private formBuilder: FormBuilder,
    private changeDataService: ChangeDataService,
    private store: Store
  ) {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      fname: [null, [Validators.required]],
      lname: [null, [Validators.required]]
    });
  }

  ngOnInit() {}

  async createCredential() {
    let content = new Map<string, string>([
      ['Email', this.form.get('email').value],
      ['First Name', this.form.get('fname').value],
      ['Last Name', this.form.get('lname').value]
    ]);
    const credential = Credentials.create(
      'imageraxID',
      'Proofsys Imagerax',
      'Proofsys Imagerax Credentials',
      'https://imagerax.proofsys.io/assets/images/logo.png',
      [],
      content,
      '',
      Credentials.authCreate(content, this.changeDataService.privateKeydApp)
    );
    let msg = CredentialRequestMessage.create(credential);
    console.log(Credentials.authCreate(content, this.changeDataService.privateKeydApp));
    console.log(msg);
    this.img = await msg.generateQR();
    this.store.dispatch(new SiriusRegister(this.form.value)).subscribe(
      () => {
        console.log('success');
      },
      err => {
        console.log(err);
      }
    );
  }

  async generateQr() {
    let content = new Map<string, string>([
      ['Email', this.form.get('email').value],
      ['First Name', this.form.get('fname').value],
      ['Last Name', this.form.get('lname').value]
    ]);
    const credential = Credentials.create(
      'imagerax',
      'Proofsys Imagerax',
      'Propfsys Imagerax Credentials',
      'https://imagerax.proofsys.io/assets/images/logo.png',
      [],
      content,
      '',
      Credentials.authCreate(content, this.changeDataService.privateKeydApp)
    );
    console.log(credential);
    const msg = CredentialRequestMessage.create(credential);
    this.img = await msg.generateQR();
    console.log(msg);
  }
}

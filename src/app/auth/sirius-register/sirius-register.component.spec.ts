import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiriusRegisterComponent } from './sirius-register.component';

describe('SiriusRegisterComponent', () => {
  let component: SiriusRegisterComponent;
  let fixture: ComponentFixture<SiriusRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiriusRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiriusRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

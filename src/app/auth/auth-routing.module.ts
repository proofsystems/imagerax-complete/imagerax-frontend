import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PublicGuard } from './../core/guards/public-guard.service';
import { SiriusLoginComponent } from './sirius-login/sirius-login.component';
import { SiriusRegisterComponent } from './sirius-register/sirius-register.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [PublicGuard]
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [PublicGuard]
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    canActivate: [PublicGuard]
  },
  {
    path: 'login-sirius',
    component: SiriusLoginComponent,
    canActivate: [PublicGuard]
  },
  {
    path: 'register-sirius',
    component: SiriusRegisterComponent,
    canActivate: [PublicGuard]
  },
  {
    path: 'user/reset/:id/:id',
    component: ResetPasswordComponent,
    canActivate: [PublicGuard]
  },
  {
    path: 'register-sirius',
    component: SiriusRegisterComponent,
    canActivate: [PublicGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}

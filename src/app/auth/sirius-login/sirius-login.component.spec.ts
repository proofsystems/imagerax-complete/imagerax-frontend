import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiriusLoginComponent } from './sirius-login.component';

describe('SiriusLoginComponent', () => {
  let component: SiriusLoginComponent;
  let fixture: ComponentFixture<SiriusLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiriusLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiriusLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

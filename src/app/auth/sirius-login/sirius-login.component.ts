import { Component, OnInit } from '@angular/core';
import {
  Listener,
  TransferTransaction,
  EncryptedMessage,
  PublicAccount,
  NetworkType,
  Address
} from 'tsjs-xpx-chain-sdk';
import { LoginRequestMessage, VerifytoLogin, VerifytoConfirmCredential, ApiNode } from 'siriusid-sdk';
import { UserInfoService } from '@app/core/services/user.info.service';
import { ChangeDataService } from '@app/core/services/change-data.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Navigate } from '@ngxs/router-plugin';
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginEmailToDb } from '../store/auth.action';

@Component({
  selector: 'app-sirius-login',
  templateUrl: './sirius-login.component.html',
  styleUrls: ['./sirius-login.component.scss']
})
export class SiriusLoginComponent implements OnInit {
  img = null;
  URL;
  currentNode = 'demo-sc-api-1.ssi.xpxsirius.io';
  status = null;
  lived = false;
  subscribe;
  listener: Listener;
  connect = false;
  credential_id = ['imageraxID'];
  warning = false;
  form: FormGroup;
  convertedStringBinary: any;
  convertedBinaryBase64: any;
  showImg = false;
  credentials: any;

  ngOnInit() {}

  constructor(
    private changeDataService: ChangeDataService,
    private userInfo: UserInfoService,
    private router: Router,
    private formBuilder: FormBuilder,
    private store: Store
  ) {
    ApiNode.apiNode = 'https://' + this.currentNode;
    ApiNode.networkType = NetworkType.PRIVATE_TEST;
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.loginRequestAndVerify();
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]]
    });
  }

  async loginRequestAndVerify() {
    const loginRequestMessage = LoginRequestMessage.create(this.changeDataService.publicKeydApp, this.credential_id);
    const sessionToken = loginRequestMessage.getSessionToken();
    this.img = await loginRequestMessage.generateQR();
    console.log(sessionToken);
    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0 && !this.connect) {
        this.warning = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        this.connection(sessionToken);
        // this.img = false;
      } else if (listenerAsAny.webSocket.readyState == 1 && !this.connect) {
        this.showImg = true;
      } else if (this.connect) {
        clearInterval(interval);
      }
    }, 1000);
  }

  async connection(sessionToken) {
    this.listener.open().then(() => {
      this.warning = false;
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {
        VerifytoLogin.verify(transaction, sessionToken, this.credential_id, this.changeDataService.privateKeydApp).then(
          verify => {
            if (verify) {
              console.log(transaction);
              this.connect = true;
              this.changeDataService.addressSiriusid = transaction.signer.address.plain();
              this.userInfo.userInfo = VerifytoLogin.credentials[0].content;
              const payload = {
                content: this.userInfo.userInfo
              };
              this.store.dispatch(new LoginEmailToDb(payload));
            } else {
              console.log('Transaction unmatched');
            }
          }
        );
      });
    });
  }

  async refresh() {
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }
}

import { AuthService } from '@app/core/services/auth.service';
import { CategoriesService } from '@app/core/services/categories.service';
import { Profile, ProfileStateModel } from '@app/core/models/profile.model';
import { Requests, RequestsStateModel } from '@app/core/models/request.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError, pipe } from 'rxjs';
import { UsersService } from '@app/core/services/users.service';
import { Navigate } from '@ngxs/router-plugin';
import { Categories, CategoriesStateModel } from '@app/core/models/categories.model';
import { GetAllPosts } from '../../post/store/post.action';
// NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import {
  GetProfile,
  EditProfile,
  ViewUserProfile,
  GetFollowers,
  GetFollowings,
  GetAllRequest,
  FollowUserPrivate,
  FollowUserPublic,
  AcceptRequest,
  GetAllCategoriesProfile,
  SelectCategory
} from './profile.action';
import { ReplaceProfilePhoto } from '@app/auth/store/auth.action';
import swal from 'sweetalert2';

@State<CategoriesStateModel>({
  name: 'categories',
  defaults: {
    cat: null,
    cats: []
  }
})
@State<ProfileStateModel>({
  name: 'profile',
  defaults: {
    profile: null,
    profiles: [],
    session_token: null
  }
})
@State<RequestsStateModel>({
  name: 'request',
  defaults: {
    request: null,
    requests: []
  }
})
export class ProfileState {
  @Selector()
  static getProfile(state: ProfileStateModel) {
    return state.profile;
  }

  @Selector()
  static getAllCategories(state: CategoriesStateModel) {
    return state.cats;
  }

  @Selector()
  static getSessionToken(state: ProfileStateModel) {
    return state.session_token;
  }

  @Selector()
  static viewUserProfile(state: ProfileStateModel) {
    return state.profile;
  }

  @Selector()
  static getFollowers(state: ProfileStateModel) {
    return state.profiles;
  }

  @Selector()
  static getAllRequest(state: RequestsStateModel) {
    return state.requests;
  }

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private store: Store,
    private categoriesService: CategoriesService
  ) {}

  @Action(SelectCategory)
  selectCategory({ patchState }: StateContext<CategoriesStateModel>, { payload }: SelectCategory) {
    return this.categoriesService.selectCategories(payload).pipe(
      tap((result: Categories[]) => {
        this.store.dispatch(new GetAllPosts());
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllCategoriesProfile)
  getAllCategories({ patchState }: StateContext<CategoriesStateModel>, {  }: GetAllCategoriesProfile) {
    return this.categoriesService.getAllCategories().pipe(
      tap((result: Categories[]) => {
        patchState({
          cats: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetFollowers)
  getFollowers({ patchState }: StateContext<ProfileStateModel>, { id }: GetFollowers) {
    return this.usersService.getFollowers(id).pipe(
      tap((result: Profile[]) => {
        patchState({
          profiles: result
        });
        console.log(result);
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(AcceptRequest)
  acceptRequest({ patchState }: StateContext<ProfileStateModel>, { id }: AcceptRequest) {
    return this.usersService.confirmRequest(id).pipe(
      tap((result: any) => {
        patchState({});
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllRequest)
  getAllRequest({ patchState }: StateContext<RequestsStateModel>, {  }: GetAllRequest) {
    return this.usersService.getAllFriendRequest().pipe(
      tap((result: Requests[]) => {
        patchState({
          requests: result['requests']
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetFollowings)
  getFollowings({ patchState }: StateContext<ProfileStateModel>, { id }: GetFollowings) {
    return this.usersService.getFollowing(id).pipe(
      tap((result: Profile[]) => {
        patchState({
          profiles: result
        });
        console.log(result);
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(ViewUserProfile)
  viewUserProfile({ patchState }: StateContext<ProfileStateModel>, { id }: ViewUserProfile) {
    return this.usersService.viewUser(id).pipe(
      tap((result: Profile) => {
        patchState({
          profile: result
        });
        console.log(result);
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetProfile)
  getProfile({ patchState }: StateContext<ProfileStateModel>, {  }: GetProfile) {
    return this.authService.getProfile().pipe(
      tap((result: Profile) => {
        this.store.dispatch(new ReplaceProfilePhoto(result.image));
        console.log(result);
        patchState({
          profile: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(EditProfile)
  editProfile({ patchState }: StateContext<ProfileStateModel>, { payload }: EditProfile) {
    return this.usersService.updateUser(payload).pipe(
      tap((result: Profile) => {
        console.log(result);
        patchState({
          profile: result,
          session_token: result.session_token
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(FollowUserPrivate)
  followUserPrivate({ patchState }: StateContext<ProfileStateModel>, { id }: FollowUserPrivate) {
    return this.usersService.followUserPrivateAccount(id).pipe(
      tap((result: Profile) => {
        patchState({});
        swal.fire('Successfully', 'Request Sent', 'success').then(() => {
          this.store.dispatch(new ViewUserProfile(id));
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}

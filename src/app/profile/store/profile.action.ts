export class GetProfile {
  static readonly type = '[Profile Page] GetProfile';
}

export class EditProfile {
  static readonly type = '[Edit Profile Page] EditProfile';
  constructor(public payload: FormData) {}
}

export class ViewUserProfile {
  static readonly type = '[ View User Profile Page ] ViewUserProfile';
  constructor(public id: number) {}
}

export class GetFollowers {
  static readonly type = '[ Followers Page ]  GetFollowers';
  constructor(public id: number) {}
}

export class GetFollowings {
  static readonly type = '[ Followers Page ]  GetFollowings';
  constructor(public id: number) {}
}

export class GetAllRequest {
  static readonly type = ' [ Get All Request ] GetAllRequest';
}

export class BlockUser {
  static readonly type = '[ Block User ] BlockUser';
  constructor(public id: number) {}
}
export class FollowUserPublic {
  static readonly type = '[ Follow User Public Account ] FollowUserPublic';
  constructor(public id: number) {}
}

export class FollowUserPrivate {
  static readonly type = '[ Follow User Private Account ] FollowUserPrivate';
  constructor(public id: number) {}
}

export class AcceptRequest {
  static readonly type = '[ Accept Friend Request ]  AcceptRequest';
  constructor(public id: number) {}
}

export class GetAllCategoriesProfile {
  static readonly type = ' [ All Category ] GetAllCategoriesProfile ';
}

export class SelectCategory {
  static readonly type = '[ Select Cateogry ]';
  constructor(public payload: any) {}
}

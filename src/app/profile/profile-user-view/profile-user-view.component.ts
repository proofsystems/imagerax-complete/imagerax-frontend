import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Profile } from '@app/core/models/profile.model';
import { Navigate } from '@ngxs/router-plugin';
import { ViewUserProfile, FollowUserPrivate } from '../store/profile.action';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileState } from '../store/profile.state';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { faPen, faUserCheck, faUserAltSlash } from '@fortawesome/free-solid-svg-icons';
import { AuthState } from '@app/auth/store/auth.state';
import { PostState } from '@app/post/store/post.state';
import { UserPostById } from '@app/post/store/post.action';
import { Post } from '@app/core/models/post.model';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { GetFollowers, GetFollowings } from '@app/profile/store/profile.action';

@Component({
  selector: 'app-profile-user-view',
  templateUrl: './profile-user-view.component.html',
  styleUrls: ['./profile-user-view.component.scss']
})
export class ProfileUserViewComponent implements OnInit {
  @Select(ProfileState.getFollowers) profiles$: Observable<Profile[]>;
  readonly ENDPOINT_URL = environment.urlForImage;
  options = [
    {
      id: '1',
      name: 'Followers'
    },
    {
      id: '2',
      name: 'Following'
    }
  ];

  user_id = this.route.snapshot.params['id'];

  @Select(ProfileState.viewUserProfile) profile$: Observable<Profile>;
  @Select(PostState.userPostById) posts$: Observable<Post[]>;

  faPen = faPen;
  faFollow = faUserCheck;
  faFollow2 = faUserAltSlash;
  provider = this.store.selectSnapshot(AuthState.getProvider);

  constructor(private store: Store, private route: ActivatedRoute) {}

  ngOnInit() {
    this.store.dispatch(new ViewUserProfile(this.user_id));
    // this.store.dispatch(new UserPostById(this.user_id));
  }

  followUserPrivate(id: number) {
    this.store.dispatch(new FollowUserPrivate(this.user_id)).subscribe(user => {
      console.log(user);
    });
  }

  getFollowers() {
    this.store.dispatch(new GetFollowers(this.user_id));
  }

  getFollowings() {
    this.store.dispatch(new GetFollowings(this.user_id));
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  gotoProfile(id: number) {
    this.store.dispatch(new Navigate([`/profile/profile-view/${id}`]));
  }

  gotoFollowers(id: number) {
    this.store.dispatch(new Navigate([`/followers/${id}`]));
  }

  gotoFollowings(id: number) {
    this.store.dispatch(new ViewUserProfile(this.user_id)).subscribe(() => {
      this.store.dispatch(new Navigate([`/profile/profile-view/${id}`]));
    });
  }

  onTabChange(event: NgbTabChangeEvent) {
    switch (event.nextId) {
      case '1':
        // this.getFollowings();
        break;
      case '2':
        // this.getFollowings();
        break;
      default:
        console.log('none');
    }
  }
}

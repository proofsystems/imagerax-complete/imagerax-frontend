import { Component } from '@angular/core';
import { Profile } from '@app/core/models/profile.model';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

// NGXS
import { Store, Select } from '@ngxs/store';
import { ProfileState } from '../store/profile.state';
import { GetProfile } from '../store/profile.action';
import { PostState } from '@app/post/store/post.state';
import { Post } from '@app/core/models/post.model';
import { GetOwnPosts, GetPost } from '@app/post/store/post.action';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { AuthState } from '@app/auth/store/auth.state';
import { Navigate } from '@ngxs/router-plugin';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent {
  options = [
    {
      id: '1',
      name: 'Post'
    },
    {
      id: '2',
      name: 'Pins'
    }
  ];

  // NGXS
  provider = this.store.selectSnapshot(AuthState.getProvider);
  @Select(ProfileState.getProfile) profile$: Observable<Profile>;
  @Select(PostState.getOwnPost) posts$: Observable<Post[]>;

  faPen = faPen;

  constructor(private store: Store) {
    this.store.dispatch(new GetProfile());
    this.store.dispatch(new GetOwnPosts());
  }

  getUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  gotoEdit() {
    this.store.dispatch(new Navigate(['profile/edit']));
  }

  // openModal(id: number) {
  //   this.store.dispatch(new GetPost(id)).subscribe(() => {
  //     this.modalService.open(PostModalComponent);
  //   });
  // }
}

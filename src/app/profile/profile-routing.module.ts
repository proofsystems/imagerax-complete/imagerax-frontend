import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { ProfileUserViewComponent } from '../profile/profile-user-view/profile-user-view.component';
import { EditProfileGuard } from '@app/core/guards/edit-profile-guard.service';
import { UserFollowersComponent } from '@app/user-followers/user-followers.component';
import { UserFollowingsComponent } from '@app/user-followings/user-followings.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileViewComponent
  },
  {
    path: 'profile-view/:id',
    component: ProfileUserViewComponent
  },
  {
    path: 'edit',
    component: ProfileEditComponent,
    canActivate: [EditProfileGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {}

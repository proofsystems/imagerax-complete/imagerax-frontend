import { NgModule } from '@angular/core';
import { ProfileRoutingModule } from './profile-routing.module';
import { SharedModule } from '@app/shared/shared.module';

// NGXS
import { NgxsModule } from '@ngxs/store';
import { ProfileState } from './store/profile.state';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ProfileUserViewComponent } from './profile-user-view/profile-user-view.component';

@NgModule({
  declarations: [
    ProfileViewComponent,
    ProfileEditComponent,
    ProfileUserViewComponent
  ],
  imports: [ProfileRoutingModule, SharedModule, NgxsModule.forFeature([ProfileState])]
})
export class ProfileModule {}

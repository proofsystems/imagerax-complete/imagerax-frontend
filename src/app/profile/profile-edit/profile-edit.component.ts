import { Component } from '@angular/core';
import { Observable, from } from 'rxjs';
import { Profile } from '@app/core/models/profile.model';
import { Post } from '@app/core/models/post.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Country } from '@app/core/models/country.model';
import { setAsTouched } from '@app/shared/reusable-custom-validators/set-as-touched.validator';
import { environment } from '@env/environment';

// NGXS
import { Store, Select } from '@ngxs/store';
import { ProfileState } from '../store/profile.state';
import { GetProfile, EditProfile } from '../store/profile.action';
import { PreviewImageVideo } from '@app/post/store/post.action';
import { PostState } from '@app/post/store/post.state';
import { Navigate } from '@ngxs/router-plugin';
import { GetAllCountries } from '@app/shared/store/country.action';
import { CountryState } from '@app/shared/store/country.state';
import { SiriusIdModalComponent } from '@app/shared/modals/sirius-id-modal/sirius-id-modal.component';
import { PrivatekeyExportComponent } from '@app/shared/modals/privatekey-export/privatekey-export.component';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthState } from '@app/auth/store/auth.state';
import swal from 'sweetalert2';

//Sirius ID Sdk

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent {
  url: string | ArrayBuffer;
  selectedImage: File;
  selectedCoverPhoto: File;
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  currentNode = `${environment.nodeUrl}`;
  session_token = this.store.selectSnapshot(AuthState.getUserSessionToken);

  // NGXS
  @Select(ProfileState.getProfile) profile$: Observable<Profile>;
  @Select(PostState.getFileUrl) url$: Observable<Post>;
  @Select(CountryState.getAllCountries) countries$: Observable<Country[]>;

  constructor(private store: Store, private formBuilder: FormBuilder, private modalService: NgbModal) {
    this.form = this.formBuilder.group({
      fname: [null, Validators.required],
      lname: [null, Validators.required],
      email: [null, Validators.required],
      country_id: [null, Validators.required],
      description: [null, Validators.required],
      image: [null],
      cover_photo: [null]
    });
    this.store.dispatch(new GetAllCountries());
    this.store.dispatch(new GetProfile()).subscribe(val => {
      console.log(val);
      setAsTouched(this.form);
      // this.form.patchValue(val.profile.profile);
      this.form.get('fname').setValue(val.categories.profile.fname);
      this.form.get('lname').setValue(val.categories.profile.lname);
      this.form.get('email').setValue(val.categories.profile.email);
      this.form.get('country_id').setValue(val.categories.profile.country_id);
      this.form.get('description').setValue(val.categories.profile.description);
    });
    this.session_token = this.store.selectSnapshot(AuthState.getUserSessionToken);
  }

  getUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  onSelectImage(event: Event) {
    console.log(event.target);
    if (this.selectedImage === null || this.selectedImage === undefined) {
      this.selectedImage = (event.target as HTMLInputElement).files[0];
      this.store.dispatch(new PreviewImageVideo(event, this.url));
    }
  }

  openSiriusIdModal() {
    const modal = this.modalService.open(SiriusIdModalComponent);
    modal.result.then(() => {});
    // const modal = this.modalService.open(TipModalComponent);
    // modal.componentInstance.userWallet = this.userWallet;
    // modal.result.then(() => {});
  }

  openExportPrivateKey() {
    const modal = this.modalService.open(PrivatekeyExportComponent);
    modal.result.then(() => {});
  }

  onSelectCoverPhoto(event: Event) {
    console.log(event.target);
    if (this.selectedCoverPhoto === null || this.selectedCoverPhoto === undefined) {
      this.selectedCoverPhoto = (event.target as HTMLInputElement).files[0];
      this.store.dispatch(new PreviewImageVideo(event, this.url));
    }
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  setAccountAsPrivate() {
    const formData = new FormData();
    formData.append('is_private', '1');
    this.store.dispatch(new EditProfile(formData)).subscribe(
      () => {
        this.setSubmitButton(true);
        swal.fire('Sucess', 'Account set to Private', 'success');
        this.store.dispatch(new GetProfile());
      },
      err => {
        console.log(err);
        this.setSubmitButton(true);
      }
    );
  }

  setAccountAsPublic() {
    const formData = new FormData();
    formData.append('is_private', '0');
    this.store.dispatch(new EditProfile(formData)).subscribe(
      () => {
        this.setSubmitButton(true);
        swal.fire('Sucesss', 'Account set to Public', 'success');
        this.store.dispatch(new GetProfile());
      },
      err => {
        console.log(err);
        this.setSubmitButton(true);
      }
    );
  }

  onEditProfile(form: FormGroup) {
    console.log(form.value);
    const formData = new FormData();
    formData.append('fname', form.value.fname);
    formData.append('lname', form.value.lname);
    formData.append('description', form.value.description);
    formData.append('country_id', form.value.country_id);
    if (this.selectedImage) {
      formData.append('image', this.selectedImage);
    }
    if (this.selectedCoverPhoto) {
      formData.append('cover_photo', this.selectedCoverPhoto);
    }
    console.log(formData);
    this.formSubmitAttempt = true;
    if (form.valid) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      console.log('yeah');
      this.store.dispatch(new EditProfile(formData)).subscribe(
        () => {
          this.setSubmitButton(true);
          this.store.dispatch(new Navigate(['/profile']));
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }
}

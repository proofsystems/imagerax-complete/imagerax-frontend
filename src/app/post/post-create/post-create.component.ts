import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Post } from '@app/core/models/post.model';
import { Board } from '@app/core/models/board.model';
import { AuthState } from '@app/auth/store/auth.state';
// NGXS
import { Store, Select } from '@ngxs/store';
import { AddPost, GetAllBoards } from '../store/post.action';
import { Navigate } from '@ngxs/router-plugin';
import { PostState } from '../store/post.state';
import { PreviewImageVideo } from './../store/post.action';

// Font Awesome
import { faArrowAltCircleUp, faArrowAltCircleLeft, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss']
})
export class PostCreateComponent {
  @ViewChild('caption', { static: false }) caption: ElementRef;
  userId = this.store.selectSnapshot(AuthState.getUserId);
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  url: string | ArrayBuffer;
  selectedFile: File;

  // Font Awesome
  faArrowAltCircleUp = faArrowAltCircleUp;
  faArrowAltCircleLeft = faArrowAltCircleLeft;
  faTrash = faTrash;

  // NGXS
  @Select(PostState.getFileUrl) url$: Observable<Post>;
  @Select(PostState.getFormat) format$: Observable<Post>;
  @Select(PostState.getAllBoards) boards$: Observable<Board[]>;

  constructor(private formBuilder: FormBuilder, private store: Store) {
    this.form = this.formBuilder.group({
      caption: [null, Validators.required],
      board_id: [null, Validators.required],
      image: [null, Validators.required]
    });

    this.form.valueChanges.subscribe(() => {
      this.caption.nativeElement.style.height = 'auto';
      this.caption.nativeElement.style.height = `${this.caption.nativeElement.scrollHeight}px`;
    });

    this.store.dispatch(new GetAllBoards());
    this.userId;
  }

  onSelectFile(event: Event) {
    if (this.selectedFile === null || this.selectedFile === undefined) {
      this.selectedFile = (event.target as HTMLInputElement).files[0];
      this.store.dispatch(new PreviewImageVideo(event, this.url));
    }
  }

  removeFile() {
    this.selectedFile = null;
    this.form.get('image').setValue(null);
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  back() {
    this.store.dispatch(new Navigate(['/home']));
  }

  onAddPost(form: FormGroup) {
    console.log(form.value);
    console.log(this.selectedFile);
    const formData = new FormData();
    formData.append('caption', form.value.caption);
    formData.append('image', this.selectedFile);
    formData.append('board_id', form.value.board_id);
    console.log(formData);
    this.formSubmitAttempt = true;
    if (form.valid) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new AddPost(formData)).subscribe(
        () => {
          this.setSubmitButton(true);
          this.store.dispatch(new Navigate(['/home']));
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }
}

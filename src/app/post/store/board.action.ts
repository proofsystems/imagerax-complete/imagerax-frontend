import { Board } from '@app/core/models/board.model';

export class GetAllBoards {
  static readonly type = '[ Post New ] GetAllBoards';
}

export class CreateBoard {
  static readonly type = '[ Create Board Modal ] CreateBoard';
  constructor(public payload: Board) {}
}

export class GetOwnBoard {
  static readonly type = '[ View My Own Board ] GetOwnBoard';
}

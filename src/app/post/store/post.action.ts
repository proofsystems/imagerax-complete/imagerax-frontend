import { Post } from '@app/core/models/post.model';

export class GetAllPosts {
  static readonly type = '[Home Page] GetAllPosts';
}

export class GetPostsPerPage {
  static readonly type = '[Home Page] GetPostsPerPage';

  constructor(public key: string, public pageNumber: string) {}
}

export class GetPost {
  static readonly type = '[Home Page Modal] GetPost';

  constructor(public id: number) {}
}

export class GetOwnPosts {
  static readonly type = '[Profile Page] GetOwnPosts';
}

export class SearchPost {
  static readonly type = '[Navbar] SearchPost';
  constructor(public payload: { val: string }) {}
}

export class GetAllTrends {
  static readonly type = '[Trends Page] GetAllTrends';
}

export class GetAllTrendsToday {
  static readonly type = '[Trends Page] GetAllTrendsToday';
}

export class GetAllTrendsWeek {
  static readonly type = '[Trends Page] GetAllTrendsWeek';
}

export class GetAllTrendsMonth {
  static readonly type = '[Trends Page] GetAllTrendsMonth';
}

export class GetTrendsPerPage {
  static readonly type = '[Trends Page] GetTrendsPerPage';

  constructor(public key: string, public pageNumber: string) {}
}

export class GetAllFavorites {
  static readonly type = '[Favorites Page] GetAllFavorites';
}

export class GetAllFavoritesToday {
  static readonly type = '[Favorites Page] GetAllFavoritesToday';
}

export class GetAllFavoritesWeek {
  static readonly type = '[Favorites Page] GetAllFavoritesToday';
}

export class GetAllFavoritesMonth {
  static readonly type = '[Favorites Page] GetAllFavoritesMonth';
}

export class GetFavoritesPerPage {
  static readonly type = '[Favorites Page] GetFavoritesPerPage';

  constructor(public key: string, public pageNumber: string) {}
}

export class AddPost {
  static readonly type = '[Post Create Page] AddPost';
  constructor(public payload: FormData) {}
}

export class DeletePost {
  static readonly type = '[Home Page] DeletePost';
  constructor(public payload: Post) {}
}

export class EditPost {
  static readonly type = '[Home Page] EditPost';
  constructor(public payload: Post) {}
}

export class LikePost {
  static readonly type = '[Home Page] LikePost';
  constructor(public payload: Post) {}
}

export class PreviewImageVideo {
  static readonly type = '[Post Create Page] PreviewImageVideo';
  constructor(public event: Event, public url: string | ArrayBuffer) {}
}

export class GetAllBoards {
  static readonly type = '[ Post Create Page ] GetAllBoards';
}
export class PinPost {
  static readonly type = '[ Pin Post Modal Page ] Pin Post';
  constructor(public payload: Post) {}
}

export class UserPostById {
  static readonly type = '[ UserViewProfilePage ]  userPostById';
  constructor(public id: number) {}
}

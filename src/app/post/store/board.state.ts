import { PostsService } from '@app/core/services/posts.service';
import { Board, BoardStateModel } from '@app/core/models/board.model';
import { tap, catchError, throttleTime } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { BoardService } from '@app/core/services/board.service';

//NGXS
import { State, Action, StateContext, Selector, Store, Select } from '@ngxs/store';
import { CreateBoard, GetOwnBoard } from './board.action';
import { Navigate } from '@ngxs/router-plugin';

@State<BoardStateModel>({
  name: 'board',
  defaults: {
    board: null,
    boards: [],
    post: []
  }
})
export class BoardState {
  @Selector()
  static getAllBoards(state: BoardStateModel) {
    return state.boards;
  }

  @Selector()
  static getOwnBoard(state: BoardStateModel) {
    return state.boards;
  }

  constructor(private boardService: BoardService, private store: Store) {}

  @Action(CreateBoard)
  createBoard({ patchState }: StateContext<BoardStateModel>, { payload }: CreateBoard) {
    return this.boardService.createBoard(payload).pipe(
      tap((result: any) => {}),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOwnBoard)
  getOwnBoard({ patchState }: StateContext<BoardStateModel>, {  }: GetOwnBoard) {
    return this.boardService.getMyBoards().pipe(
      tap((result: Board[]) => {
        patchState({
          boards: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}

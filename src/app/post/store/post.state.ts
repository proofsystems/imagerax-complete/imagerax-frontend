import { PostsService } from '@app/core/services/posts.service';
import { Post, PostStateModel } from '@app/core/models/post.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AuthState } from '@app/auth/store/auth.state';
import swal from 'sweetalert2';

// NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import {
  GetAllPosts,
  AddPost,
  GetPost,
  DeletePost,
  LikePost,
  PreviewImageVideo,
  GetPostsPerPage,
  GetAllTrends,
  GetAllTrendsToday,
  GetAllTrendsWeek,
  GetAllTrendsMonth,
  GetTrendsPerPage,
  GetAllFavorites,
  GetAllFavoritesToday,
  GetAllFavoritesWeek,
  GetAllFavoritesMonth,
  GetFavoritesPerPage,
  GetOwnPosts,
  SearchPost,
  GetAllBoards,
  PinPost,
  UserPostById
} from './post.action';

import { Board, BoardStateModel } from '@app/core/models/board.model';
import { PathLocationStrategy } from '@angular/common';
import { Navigate } from '@ngxs/router-plugin';

@State<PostStateModel>({
  name: 'post',
  defaults: {
    posts: [],
    post: null,
    fileUrl: null,
    format: null,
    totalPages: null,
    itemsPerPage: null,
    currentPage: null,
    searchText: null
  }
})
@State<BoardStateModel>({
  name: 'board',
  defaults: {
    board: null,
    boards: [],
    post: []
  }
})
export class PostState {
  @Selector()
  static getAllPosts(state: PostStateModel) {
    return state.posts;
  }

  @Selector()
  static userPostById(state: PostStateModel) {
    return state.posts;
  }

  @Selector()
  static filteredPosts(state: PostStateModel) {
    return state.posts
      .filter((p: Post) => {
        console.log(p);
        if (state.searchText) {
          if (p.caption.toLowerCase().includes(state.searchText.toLowerCase())) {
            return p;
          }
        } else {
          return p;
        }
      })
      .sort((a: any, b: any) => {
        return a - b;
      });
  }

  @Selector()
  static getPost(state: PostStateModel) {
    return state.post;
  }

  @Selector()
  static getOwnPost(state: PostStateModel) {
    return state.posts;
  }

  @Selector()
  static getFileUrl(state: PostStateModel) {
    return state.fileUrl;
  }

  @Selector()
  static getFormat(state: PostStateModel) {
    return state.format;
  }

  @Selector()
  static getTotalPages(state: PostStateModel) {
    return state.totalPages;
  }

  @Selector()
  static getItemsPerPage(state: PostStateModel) {
    return state.itemsPerPage;
  }

  @Selector()
  static getCurrentPage(state: PostStateModel) {
    return state.currentPage;
  }

  @Selector()
  static getAllBoards(state: BoardStateModel) {
    return state.boards;
  }

  constructor(private postsService: PostsService, private store: Store) {}

  @Action(UserPostById)
  userPostById({ patchState }: StateContext<PostStateModel>, { id }: UserPostById) {
    return this.postsService.getPostImageById(id).pipe(
      tap((result: Post[]) => {
        patchState({
          posts: result
        }),
          catchError(err => {
            return throwError(err);
          });
      })
    );
  }

  @Action(GetAllBoards)
  getAllBoards({ patchState }: StateContext<BoardStateModel>, {  }: GetAllBoards) {
    const userId = this.store.selectSnapshot(AuthState.getUserId);
    return this.postsService.getAllBoards().pipe(
      tap((result: Board[]) => {
        patchState({
          boards: result.filter(boards => boards.user_id == userId)
        });
        if (result.length == 0) {
          swal.fire('Success', 'Board is Empty', 'success').then(() => {});
        }
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllPosts)
  getAllPosts({ patchState }: StateContext<PostStateModel>, {  }: GetAllPosts) {
    return this.postsService.getAll().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetPostsPerPage)
  getPostsPerPage(ctx: StateContext<PostStateModel>, { key, pageNumber }: GetPostsPerPage) {
    return this.postsService.getPostsPerPage(key, pageNumber).pipe(
      tap((result: Post[]) => {
        // console.log(result);
        ctx.patchState({
          posts: result['data'],
          totalPages: result['total'],
          itemsPerPage: result['per_page'],
          currentPage: result['current_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetPost)
  getPost(ctx: StateContext<PostStateModel>, { id }: GetPost) {
    return this.postsService.getPost(id).pipe(
      tap((result: Post) => {
        // console.log(result);
        ctx.patchState({
          post: result
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOwnPosts)
  getOwnPosts({ patchState }: StateContext<PostStateModel>, {  }: GetOwnPosts) {
    return this.postsService.getOwn().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(PinPost)
  pinPost({ patchState }: StateContext<PostStateModel>, { payload }: PinPost) {
    return this.postsService.pinPost(payload).pipe(
      tap((result: any) => {
        swal.fire('Pinned Post', 'You have pin this post', 'success').then(() => {
          this.store.dispatch(new Navigate(['/home']));
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(SearchPost)
  setFilter({ patchState }: StateContext<PostStateModel>, { payload }: SearchPost) {
    patchState({
      searchText: payload.val
    });
  }

  @Action(GetAllTrends)
  getAllTrends({ patchState }: StateContext<PostStateModel>, {  }: GetAllTrends) {
    return this.postsService.getAll().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllTrendsToday)
  getAllTrendsToday({ patchState }: StateContext<PostStateModel>, {  }: GetAllTrendsToday) {
    return this.postsService.getAllTrendsToday().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllTrendsWeek)
  getAllTrendsWeek({ patchState }: StateContext<PostStateModel>, {  }: GetAllTrendsWeek) {
    return this.postsService.getAllTrendsWeek().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllTrendsMonth)
  getAllTrendsMonth({ patchState }: StateContext<PostStateModel>, {  }: GetAllTrendsMonth) {
    return this.postsService.getAllTrendsMonth().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetTrendsPerPage)
  getTrendsPerPage(ctx: StateContext<PostStateModel>, { key, pageNumber }: GetTrendsPerPage) {
    return this.postsService.getTrendsPerPage(key, pageNumber).pipe(
      tap((result: Post[]) => {
        // console.log(result);
        ctx.patchState({
          posts: result['data'],
          totalPages: result['total'],
          itemsPerPage: result['per_page'],
          currentPage: result['current_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetAllFavorites)
  getAllFavorites({ patchState }: StateContext<PostStateModel>, {  }: GetAllFavorites) {
    return this.postsService.getAllFavorites().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllFavoritesToday)
  getAllFavoritesToday({ patchState }: StateContext<PostStateModel>, {  }: GetAllFavoritesToday) {
    return this.postsService.getAllFavoritesToday().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllFavoritesWeek)
  getAllFavoritesWeek({ patchState }: StateContext<PostStateModel>, {  }: GetAllFavoritesWeek) {
    return this.postsService.getAllFavoritesWeek().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllFavoritesMonth)
  getAllFavoritesMonth({ patchState }: StateContext<PostStateModel>, {  }: GetAllFavoritesMonth) {
    return this.postsService.getAllFavoritesMonth().pipe(
      tap((result: Post[]) => {
        // console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetFavoritesPerPage)
  getFavoritesPerPage(ctx: StateContext<PostStateModel>, { key, pageNumber }: GetFavoritesPerPage) {
    return this.postsService.getFavoritesPerPage(key, pageNumber).pipe(
      tap((result: Post[]) => {
        // console.log(result);
        ctx.patchState({
          posts: result['data'],
          totalPages: result['total'],
          itemsPerPage: result['per_page'],
          currentPage: result['current_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(AddPost)
  addPost(ctx: StateContext<PostStateModel>, { payload }: AddPost) {
    return this.postsService.addPost(payload).pipe(
      tap(() => {
        // return ctx.dispatch(new GetBranchesPerPage('', '1'));
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(DeletePost)
  deletePost({ getState, setState }: StateContext<PostStateModel>, { payload }: DeletePost) {
    return this.postsService.deletePost(payload).pipe(
      tap(() => {
        const state = getState();
        const filteredArray = state.posts.filter(item => item.id !== payload.id);
        setState({
          ...state,
          posts: filteredArray
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(LikePost)
  likePost(ctx: StateContext<PostStateModel>, { payload }: LikePost) {
    return this.postsService.likePost(payload).pipe(
      tap(() => {}),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(PreviewImageVideo)
  previewImageVideo(ctx: StateContext<PostStateModel>, { event, url }: PreviewImageVideo) {
    const file = (event.target as HTMLInputElement).files && (event.target as HTMLInputElement).files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      if (file.type.indexOf('image') > -1) {
        ctx.patchState({
          format: 'image'
        });
      } else if (file.type.indexOf('video') > -1) {
        ctx.patchState({
          format: 'video'
        });
      }
      reader.onload = event2 => {
        url = (event2.target as FileReader).result;
        ctx.patchState({
          fileUrl: url
        });
      };
    }
  }
}

import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { PostRoutingModule } from './post-routing.module';
import { PostCreateComponent } from './post-create/post-create.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PostDetailComponent } from './post-detail/post-detail.component';

@NgModule({
  declarations: [PostCreateComponent, PostDetailComponent],
  imports: [SharedModule, PostRoutingModule, FontAwesomeModule]
})
export class PostModule {}

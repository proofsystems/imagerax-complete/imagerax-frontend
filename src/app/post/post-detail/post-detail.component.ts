import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { confirmDelete } from '@app/shared/reusable-functions/sweet-alerts';
import { Comment } from '@app/core/models/comment.model';
import { TipModalComponent } from '@app/shared/modals/tip-modal/tip-modal.component';
import { QrModalComponent } from '@app/shared/modals/qr-modal/qr-modal.component';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
// NGXS
import { Store, Select } from '@ngxs/store';
import { PostState } from '@app/post/store/post.state';
import { Post } from '@app/core/models/post.model';
import { AuthState } from '@app/auth/store/auth.state';
import { AddComment, DeleteComment } from '@app/shared/store/comment.action';
import { LikeComment } from '@app/shared/store/comment.action';
import { GetPost, DeletePost, LikePost } from '@app/post/store/post.action';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons/faTwitterSquare';
import { faPinterest } from '@fortawesome/free-brands-svg-icons/faPinterest';
import { Navigate } from '@ngxs/router-plugin';

// NG-Bootstrap Modal
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

// SweetAlert2
import swal from 'sweetalert2';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent {
  id: number;
  form: FormGroup;
  profilePhoto: string;
  userWallet: string;
  closeResult: string;
  coverPhoto: string;
  fbIcon = faFacebookSquare;
  pinIcon = faPinterest;
  tweetIcon = faTwitterSquare;
  readonly ENDPOINT_URL = environment.urlForImage;

  // NGXS
  @Select(PostState.getPost) post$: Observable<Post>;
  userId = this.store.selectSnapshot(AuthState.getUserId);
  // userWallet = this.store.selectSnapshot(AuthState.getUserWalletAddress);

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.getPost();
    this.form = this.fb.group({
      post_id: [null, Validators.required],
      comment: [null, Validators.required]
    });
    this.profilePhoto = this.store.selectSnapshot(AuthState.getProfilePhoto);
    // console.clear();
    // console.log('WALLET: ', this.userWallet);
  }

  gotoProfile(id: number) {
    this.store.dispatch(new Navigate([`profile/profile-view/${id}`]));
  }

  getPost() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.store.dispatch(new GetPost(params.id)).subscribe(val => {
        console.log(val);
        this.userWallet = val.post.post.post_by.wallet_address;
        console.clear();
        console.log('Owners Address: ', this.userWallet);
        this.form.get('post_id').setValue(params.id);
      });
    });
  }

  getUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  onAddComment(form: FormGroup) {
    console.log(form.value);
    this.store.dispatch(new AddComment(form.value)).subscribe(
      () => {
        this.store.dispatch(new GetPost(form.value.post_id)).subscribe(() => form.get('comment').reset());
      },
      err => {
        console.log(err);
      }
    );
  }

  onDeletePost(data: Post) {
    confirmDelete(`${data.caption}`).then(result => {
      console.log(result);
      if (result.value) {
        this.store.dispatch(new DeletePost(data)).subscribe(
          () => {
            swal.fire('Success', 'Successfully Deleted', 'success').then(() => {
              this.location.back();
            });
          },
          err => {
            console.log(err);
          }
        );
      }
    });
  }

  onDeleteComment(data: Comment) {
    console.log(data);
    confirmDelete(`${data.comment}`).then(result => {
      console.log(result);
      if (result.value) {
        this.store.dispatch(new DeleteComment(data)).subscribe(
          () => {
            this.store.dispatch(new GetPost(this.form.get('post_id').value)).subscribe(() => {
              swal.fire('Success', 'Successfully Deleted', 'success');
            });
          },
          err => {
            console.log(err);
          }
        );
      }
    });
  }

  onLikeComment(data: Comment) {
    console.log(data);
    this.store.dispatch(new LikeComment(data)).subscribe(
      () => {
        this.store.dispatch(new GetPost(this.form.get('post_id').value));
      },
      err => {
        console.log(err);
      }
    );
  }

  onLikePost(data: Post) {
    console.log(data);
    this.store.dispatch(new LikePost(data)).subscribe(
      () => {
        if (this.form.get('post_id').value !== null) {
          this.store.dispatch(new GetPost(this.form.get('post_id').value));
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  getImage(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  findIdEqualUserId(likes: Post[]) {
    const found = likes.some(el => el.user_id == this.userId);
    if (!found) {
      return false;
    } else {
      return true;
    }
  }

  openTipModal() {
    // this.modalService.open(TipModalComponent);
    const modal = this.modalService.open(TipModalComponent);
    modal.componentInstance.userWallet = this.userWallet;
    modal.result.then(() => {});
  }

  openQrModal() {
    this.modalService.open(QrModalComponent);
  }

  showQrCode(content) {
    this.modalService.open(content, { size: 'sm' }).result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

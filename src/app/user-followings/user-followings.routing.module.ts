import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserFollowingsComponent } from './user-followings.component';

const routes: Routes = [
  {
    path: '',
    component: UserFollowingsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule]
})
export class UserFollowingsRoutingModule {}

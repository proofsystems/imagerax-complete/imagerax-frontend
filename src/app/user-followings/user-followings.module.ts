import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { UserFollowingsComponent } from './user-followings.component';
import { UserFollowingsRoutingModule } from './user-followings.routing.module';

@NgModule({
  declarations: [UserFollowingsComponent],
  imports: [SharedModule, UserFollowingsRoutingModule]
})
export class UserFollowingsModule {}

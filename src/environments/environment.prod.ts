export const environment = {
  production: true,
  url: 'https://imagerax-api.proofsys.io/api/v1',
  urlForImage: 'https://imagerax-api.proofsys.io/',
  nodeUrl: 'https://bctestnet1.brimstone.xpxsirius.io'
};
